/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  FileService,
} from "@core-services";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAPPER
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default {
  "employeeNumber": "employee_sn",
  "name": "technician_name",
  "phoneNumber": "technician_contact_number",
  "imageUri": (item, index) => {
    const base = item.technician_photo_link;

    if (!base) {
      return "";
    }

    const baseExplosion = base.split("/");

    if (baseExplosion.length < 2) {
      return "";
    }

     const baseName = baseExplosion[baseExplosion.length - 1];

     const baseNameExplosion = baseName.split(".");

     if (baseNameExplosion.length !== 2) {
       return "";
     }

    const fileName = baseNameExplosion.join("/");

    return FileService.constructUrl("technicianImage", fileName);
  },
};
