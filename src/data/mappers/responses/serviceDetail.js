/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAPPER
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default {
  "type": (item, index) => {
    return "preventive";
  },
  "status": (item, index) => {
    switch (item.service_status) {
      case "CONFIRM QUOTATION":
        return "QUOTED";

      case "CONFIRM SCHEDULE":
      case "CONFIRM SCHEDULE BY USER":
        return "SCHEDULED";

      case "ON THE WAY":
        return "DISPATCHED";

      case "JOB IN PROGRESS":
        return "IN_PROGRESS";

      case "JOB COMPLETE":
        return "COMPLETED";
    }
  },

  "model": "equipment_model",
  "equipmentNumber": "equipment_number",
  "serialNumber": "unit_serial_number",
  "serviceRequestNumber": "service_request_number",
  "serviceOrderNumber": "service_order_number",

  "isScheduleConfirmed": (item, index) => {
    return (item.service_status === "CONFIRM SCHEDULE BY USER");
  },
  "isQuotationApproved": (item, index) => {
    return (item.quotation_status === "Q - Approved" || item.quotation_status === "Q - Won");
  },
};
