/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import moment from "moment";

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  FileService,
} from "@core-services";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAPPER
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default {
  shiptoId: "customer_shipto_id",
  model: "model",
  family: "family",
  equipmentNumber: (item, key) => {
    return item.customer_equipment_number ? item.customer_equipment_number : item.equipment_number;
  },
  serialNumber: "unit_serial_number",
  smu: (item, key) => {
    return Math.round(item.smu);
  },
  warrantyExpirationDate: (item, key) => {
    if (!item.warranty_expiration) {
      return null;
    }

    return moment(item.warranty_expiration.split(" ")[0], "M/D/YYYY").format("x") * 1;
  },
  lastServiceDate: (item, key) => {
    if (!item.last_service_order_date) {
      return null;
    }

    return moment(item.last_service_order_date.split(" ")[0], "M/D/YYYY").format("x") * 1;
  },
  isUnderWarranty: (item, key) => {
    if (!item.warranty_expiration) {
      return false;
    }

    const warrantyDate = moment(item.warranty_expiration.split(" ")[0], "M/D/YYYY").format("x") * 1;

    if (!warrantyDate) {
      return false;
    }

    const today = moment(new Date());
    return warrantyDate >= (today.format("x") * 1);
  },
  location: "last_unit_location",
  lastServiceType: "last_service_description",
  imageUri: (item, key) => {
    return FileService.constructUrl("unitFamilyImage", item.family);
  },
};
