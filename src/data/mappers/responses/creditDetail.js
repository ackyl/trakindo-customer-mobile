/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAPPER
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default {
  payerId: "customer_payer_id",
  currency: "currency",
  limit: "credit_limit",
  exposure: "exposure_amount",
  unallocated: (item, key) => {
    const unallocatedAmount = (item.unallocated_amount * 1);

    return unallocatedAmount >= 0 ? unallocatedAmount : unallocatedAmount * -1;
  },
  remaining: (item, key) => {
    if (item.credit_limit === null || item.credit_limit === undefined) {
      return 0;
    }

    if (item.exposure_amount === null || item.exposure_amount === undefined) {
      return 0;
    }

    return (item.credit_limit * 1) - (item.exposure_amount * 1);
  },
  isUnavailable: (item, key) => {
    if (item.credit_limit === null || item.credit_limit === undefined) {
      return true;
    }

    if (item.exposure_amount === null || item.exposure_amount === undefined) {
      return true;
    }

    return ((item.credit_limit * 1) === 0) && ((item.exposure_amount * 1) === 0);
  },
};
