import companyDetail from "./companyDetail";
import salesRepDetail from "./salesRepDetail";
import contactDetail from "./contactDetail";
import virtualAccountDetail from "./virtualAccountDetail";

import payerDetail from "./payerDetail";
import creditDetail from "./creditDetail";
import soaFileDetail from "./soaFileDetail";

import invoiceDetail from "./invoiceDetail";
import invoiceBrief from "./invoiceBrief";

import serviceBrief from "./serviceBrief";
import serviceDetail from "./serviceDetail";
import serviceProgressDetail from "./serviceProgressDetail";
import serviceTechnicianDetail from "./serviceTechnicianDetail";
import serviceRatingDetail from "./serviceRatingDetail";
import serviceQuotationDetail from "./serviceQuotationDetail";

import unitDetail from "./unitDetail";
import unitFamilyDetail from "./unitFamilyDetail";

import partDetail from "./partDetail";
import shippingStatusDetail from "./shippingStatusDetail";

import partItemGroupBrief from "./partItemGroupBrief";
import partItemDetail from "./partItemDetail";
import partItemGroupDetail from "./partItemGroupDetail";

import notificationDetail from "./notificationDetail";
import branchOfficeDetail from "./branchOfficeDetail";

import settingDetail from "./settingDetail";



export {
	companyDetail,
  salesRepDetail,
  contactDetail,
  virtualAccountDetail,

  payerDetail,
  creditDetail,
  soaFileDetail,

  invoiceDetail,
  invoiceBrief,

  serviceBrief,
  serviceDetail,
  serviceProgressDetail,
  serviceTechnicianDetail,
  serviceRatingDetail,
  serviceQuotationDetail,

  unitDetail,
  unitFamilyDetail,

  partDetail,

  partItemGroupBrief,
  partItemDetail,
  partItemGroupDetail,

  shippingStatusDetail,

  notificationDetail,

  branchOfficeDetail,
  settingDetail,
};
