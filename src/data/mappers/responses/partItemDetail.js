/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import moment from "moment";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAPPER
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default {
  "caseNumber": "case_number",
  "eta": (item, key) => {
    let eta = null;
    switch (item.pickup_status) {
      case "taken":
        eta = item.gate_pass_date;
        break;

      case "ready":
        eta = item.gi_date;
        break;

      case "outstanding":
      default:
        eta = item.latest_eta;
    }

    if (!eta) {
      return "";
    }

    return moment(eta.substring(0, 10)).format("x") * 1;
  },
  isTrackable: (item, key) => {
    return !!item.tracking_status;
  },
  "quantity": "quantity",
  "originCode": (item, key) => {
    if (!item.origin) {
      return "";
    }

    const originExplosion = item.origin.split(" - ");

    return originExplosion[0];
  },
  "origin": (item, key) => {
    if (!item.origin) {
      return "";
    }

    const originExplosion = item.origin.split(" - ");
    if (originExplosion.length < 2) {
      return originExplosion[0];
    }

    return originExplosion[1];
  },
  "destination": "destination",
  "pickupStatus": "pickup_status",
  "status": (item, key) => {
    return "ON_TIME";
  },
};
