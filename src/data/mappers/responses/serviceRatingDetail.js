/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
import moment from "moment";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAPPER
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default {
  "rating": "rating",
  "status": (item, index) => {
    if (!item.description) {
      return "-";
    }

    const statusExplosion = item.description.split(";");

    if (statusExplosion.length < 2) {
      return "-";
    }

    return statusExplosion[1];
  },
  type: (item, index) => {
    if (!item.description) {
      return "daily";
    }

    const statusExplosion = item.description.split(";");

    return statusExplosion[0] === "1" ? "overall" : "daily";
  },
  "date": (item, index) => {
    return moment(item.rated_for_date, "MM/DD/YYYY hh:mm:ss A").format("x") * 1;
  },
};
