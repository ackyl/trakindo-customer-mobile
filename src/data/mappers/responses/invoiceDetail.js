/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import moment from "moment";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAPPER
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default {
  soldtoID: "sold_to_party",
  invoiceNumber: "invoice_number",
  poNumber: (item, key) => {
    const pos = item
      .po_number.split(",")
      .map(poItem => poItem.trim())
      .filter(po => po && po.length > 0);

    if (pos.length > 1) {
      return "-";
    }

    return pos[0].trim();
  },
  poNumbers: (item, key) => {
    return item.po_number.split(",")
      .map(poItem => poItem.trim())
      .filter(po => po && po.length > 0);

  },
  orderType: (item, key) => {
    return item.type === "SERVICE" ? "service" : "part";
  },
  currency: "currency",
  amount: "amount",
  dueDate: (item, key) => {
    return moment(item.due_date, "DD-MM-YYYY").format("x") * 1;
  },
  status: (item, key) => {
    return item.status === "Paid" ? "CLOSED" : "OPEN";
  },
  isOverdue: (item, key) => {
    if (!item.due_date) {
      return false;
    }

    const dueDate = moment(item.due_date, "DD-MM-YYYY").format("x") * 1;

    return dueDate <= (moment(new Date()).subtract(1, "day").format("x") * 1);
  },
  receiveDate: (item, key) => {
    return moment(item.transmittal_advice_date, "DD-MM-YYYY").format("x") * 1;
  },
  file: (item, key) => {
    const printDate = item.pdf_printed_date;

    if (!printDate || printDate.length < 1) {
      return null;
    }

    const changeDate = moment(item.pdf_printed_date.split(" ")[0], "MM/D/YYYY");

    const folder = changeDate.format("MM-YYYY");

    return `${folder}/${item.invoice_number}`;
  },
};
