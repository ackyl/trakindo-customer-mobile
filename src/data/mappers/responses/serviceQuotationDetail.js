/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
import moment from "moment";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAPPER
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default {
  "quotationNumber": "service_quotation_number",
  "initialQuotationNumber": "initial_quotation_number",
  "expirationDate": (item, index) => {
    if (!item.service_quotation_expiry_date) {
      return  null;
    }

    return moment(item.service_quotation_expiry_date.split(" ")[0], "YYYY-MM-DD").format("x") * 1;
  },
  amount: (item, index) => {
    return item.service_quotation_amount * 1;
  },
  shouldShowQuotation: (item, index) => {
    return item.quotation_status ? item.quotation_status === "Q - Submitted" : false;
  },
  file: (item, key) => {
    const generationDate = item.service_quotation_generation_date;

    if (!generationDate || generationDate.length < 1) {
      return null;
    }

    const changeDate = moment(item.service_quotation_generation_date.split(" ")[0], "YYYY-MM-DD");

    const folder = changeDate.format("YYYY-MM");

    return `${folder}/${item.service_quotation_number}`;
  },
};
