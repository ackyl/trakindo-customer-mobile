/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import moment from "moment";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAPPER
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default {
  "poNumber": "po_number",
  "committedDate": (item, key) => {
    return item.original_eta ? moment(item.original_eta, "DD-MM-YYYY").format("x") * 1 : null;
  },
  "latestEta": (item, key) => {
    return item.latest_eta ? moment(item.latest_eta, "DD-MM-YYYY").format("x") * 1 : null;
  },
  "totalItems": "total_number_of_items",
  "branchCode": "sales_office_code",
  "branchLocation": (item, key) => {
    if (!item.sales_office_city) {
      return "";
    }

    const officeCityExplosion = item.sales_office_city.split(" - ");
    if (officeCityExplosion.length < 2) {
      return officeCityExplosion[0];
    }

    return officeCityExplosion[1];
  },
  "isDelayed": (item, key) => {
    return (moment(item.latest_eta, "DD-MM-YYYY").format("x") * 1) > (moment(item.original_eta, "DD-MM-YYYY").format("x") * 1);
  },
  "updatedAt": (item, key) => {
    return moment(item.last_updated_at, "DD-MM-YYYY").format("x") * 1;
  },
};
