/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
import moment from "moment";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAPPER
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default {
  "soldto_id": "soldtoId",
  "payer_id": "payerId",
  "category": "serviceType",
  description: (item, index) => {
    return `${item.serviceType} Mobile APP ${item.soldtoId}`;
  },
  unit_serial_number: (item, index) => {
    return item.unit ? item.unit.serialNumber : "";
  },
  customer_equiment_number: (item, index) => {
    return item.unit ? item.unit.customerEquipmentNumber : "";
  },
  equipment_model: (item, index) => {
    return item.unit ? item.unit.model : "";
  },
  equipment_location: "location",
  sales_office: (item, index) => {
    return item.branchOffice ? item.branchOffice.code : "";
  },
  sales_rep: (item, index) => {
    return item.salesRep ? item.salesRep.employeeNumber : "";
  },
  need_by_date: (item, index) => {
    return moment(item.date).format("YYYY-MM-DD HH:mm:ss");
  },
  note: (item, index) => {
    let note = "";

    note += `NOTE: ${item.note}\n`;
    note += `SMU: ${item.smu}\n`;
    note += `PIC NAME: ${item.name}\n`;
    note += `PIC PHONE NUMBER: ${item.phoneNumber}\n`;

    return note;
  },
};
