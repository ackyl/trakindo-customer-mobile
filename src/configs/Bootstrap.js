import { configureCognito } from "./customs/AWSAmplify";


export default {

  init: () => {
    configureCognito();
  },

// ----------------------------------------

  ready: () => {

  },

// ----------------------------------------

  destroy: () => {

  },

};
