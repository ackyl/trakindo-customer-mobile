/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import Amplify from "aws-amplify";
import {
  Auth,
} from "aws-amplify";

// ----------------------------------------
// CONFIG IMPORTS
// ----------------------------------------
import {
  AWS_COGNITO_IDENTITY_POOL_ID,
  AWS_COGNITO_REGION,
  AWS_COGNITO_USER_POOL_ID,
  AWS_COGNITO_USER_POOL_WEB_CLIENT_ID,
} from "react-native-dotenv";
import MyStorage from "./AWSAmplifyStorage";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const configureCognito = () => {
  Amplify.configure({
    Auth: {
      identityPoolId: AWS_COGNITO_IDENTITY_POOL_ID,
      region: AWS_COGNITO_REGION,
      userPoolId: AWS_COGNITO_USER_POOL_ID,
      userPoolWebClientId: AWS_COGNITO_USER_POOL_WEB_CLIENT_ID,
      storage: MyStorage,
      oauth: {
        domain: "tmt-trakindo.auth.ap-southeast-1.amazoncognito.com",
        scope: [
          "phone",
          "email",
          "profile",
          "openid",
          "aws.cognito.signin.user.admin",
        ],
        // redirectSignIn: "trakindomobileApp://ResourceLoader",
        redirectSignIn: "trakindomobileApp://CompanyList",
        redirectSignOut: "trakindomobileApp://ResourceLoader",
        // redirectSignOut: "trakindomobileApp://Login",
        responseType: "code", // or "token", note that REFRESH token will only be generated when the responseType is code
      },
    },
  });
};

// ----------------------------------------

export const refreshToken = (onSuccess, onError) => {
  Auth
    .currentAuthenticatedUser()
    .then(cognitoUser => {
      Auth
        .currentSession()
        .then(currentSession => {
          cognitoUser
            .refreshSession(currentSession.refreshToken, (err, session) => {
              if (err) {
                onError(err);

                return false;
              }

              const attributes = session.idToken.payload;
              const roleId = 666;
              // const roleId = attributes["custom:role"] ? attributes["custom:role"] : 1;

              const data = {
                accessToken: session.accessToken.jwtToken,
                refreshToken: session.refreshToken.jwtToken,
                idToken: session.idToken.token,

                userId: attributes.sub,
                roleId,
                soldtoId: attributes["custom:soldto_id"] ? attributes["custom:soldto_id"] : "",
                salesRepId: attributes["custom:salesRepId"] ? attributes["custom:salesRepId"] : "",

                noBaseSet: true,
              };

              onSuccess(data);
          });
        });
    })
    .catch(err => {
      onError(err);
    });
};


