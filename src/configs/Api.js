import {BASE_API_URL} from "react-native-dotenv";
import { refreshToken } from "./customs/AWSAmplify";

export default {
  baseUrl: BASE_API_URL ? BASE_API_URL : "http://localhost:8000",

// ----------------------------------------

  timeout: 60,

// ----------------------------------------

  devLog: false,

// ----------------------------------------

  breakDevLogToken: false,

// ----------------------------------------

  breakDevLogParams: false,

// ----------------------------------------

  breakDevLogResult: false,

// ----------------------------------------

  devLogApis: [
    "logout",
  ],

// ----------------------------------------

  forceMode: {
    // loginCustomer: "API",
    logout: "API",

    fetchUnitFamilyList: "API",
    getUnitDetail: "API",
    fetchCompanyList: "API",
    fetchUnitList: "API",
    fetchSalesRepList: "API",
    fetchContactList: "API",
    fetchVirtualAccountList: "API",
    fetchSoaFileList: "API",

    fetchPayerList: "API",
    getCredit: "API",
    fetchInvoiceList: "API",

    fetchPartList: "API",
    getItemGroupBrief: "API",
    fetchPartItemGroupList: "API",
    fetchShippingStatusList: "API",

    fetchServiceList: "API",
    getServiceBrief: "API",
    createServiceRequest: "API",
    fetchServiceQuotationList: "API",
    getServiceScheduledDetail: "API",
    getServiceOnProgressDetail: "API",
    confirmServiceSchedule: "API",
    confirmServiceDocuments: "API",

    fetchNotificationList: "API",

    fetchBranchOfficeList: "API",
    getSetting: "API",

    invoicePdf: "API",
    soaPdf: "API",
    unitFamilyImage: "API",
    serviceQuotatonPdf: "API",
    serviceAgreementPdf: "API",
    serviceTnCPdf: "API",
    technicianImage: "API",
  },

// ----------------------------------------

  getToken: (getState) => {
    return getState().auth.get("accessToken");
  },

// ----------------------------------------

  refreshToken: (onSuccess, onError) => {
    refreshToken(
      (session) => onSuccess(session),
      (err) => onError(err),
    );
  },

// ----------------------------------------

  isSuccess: (response) => {
    const {
      status,
      data,
    } = response;

    if (status !== 200) {
      return false;
    }

    if (data.status !== "SUCCESS") {
      return false;
    }

    return true;
  },

// ----------------------------------------

  isError: (response) => {
    const {
      data,
    } = response;

    if (data.status !== "ERROR") {
      return false;
    }

    return true;
  },

// ----------------------------------------

  extractData: (body) => {
    return body.data;
  },


// ----------------------------------------

  extractError: (body) => {
    return {
      code: body.code,
      message: body.message,
    };
  },

};
