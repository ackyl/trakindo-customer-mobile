export default {
	shouldPersist: true,

// ----------------------------------------

  persistReducers: [
    "auth",
    "cache",
    "core",
  ],

// ----------------------------------------

  volatileReducers: [
    "company",
    "credit",
    "invoice",
    "part",
    "partItem",
    "partShipping",
    "service",
    "unit",
    "misc",
    "error",
  ],
};
