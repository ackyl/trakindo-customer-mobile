import {ONE_SIGNAL_TOKEN} from "react-native-dotenv";

export default {
  token: ONE_SIGNAL_TOKEN ? ONE_SIGNAL_TOKEN : null,

// ----------------------------------------

  onReceived(notification) {
    console.warn("Notification received: ", notification);
  },

// ----------------------------------------

  onOpened(openResult) {
    console.warn("Message: ", openResult.notification.payload.body);
    console.warn("Data: ", openResult.notification.payload.additionalData);
    console.warn("isActive: ", openResult.notification.isAppInFocus);
    console.warn("openResult: ", openResult);
  },

// ----------------------------------------

  onIds(device) {
    console.warn("Device info: ", device);
  },

};
