import {SENTRY_DSN} from "react-native-dotenv";

export default {
  dsn: SENTRY_DSN ? SENTRY_DSN : null,

// ----------------------------------------

  enable: true,

// ----------------------------------------

  consoleLogging: false,
};
