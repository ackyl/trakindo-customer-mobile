export default {
  main: {
    app_logo: {
      image: require("@app-images/main/logo-apps.png"),
      width: 120,
      height: 85,
    },
    logo: {
      image: require("@app-images/main/cbm-e-trakindo-logo-white-line-20160531.png"),
      width: 121,
      height: 32,
    },
    tagline: {
      image: require("@app-images/main/group-48.png"),
      width: 312,
      height: 19,
    },
    background: {
      image: require("@app-images/main/image-blue-gradient.png"),
      width: 375,
      height: 250,
    },
    card_background: {
      image: require("@app-images/main/group-19.png"),
      width: 165,
      height: 176,
    },
  },
  resource: {
    background: {
      image: require("@app-images/resource/oval-large.png"),
      width: 375,
      height: 393,
    },
  },
  login: {
    background: {
      image: require("@app-images/login/group-11.png"),
      width: 375,
      height: 667,
    },
  },
  home: {
    background: {
      image: require("@app-images/home/group-11.png"),
      width: 375,
      height: 250,
    },
  },
  icon: {
    part: {
      image: require("@app-images/icon/sa-package.png"),
      width: 40,
      height: 40,
    },
    service: {
      image: require("@app-images/icon/sa-service-1.png"),
      width: 40,
      height: 40,
    },
    preventive_service: {
      image: require("@app-images/icon/sa-service-maintanance.png"),
      width: 48,
      height: 48,
    },
    troubleshot_service: {
      image: require("@app-images/icon/sa-troubleshot.png"),
      width: 48,
      height: 48,
    },
  },
  bank: {
    citibank: {
      image: require("@app-images/external/group-10.png"),
      width: 70,
      height: 44,
    },
  },
  sample: {
    unit: {
      image: require("@app-images/sample/group-2.png"),
      width: 80,
      height: 80,
    },
    unit_large: {
      image: require("@app-images/sample/bitmap.png"),
      width: 252,
      height: 189,
    },
  },
};
