export default {
  // Company Selection
    // Main
    "preparing_your_apps": "Mempersiapkan aplikasi",
    "trakindo_customer_app_|_version": "Trakindo Customer App | Versi",
    "list_of_customer": "Daftar Pelanggan",

    // List
    "customer_id": "ID Pelanggan",

  // Home
    // Menu
    "request_service": "Permintaan Servis",
    "invoices": "Tagihan",
    "parts_tracking": "Lacak Parts",
    "service_tracking": "Lacak Servis",
    "credit_status": "Status Kredit",
    "units": "Unit",

      // Request Service
      "preventive_maintenance": "Periodic Maintenance",
      "troubleshooting": "Troubleshooting",
      "analyze_asset_data,_run_diagnostic_tests_on_products_and_determine_potential_problems.":
        "Menganalisis data aset, menjalankan tes diagnostik pada produk dan menentukan potensi masalah.",
      "routine_maintenance_to_preventing_any_unplanned_downtime_and__unanticipated_equipment_failure.": "",

        // Preventive Maintenance
        "choose_equipment": "Pilih Equipment",
        "unit_smu_meter_(optional)": "SMU (pilihan)",
        "need_by_date": "Tanggal Dibutuhkan",
        "equipment_location_(optional)": "Lokasi Equipment (optional)",
        "can’t_find_my_equipment": "Equipment tidak ditemukan",
        "can't_find_your_equipment?": "Equipment Anda tidak ditemukan",
        "if_you_cant_find_your_equipment,_you_can_register_it_by_send_us_your_equipment_information_or_call_us_to_do_urgent_service.":
          "Jika equipment Anda tidak ditemukan, Anda dapat mendaftarkannya dengan mengirimkan data atau menghubungi kami.",
        "call_for_urgent_service": "Hubungi",
        "register_equipment": "Daftarkan Equipment",
        "trakindo_nearest_branch_to_equipment": "Cabang Trakindo Terdekat",
        "sales_rep": "PS Sales Representative",
        "pic_name": "Nama Kontak",
        "pic_contact": "Nomor Kontak",
        "pm_type:_(250/500/1000)": "Tipe PM: (250/500/1000)",
        "send_request": "Kirim",

        // Troubleshooting
        "details_of_problem": "Detail Masalah",
        "troubleshooting_maintenance": "Troubleshooting",

          // Preventive & Troubleshooting Error Handling
          "unit_cannot_be_empty": "Lengkapi unit",
          "need-by-date_cannot_be_empty": "Lengkapi tanggal",
          "branch_office_cannot_be_empty": "Lengkapi cabang",
          "sales_rep_cannot_be_empty": "Pilih nama sales",
          "pic_name_cannot_be_empty": "Lengkapi nama kontak",
          "pic_contact_cannot_be_empty": "Lengkapi nomor kontak",
          "note_cannot_be_empty" : "Lengkapi detail masalah",

          // Preventive & Troubleshooting Success Message
          "order_created": "Pesanan Dibuat",
          "ticket_no": "Nomor Tiket",
          "direct_process": "Hubungi",
          "your_service_request_is_now_created.\nplease_check_your_email.\nthis_change_will_be_reflected_in_your_app_within_1_hour.":
            "Permintaan servis telah dibuat.\nSilakan periksa email Anda.\n*Daftar pesanan servis pada aplikasi akan diperbarui dalam waktu 1 jam", // Tidak Ada Di Excel
          "our advisor will call you within 2 hours": "Advisor kami akan menghubungi anda dalam 2 jam",

        // Request Detail Modal
        "request_detail": "Lihat",
        "equipment_location": "Lokasi Equipment",
        "trakindo_branch": "Cabang Trakindo",
        "note": "Catatan",

      // Invoices
        // Overdue
        "overdue": "Overdue",
        "current": "Current",
        "history": "Riwayat",
        "no_overdue_invoice_found": "Tagihan Overdue Tidak Ditemukan",
        "total_overdue": "Total Overdue",

        // Current
        "due_date": "Tanggal Jatuh Tempo",
        "po_number": "Nomor PO",
        "total_current": "Total Tagihan Belum Jatuh Tempo",

        // History
        "closed": "Selesai",

        // Invoice Detail
        "invoice_detail": "Detail Tagihan",
        "invoice_number": "Nomor Tagihan",
        "receive_date": "Tanggal Terima",
        "amount": "Jumlah",
        "payment_information": "Metode Pembayaran",
        "download_invoice_pdf": "Unduh Tagihan PDF",
        "download_this_invoice_pdf_file_for_more_detail_information": "Unduh detail tagihan",

        // Payment Information
        "bank_account": "Rekening Bank",
        "currency": "Mata Uang",
        "account_name": "Nama Akun",
        "pay_your_invoices_by_using_our_virtual_account_information,_if_payment_has_not_been_allocated,_then_the_amount_information_will_be_accommodated_in_the_unallocated_amount_menu.":
          "Segera lakukan pembayaran melalui Virtual Account Anda. Jika pembayaran belum dialokasikan pada Tagihan tertentu, maka Jumlah yang Anda bayarkan dapat Anda lihat pada menu Pembayaran Diterima.",

      // Finance
      "finance": "Finance",
      "account_:": "Akun :",
      "remaining_balance": "Sisa Pagu Open Account",
      "credit_limit": "Fasilitas Open Account",
      "credit_exposure": "Paparan Kredit",
      "account_number": "Nomor Akun",
      "call_us": "Hubungi Kami",
      "blocked": "Penangguhan Sementara",
      "call_us_if_you_need_help_about_your_credit_status_and_agreement":
        "Hubungi Kami untuk bantuan status open account dan persetujuan",

      "payment_received": "Pembayaran Diterima",
      "call_to_allocate": "Hubungi Kami Mengenai Alokasi",
      "learn_more": "Pelajari Lebih Lanjut",
      "*updated_once_a_day": " *Diperbarui Harian",
      "you_have_unalocated_amount,_call_us_to_discuss_more_about_alocation_process":
        "Anda memiliki dana yang belum dialokasikan. Hubungi kami untuk alokasi.",

      "*only_idr_invoices_in_app._if_further_information_is_needed,_please_contact_your_sales.":
        "*Tagihan hanya tersedia dalam rupiah.",
      "see_invoices": "Lihat Tagihan",

      "statement_of_account": "Ringkasan Jumlah Tagihan",
      "payment_cut_off_date": "Tanggal Pembayaran",
      "statement_of_accounts": "Ringkasan Jumlah Tagihan",
      "no_soa_file_found": "File Tidak Ditemukan",

      "show_invoices": "Lihat Tagihan",
      "call_us_to_extend": "Hubungi Kami",

      // Units
      "all_equipment": "Semua Equipment",
      "equipment_families": "Jenis Equipment",
      "equipment_number": "Nomor Equipment",
      "last_service": "Servis Terakhir",
      "serial_number": "Nomor Seri",
      "warranty_expire_date" : "Warranty Expire",
      "location_(updated)": "Lokasi",
      "service_equipment": "Servis Equipment",
      "unit_smu_meter": "SMU",
      "last_service_description": "Catatan Servis Terakhir",
      "status_meter_unit_or_hour_meter_is_a_gauge_or_instrument_that_tracks_and_records_elapsed_time,_normally_displayed_in_hours_and_tenths_of_hours._the_majority_of_hour_meters_are_used_to_log_running_time_of_equipment_to_assure_proper_maintenance_of_expensive_machines_or_systems._this_maintenance_typically_involves_replacing,_changing_or_checking_parts,_belts,_filters,_oil,_lubrication_or_running_condition_in_engines,_motors,_blowers,_and_fans,_to_name_a_few.":
        "Servis Meter Unit adalah instrumen yang melacak dan mencatat waktu operasi unit, ditampilkan dalam satuan jam, untuk memastikan perawatan dilakukan tepat waktu.",

    // Reminder
    "reminder": "Pengingat",
    "outstanding_invoice": "Tagihan Belum Dibayar",
    "service": "Servis",
    // Part Order
    "part_orders": "Pesanan Suku Cadang",
    "no_part_found": "Parts Tidak Ditemukan",
    "commited_date": "Tanggal Pemenuhan",
    "total_items": "Total Suku Cadang",

      // Parts Order Tracking
      "parts_order_tracking": "Lacak Pesanan Parts",
      "update": "Perbarui",
      "po_customer": "PO Pelanggan",
      "outstanding": "Belum Tiba",
      "ready_to_pickup": "Siap Diambil",
      "taken": "Sudah Diambil",
      "branch_location": "Lokasi Cabang",
      "part_list": "Daftar Suku Cadang",
      "part_number:": "Nomor Suku Cadang:",
      "delay": "Terlambat",
      "eta": "ETA",
      "quantity:": "Jumlah:",
      "track": "Lacak",

        // Track
        "quantity": "Jumlah",
        "origin": "Lokasi Asal",
        "destination": "Tujuan",
        "in_process_for_shipment": "Proses Pengiriman",
        "left_origin_facility": "Dikirim Dari Lokasi Asal",
        "in_transit": "Transit",
        "delayed": "Terlambat",
        "waiting_next_flight": "Menunggu Pengiriman Berikutnya",
        "delivered": "Terkirim",
        "need_help?": "Bantuan",
        "on_time": "Tepat Waktu",

    // Service
    "no_service_found": "Permintaan Servis Tidak Ditemukan",
    "equipment_model": "Model Equipment",
    "confirm_schedule": "Konfirmasi Jadwal",
    "confirm_quotation": "Konfirmasi Penawaran",
    "service_request": "Permintaan Servis",
    "service_order": "Pesanan Servis",
    "job_in_progress": "Pekerjaan Dalam Proses",
    "job_complete": "Pekerjaan Selesai",

      // Service Detail
      "service_detail": "Detail Servis",
      "confirm_order": "Konfirmasi Permintaan", // UI Jadi Bermasalah, Tidak Muat
      "scheduling": "Penjadwalan",
      "on_the_way": "Dalam Perjalanan", // UI Jadi Bermasalah, Tidak Muat
      "in_progress": "Dalam Pengerjaan", // UI Jadi Bermasalah, Tidak Muat
      "complete": "Selesai",
      "estimated_arrival": "Perkiraan Kedatangan",
      "technicians": "Teknisi",
      "call_to_reschedule": "Hubungi Untuk Penjadwalan Ulang",

      // Approve Quotation
      "approve_quotation": "Setujui Penawaran",
      "quotation": "Penawaran",
      "service_agreement": "Service Agreement",
      "open": "Buka",
      "submit_&_continue": "Lanjutkan",
      "terms_and_conditions": "Syarat dan Ketentuan",
      "please_read_and_approve_these_two_documents_below,_to_continue_your_service_order":
        "Harap dibaca dan berikan persetujuan untuk melanjutkan Permintaan Servis",

        // Quotation
        "approve": "Setuju",
        "call_for_clarification": "Hubungi Untuk Penjelasan",

        // Service Agreement
        "input_service_quotation/reference_number_to_confirm": "Ketik nomor Penawaran Servis / Referensi untuk konfirmasi",
        "service_quotation_number_(reference_number)": "Nomor Penawaran Servis (Nomor Referensi)",
        "invalid_service_quotation_number": "Nomor Penawaran Salah",
        "service_quotation_cannot_be_empty": "Penawaran servis harus diisi",
        "agree": "Setuju",
        "by_clicking_agree_button,_you_have_read_and_agreed_to_these_terms_and_conditions":
          'Dengan menekan tombol "Setuju", menyatakan Anda sudah membaca dan menyetujui persyaratan dan ketentuan yang berlaku',

      // Service Order Approved
      "quotation_approved": "Penawaran Disetujui",
      "see_service_orders": "Lihat Perintah Servis",
      "please_wait_for_a_moment,_now_we_are_proceed_to_do_scheduling_for_your_service_order.\nplease_check_your_email.\nthis_change_will_be_reflected_in_your_app_within_1_hour.":
      "Perintah Servis Anda sudah kami terima, Kami akan menghubungi Anda untuk konfirmasi jadwal servis.",

      // Service Order Completed
      "service_order_completed": "Perintah Servis Selesai",
      "all_work_for_the_service_order_has_been_completed.\nthank_you_for_using_our_service.": "Seluruh pekerjaan servis telah selesai.\nTerima kasih telah menggunakan layanan kami.",

  // Order
    "orders": "Permintaan",
    "parts": "Parts",
    "services": "Servis",
    "search": "Cari",

  // Inbox
    "inbox": "Kotak Masuk",
    "you_have_overdue_invoices": "Anda memiliki Tagihan Overdue",
    "parts_left_origin_facility": "Parts telah dikirim dari lokasi asal",
    "your_service_quotation_is_ready": "Penawaran servis sudah tersedia",
    "your_credit_is_over_limit": "Anda melewati batas fasilitas open account",

    // Inbox Detail
    "your_account_has_been_suspended_because_payment_overdue,_please_pay_this_immedietly_or_call_us_for_more_information":
      "Akun Anda ditangguhkan sementara karena keterlambatan pembayaran tagihan. Silakan lakukan pembayaran.",
    "your_account_has_been_suspended,_call_us_to_discuss_more_about_your_credit_limit_agreement":
      "Akun Anda ditangguhkan sementara, silakan hubungi kami untuk aktifasi kembali.",

  // Help Center
    "help_center": "Pusat Bantuan",
    "call_center_helpline": "Pusat Bantuan Telepon",
    "call": "Panggilan",
    "email": "Email",
    "send_email": "Kirim Email",
    "live_chat": "Live Chat",
    "chat": "Chat",

  // Account
    "account" : "Akun",
    "contact_name": "Nama Kontak",
    "contact_no.": "Nomor Kontak",
    "address": "Alamat",
    "registered_equipments": "Equipment Terdaftar",
    "see_all": "Lihat Semua",
    "register_your_equipments": "Daftarkan Equipment",
    "contacts": "Kontak",
    "access": "Akses",

  // Logout // Tidak Ada Di Excel
    "log_out": "Keluar",
    "are_you_sure_want_to_log_out_now?": "Anda yakin ingin keluar Aplikasi?",
    "ok": "Ya",
    "cancel": "Batal",

  // Login // Tidak Ada Di Excel
    "welcome": "Selamat Datang",
    "login_to_trakindo_customer_app": "Masuk ke Trakindo Customer App",
    "login_with_trakindo_id": "Masuk dengan ID Trakindo",
    "login": "Masuk",
    "email_address": "Alamat Email",
    "password": "Kata Sandi",

    // Login Error Handling
    "email_cannot_be_empty": "Email harus diisi",
    "password_cannot_be_empty": "Kata sandi harus diisi",
    "invalid_email": "Email tidak terdaftar",
    "user_not_found": "User tidak ditemukan",
    "password_invalid": "Kata sandi salah",

    // Error Modal // Tidak Ada Di Excel
    "internal_server_error": "Gangguan Server",
    "the_server_encountered_an_internal_error_or_misconfiguration_and_was_unable_to_complete_your_request":
      "Server Mengalami Gangguan",
    "network_connection_error": "Gangguan Koneksi Internet",
    "the_app_is_taking_a_long_time_to_load._please_ensure_that_a_strong_internet_connection":
      "Waktu memuat terlalu lama. Periksa koneksi internet Anda.",
    "retry": "Coba Kembali",
    "file_not_found": "File Tidak Ditemukan",
    "it_may_have_been_moved,_renamed_or_deleted": "Kemungkinan sudah dipindah, dirubah, atau dihapus",
    "session_expired": "Sesi Telah Berakhir",
    "please_re-login_ro_renew_your_session": "Silahkan login kembali untuk perbarui sesi",

};
