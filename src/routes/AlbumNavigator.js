/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { Provider } from "react-redux";
import {
  Router,
  Scene,
} from "react-native-router-flux";

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  StoreService,
} from "@core-services";

// ----------------------------------------
// PAGE IMPORTS
// ----------------------------------------
import {
  Main,
  StyleGuide,
  Auth,
  Company,
  Invoice,
  Credit,
  Part,
  PartItem,
  Service,
  ServiceRequest,
  Unit,
  SalesRep,
  Technician,
  HelpContact,
  Misc,
  Header,
  Footer,
} from "@app-albums";
// ----------------------------------------




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  NAVIGATIONS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class App extends Component<{}> {
  render() {
    return (
      <Provider store={ StoreService.init() }>
        <Router>
          <Scene key="root" hideNavBar={true} >

            {/* MAIN */}
            <Scene key="Main" component={Main} />
            <Scene key="StyleGuide" component={StyleGuide}/>
            <Scene key="Auth" component={Auth} />
            <Scene key="Company" component={Company}/>
            <Scene key="Invoice" component={Invoice}/>
            <Scene key="Credit" component={Credit}/>
            <Scene key="Part" component={Part}/>
            <Scene key="PartItem" component={PartItem}/>
            <Scene key="Service" component={Service}/>
            <Scene key="ServiceRequest" component={ServiceRequest}/>
            <Scene key="Unit" component={Unit}/>
            <Scene key="SalesRep" component={SalesRep}/>
            <Scene key="Technician" component={Technician}/>
            <Scene key="HelpContact" component={HelpContact}/>
            <Scene key="Misc" component={Misc}/>
            <Scene key="Header" component={Header}/>
            <Scene key="Footer" component={Footer}/>
          </Scene>
        </Router>
      </Provider>
    );
  }
}
