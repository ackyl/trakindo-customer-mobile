/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { Provider } from "react-redux";
import {
  Router,
  Scene,
} from "react-native-router-flux";
import BackgroundTimer from "react-native-background-timer";

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  StoreService,
} from "@core-services";
import ApiConfig from "@app-configs/Api";

// ----------------------------------------
// PAGE IMPORTS
// ----------------------------------------
import {
  Login,
} from "@app-pages/Auth";
// ----------------------------------------
import {
  ResourceLoader,
  CompanySelection,
  Landing as Home,
} from "@app-pages/Main";
// ----------------------------------------
import {
  Landing as FinanceLanding,
} from "@app-pages/Finance";
// ----------------------------------------
import {
  List as InvoiceList,
  Detail as InvoiceDetail,
} from "@app-pages/Finance/Invoice";
// ----------------------------------------
import {
  List as UnitList,
  Detail as UnitDetail,
} from "@app-pages/Company/Unit";
// ----------------------------------------
import {
  CreateRequest,
  UnitNotFound,
  RequestCreated,
  DocumentApproval,
  OrderApproved,
  OrderConfirmation,
  QuotationApproval,
  TnCApproval,
} from "@app-pages/Service/Request";
// ----------------------------------------
import {
  Detail as PartDetail,
} from "@app-pages/Part/Tracking";
// ----------------------------------------
import {
  Detail as ServiceDetail,
} from "@app-pages/Service/Tracking";
// ----------------------------------------

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  AuthAction,
} from "@app-actions";

const store = StoreService.init();



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  NAVIGATIONS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class App extends Component<{}> {

  componentDidMount() {
    BackgroundTimer.runBackgroundTimer(() => {
      ApiConfig.refreshToken(
        (data) => {
          data.deviceId = store.getState().auth.get("deviceId");

          store.dispatch(AuthAction.loginCustomer(
            data,
            () => {}
          ));

          console.warn("success refresh token", data.accessToken);
        },
        (err) => {
          console.warn("error refresh token", err);
        }
      );
    },
    (15 * 60 * 1000));
  }

// ----------------------------------------

  componentWillUnmount() {
    BackgroundTimer.stopBackgroundTimer();
  }

// ----------------------------------------

  render() {
    return (
      <Provider store={ store }>
        <Router>
          <Scene key="root" hideNavBar={true} >

            {/* AUTH */}
            <Scene key="Login" component={Login}/>

            {/* MAIN */}
            <Scene key="ResourceLoader" component={ResourceLoader} initial/>
            <Scene key="CompanySelection" component={CompanySelection}/>
            <Scene key="Home" component={Home}/>

            {/* FINANCE */}
            <Scene key="FinanceLanding" component={FinanceLanding}/>
            <Scene key="InvoiceList" component={InvoiceList}/>
            <Scene key="InvoiceDetail" component={InvoiceDetail}/>

            {/* COMPANY */}
            <Scene key="UnitList" component={UnitList}/>
            <Scene key="UnitDetail" component={UnitDetail}/>

            {/* Services */}
            <Scene key="CreateRequest" component={CreateRequest}/>
            <Scene key="UnitNotFound" component={UnitNotFound}/>
            <Scene key="RequestCreated" component={RequestCreated}/>
            <Scene key="DocumentApproval" component={DocumentApproval}/>
            <Scene key="OrderApproved" component={OrderApproved}/>
            <Scene key="ServiceDetail" component={ServiceDetail}/>
            <Scene key="OrderConfirmation" component={OrderConfirmation}/>
            <Scene key="QuotationApproval" component={QuotationApproval}/>
            <Scene key="TnCApproval" component={TnCApproval}/>

            {/* Parts */}
            <Scene key="PartDetail" component={PartDetail}/>

          </Scene>
        </Router>
      </Provider>
    );
  }
}
