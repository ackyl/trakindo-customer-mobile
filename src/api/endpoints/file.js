/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export const invoicePdf = {
  "endpoint": "/finance/invoice_download/",
  "method": "GET",
  "data": {},
};

// ----------------------------------------

export const soaPdf = {
  "endpoint": "/finance/soa_download/",
  "method": "GET",
  "data": {},
};

// ----------------------------------------

export const unitFamilyImage = {
  "endpoint": "/finance/image/UnitFamily/",
  "method": "GET",
  "data": {},
};

// ----------------------------------------

export const serviceQuotatonPdf = {
  "endpoint": "/service/quotaion_download/",
  "method": "GET",
  "data": {},
};

// ----------------------------------------

export const serviceAgreementPdf = {
  "endpoint": "/service/serviceAgreement_download/",
  "method": "GET",
  "data": {},
};

// ----------------------------------------

export const serviceTnCPdf = {
  "endpoint": "/Service/termofcondiction/",
  "method": "GET",
  "data": {},
};

// ----------------------------------------

export const technicianImage = {
  "endpoint": "/service/technicianImage/",
  "method": "GET",
  "data": {},
};

