/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export const fetchServiceList = {
  "endpoint": "/service/orders/",
  "method": "GET",
  "data": {},
};

// ----------------------------------------

export const getServiceBrief = {
  "endpoint": "/service/brief/",
  "method": "GET",
  "data": {},
};

// ----------------------------------------

export const createServiceRequest = {
  "endpoint": "/service/CreateServiceRequest/",
  "method": "POST",
  "data": {},
};

// ----------------------------------------

export const fetchServiceQuotationList = {
  "endpoint": "/service/quotations/",
  "method": "GET",
  "data": {},
};

// ----------------------------------------

export const getServiceScheduledDetail = {
  "endpoint": "/service/order_technicians/",
  "method": "GET",
  "data": {},
};

// ----------------------------------------

export const getServiceOnProgressDetail = {
  "endpoint": "/service/order_rating/",
  "method": "GET",
  "data": {},
};

// ----------------------------------------

export const confirmServiceSchedule = {
  "endpoint": "/service/ApprovalSchedule/",
  "method": "POST",
  "data": {},
};

// ----------------------------------------

export const confirmServiceDocuments = {
  "endpoint": "/service/ApprovalQuotation/",
  "method": "POST",
  "data": {},
};

