import * as auth from "./auth";
import * as company from "./company";
import * as finance from "./finance";
import * as part from "./part";
import * as service from "./service";
import * as file from "./file";
import * as misc from "./misc";
import * as notification from "./notification";

export default {
  ... auth,
  ... company,
  ... finance,
  ... part,
  ... service,
  ... file,
  ... misc,
  ... notification,
};
