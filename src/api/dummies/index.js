import * as auth from "./auth";
import * as company from "./company";
import * as finance from "./finance";
import * as service from "./service";
import * as file from "./file";
import * as part from "./part";
import * as notification from "./notification";
import * as misc from "./misc";

export default {
  ... auth,
  ... company,
  ... finance,
  ... service,
  ... part,
  ... file,
  ... notification,
  ... misc,
};
