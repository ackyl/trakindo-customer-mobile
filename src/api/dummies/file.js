/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function invoicePdf() {
  return "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
}

// ----------------------------------------

export function soaPdf() {
  return "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
}

// ----------------------------------------

export function unitFamilyImage() {
  return "https://imgd.aeplcdn.com/1280x720/n/bw/models/colors/suzuki-burgman-street-pearl-mirage-white-1531998385099.jpg?20190103151915&q=80";
}

// ----------------------------------------

export function serviceQuotatonPdf() {
  return "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
}

// ----------------------------------------

export function serviceAgreementPdf() {
  return "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
}

// ----------------------------------------

export function serviceTnCPdf() {
  return "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
}

// ----------------------------------------

export function technicianImage() {
  return "https://imgd.aeplcdn.com/1280x720/n/bw/models/colors/suzuki-burgman-street-pearl-mirage-white-1531998385099.jpg?20190103151915&q=80";
}

// ----------------------------------------
