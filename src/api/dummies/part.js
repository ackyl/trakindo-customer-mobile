/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function fetchPartList(param) {
  let data = require("@app-data-dummies/parts");

  if (param.search !== null && param.search !== undefined) {
    data = data.filter((item) => item.po_number.indexOf(param.search) > -1);
  }

  return {
    part_orders: data,
  };
}

// ----------------------------------------

export function getItemGroupBrief(param) {
  let data = require("@app-data-dummies/part_item_counter");

  return {
    item_counter: data,
  };
}

// ----------------------------------------

export function fetchPartItemGroupList(param) {
  let data = require("@app-data-dummies/part_item_groups");

  return {
    items: data,
  };
}

// ----------------------------------------

export function fetchShippingStatusList(param) {
  let data = require("@app-data-dummies/part_item_shipping_statuses");

  return {
    shipping_status: data,
  };
}
