/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function fetchCompanyList(param) {
  let data = require("@app-data-dummies/companies");

  if (param.search) {
    data = data.filter(item => item.customer_name.trim().toLowerCase().indexOf(param.search.trim().toLowerCase()) > -1);
  }

  return {
    companies: data,
  };
}

// ----------------------------------------

export function fetchUnitList(param) {
  let data = require("@app-data-dummies/units");

  if (param.family) {
    data = data.filter(item => item.family === param.family);
  }

  return {
    units: data,
  };
}

// ----------------------------------------

export function getUnitDetail(param) {
  let data = require("@app-data-dummies/units");

  return {
    unit: data[0],
  };
}

// ----------------------------------------

export function fetchUnitFamilyList(param) {
  let data = require("@app-data-dummies/unit_families");

  return {
    unit_families: data,
  };
}

// ----------------------------------------

export function fetchSalesRepList(param) {
  let data = require("@app-data-dummies/sales_reps");

  return {
    sales_reps: data,
  };
}

// ----------------------------------------

export function fetchContactList(param) {
  let data = require("@app-data-dummies/contacts");

  return {
    contacts: data,
  };
}

// ----------------------------------------

export function fetchVirtualAccountList(param) {
  let data = require("@app-data-dummies/virtual_accounts");

  return {
    accounts: data,
  };
}
