/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import moment from "moment";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function fetchPayerList(param) {
  let data = require("@app-data-dummies/payers");

  // data = data.filter(item => item.soldto_id === param.soldto_id);

  return {
    payers: data,
  };
}

// ----------------------------------------

export function getCredit(param) {
  let data = require("@app-data-dummies/credit");
  // data = data.filter(item => item.soldto_id === param.soldto_id);

  return {
    credit_limit: data,
  };
}

// ----------------------------------------

export function fetchInvoiceList(param) {
  let data = require("@app-data-dummies/invoices");

  // data = data.filter(item => item.payer_id === param.payer_id);

  if (param.status) {
    param.status = param.status === "OPEN" ? "Open" : "Paid";
    data = data.filter(item => item.status === param.status);
  }

  if (param.overdue === "true") {
    data = data.filter(item => (moment(item.due_date, "DD-MM-YYYY").format("x") * 1) <= moment(new Date()).subtract(1, "day").format("x") * 1);
  } else if (param.status !== "Paid") {
    data = data.filter(item => (moment(item.due_date, "DD-MM-YYYY").format("x") * 1) > (new Date()).getTime());
  }

  return {
    invoices: data,
  };
}

// ----------------------------------------

export function getInvoiceBrief(param) {
  let data = require("@app-data-dummies/invoices");

  data = data
    .filter(item => item.payer_id === param.payer_id)
    .filter(item => item.status === "OPEN");

  return {
    invoice_brief: {
      currency: data[0] ? data[0].currency : "IDR",
      no_of_overdue_invoices: data.filter(item => item.due_date <= (moment(new Date()).subtract(1, "day").format("x") * 1)).length,
      no_of_current_invoices: data.filter(item => item.due_date >= (new Date()).getTime()).length,
    },
  };
}

// ----------------------------------------

export function fetchSoaFileList(param) {
  let data = require("@app-data-dummies/soa_files");

  // data = data.filter(item => item.soldto_id === param.soldto_id);

  return {
    files: data,
  };
}

