/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function fetchServiceList(param) {
  let data = require("@app-data-dummies/services");

  return {
    orders: data,
  };
}

// ----------------------------------------

export function getServiceBrief(param) {
  return {
    brief: {
      number_of_units: 27,
    },
  };
}

// ----------------------------------------

export function createServiceRequest(param) {
  return {
    service_request_number: "8000004488",
  };
}

// ----------------------------------------

export function fetchServiceQuotationList(param) {
  let data = require("@app-data-dummies/service_quotations");

  return {
    quotations: data,
  };
}

// ----------------------------------------

export function confirmServiceDocuments(param) {
  return {};
}

// ----------------------------------------

export function confirmServiceSchedule(param) {
  return {};
}

// ----------------------------------------

export function getServiceScheduledDetail(param) {
  let data = require("@app-data-dummies/service_detail");

  return {
    order_technicians: data,
  };
}

// ----------------------------------------

export function getServiceOnProgressDetail(param) {
  let data = require("@app-data-dummies/service_detail");

  return {
    orders: data,
  };
}
