// ------------------------------------------------
// ------------------------------------------------
// IMPORTS
// ------------------------------------------------
import {GET_BASE} from "./Base/Sizes";
import {INPUT as TColor} from "./Base/Colors";


// ------------------------------------------------
// ------------------------------------------------
// CONSTS
// ------------------------------------------------

const KEYBOARD = {
	default: "default",
	numeric: "number-pad",
	email: "email-address",
};

// ------------------------------------------------

const AUTOCAPITALIZE = {
	none: "none",
	characters: "characters",
	words: "words",
	sentences: "sentences",
};

// ------------------------------------------------

const TYPE = {
  pushedUp: "pushedUp",
  fixed: "fixed",
};

// ------------------------------------------------

const INPUT = {
  inactiveBorderColor: TColor.borderColorDark,
  activeBorderColor: TColor.borderColorActiveDark,
  height: GET_BASE(3),
  type: TYPE.pushedUp,
	hPadding: 0,
	vPadding: 0,
	secured: false,
	keyboard: KEYBOARD.default,
	autoCapitalize: AUTOCAPITALIZE.none,
	autoCorrect: false,
  textMode: "dark",
  textLabelMode: "dark2",
  textPlaceholderMode: "dark2",
  textTheme: "InputValue",
  labelTheme: "InputLabel",
};


// ------------------------------------------------
// ------------------------------------------------
// CONFIGS
// ------------------------------------------------

export default {
	default: {
		... INPUT,
	},

	String: {
		... INPUT,
	},

	Number: {
		... INPUT,
		keyboard: KEYBOARD.numeric,
	},

	Email: {
		... INPUT,
		keyboard: KEYBOARD.email,
	},

	Password: {
		... INPUT,
		secured: true,
	},
};
