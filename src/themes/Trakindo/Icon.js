// ------------------------------------------------
// ------------------------------------------------
// IMPORTS
// ------------------------------------------------
import {GET_BASE} from "./Base/Sizes";
import {TEXT as TColor} from "./Base/Colors";



// ------------------------------------------------
// ------------------------------------------------
// CONSTS
// ------------------------------------------------

const TYPE = {
	FA5: "FA5",
  TRAKINDO: "TRAKINDO",
};

// ------------------------------------------------

const FA5 = {
  type: TYPE.FA5,
  size: 20,
  color: TColor.dark,
};

// ------------------------------------------------

const TRAKINDO = {
  type: TYPE.TRAKINDO,
  size: 20,
  color: TColor.light,
  colorInverse: TColor.dark,
};

// ------------------------------------------------

const ICON = {
  ... TRAKINDO,
};


// ------------------------------------------------
// ------------------------------------------------
// CONFIGS
// ------------------------------------------------

export default {
	default: {
		... ICON,
	},

// ------------------------------------------------

  TrakindoSys: {
    ... ICON,
  },

  TrakindoFeature: {
    ... ICON,
    size: 40,
    color: TColor.orange,
  },

  TrakindoNav: {
    ... ICON,
    color: TColor.light2,
  },

	FA5: {
		... FA5,
	},
};
