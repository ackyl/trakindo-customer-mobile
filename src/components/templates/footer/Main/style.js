/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import {
  COLOR,
} from "./consts";


export default {
  menu: {
    inactive: {
      clickableContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
    },

    active: {
      innerContainer: {
        position: "absolute",
        bottom: 0,
        backgroundColor: COLOR.orange,
        borderWidth: Config.base(.75),
        borderColor: COLOR.gray,
        borderRadius: 50,
      },

      clickableContainer: {
        flex: -1,
        width: Config.base(10),
        height: Config.base(10),
        justifyContent: "center",
        alignItems: "center",
      },
    },
  },

  filler: {
    position: "absolute",
    bottom: -Config.base(5),
    width: Device.screen.width,
    height: Config.base(5),
    backgroundColor: COLOR.gray,
  },
};
