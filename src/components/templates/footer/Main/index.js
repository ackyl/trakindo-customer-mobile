/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Animated,
  TouchableOpacity,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  MENU,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  IconTrakindoNav,
} from "@app-components-core/Icon";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Main extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    const activeMenu = props.activeMenu ? props.activeMenu : MENU[0].menu;

    const menus = {};
    MENU.map((menuItem, index) => {
      const {
        menu,
      } = menuItem;

      menus[menu] = {
        dimension: new Animated.Value(activeMenu === menu ? Config.base(8.75) : 0),
        opacity: new Animated.Value(activeMenu === menu ? 1 : -1),
      };
    });

    this.state = {
      menus,

      activeMenu,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      if (this.props.activeMenu !== nextProps.activeMenu) {
        this.switchMenu(nextProps.activeMenu);
      }

      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  toggleOn(menu) {
    Animated.timing(menu.dimension, {
      toValue: Config.base(8.75),
      duration: 250,
      delay: 0,
    }).start();
    Animated.timing(menu.opacity, {
      toValue: 1,
      duration: 250,
      delay: 0,
    }).start();
  }

// ----------------------------------------


  toggleOff(menu) {
    Animated.timing(menu.dimension, {
      toValue: 0,
      duration: 250,
      delay: 0,
    }).start();
    Animated.timing(menu.opacity, {
      toValue: -1,
      duration: 250,
      delay: 0,
    }).start();
  }

// ----------------------------------------

  switchMenu(menu) {
    const {
      onSwitch,
    } = this.props;

    const {
      menus,
      activeMenu,
    } = this.state;

    if (activeMenu === menu) {
      return null;
    }

    this.toggleOff(menus[activeMenu]);
    this.toggleOn(menus[menu]);

    this.setState({activeMenu: menu});

    if (onSwitch) {
      onSwitch(menu);
    }
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderActiveMenu(menu:string, icon:string, index:number) {
    const {
      dimension,
      opacity,
    } = this.state.menus[menu];

    let sizeActive = 28;
    if (menu === "inbox"){
      sizeActive = 21;
    }
    else if (menu === "order"){
      sizeActive = 26;
    }

    return (
        <Spacer animated vAlign="center" hAlign="center" style={ [Styles.menu.active.innerContainer, {
          width: dimension,
          height: dimension,
          opacity: opacity,
        }] } key={ index }>
          <TouchableOpacity
            style={ Styles.menu.active.clickableContainer }
          >
            <IconTrakindoNav name={ icon } size={ sizeActive } inverse/>
          </TouchableOpacity>
        </Spacer>
    );
  }

// ----------------------------------------

  _renderInactiveMenu(menu:string, icon:string, index:number) {

    let sizeInactive = 24;
    if (menu === "inbox"){
      sizeInactive = 18;
    }
    else if (menu === "order"){
      sizeInactive = 22;
    }

    return (
      <TouchableOpacity
        style={ Styles.menu.inactive.clickableContainer }
        onPress={ () => this.switchMenu(menu) }
        key={ index }
      >
        <Spacer flex={ 1 } vAlign="center" hAlign="center">
          <IconTrakindoNav name={ icon } size={ sizeInactive }/>
        </Spacer>

        { this._renderActiveMenu(menu, icon, index) }
      </TouchableOpacity>
    );
  }

// ----------------------------------------

  _renderMenu(menu:string, icon:string, index:number) {
    return this._renderInactiveMenu(menu, icon, index);
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer style={{
        height: Config.base(6),
        backgroundColor: COLOR.gray,
      }}>
        <Padder flex={ 1 }>
          <Spacer flex={ 1 } row>
            {
              MENU.map((menuItem, index) => {
                const {
                  menu,
                  icon,
                } = menuItem;

                return this._renderMenu(menu, icon, index);
              })
            }
          </Spacer>
        </Padder>

        <Spacer style={ Styles.filler }/>
      </Spacer>
    );
  }

}
