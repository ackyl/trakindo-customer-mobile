/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import LinearGradient from "react-native-linear-gradient";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ModalBase,
} from "@core-components-bases";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ModalPartial extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderChildren() {
    return (
      <Spacer flex={ 1 }>

        <Spacer
          flex={ 1 }
          onPress={ this.props.onClosePress }
          activeOpacity={ 1 }
        >
          <LinearGradient colors={[
            "transparent",
            COLOR.black,
          ]} style={ Styles.gradient }/>
        </Spacer>

        <Spacer backgroundColor={ COLOR.gray }>
          <Spacer style={ Styles.rounder }/>

          <Spacer bottom={ Device.isIphoneX ? 6 : 3 }>
            <Padder>
              { this.props.children }
            </Padder>
          </Spacer>
        </Spacer>

      </Spacer>
    );
  }

// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalBase
        backgroundColor="transparent"
        { ... this.props }
      >

        <Spacer flex={ 1 } style={ Styles.container }>
          { this._renderChildren() }
        </Spacer>

      </ModalBase>
    );
  }


// ----------------------------------------

}
