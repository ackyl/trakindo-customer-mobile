/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  ScrollView,
} from "react-native";
import ShadowView from "react-native-simple-shadow-view";
import { BlurView } from "@react-native-community/blur";


// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ModalBase,
} from "@core-components-bases";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Modal extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      isCloseButtonSolid: false,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onContentScrolled(event) {
    const {
      onScroll,
    } = this.props;

    const yOffset = event.nativeEvent.contentOffset.y;

    const isCloseButtonSolid = yOffset > Config.base(2);

    if (isCloseButtonSolid !== this.state.isCloseButtonSolid) {
      this.setState({
        isCloseButtonSolid,
      });
    }

    if (!onScroll) {
      return null;
    }

    onScroll(yOffset);
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderBackground() {
    return (
      <BlurView
        // viewRef={findNodeHandle(this._viewRef)}
        style={ Styles.background }
        // blurType="light"
        blurAmount={10}
      />
    );
  }

// ----------------------------------------

  _renderChildren() {
    return (
      <Spacer flex={ 1 }>
        <Padder flex={ 1 } overflowable>
          { this.props.children }
        </Padder>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderMain() {
    const {
      scrollable,
      bottomContent,
    } = this.props;

    if (!scrollable) {
      return (
        <Spacer
          height={ Device.windows.height }
          style={ [
            Styles.contentContainer,
            bottomContent ? {paddingBottom: 0} : {},
          ] }
        >
          { this._renderChildren() }
        </Spacer>
      );
    }

    return (
      <ScrollView
        style={ Styles.container }
        contentContainerStyle={ Styles.contentContainer }
        scrollEnabled={ this.props.scrollable ? this.props.scrollable : false }
        onScroll={ (event) => this.onContentScrolled(event) }
        scrollEventThrottle={ 16 }
      >
        { this._renderChildren() }
      </ScrollView>
    );
  }

// ----------------------------------------

  _renderCloseButton() {
    const {
      onClosePress,
      unclosable,
    } = this.props;

    if (unclosable) {
      return null;
    }

    return (
      <ShadowView
        style={ [
          Styles.closeButton.shadow,
          {backgroundColor: this.state.isCloseButtonSolid ? COLOR.gray : "transparent",}
        ] }
      >
        <Spacer onPress={ onClosePress }>
          <IconTrakindoSys name="close" size={ 24 }/>
        </Spacer>
      </ShadowView>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalBase
        backgroundColor={ "transparent" }
        { ... this.props }
        scrollable={ false }
      >
        { this._renderBackground() }

        <Spacer flex={ 1 } top={ Device.isAndroid ? 3 : 0 } cref={ (ref) => {this._viewRef = ref;} }>
          { this._renderMain() }

          { this._renderCloseButton() }
        </Spacer>

      </ModalBase>
    );
  }


// ----------------------------------------

}
