/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import {
  COLOR,
} from "./consts";


export default {
  contentContainer: {
    paddingTop: Device.isAndroid ? 0 : Config.base(2.3),
    paddingBottom: Device.isIphoneX ? Config.base(5) : Config.base(2),
  },

  gradient: {
    flex: 1,
  },

  background: {
    position: "absolute",
    width: Device.windows.width,
    height: Device.windows.height,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: COLOR.black,
    opacity: .985,
  },

  closeButton: {
    shadow: {
      shadowColor: "rgba(0, 0, 0, .5)",
      shadowOpacity: 1,
      shadowRadius: 2,
      shadowOffset: { width: 0, height: 4 },
      position: "absolute",
      right: Config.base(2),
      padding: Config.base(.5),
      borderRadius: Config.base(1),
    },
  },
};

