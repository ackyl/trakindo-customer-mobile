/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ScreenBase as CoreBase,
} from "@core-components-bases";
import Header from "@app-components-headers/Main";
import Footer from "@app-components-footers/Main";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  NetworkConnection as NetworkConnectionModal,
  InternalServer as InternalServerModal,
  FileNotFound as FileNotFoundModal,
  Unauthorized as UnauthorizedModal,
} from "@app-modals/Error";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as ErrorSelector from "@app-selectors/error";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  AuthAction,
  ErrorAction,
} from "@app-actions";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  ERROR,
} from "@app-events";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Base extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      yOffset: 0,

      canShowError: false,
      key: 0,
    };
  }

// ----------------------------------------

  componentDidMount() {
    this.props.resetError();

    setTimeout(
      () => {
        this.setState({
          canShowError: true,
        });
      },
      1000
    );
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  retry() {
    this.props.resetError();

    Navigation.reset("ResourceLoader");
  }

// ----------------------------------------

  relogin() {
    this.props.resetError();

    this.props.logout(
      {},
      () => Navigation.replace("Login")
    );
  }

// ----------------------------------------

  dismissError() {
    this.props.resetError();

    this.setState({
      canShowError: false,
    });
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderHeader(isFloatingHeader = false) {
    const {
      title,
      leftHeaderMode,
      onLeftHeaderPress,
      rightHeaderMode,
      onRightHeaderPress,
      solidHeader,
    } =  this.props;

    const {
      yOffset,
    } = this.state;

    if (isFloatingHeader) {
      return null;
    }

    if (!leftHeaderMode && !rightHeaderMode && !title) {
      return null;
    }

    return (
      <Header
        title={ title }
        mode={ solidHeader || yOffset > Config.base(6) ? "solid" : null }
        leftMode={ leftHeaderMode }
        onLeftPress={ onLeftHeaderPress }
        rightMode={ rightHeaderMode }
        onRightPress={ onRightHeaderPress }
      />
    );
  }

// ----------------------------------------

  _renderFooter() {
    const {
      hasFooter,
      footerCounter,
      onSwitch,
    } = this.props;

    const counter = footerCounter ? footerCounter : null;

    if (!hasFooter) {
      return null;
    }

    return (
      <Footer
        counter={ counter }
        activeMenu={ this.props.activeFooter }
        onSwitch={ onSwitch }
      />
    );
  }

// ----------------------------------------

  _renderErrorModal() {
    // if (!this.props.hasError.data || !this.state.canShowError || this.props.noErrorModal) {
    if (!this.props.hasError.data || this.props.noErrorModal) {
      return null;
    }

    const {
      type,
    } = this.props.error.data;

    switch (type) {
      case "NetworkError":
        return (
          <NetworkConnectionModal
            isActive={ true }
            onClosePress={ () => this.dismissError() }
            navData={{
              error: this.props.error.data,
              onRetryPressed: () => this.retry(),
            }}
          />
        );

      case "ServerError":
        return (
          <InternalServerModal
            isActive={ true }
            onClosePress={ () => this.dismissError() }
            navData={{
              error: this.props.error.data,
            }}
          />
        );

      case "FileNotFoundError":
        return (
          <FileNotFoundModal
            isActive={ true }
            onClosePress={ () => this.dismissError() }
            navData={{
              error: this.props.error.data,
            }}
          />
        );

      case "UnauthorizedError":
        return (
          <UnauthorizedModal
            isActive={ true }
            navData={{
              error: this.props.error.data,
              onReloginPressed: () => this.relogin(),
            }}
          />
        );
    }
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CoreBase
        headerRender={ this._renderHeader() }
        footerRender={ this._renderFooter() }
        baseColor={ COLOR.black }
        inverseStatus
        onScroll={ (yOffset) => this.setState({yOffset: yOffset <= 0 ? 0 : yOffset}) }
        { ... this.props }
        key={ this.state.key }
      >
        { this.props.children }

        { this._renderErrorModal() }
      </CoreBase>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    error: ErrorSelector.error(state, props),

    hasError: ErrorSelector.haseError(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    resetError: (data, onComplete) => dispatch(ErrorAction.reset(data, onComplete)),
    logout: (data, onComplete) => dispatch(AuthAction.directLogout(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Base);


