/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import {
  COLOR,
} from "./consts";


export default {
  container: {
    paddingTop: Device.isAndroid ? 0 : Config.base(2.3),
  },

  gradient: {
    flex: 1,
  },

  rounder: {
    top: -Config.base(1),
    width: Device.windows.width,
    height: Config.base(2),
    borderRadius: Config.base(1),
    backgroundColor: COLOR.gray,
  },
};

