/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import {
  COLOR,
} from "./consts";


export default {
  filler: {
    position: "absolute",
    top: -Config.base(7),
    width: Device.screen.width,
    height: Config.base(11) + (Device.isAndroid ? 2 : 0),
    // borderRadius: Config.base(1),
  },

  fillerShadow: {
    flex: 1,
    shadowColor: "rgba(0, 0, 0, .5)",
    shadowOpacity: 1,
    shadowRadius: 2,
    shadowOffset: { width: 0, height: 8 },
    backgroundColor: COLOR.gray,
    // borderRadius: Config.base(1),
  },
};
