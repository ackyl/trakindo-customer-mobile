/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Animated,
} from "react-native";
import ShadowView from "react-native-simple-shadow-view";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBM,
} from "@app-components-core/Text";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  HeaderBase,
} from "@core-components-bases";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Main extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      filler: {
        top: new Animated.Value(!props.mode ? -Config.base(18) : -Config.base(7)),
        opacity: new Animated.Value(!props.mode ? .2 : 1),
      },
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      if (this.props.mode !== nextProps.mode) {
        this.switchMode(nextProps.mode);
      }


      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------

  switchMode(mode) {
    if (!mode) {
      Animated.timing(this.state.filler.top, {
        toValue: -Config.base(18),
        duration: 300,
        delay: 0,
      }).start();
      Animated.timing(this.state.filler.opacity, {
        toValue: .2,
        duration: 300,
        delay: 0,
      }).start();
    } else {
      Animated.timing(this.state.filler.top, {
        toValue: -Config.base(7),
        duration: 300,
        delay: 0,
      }).start();
      Animated.timing(this.state.filler.opacity, {
        toValue: 1,
        duration: 300,
        delay: 0,
      }).start();
    }
  }

// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  getColor(menu) {
    let {
      color,
      mode,
    } = this.props;

    if (color) {
      return color;
    }

    switch (mode) {
      case "solid":
        return COLOR.gray;
    }

    return null;
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLeftIcon() {
    const {
      leftMode,
      onLeftPress,
    } = this.props;

    let icon = null;
    switch (leftMode) {
      case "back":
        icon = "back";
        break;
    }

    let iconRender = null;
    if (icon) {
      iconRender = <IconTrakindoSys name={ icon }/>;
    }

    return (
      <Spacer
        onPress={ onLeftPress }
      >
        <Spacer flex={ 1 } vAlign="center" hAlign="center">
          <Padder>
            { iconRender }

            <Spacer horizontal space={ 2 }/>
          </Padder>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderRightIcon() {
    const {
      rightMode,
      onRightPress,
    } = this.props;

    let icon = null;
    switch (rightMode) {
      case "close":
        icon = "close";
        break;

      case "logout":
        icon = "logout";
        break;

      case "download":
        icon = "download";
        break;
    }

    let iconRender = null;
    if (icon) {
      iconRender = <IconTrakindoSys name={ icon }/>;
    }

    return (
      <Spacer
        onPress={ onRightPress }
      >
        <Spacer flex={ 1 } vAlign="center" hAlign="center">
          <Padder>
            <Spacer horizontal space={ 2 }/>

            { iconRender }
          </Padder>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderTitle() {
    const {
      title,
    } = this.props;

    return (
      <Spacer flex={ 1 } vAlign="center" hAlign="center">
        <TextBM
          mode="light"
        >
          { title }
        </TextBM>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderFiller() {
    const {
      mode,
    } = this.props;

    return (
      <Spacer
        animated
        style={[
          Styles.filler,
          mode === "solid" ? {backgroundColor: COLOR.gray} : {},
          {
            top: this.state.filler.top,
            opacity: this.state.filler.opacity,
          },
        ]}
      >
        <ShadowView style={ Styles.fillerShadow }/>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <HeaderBase
        top={ 1.5 }
        bottom={ 1.5 }
      >
        <Spacer flex={ 1 }>
          { this._renderFiller() }

          <Spacer row>
            { this._renderLeftIcon() }

            { this._renderTitle() }

            { this._renderRightIcon() }
          </Spacer>
        </Spacer>
      </HeaderBase>
    );
  }

}
