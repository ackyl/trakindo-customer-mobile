/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, { Component } from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import Styles from "./style";
import {
  Config,
  Device,
} from "@core-utils";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBM,
} from "@app-components-core/Text";
import GButton from "@core-components-generics/Button";

// ----------------------------------------
// CONSTANTS
// ----------------------------------------
const THEME = Config.theme("Button");
const COLOR = Config.theme("Color.INPUT");




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Button extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  getColor(mainConfig) {
    const {
      disabled,
      mode,
    } = this.props;

    if (disabled) {
      return mode === "light" ? COLOR.borderColorLight : COLOR.borderColorDark;
    }

    return mainConfig.color;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderText(mainConfig) {
    const textTheme = mainConfig.textTheme ? mainConfig.textTheme : "ButtonCompact";

    const {
      mode,
    } = this.props;

    return (
      <TextBM
        theme={ textTheme }
        mode={
          this.props.disabled ?
            (mode === "light" ? "dark2" : mainConfig.textDisableMode)
          :
            (mode === "light" ? "dark" : mainConfig.textMode)
        }
      >
        { this.props.children }
      </TextBM>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const mainConfig = THEME[this.props.theme ? this.props.theme : "default"];

    return (
      <GButton
        { ... mainConfig }
        { ... this.props }
        onPress={ this.props.onPress }
        color={ this.props.color && !this.props.disabled ? this.props.color : this.getColor(mainConfig) }
      >
        { this._renderText(mainConfig) }
      </GButton>
    );
  }


// ----------------------------------------

}
