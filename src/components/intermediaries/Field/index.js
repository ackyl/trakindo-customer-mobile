/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextBS,
} from "@app-components-core/Text";
import {
  Spacer,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Field extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderValue() {
    const {
      value,
      textTheme,
      wrap,
    } = this.props;

    return (
      <TextBS
        theme={ textTheme }
        numberOfLines={ wrap ? 0 : 1 }
        { ... this.props }
      >
        { value }
      </TextBS>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      label,
      mode,
      preLabel,
      postLabel,
    } = this.props;

    return (
      <Spacer>
        <Spacer row vAlign="center">
          { preLabel }

          <TextBS
            mode={ mode === "light" ? "light2" : "dark2" }
            numberOfLines={ 1 }
          >
            { label }
          </TextBS>

          { postLabel }
        </Spacer>

        { this._renderValue() }
      </Spacer>
    );
  }
}
