/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  View,
  Animated,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import Styles from "./style";
import {
  Config,
  Device,
} from "@core-utils";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBM,
  TextBS,
  TextSmall,
} from "@app-components-core/Text";
import GInput from "@core-components-generics/Input";
import {
  Spacer,
} from "@core-components-enhancers";
// import Text from "@core-components-generics/Text";
import {
  IconTrakindoSys,
  IconFA5,
} from "@app-components-core/Icon";
import {
  TranslateFormatter,
} from "@core-components-formatters";

// ----------------------------------------
// CONSTANTS
// ----------------------------------------
const THEME = Config.theme("Input");
const TEXT_THEME = Config.theme("Text");
const COLOR = Config.theme("Color.BASE");


const LABEL = {
  active: {
    position: Config.base(0),
    size: 12,
    line: 18,
    opacity: 1,
  },
  inactive: {
    position: Config.base(3.7) - 1.5,
    size: 16,
    line: 24,
    opacity: 1,
  },
};




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Input extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      position: new Animated.Value(this.props.value ? LABEL.active.position : LABEL.inactive.position),
      size: new Animated.Value(this.props.value ? LABEL.active.size : LABEL.inactive.size),
      line: new Animated.Value(this.props.value ? LABEL.active.line : LABEL.inactive.line),
      opacity: new Animated.Value(this.props.value ? LABEL.active.opacity : LABEL.inactive.opacity),

      isFocused: false,
      isSecured: true,

      value: this.props.value,

      noOfLines: 1,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state !== nextState) {
      return true;
    }

    if (this.props !== nextProps) {
      if (!this.props.value && nextProps.value) {
        this.setState({
          position: new Animated.Value(0),
          size: new Animated.Value(12),
          line: new Animated.Value(16),
          opacity: new Animated.Value(1),

          value: nextProps.value,
        });

        if (nextProps.editable === false) {
          this.focus(true);
        }
      } else if (!!this.props.value && !nextProps.value && nextProps.editable === false) {
        this.setState({
          value: nextProps.value,
        });

        this.focus(false, true);
      } else if (this.state.value !== nextProps.value) {
        this.updateValue(nextProps.value);
      }

      return true;
    }

    return false;
  }

// ---------------------------------------------------

  componentDidMount() {
    if (this.props.cref) {
      this.props.cref(this.inpRef);
    }
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  focus(isUp = false, useForce = false) {
    if (isUp) {

      if (this.props.onFocus) {
        this.props.onFocus();
      }

      Animated.timing(this.state.position, {
        toValue: LABEL.active.position,
        duration: 100,
        delay: 0,
      }).start();
      Animated.timing(this.state.size, {
        toValue: LABEL.active.size,
        duration: 100,
        delay: 0,
      }).start();
      Animated.timing(this.state.line, {
        toValue: LABEL.active.line,
        duration: 100,
        delay: 0,
      }).start();
      Animated.timing(this.state.opacity, {
        toValue: LABEL.active.opacity,
        duration: 100,
        delay: 0,
      }).start();

      this.setState({
        isFocused: true,
      });
    } else {

      if (this.props.onBlur) {
        this.props.onBlur();
      }

      if (this.state.value === null || this.state.value === undefined || this.state.value.trim().length < 1 || useForce) {
        Animated.timing(this.state.position, {
          toValue: LABEL.inactive.position,
          duration: 100,
          delay: 0,
        }).start();
        Animated.timing(this.state.size, {
          toValue: LABEL.inactive.size,
          duration: 100,
          delay: 0,
        }).start();
        Animated.timing(this.state.line, {
          toValue: LABEL.inactive.line,
          duration: 100,
          delay: 0,
        }).start();
        Animated.timing(this.state.opacity, {
          toValue: LABEL.inactive.opacity,
          duration: 100,
          delay: 0,
        }).start();
      }

      this.setState({
        isFocused: false,
      });

      const {
        value,
      } = this.state;

      this.updateValue(value !== null && value !== undefined ? value.trim() : "");
    }
  }

// ----------------------------------------

  setNoOfLines(noOfLines:number = 1) {
    if (this.state.noOfLines === noOfLines) {
      return null;
    }

    this.setState({
      noOfLines,
    });
  }

// ----------------------------------------

  updateValue(value) {
    this.setState({value});

    if (this.props.onChangeText) {
      this.props.onChangeText(value);
    }
  }

// ----------------------------------------

  getHeight(mainConfig) {
    const {
      height,
    } = this.props;

    if (height !== undefined && height !== null) {
      return height;
    }

    return (mainConfig.height * this.state.noOfLines) + 6;
  }

// ----------------------------------------

  getBorderColor(mainConfig) {
    const {
      mode,
      borderColor,
      borderFocusColor,
      noBorder,
    } = this.props;

    if (noBorder) {
      return "transparent";
    }

    const {
      isFocused,
    } = this.state;

    if (isFocused) {
      return borderFocusColor ? borderFocusColor : mainConfig.activeBorderColor;
    }

    if (borderColor) {
      return borderColor;
    }

    switch (mode) {
      case "light":
        return mainConfig.inactiveBorderColor;

      case "dark":
      default:
        return COLOR.blackT0;
    }
  }

// ----------------------------------------

  getLabelColor(mainConfig) {
    const {
      mode,
      labelColor,
    } = this.props;

    if (labelColor) {
      return labelColor;
    }

    switch (mode) {
      case "light":
        return COLOR.whiteT4;

      case "dark":
      default:
        return COLOR.grayT4;
    }
  }

// ----------------------------------------

  getTextColor(mainConfig) {
    const {
      mode,
      color,
    } = this.props;

    if (color) {
      return color;
    }

    switch (mode) {
      case "light":
        return COLOR.white;

      case "dark":
      default:
        return COLOR.black;
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLeftIcon(mainConfig) {
    if (!this.props.leftIcon) {
      return null;
    }

    return (
      <View style={ Styles.left.container }>
        <View style={ Styles.left.icon.container }>
          <IconFA5 name={ this.props.leftIcon } color={ this.state.isFocused ? mainConfig.activeColor : mainConfig.inactiveColor }/>
        </View>
      </View>
    );
  }

// ----------------------------------------

  _renderRightButton(mainConfig) {
    if (!this.props.rightIcon && !mainConfig.secured) {
      return null;
    }

    let rightIcon = this.props.rightIcon;
    let color = this.props.rightIconColor ? this.props.rightIconColor : this.getLabelColor(mainConfig);
    let onPress = null;

    if (this.props.onRightIconPress) {
      onPress = () => this.props.onRightIconPress();
    }

    if (mainConfig.secured && this.state.isSecured) {
      rightIcon = "eye";
      onPress = () => this.setState({isSecured: false});
    } else if (mainConfig.secured && !this.state.isSecured) {
      rightIcon = "eye-slash";
      onPress = () => this.setState({isSecured: true});
    }

    const main = <View style={ [Styles.left.icon.container, {alignItems: "flex-end"}] }>
      <IconFA5 name={ rightIcon } color={ color }/>
    </View>;

    if (onPress) {
      return (
        <Spacer style={ Styles.left.container } onPress={ () => onPress() }>
          { main }
        </Spacer>
      );
    }

    return (
      <View style={ Styles.left.container }>
        { main }
      </View>
    );
  }

// ----------------------------------------

  _renderLabel(mainConfig) {
    const {
      label,
    } = this.props;

    if (!label) {
      return null;
    }

    return (
      <Spacer height={ 24 } vAlign="flex-end">
        <TextBM
          animated
          theme={ mainConfig.labelTheme }
          color={ this.getLabelColor(mainConfig) }
          size={ this.state.size }
          line={ this.state.line }
          style={{
            top: this.state.position,
            opacity: this.state.opacity,
          }}
        >
          { label }
        </TextBM>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderPlaceholder(mainConfig, textStyle) {

    const {
      value,
      label,
      placeholder,
    } = this.props;

    if (value !== "" || label){
      return null;
    }

    return (
      <Spacer
        style={ Styles.placeholder.container }
        height={ this.getHeight(mainConfig) }
        vAlign="flex-end"
      >
        <TextBM
          style={ textStyle }
          color={ this.getLabelColor(mainConfig) }
        >
          <TranslateFormatter flat>
            { placeholder }
          </TranslateFormatter>
        </TextBM>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderMain(mainConfig) {
    const {
      autoCorrect,
      autoCapitalize,
      keyboard,
      onPress,
    } = this.props;

    const textConfig = TEXT_THEME[mainConfig.textTheme ? mainConfig.textTheme : "default"];

    const textStyle = {
      fontSize: textConfig.size,
      lineHeight: textConfig.line - 2,
      letterSpacing: textConfig.spacing,
      fontFamily: textConfig.family,
      top: -1,
    };

    return (
      <Spacer
        flex={ 1 }
        row
        height={ this.getHeight(mainConfig) }
        vAlign="center"
        borderBottomColor={ this.getBorderColor(mainConfig) }
        borderBottomWidth={ 2 }
        noChildEvent={ !!onPress }
      >
        <Spacer
          flex={ 1 }
          height={ this.getHeight(mainConfig) }
        >



          { this._renderPlaceholder(mainConfig, textStyle) }

          <GInput
            { ... mainConfig }
            { ... this.props }
            value={ this.state.value }
            onFocus={ () => this.focus(true) }
            onBlur={ () => this.focus(false) }
            placeholder={ "" }
            // placeholderTextColor={ this.getLabelColor(mainConfig) }
            style={ [textStyle, { color: this.getTextColor(mainConfig) }] }
            onChangeText={ (value) => this.updateValue(value) }
            cref={ (r) => {this.inpRef = r;} }
            returnKeyType={ this.props.nextInput ? "next" : "default" }
            onSubmitEditing={ this.props.nextInput ? () => this.props.nextInput.focus() : () => {} }
            secured={ mainConfig.secured && this.state.isSecured }
            autoCorrect={ autoCorrect ? autoCorrect : mainConfig.autoCorrect }
            autoCapitalize={ autoCapitalize ? autoCapitalize : mainConfig.autoCapitalize }
            keyboardType={ keyboard ? keyboard : mainConfig.keyboard }
            onContentSizeChange={ (event) => this.setNoOfLines(event.nativeEvent.contentSize.height / 22) }
            line={ this.getHeight(mainConfig) }
            scrollEnabled={ false }
          />
        </Spacer>

        { this._renderRightButton(mainConfig) }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderError(mainConfig) {
    if (!this.props.error) {
      return null;
    }

    return (
      <TextSmall color={ COLOR.red }>{ this.props.error }</TextSmall>
    );
  }

// ----------------------------------------

  _renderHint(mainConfig) {
    if (!this.props.hint) {
      return null;
    }

    return (
      <TextBS color={ mainConfig.hintColor } light style={ Styles.hint }>{ this.props.hint }</TextBS>
    );
  }

// ----------------------------------------

  _renderBottoms(mainConfig) {
    return (
      <View style={ Styles.bottomText.grouper }>
        { this._renderError(mainConfig) }

        { this._renderHint(mainConfig) }
      </View>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      onPress,
    } = this.props;

    const mainConfig = THEME[this.props.theme ? this.props.theme : "default"];

    return (
        <Spacer
          onPress={ onPress ? onPress : () => this.inpRef.focus() }
          noChildEvent={ !!onPress }
        >
          { this._renderLabel(mainConfig) }

          <Spacer height={ this.getHeight(mainConfig) }>
            { this._renderMain(mainConfig) }
          </Spacer>


          { this._renderBottoms(mainConfig) }
        </Spacer>
    );
  }


// ----------------------------------------

}
