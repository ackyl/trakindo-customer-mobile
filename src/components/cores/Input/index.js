import InputString from "./InputString";
import InputNumber from "./InputNumber";
import InputEmail from "./InputEmail";
import InputPassword from "./InputPassword";

export {
	InputString,
  InputNumber,
  InputEmail,
  InputPassword,
};
