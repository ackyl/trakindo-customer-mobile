import ImageLocal from "./ImageLocal";
import ImageUri from "./ImageUri";

export {
	ImageLocal,
  ImageUri,
};
