/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, { Component } from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
} from "@core-utils";
import Styles from "./style";
const COLOR = Config.colors();

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import Wrapper from "@app-components-intermediaries/Wrapper";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class StatusTagService extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  getColor() {
    switch (this.props.children) {
      case "QUOTED":
      case "SCHEDULED":
      case "DISPATCHED":
      case "IN_PROGRESS":
      case "COMPLETED":
      default:
        return COLOR.black;
    }
  }

// ----------------------------------------

  getText() {
    switch (this.props.children) {
      case "QUOTED":
        return "CONFIRM QUOTATION";

      case "SCHEDULED":
        return "CONFIRM SCHEDULE";

      case "DISPATCHED":
        return "ON THE WAY";

      case "IN_PROGRESS":
        return "JOB IN PROGRESS";

      case "COMPLETED":
        return "JOB COMPLETE";

      default:
        return this.props.children;
    }
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Wrapper
        theme={ "StatusTag" }
        { ... this.props }
        color={ this.getColor() }
      >
        { this.getText() }
      </Wrapper>
    );
  }


// ----------------------------------------

}
