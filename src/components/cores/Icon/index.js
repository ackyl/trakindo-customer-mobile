import IconTrakindoSys from "./IconTrakindoSys";
import IconTrakindoFeature from "./IconTrakindoFeature";
import IconTrakindoNav from "./IconTrakindoNav";
import IconFA5 from "./IconFA5";

export {
	IconTrakindoSys,
  IconTrakindoFeature,
  IconTrakindoNav,
  IconFA5,
};
