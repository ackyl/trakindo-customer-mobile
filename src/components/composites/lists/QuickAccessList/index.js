/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  QUICK_ACCESSES,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  QuickAccessCard,
} from "@app-components-cards";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class QuickAccessList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderCard(label:string, icon:string, screen:string = "", module:string, index:number) {
    const {
      order,
      onItemPress,
      allowedModules,
    } = this.props;

    return (
      <HorizontalAnimator order={ index + order } key={ index }>
        <QuickAccessCard
          label={ label }
          icon={ icon }
          onPress={ () => onItemPress(screen, index) }
          disabled={ allowedModules.indexOf(module) < 0 }
        />
      </HorizontalAnimator>
    );
  }

// ----------------------------------------

  _renderRow(rowIndex:number) {
    return (
      <Spacer row hAlign="space-between">
        {
          QUICK_ACCESSES[rowIndex].map((menu, index) => {
            const {
              label,
              icon,
              screen,
              module,
            } = menu;

            return this._renderCard(label, icon, screen, module, (rowIndex * 3) + index);
          })
        }
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        { this._renderRow(0) }

        <Spacer space={ 1 }/>

        { this._renderRow(1) }
      </Spacer>
    );
  }

}
