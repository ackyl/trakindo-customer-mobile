/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  Config,
} from "@core-utils";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  CONSTANTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const COLOR = {
  ... Config.colors(),
};

// ----------------------------------------

export const QUICK_ACCESSES = [
  [
    {
      label: "Request Service",
      icon: "request-service",
      screen: "ServiceRequest",
      module: "SERVICE",
    },
    {
      label: "Service Tracking",
      icon: "service-tracking",
      screen: "ServiceList",
      module: "SERVICE",
    },
    {
      label: "Parts Tracking",
      icon: "parts-tracking-1",
      screen: "PartList",
      module: "PART",
    },
  ],
  [
    {
      label: "Credit Status",
      icon: "parts-tracking",
      screen: "FinanceLanding",
      module: "FINANCE",
    },
    {
      label: "Invoices",
      icon: "invoice",
      screen: "InvoiceList",
      module: "FINANCE",
    },
    {
      label: "Units",
      icon: "units",
      screen: "UnitList",
      module: "COMPANY",
    },
  ],
];
