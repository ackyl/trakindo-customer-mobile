/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  List,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
  Lister,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  CardGlass,
} from "@app-components-core/Card";
import {
  PartShippingTrackingItemFragment,
} from "@app-components-fragments";





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartShippingTrackingItemList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new List(this);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return this.Base.shouldComponentUpdate(nextProps, nextState);
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLister(items = []) {
    return (
      <Lister
        static
        renderer={ (item, index) => this._renderFragment(item, index) }
      >
        { items }
      </Lister>
    );
  }

// ----------------------------------------

  _renderFragment(item = {}, index:number) {
    const {
      data,
      loading,
      order,
    } = this.props;

    return (
      <PartShippingTrackingItemFragment
        { ... item }
        loading={ loading }
        order={ index + order }
        key={ !loading ? index : `${index}-loading` }
        isFirst={ index === 0 }
        isLast={ (index === data.length - 1) || loading && index === 2 }
      />
    );
  }

// ----------------------------------------

  _renderEmptyState() {
    return null;
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return this.Base.render();
  }

}
