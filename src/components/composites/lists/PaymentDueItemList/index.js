/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  List,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  Lister,
} from "@core-components-enhancers";
import {
  PaymentDueItemFragment,
} from "@app-components-fragments";





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartItemGroupItemList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new List(this);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return this.Base.shouldComponentUpdate(nextProps, nextState);
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLister(items = []) {
    return (
      <Lister
        static
        renderer={ (item, index) => this._renderFragment(item, index) }
        separator={ (index) => <Spacer borderBottomColor={ COLOR.whiteT1 } key={ index }/> }
      >
        { items }
      </Lister>
    );
  }

// ----------------------------------------

  _renderFragment(item = {}, index:number) {
    return (
      <PaymentDueItemFragment
        { ... item }
        key={ index }
      />
    );
  }

// ----------------------------------------

  _renderEmptyState() {
    return null;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return this.Base.render();
  }

}
