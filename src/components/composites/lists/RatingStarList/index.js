/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  RatingStarFragment,
} from "@app-components-fragments";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class RatingStarList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderRatingStar(index:number) {
    const {
      rating,
      size,
      loading,
      order,
    } = this.props;

    const isActive = !loading && index <= rating;

    return (
      <HorizontalAnimator order={ index + order } key={ !loading ? index : `${index}-loading` }>
        <RatingStarFragment
          isActive={ isActive }
          size={ size }
          loading={ loading }
        />
      </HorizontalAnimator>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer row>
        { this._renderRatingStar(1) }
        { this._renderRatingStar(2) }
        { this._renderRatingStar(3) }
        { this._renderRatingStar(4) }
        { this._renderRatingStar(5) }
      </Spacer>
    );
  }

}
