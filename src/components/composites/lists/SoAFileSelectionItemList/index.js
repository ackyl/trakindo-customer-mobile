/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  List,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
  Lister,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  CardGlass,
} from "@app-components-core/Card";
import {
  SoAFileSelectionItemFragment,
} from "@app-components-fragments";





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class SoAFileSelectionItemList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new List(this);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return this.Base.shouldComponentUpdate(nextProps, nextState);
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onItemSelected(item, index:number) {
    const {
      onItemPress,
    } = this.props;

    if (onItemPress) {
      onItemPress(item, index);
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLister(items = []) {
    return (
      <CardGlass
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <Lister
          static
          renderer={ (item, index) => this._renderFragment(item, index) }
          separator={ (index) => <Spacer borderBottomColor={ COLOR.whiteT1 } key={ index }/> }
        >
          { items }
        </Lister>
      </CardGlass>
    );
  }

// ----------------------------------------

  _renderFragment(item = {}, index:number) {
    const {
      loading,
      order,
    } = this.props;

    return (
      <HorizontalAnimator order={ index + order } key={ !loading ? index : `${index}-loading` }>
        <SoAFileSelectionItemFragment
          { ... item }
          onPress={ () => this.onItemSelected(item, index) }
          loading={ loading }
        />
      </HorizontalAnimator>
    );
  }

// ----------------------------------------

  _renderEmptyState() {
    return (
      <Padder>
        <Spacer top={ 8 } space={ 8 } vAlign="center" hAlign="center">
          <TextH2
            mode="light2"
            bold
          >
            No SoA File Found
          </TextH2>
        </Spacer>
      </Padder>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return this.Base.render();
  }

}
