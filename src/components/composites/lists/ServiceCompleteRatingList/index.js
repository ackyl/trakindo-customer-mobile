/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  ScrollView,
} from "react-native";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  List,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";
import {
  NumberFormat,
} from "@core-components-formatters";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  ServiceCompleteRatingCard,
} from "@app-components-cards";






/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class RatingStarList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new List(this);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return this.Base.shouldComponentUpdate(nextProps, nextState);
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLister(items = []) {
    let {
      width,
    } = this.props;

    if (!width) {
      width = Device.windows.width;
    }

    const paddingHorizontal = Config.paddingHorizontal(1) + Config.base(0.5);

    return (
      <ScrollView
        horizontal
        decelerationRate="fast"
        snapToInterval={ width - (Config.paddingHorizontal(2)) }
        contentContainerStyle={{
          paddingLeft: paddingHorizontal,
          paddingRight: paddingHorizontal - Config.base(1),
        }}
      >
        {
          items.map((item, index) => {
            return this._renderCard(item, index);
          })
        }
      </ScrollView>
    );
  }

// ----------------------------------------

  _renderCard(item = {}, index:number) {
    const {
      order,
      loading,
    } = this.props;

    let {
      width,
    } = this.props;

    if (!width) {
      width = Device.windows.width;
    }

    return (
      <HorizontalAnimator order={ index + order } key={ !loading ? index : `${index}-loading` }>
        <Spacer row>
          <Spacer width={ width - (Config.paddingHorizontal(2.5)) }>
            <ServiceCompleteRatingCard
              { ... item }
              stage={ <NumberFormat value={ index + 1 } format={ (value) => "Day " + (value < 10 ? "0" : "") + value }/> }
              loading={ loading }
            />
          </Spacer>

          <Spacer horizontal space={ 1 }/>
        </Spacer>
      </HorizontalAnimator>
    );
  }

// ----------------------------------------

  _renderEmptyState() {
    return null;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return this.Base.render();
  }

}
