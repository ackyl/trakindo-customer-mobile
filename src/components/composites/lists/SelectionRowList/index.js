/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  List,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
  Lister,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  CardGlass,
} from "@app-components-core/Card";
import {
  SelectionRowFragment,
} from "@app-components-fragments";





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class SelectionRowList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new List(this);

    this.state = {
      activeIndex: props.activeIndex !== null && props.activeIndex !== undefined ?
          props.activeIndex
        :
          (props.defaultIndex !== null && props.defaultIndex !== undefined ?
              props.defaultIndex
            :
              -1
          ),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (
      nextProps.activeIndex !== undefined &&
      nextProps.activeIndex !== null &&
      nextProps.activeIndex !== this.state.activeIndex
    ) {
      const items = nextProps.data;

      this.onItemSelected(items[nextProps.activeIndex], nextProps.activeIndex);
    }

    return this.Base.shouldComponentUpdate(nextProps, nextState);
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onItemSelected(item, index:number) {
    if (this.state.activeIndex === index) {
      return null;
    }

    this.setState({
      activeIndex: index,
    });

    const {
      onItemPress,
    } = this.props;

    if (onItemPress) {
      onItemPress(item, index);
    }

  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLister(items = []) {
    return (
      <CardGlass
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <Lister
          static
          renderer={ (item, index) => this._renderFragment(item, index) }
          separator={ (index) => <Spacer borderBottomColor={ COLOR.whiteT1 } key={ index }/> }
        >
          { items }
        </Lister>
      </CardGlass>
    );
  }

// ----------------------------------------

  _renderFragment(item = {}, index:number) {
    const {
      loading,
      loadingWidth,
      order,
    } = this.props;

    const {
      activeIndex,
    } = this.state;

    return (
      <HorizontalAnimator order={ index + order } noAnimation={ (index + order) > 20 } key={ !loading ? index : `${index}-loading` }>
        <SelectionRowFragment
          { ... item }
          isActive={ activeIndex === index }
          onPress={ () => this.onItemSelected(item, index) }
          loading={ loading }
          loadingWidth={ loadingWidth }
        />
      </HorizontalAnimator>
    );
  }

// ----------------------------------------

  _renderEmptyState() {
    const {
      emptyMessage,
    } = this.props;

    return (
      <Padder>
        <Spacer top={ 8 } space={ 8 } vAlign="center" hAlign="center">
          <TextH2
            mode="light2"
            bold
          >
            { emptyMessage }
          </TextH2>
        </Spacer>
      </Padder>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return this.Base.render();
  }

}
