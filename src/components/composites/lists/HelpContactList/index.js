/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  List,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  SendEmailCard,
  LiveChatCard,
  CallCenterCallCard,
} from "@app-components-cards";





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class HelpContactList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderCallCenter() {
    const {
      callCenterHelpline,
      loading,
      order,
    } = this.props;

    if (!loading && !callCenterHelpline) {
      return null;
    }

    return (
      <HorizontalAnimator order={ order } key={ !loading ? 0 : "0-loading" }>
        <Spacer space={ 1 }>
          <CallCenterCallCard
            phoneNumber={ callCenterHelpline }
            loading={ loading }
          />
        </Spacer>
      </HorizontalAnimator>
    );
  }

// ----------------------------------------

  _renderSupportEmail() {
    const {
      emailSupport,
      loading,
      order,
    } = this.props;

    if (!loading && !emailSupport) {
      return null;
    }

    return (
      <HorizontalAnimator order={ order + 1 } key={ !loading ? 1 : "1-loading" }>
        <Spacer space={ 1 }>
          <SendEmailCard
            email={ emailSupport }
            loading={ loading }
          />
        </Spacer>
      </HorizontalAnimator>
    );
  }

// ----------------------------------------

  _renderLiveChatLink() {
    const {
      liveChatLink,
      loading,
      order,
    } = this.props;

    if (!loading && !liveChatLink) {
      return null;
    }

    return (
      <HorizontalAnimator order={ order + 2 } key={ !loading ? 2 : "2-loading" }>
        <LiveChatCard
          url={ liveChatLink }
          loading={ loading }
        />
      </HorizontalAnimator>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        { this._renderCallCenter() }

        { this._renderSupportEmail() }

        { this._renderLiveChatLink() }
      </Spacer>
    );
  }

}
