/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import {
  COLOR,
} from "./consts";


export default {
  container: {
    compose: (isRight:boolean = false) => { 
      const composedStyle = [{
        flex: 1,
        justifyContent: "space-between",
        backgroundColor: COLOR.orange,
        borderRadius: 8,
        padding: Config.base(2),
      }];

      if (isRight) {
        composedStyle.push({
          flex: -1,
          minWidth: Config.base(15),
          backgroundColor: COLOR.white,
          marginLeft: Config.base(1),
        });
      }

      return composedStyle;
    },
  },

  left: {
    icon: {
      container: {
        flex: -1,
        position: "absolute",
        top: Config.base(1.5),
        right: Config.base(1.5),
        width: Config.base(4),
        height: Config.base(4),
        borderRadius: 50,
        backgroundColor: COLOR.black,
      },
    },
  },
};
