/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  PaymentDueReminderCard,
  ServiceSummaryReminderCard,
} from "@app-components-cards";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class QuickReminderList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderRemainderPaymentDue() {
    const {
      payment,
      loadingPayment,
      onPaymentPress,
      isFinanceAllowed,
    } = this.props;

    if (!isFinanceAllowed) {
      return null;
    }

    const {
      currency,
      currentAmount,
      overdueAmount,
    } = (payment ? payment : {});

    return (
      <Spacer flex={ 7 }>
        <PaymentDueReminderCard
          currency={ currency }
          currentAmount={ currentAmount }
          overdueAmount={ overdueAmount }
          loading={ loadingPayment }
          onPress={ onPaymentPress }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderRemainderServiceSummary() {
    const {
      order,
      service,
      loadingServiceSummary,
      onServicePress,
      isServiceAllowed,
    } = this.props;

    if (!isServiceAllowed) {
      return null;
    }

    const {
      numberOfUnits,
    } = (service ? service : {});

    return (
      <Spacer flex={ 4 }>
        <HorizontalAnimator order={ order + 1 } style={{ flex: -1 }}>
          <ServiceSummaryReminderCard
            numberOfUnits={ numberOfUnits }
            loading={ loadingServiceSummary }
            onPress={ onServicePress }
          />
        </HorizontalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDivider() {
    const {
      isServiceAllowed,
      isFinanceAllowed,
    } = this.props;

    if (!isServiceAllowed || !isFinanceAllowed) {
      return null;
    }

    return (
      <Spacer horizontal space={ 1 }/>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      order,
    } = this.props;

    return (
      <HorizontalAnimator order={ order } style={ Styles.container.compose() }>
        <Spacer row>
          { this._renderRemainderPaymentDue() }

          { this._renderDivider() }

          { this._renderRemainderServiceSummary() }
        </Spacer>
      </HorizontalAnimator>
    );
  }

}
