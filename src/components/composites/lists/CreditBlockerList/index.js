/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  CreditBlockerCard,
} from "@app-components-cards";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CreditStatusList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderStatusOverdue() {
    const {
      hasOverdue,
      order,
      showInvoices,
      onCallPress,
    } = this.props;

    if (!hasOverdue) {
      return null;
    }

    return (
      <HorizontalAnimator order={ order }>
        <CreditBlockerCard
          type={ "OVERDUE" }
          onCallPress={ onCallPress }
          onDetailPress={ showInvoices ? () => showInvoices() : null }
        />
      </HorizontalAnimator>
    );
  }

// ----------------------------------------

  _renderStatusOverlimit() {
    const {
      isOverlimit,
      hasOverdue,
      order,
      onCallPress,
    } = this.props;

    if (!isOverlimit) {
      return null;
    }

    return (
      <HorizontalAnimator order={ hasOverdue ? (order + 1) : (order + 2) }>
        <CreditBlockerCard
          type={ "OVERLIMIT" }
          onCallPress={ onCallPress }
        />
      </HorizontalAnimator>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      hasOverdue,
      isOverlimit,
    } = this.props;

    if (!hasOverdue && !isOverlimit) {
      return null;
    }

    return (
      <Spacer>
        { this._renderStatusOverdue() }

        {
          hasOverdue && isOverlimit ?
            <Spacer space={ 1 }/>
          :
            null
        }

        { this._renderStatusOverlimit() }
      </Spacer>
    );
  }

}
