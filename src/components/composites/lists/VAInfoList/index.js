/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  List,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Lister,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  VAInfoCard,
} from "@app-components-cards";





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class VAInfoList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new List(this);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return this.Base.shouldComponentUpdate(nextProps, nextState);
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLister(items = []) {
    const {
      padded,
    } = this.props;

    return (
      <Lister
        static
        space={ 1 }
        padded={ padded }
        renderer={ (item, index) => this._renderCard(item, index) }
      >
        { items }
      </Lister>
    );
  }

// ----------------------------------------

  _renderCard(item = {}, index:number) {
    const {
      order,
      loading,
    } = this.props;

    return (
      <HorizontalAnimator order={ index + order } key={ !loading ? index : `${index}-loading` }>
        <VAInfoCard
          { ... item }
          loading={ loading }
        />
      </HorizontalAnimator>
    );
  }

// ----------------------------------------

  _renderEmptyState() {
    return null;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return this.Base.render();
  }

}
