/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  ScrollView,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ServiceProgressTrackingItemFragment,
} from "@app-components-fragments";





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartShippingTrackingItemList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      canAutoScroll: true,
    };

    this._listRef = null;
  }

// ----------------------------------------

  componentDidMount() {
    setTimeout(
      () => {
        this.scrollList();
      },
      200
    );
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      this.scrollList(nextProps.status);

      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  getData() {
    const data = [
      {
        status: "QUOTED",
        updatedAt: 1571249204000,
      },
      {
        status: "SCHEDULED",
        updatedAt: 1571249204000,
      },
      {
        status: "DISPATCHED",
        updatedAt: 1571249204000,
      },
    ];

    let base = [
      "QUOTED",
      "SCHEDULED",
      "DISPATCHED",
      "IN_PROGRESS",
      "COMPLETED",
    ];

    return base.map((baseStatus, inde) => {
      const dataItem = data.filter((item) => item.status === baseStatus);

      if (dataItem.length > 0) {
        return {
          status: baseStatus,
          updatedAt: dataItem[0].updatedAt,
        };
      }

      return {
        status: baseStatus,
        updatedAt: 0,
      };
    });
  }

// ----------------------------------------

  scrollList(status) {
    if (!status) {
      status = this.props.status;
    }

    const data = this.getData();

    let currentStatusIndex = 0;
    for (let x in data) {
      if (data[x].status === status) {
        currentStatusIndex = x;
        break;
      }
    }

    if (currentStatusIndex > 1) {
      this._listRef.scrollToEnd();
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderProgress(item = {}, index:number) {
    const {
      status,
      order,
    } = this.props;

    const data = this.getData();

    return (
      <ServiceProgressTrackingItemFragment
        { ... item }
        order={ index + order }
        key={ index }
        isLast={ index === data.length - 1 }
        isCurrent={ item.status === status }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ScrollView
        ref={ ref => {this._listRef = ref;} }
        horizontal
        contentContainerStyle={{
          paddingHorizontal: Config.paddingHorizontal(),
        }}
      >
        {
          this.getData().map((item, index) => {
            return this._renderProgress(item, index);
          })
        }
      </ScrollView>
    );
  }

}
