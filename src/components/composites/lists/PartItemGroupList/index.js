/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  List,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  Padder,
  Spacer,
  Lister,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  PartItemGroupCard,
} from "@app-components-cards";





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartItemGroupList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new List(this);

    this.state = {
      activeIndex: -1,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return this.Base.shouldComponentUpdate(nextProps, nextState);
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onExpand(newIndex, isExpanded) {
    const {
      activeIndex,
    } = this.state;

    if (activeIndex === newIndex && !isExpanded) {
      this.setState({
        activeIndex: -1,
      });
    } else {
      this.setState({
        activeIndex: newIndex,
      });
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLister(items = []) {
    const {
      loading,
      page,
      loadNext,
      padded,
      scrollEnabled,
      onScroll,
    } = this.props;

    return (
      <Lister
        list
        space={ 1 }
        loading={ loading }
        loadNext={ loadNext }
        page={ page }
        padded={ padded }
        renderer={ (item, index) => this._renderCard(item, index) }
        scrollEnabled={ scrollEnabled }
        onScroll={ onScroll }
        noRefresh
      >
        { items }
      </Lister>
    );
  }

// ----------------------------------------

  _renderCard(item = {}, index:number) {
    const {
      order,
      onItemPress,
      loading,
      page,
    } = this.props;

    const {
      activeIndex,
    } = this.state;

    return (
      <VerticalAnimator order={ index + order } noAnimation={ page > 2 && !loading } key={ !loading ? index : `${index}-loading` }>
        <PartItemGroupCard
          { ... item }
          onItemPress={ onItemPress ? (itemDetail, itemIndex) => onItemPress({
            ... item,
            ... itemDetail,
            items: undefined,
          }, index, itemIndex) : null }
          loading={ loading }
          onExpand={ (isExpanded) => this.onExpand(index, isExpanded) }
          expanded={ !loading && activeIndex === index }
        />
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderEmptyState() {
    const {
      emptyText,
    } = this.props;

    return (
      <Padder>
        <Spacer top={ 8 } space={ 8 } vAlign="center" hAlign="center">
          <TextH2
            mode="light2"
            bold
          >
            { emptyText ? emptyText : "No Parts Found" }
          </TextH2>
        </Spacer>
      </Padder>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return this.Base.render();
  }

}
