/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  List,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  Padder,
  Spacer,
  Lister,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  ServiceCard,
} from "@app-components-cards";





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceList extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new List(this);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return this.Base.shouldComponentUpdate(nextProps, nextState);
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLister(items = []) {
    const {
      loading,
      page,
      loadNext,
      padded,
      noPaddingBottom,
    } = this.props;

    return (
      <Lister
        list
        space={ 1 }
        loading={ loading }
        loadNext={ loadNext }
        page={ page }
        padded={ padded }
        noPaddingBottom={ noPaddingBottom }
        renderer={ (item, index) => this._renderCard(item, index) }
      >
        { items }
      </Lister>
    );
  }

// ----------------------------------------

  _renderCard(item = {}, index:number) {
    const {
      order,
      onItemPress,
      loading,
      page,
    } = this.props;

    return (
      <VerticalAnimator order={ index + order } noAnimation={ page > 2 && !loading } key={ !loading ? index : `${index}-loading` }>
        <ServiceCard
          { ... item }
          onPress={ () => onItemPress(item, index) }
          loading={ loading }
        />
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderEmptyState() {
    return (
      <Padder>
        <Spacer top={ 8 } space={ 8 } vAlign="center" hAlign="center">
          <TextH2
            mode="light2"
            bold
          >
            No Service Found
          </TextH2>
        </Spacer>
      </Padder>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return this.Base.render();
  }

}
