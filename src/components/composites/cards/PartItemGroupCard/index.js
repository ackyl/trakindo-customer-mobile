/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  StatusTagPart,
} from "@app-components-core/StatusTag";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
  CardGlass,
} from "@app-components-core/Card";
import {
  PartItemGroupItemList,
} from "@app-components-lists";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartItemGroupCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      expanded: props.expanded ? props.expanded : false,
      lastUpdated: (new Date().getTime()),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      if (
        nextProps.expanded !== null &&
        nextProps.expanded !== undefined &&
        nextProps.expanded !== nextState.expanded
      ) {
        this.onExpand(nextProps.expanded);
      }

      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onExpand(newExpand) {
    let {
      expanded,
      lastUpdated,
    } = this.state;

    if (expanded === newExpand || (lastUpdated + 500) > (new Date().getTime())) {
      return false;
    }

    this.setState({
      expanded: newExpand,
      lastUpdated: (new Date().getTime()),
    });

    let {
      onExpand,
    } = this.props;

    if (onExpand) {
      onExpand(newExpand);
    }
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderMain() {
    const {
      loading,
      description,
      partNumber,
    } = this.props;

    let {
      items,
    } = this.props;

    items = items ? items : [];

    const hasDelay = items.filter(item => item.isDelayed).length > 0;

    return (
      <Spacer>
        <Spacer row>
          <TextH2
            bold
            loadingWidth={ Config.base(18) }
            loading={ loading }
          >
            { description }
          </TextH2>

          {
            !loading && hasDelay ?
              (
                <StatusTagPart top={ .8 } left={ 4 }>
                  DELAY
                </StatusTagPart>
              )
            :
              null
          }
        </Spacer>

        <Spacer row>
          <TextBS
            mode="dark2"
          >
            Part number:
          </TextBS>

          <Spacer horizontal space={ .5 }/>

          <TextBS
            mode="dark2"
            loadingWidth={ Config.base(8) }
            loading={ loading }
          >
            { partNumber }
          </TextBS>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderExpansionButton() {
    const {
      expanded,
    } = this.state;

    const {
      loading,
    } = this.props;

    if (loading) {
      return null;
    }

    return (
      <IconTrakindoSys name={ expanded ? "arrow-up" : "arrow-down" } size={ 24 } color={ COLOR.black }/>
    );
  }

// ----------------------------------------

  _renderDetails() {
    const {
      expanded,
    } = this.state;

    const {
      loading,
      items,
      onItemPress,
    } = this.props;

    if (loading) {
      return null;
    }

    if (!expanded) {
      return null;
    }

    return (
      <PartItemGroupItemList
        data={ items ? items : [] }
        onItemPress={ onItemPress }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      expanded,
    } = this.state;

    const {
      loading,
    } = this.props;

    return (
      <CardGlass
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <CardBase
          onPress={ !loading ? () => this.onExpand(!expanded) : null }
        >
          <Spacer row hAlign="space-between" vAlign="center">
            { this._renderMain() }

            { this._renderExpansionButton() }
          </Spacer>
        </CardBase>

        { this._renderDetails() }
      </CardGlass>
    );
  }

}
