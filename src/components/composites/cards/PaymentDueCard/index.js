/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH1,
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  CardIconed,
  CardGlass,
} from "@app-components-core/Card";
import {
  PaymentDueItemList,
} from "@app-components-lists";
import {
  NumberFormat,
  DateFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PaymentDueCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      expanded: props.expanded ? props.expanded : true,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      if (
        nextProps.expanded !== null &&
        nextProps.expanded !== undefined &&
        nextProps.expanded !== this.state.expanded
      ) {
        this.onExpand(nextProps.expanded);
      }

      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onExpand(newExpand) {
    let {
      expanded,
    } = this.state;

    if (expanded === newExpand) {
      return false;
    }

    this.setState({
      expanded: newExpand,
    });

    let {
      onExpand,
    } = this.props;

    if (onExpand) {
      onExpand(newExpand);
    }
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    return (
      <IconTrakindoSys name="copy" color={ COLOR.orange }/>
    );
  }

// ----------------------------------------

  _renderDate() {
    return (
      <Spacer space={ 2 }>
        <TextH2>
          Outstanding Invoice
        </TextH2>

        <TextBS
          mode="dark2"
        >
          <DateFormat value={ (new Date()).getTime() }/>
        </TextBS>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderAmount() {
    const {
      currency,
      overdueAmount,
      currentAmount,
      loading,
    } = this.props;

    const totalAmount = (overdueAmount ? (overdueAmount * 1) : 0) + (currentAmount ? (currentAmount * 1) : 0);

    return (
      <Spacer flex={ 1 }>
        <TextH1
          bold
          loadingWidth={ Config.base(19) }
          loading={ loading }
          numberOfLines={ 1 }
        >
          <NumberFormat value={ totalAmount } currency={ currency }/>
        </TextH1>

        <Spacer top={ .5 } flex={ 1 }>
          <TextBS
            mode="dark2"
          >
            *Only IDR invoices in App. If further information is needed, please contact your Sales.
          </TextBS>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderExpansionButton() {
    const {
      expanded,
    } = this.state;

    return (
      <IconTrakindoSys name={ expanded ? "arrow-up" : "arrow-down" } size={ 24 } color={ COLOR.black }/>
    );
  }

// ----------------------------------------

  _renderDetailItem(circleColor:string, label:string, amount:number, onPress:func) {
    const {
      currency,
      loading,
    } = this.props;

    return (
      <Padder>
        <Spacer top={ 2 } space={ 2 } row hAlign="space-between" vAlign="flex-end">
          <FieldH2
            mode="light"
            bold
            preLabel={
              <Spacer row>
                <Spacer
                  width={ Config.base(1) }
                  height={ Config.base(1) }
                  backgroundColor={ circleColor }
                  radius={ 50 }
                />
                <Spacer horizontal space={ 1 }/>
              </Spacer>
            }
            label={ label }
            value={ <NumberFormat value={ amount } currency={ currency }/> }
            loadingWidth={ Config.base(12) }
            loading={ loading }
          />

          <ButtonCompactSecondary
            onPress={ onPress }
          >
            See Invoices
          </ButtonCompactSecondary>
        </Spacer>
      </Padder>
    );
  }

// ----------------------------------------

  _renderDetails() {
    const {
      expanded,
    } = this.state;

    if (!expanded) {
      return null;
    }

    const {
      overdueAmount,
      currentAmount,
      onOverduePress,
      onCurrentPress,
      currency,
    } = this.props;

    const data = [
      {
        type: "overdue",
        currency: currency,
        amount: overdueAmount,
        onPress: onOverduePress,
      },
      {
        type: "current",
        currency: currency,
        amount: currentAmount,
        onPress: onCurrentPress,
      },
    ];

    return (
      <PaymentDueItemList
        data={ data }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      loading,
    } = this.props;

    const {
      expanded,
    } = this.state;

    return (
      <CardGlass
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <CardIconed
          icon={ this._renderIcon() }
          onPress={ !loading ? () => this.onExpand(!expanded) : null }
        >
          <Spacer row hAlign="space-between" vAlign="center">
            <Spacer>
              { this._renderDate() }

              { this._renderAmount() }
            </Spacer>

            { this._renderExpansionButton() }
          </Spacer>
        </CardIconed>

        { this._renderDetails() }
      </CardGlass>
    );
  }

}
