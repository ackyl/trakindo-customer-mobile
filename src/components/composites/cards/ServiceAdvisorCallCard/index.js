/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
   ButtonFullPrimary,
} from "@app-components-core/Button";
import {
  TextBM,
} from "@app-components-core/Text";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardGlass,
} from "@app-components-core/Card";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceAdvisorCallCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTop() {
    return (
      <Spacer space={ 1 } hAlign="center" row vAlign="center">
        <IconTrakindoSys name="time" mode={"light"}/>

        <Spacer horizontal space={ 1 }/>

        <TextBM
          mode="light"
        >
          Our advisor will call you within 2 hours
        </TextBM>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderBottom() {
    const {
      onPress,
    } = this.props;

    return (
      <Spacer top={ 1.5 }>
        <ButtonFullPrimary
          onPress={ onPress }
        >
          Direct Process
        </ButtonFullPrimary>
      </Spacer>

    );
  }

// ----------------------------------------


  _renderDivider() {
    return (
      <Spacer row hAlign="center" vAlign="center">
        <Spacer flex={ 1 } borderBottomColor={ COLOR.whiteT1 }/>

        <Spacer row>
          <Spacer horizontal space={ 1 }/>

          <TextBM mode="light">
            or
          </TextBM>

          <Spacer horizontal space={ 1 }/>
        </Spacer>

        <Spacer flex={ 1 } borderBottomColor={ COLOR.whiteT1 }/>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardGlass>
        { this._renderTop() }

        { this._renderDivider() }

        { this._renderBottom() }
      </CardGlass>
    );
  }

}
