/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ImageLocal,
} from "@app-components-core/Image";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  StatusTagPart,
} from "@app-components-core/StatusTag";
import {
  FieldH1,
  FieldH2,
} from "@app-components-core/Field";
import {
  DateFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartDetailCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderImage() {
    return (
      <ImageLocal name="icon.part" relWidth={ 97 }/>
    );
  }

// ----------------------------------------

  _renderPo() {
    const {
      poNumber,
    } = this.props;

    return (
      <Spacer space={ 2 }>
        <FieldH1
          bold
          label="PO Customer"
          value={ poNumber }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderStatus() {
    const {
      committedDate,
      latestEta,
      isDelayed,
    } = this.props;

    if (!committedDate || !latestEta) {
      return null;
    }

    return (
      <StatusTagPart top={ -1 } left={ 4 }>
        { isDelayed ? "DELAY" : "ON_TIME" }
      </StatusTagPart>
    );
  }

// ----------------------------------------

  _renderCommitedDate() {
    const {
      committedDate,
    } = this.props;

    return (
      <Spacer row vAlign="flex-end">
        <FieldH2
          label="Commited Date"
          value={ committedDate ? <DateFormat value={ committedDate }/> : "-" }
        />

        { this._renderStatus() }
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardBase>

        <Spacer row>
          <Spacer flex={ 1 } top={ .5 } space={ .5 }>
            { this._renderPo() }

            { this._renderCommitedDate() }
          </Spacer>

          { this._renderImage() }
        </Spacer>

      </CardBase>
    );
  }

}
