/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Linking,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  ImageUri,
} from "@app-components-core/Image";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardIconed,
  CardReflected,
} from "@app-components-core/Card";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class TechnicianCallCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  call(number:number) {
    let phoneNumber = `telprompt:${number}`;
    if (Device.isAndroid) {
      phoneNumber = `tel:${number}`;
    }

    Linking.openURL(phoneNumber);
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderAvatar() {
    const {
      loading,
      imageUri,
    } = this.props;

    return (
      <ImageUri
        loadingWidth={ Config.base(4) }
        loadingHeight={ Config.base(4) }
        width={ Config.base(4) }
        height={ Config.base(4) }
        loading={ loading }
        uri={ imageUri }
      />
    );
  }

// ----------------------------------------

  _renderPhoneNumber() {
    const {
      employeeNumber,
      loading,
    } = this.props;

    return (
      <TextH2
        mode="light"
        loadingWidth={ Config.base(13) }
        loading={ loading }
      >
        { employeeNumber }
      </TextH2>
    );
  }

// ----------------------------------------

  _renderName() {
    const {
      name,
      loading,
    } = this.props;

    return (
      <TextBS
        mode="light2"
        loadingWidth={ Config.base(16) }
        loading={ loading }
      >
        { name }
      </TextBS>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      loading,
      phoneNumber,
    } = this.props;

    if (!phoneNumber) {
      return null;
    }

    return (
      <ButtonCompactSecondary
        onPress={ !loading ? () => this.call(phoneNumber) : null }
      >
        Call
      </ButtonCompactSecondary>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardReflected
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <CardIconed
          icon={ this._renderAvatar() }
          color="transparent"
        >
          <Spacer row  vAlign="flex-end" hAlign="space-between">
            <Spacer flex={ 1 }>
              { this._renderName() }

              { this._renderPhoneNumber() }
            </Spacer>

            { this._renderButton() }
          </Spacer>
        </CardIconed>
      </CardReflected>
    );
  }

}
