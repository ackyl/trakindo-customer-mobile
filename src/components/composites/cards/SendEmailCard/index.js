/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Linking,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardIconed,
  CardReflected,
} from "@app-components-core/Card";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class SendEmailCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  send(email:email) {
    Linking.openURL(`mailto:${email}`);
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    return (
      <IconTrakindoSys name="inbox" size={ 15 } color={ COLOR.orange }/>
    );
  }

// ----------------------------------------

  _renderEmail() {
    const {
      email,
      loading,
    } = this.props;

    return (
      <TextH2
        mode="light"
        loadingWidth={ Config.base(15) }
        loading={ loading }
        numberOfLines={ 1 }
      >
        { email }
      </TextH2>
    );
  }

// ----------------------------------------

  _renderTitle() {
    return (
      <TextBS
        mode="light2"
      >
        Email
      </TextBS>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      email,
      loading,
    } = this.props;

    return (
      <ButtonCompactSecondary
        onPress={ !loading ? () => this.send(email) : null }
      >
        Send Email
      </ButtonCompactSecondary>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardReflected
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <CardIconed
          icon={ this._renderIcon() }
          color="transparent"
        >
          <Spacer row  vAlign="flex-end" hAlign="space-between">
            <Spacer flex={ 1 }>
              { this._renderTitle() }

              { this._renderEmail() }
            </Spacer>

            { this._renderButton() }
          </Spacer>
        </CardIconed>
      </CardReflected>
    );
  }

}
