/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ImageLocal,
} from "@app-components-core/Image";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  StatusTagPart,
} from "@app-components-core/StatusTag";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  NumberFormat,
  DateFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Part2Card extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

_renderIcon() {
  return (
    <ImageLocal name="icon.part"/>
  );
}

// ----------------------------------------

_renderPONumber() {
  const {
    poNumber,
    loading,
  } = this.props;

  return (
    <Spacer space={ 2 }>
      <FieldH2
        bold
        label="PO Number"
        value={ poNumber }
        loadingWidth={ Config.base(16) }
        loading={ loading }
      />
    </Spacer>
  );
}

// ----------------------------------------

_renderTop() {
  return (
    <Spacer borderBottomColor={ COLOR.blackT0 }>
      <Padder>
        <Spacer row>
          { this._renderIcon() }

          <Spacer horizontal space={ 2 }/>

          { this._renderPONumber() }

          { this._renderStatus() }
        </Spacer>
      </Padder>
    </Spacer>
  );
}

// ----------------------------------------

_renderTotalItem() {
  const {
    totalItems,
    loading,
  } = this.props;

  return (
    <FieldH2
      label={ "Total Items" }
      value={ <NumberFormat value={ totalItems }/> }
      loadingWidth={ Config.base(10) }
      loading={ loading }
    />
  );
}

// ----------------------------------------

_renderStatus() {
  const {
    committedDate,
    latestEta,
    isDelayed,
    loading,
  } = this.props;

  if (loading || !committedDate || !latestEta) {
    return null;
  }

  return (
    <StatusTagPart
      absolute
      top={ -Config.base(.5) }
      right={ -Config.base(.5) }
    >
      { isDelayed ? "DELAY" : "ON TIME" }
    </StatusTagPart>
  );
}

// ----------------------------------------

_renderCommitedDate() {
  const {
    committedDate,
    loading,
  } = this.props;

  return (
    <FieldH2
      label="Commited Date"
      value={ committedDate ? <DateFormat value={ committedDate }/> : "-" }
      loadingWidth={ Config.base(11) }
      loading={ loading }
    />
  );
}


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      onPress,
      loading,
    } = this.props;

    return (
      <CardBase
        onPress={ !loading ? onPress : null  }
        hPadding={ 0 }
      >
        { this._renderTop() }

        <Padder>
          <Spacer row top={ 2 }>
            { this._renderCommitedDate() }

            <Spacer horizontal space={ 8 }/>

            { this._renderTotalItem() }
          </Spacer>
        </Padder>
      </CardBase>
    );
  }

}
