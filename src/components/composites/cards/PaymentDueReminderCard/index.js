/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH1,
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  FieldH1,
  FieldH2,
} from "@app-components-core/Field";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  NumberFormat,
  DateFormat,
} from "@core-components-formatters";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PaymentDueReminderCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    return (
      <WrapperCircledIcon
        absolute
        top={ -Config.base(.5) }
        right={ -Config.base(.5) }
      >
        <IconTrakindoSys name="copy" color={ COLOR.orange }/>
      </WrapperCircledIcon>
    );
  }

// ----------------------------------------

  _renderDate() {
    const {
      loading,
    } = this.props;

    return (
      <Spacer space={ 2 }>
        <FieldH2
          bold
          label={ "Outstanding Invoice" }
          value={ <DateFormat value={ (new Date()).getTime() }/> }
          loadingWidth={ Config.base(10) }
          loading={ loading }
        />

        { this._renderIcon() }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderAmount() {
    const {
      currency,
      overdueAmount,
      currentAmount,
      loading,
    } = this.props;

    const totalAmount = (overdueAmount ? (overdueAmount * 1) : 0) + (currentAmount ? (currentAmount * 1) : 0);

    return (
      <FieldH1
        bold
        label={ "Total" }
        value={ <NumberFormat value={ totalAmount } currency={ currency }/> }
        loadingWidth={ Config.base(17) }
        loading={ loading }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      onPress,
      loading,
    } = this.props;

    return (
      <CardBase
        color={ COLOR.orange }
        shadowed
        shadowColor={ COLOR.orange }
      >
        <Spacer onPress={ !loading ? onPress : null }>
          { this._renderDate() }

          { this._renderAmount() }
        </Spacer>
      </CardBase>
    );
  }

}
