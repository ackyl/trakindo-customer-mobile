/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH1,
  TextBM,
} from "@app-components-core/Text";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  CardIconed,
  CardReflected,
} from "@app-components-core/Card";
import {
  NumberFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartItemCounterCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    return (
      <IconTrakindoSys name="parts-invoice" color={ COLOR.orange }/>
    );
  }

// ----------------------------------------

  _renderTotal() {
    const {
      loading,
      noOfOutstanding,
      noOfReady,
      noOfTaken,
    } = this.props;

    const total = (noOfOutstanding ? noOfOutstanding : 0) + (noOfReady ? noOfReady : 0) + (noOfTaken ? noOfTaken : 0);

    return (
      <Spacer row vAlign="flex-end">
        <TextH1
          bold
          loadingWidth={ Config.base(7) }
          loading={ loading }
        >
          { <NumberFormat value={ total } zeroLead={ 1 }/> }
        </TextH1>

        <Spacer horizontal space={ 1 }/>

        <TextBM>
          { "Total Item" + (total > 1 ? "s" : "") }
        </TextBM>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderCounterItem(label:string, value:number = 0) {
    const {
      loading,
    } = this.props;

    return (
      <Spacer>
        <Padder>
          <FieldH2
            bold
            mode="light"
            label={ label }
            value={ <NumberFormat value={ value } zeroLead={ 1 }/> }
            loadingWidth={ Config.base(5) }
            loading={ loading }
          />
        </Padder>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderCounters() {
    const {
      noOfOutstanding,
      noOfReady,
      noOfTaken,
    } = this.props;

    return (
      <Spacer row top={ 2 } space={ 2 }>
        { this._renderCounterItem("Outstanding", noOfOutstanding) }
        { this._renderCounterItem("Ready to Pickup", noOfReady) }
        { this._renderCounterItem("Taken", noOfTaken) }
      </Spacer>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardReflected
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <CardIconed
          icon={ this._renderIcon() }
          noTopPadding
        >
          <Spacer flex={ 1 } vAlign="center">
            { this._renderTotal() }
          </Spacer>
        </CardIconed>

        { this._renderCounters() }
      </CardReflected>
    );
  }

}
