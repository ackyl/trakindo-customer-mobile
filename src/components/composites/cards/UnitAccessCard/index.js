/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextHero,
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoFeature,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardImaged,
} from "@app-components-core/Card";
import {
  NumberFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class UnitAccessCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderImage() {
    return (
      <IconTrakindoFeature name="units" size={ 48 } color={ COLOR.black }/>
    );
  }

// ----------------------------------------

  _renderCounter() {
    const {
      noOfUnits,
      loading,
    } = this.props;

    return (
      <Spacer flex={ 1 }>
        <TextBS
          mode="dark2"
          numberOfLines={ 1 }
        >
          Registered Equipments
        </TextBS>

        <Spacer row vAlign="flex-end">
          <TextHero
            loadingWidth={ 40 }
            loading={ loading }
          >
            <NumberFormat value={ noOfUnits } zeroLead={ 1 }/>
          </TextHero>

          <Spacer horizontal space={ 1 }/>

          <Spacer>
            <TextH2
              numberOfLines={ 1 }
            >
              Unit{ noOfUnits > 1 ? "s" : "" }
            </TextH2>
          </Spacer>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      onPress,
    } = this.props;

    return (
      <ButtonCompactSecondary
        mode="light"
        onPress={ onPress }
      >
        See All
      </ButtonCompactSecondary>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardImaged
        image={ this._renderImage() }
        imageWidth={ Config.base(7) }
        imageHeight={ Config.base(7) }
      >
        <Spacer flex={ 1 } row hAlign="space-between" vAlign="flex-end">
          { this._renderCounter() }

          { this._renderButton() }
        </Spacer>
      </CardImaged>
    );

  }

}
