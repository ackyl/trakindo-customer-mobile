/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardIconed,
} from "@app-components-core/Card";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  DateFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class EstimateArrivedCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    return (
      <IconTrakindoSys name="time" color={ COLOR.orange }/>
    );
  }

// ----------------------------------------

  _renderEtaTime() {
    const {
      eta,
      loading,
    } = this.props;

    if (loading || !eta) {
      return null;
    }

    return (
      <Spacer row>
        <TextH2
          mode="dark2"
        >
          |
        </TextH2>

        <Spacer horizontal space={ .7 }/>

        <TextH2
          mode="dark2"
        >
          { eta ? <DateFormat value={ eta } pattern={ "HH:mm" }/> : "-" }
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderEta() {
    const {
      eta,
      loading,
    } = this.props;

    return (
      <Spacer row vAlign="flex-end">
        <FieldH2
          label="Estimated Arrival"
          value={ eta ? <DateFormat value={ eta } pattern={ "DD MMM YYYY" }/> : "-" }
          loadingWidth={ Config.base(18) }
          loading={ loading }
          bold
        />

        <Spacer horizontal space={ .5 }/>

        { this._renderEtaTime() }
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardIconed
        icon={ this._renderIcon() }
      >
        <Spacer row vAlign="center" hAlign="space-between">
          { this._renderEta() }
        </Spacer>
      </CardIconed>
    );
  }

}
