/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ImageLocal,
} from "@app-components-core/Image";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceRequestCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

_renderIcon() {
  const {
    type,
  } = this.props;

  return (
    <ImageLocal name={ type === "preventive" ? "icon.preventive_service" : "icon.troubleshot_service" }/>
  );
}

// ----------------------------------------

  _renderMain() {
    const {
      type,
    } = this.props;

    return (
      <Spacer flex={ 1 }>
        <TextH2
          bold
        >
          {type === "preventive" ? "Preventive Maintenance" : "Troubleshooting"}  
        </TextH2>

        <TextBS
          mode="dark2"
        >
          {type === "preventive" ? "Routine maintenance to preventing any unplanned downtime and  unanticipated equipment failure." : "Analyze asset data, run diagnostic tests on products and determine potential problems."}
        </TextBS>
      </Spacer>
    );
  }

// ----------------------------------------

_renderArrow() {
  return (
    <Spacer vAlign="center">
      <IconTrakindoSys name="arrow-right" color={ COLOR.black }/>
    </Spacer>
  );
}



// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      onPress,
    } = this.props;

    return (
      <CardBase
        onPress={ onPress }
      >

        <Spacer row>

          { this._renderIcon() }

          <Spacer horizontal space={ 2 }/>

          { this._renderMain() }

          <Spacer horizontal space={ 2 }/>

          { this._renderArrow() }
        </Spacer>

      </CardBase>
    );
  }

}
