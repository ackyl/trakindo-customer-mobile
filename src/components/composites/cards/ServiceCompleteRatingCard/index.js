/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, { Component } from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBS,
  TextBM,
  TextH2,
} from "@app-components-core/Text";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";
import {
  DateFormat,
} from "@core-components-formatters";
import {
  IconTrakindoSys,
  IconFA5,
} from "@app-components-core/Icon";
import {
  RatingStarList,
} from "@app-components-lists";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceCompleteRatingCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTickIcon() {
    const {
      loading,
    } = this.props;

    if (loading) {
      return null;
    }

    return (
      <WrapperCircledIcon
        width={ Config.base(3) }
        height={ Config.base(3) }
        color={ COLOR.green }
      >
        <IconFA5 name="check" size={ 12 } color={ COLOR.white }/>
      </WrapperCircledIcon>
    );
  }

// ----------------------------------------

  _renderDayStatus() {
    const {
      stage,
      status,
      loading,
    } = this.props;

    return (
      <Spacer>
        <TextH2
          bold
          loadingWidth={ Config.base(8) }
          loading={ loading }
        >
          { stage }
        </TextH2>

        <TextH2
          loadingWidth={ Config.base(13) }
          loading={ loading }
        >
          { status }
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderRatings() {
    const {
      rating,
      loading,
    } = this.props;

    return (
      <RatingStarList
        rating={ rating }
        loading={ loading }
      />
    );

  }
// ----------------------------------------
  _renderDate() {
    const {
      date,
      loading,
    } = this.props;

    return (
      <TextBM
        mode="dark2"
        loadingWidth={ Config.base(15) }
        loading={ loading }
      >
        { <DateFormat value={ date } pattern={ "DD MMM YYYY | HH:mm" }/> }
      </TextBM>
    );

  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer space={ 2 }>
        <CardBase>
          <Spacer row hAlign="space-between" >
            { this._renderDayStatus() }

            { this._renderTickIcon() }
          </Spacer>

          <Spacer space={ 2 } />

          <Spacer row vAlign="flex-end" hAlign="space-between">
            { this._renderRatings() }

            { this._renderDate() }
          </Spacer>

        </CardBase>
      </Spacer>
    );
  }

}
