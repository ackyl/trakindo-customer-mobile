/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBS,
} from "@app-components-core/Text";
import {
  ImageUri,
} from "@app-components-core/Image";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
  IconFA5,
} from "@app-components-core/Icon";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  CardGlass,
  CardImaged,
} from "@app-components-core/Card";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  DateFormat,
} from "@core-components-formatters";
import {
  UnitDropdownFragment,
} from "@app-components-fragments";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceRequestUnitFormCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      data: props.unit ? props.unit : {},
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onChange(data) {
    this.setState({
      data,
    });

    const {
      onChange,
    } = this.props;

    if (onChange) {
      onChange(data);
    }
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderImage() {
    const {
      imageUri,
    } = this.state.data;

    return (
      <ImageUri
        width={ Config.base(10) }
        height={ Config.base(10) }
        uri={ imageUri }
      />
    );
  }

// ----------------------------------------

  _renderForm() {
    const {
      unitError,
    } = this.props;

    return (
      <Spacer space={ 1.5 }>
        <UnitDropdownFragment
          value={ this.state.data }
          onChange={ (data) => this.onChange(data) }
          error={ unitError }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderEquipmentNumber() {
    const {
      equipmentNumber,
    } = this.state.data;

    return (
      <FieldH2
        mode="light"
        label={ "Equipment Number" }
        value={ !isNaN(equipmentNumber) ? (equipmentNumber * 1) : equipmentNumber }
      />
    );
  }

// ----------------------------------------

  _renderLastServiceDate() {
    const {
      lastServiceDate,
    } = this.state.data;

    return (
      <FieldH2
        mode="light"
        label={ "Last Service" }
        value={ lastServiceDate ? <DateFormat value={ lastServiceDate }/> : "-" }
      />
    );
  }

// ----------------------------------------

  _renderDetailButton() {
    const {
      onDetailPress,
    } = this.props;

    return (
      <ButtonCompactSecondary
        hPadding={ Config.base(1) }
        onPress={ onDetailPress ? () => onDetailPress(this.state.data) : null }
      >
        <IconTrakindoSys name="arrow-right"/>
      </ButtonCompactSecondary>
    );
  }

// ----------------------------------------

  _renderWarrantyStatus() {
    const {
      isUnderWarranty,
    } = this.state.data;

    if (!isUnderWarranty) {
      return null;
    }

    return (
      <Spacer top={ 2 } row vAlign="center">
        <WrapperCircledIcon
          width={ Config.base(2.3) }
          height={ Config.base(2.3) }
          color={ COLOR.green }
        >
          <IconFA5 name="check" size={ 9 } color={ COLOR.white }/>
        </WrapperCircledIcon>

        <Spacer horizontal space={ .5 }/>

        <TextBS
          bold
        >
          Warranty
        </TextBS>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDetail() {
    const {
      equipmentNumber,
    } = this.state.data;

    if (!equipmentNumber) {
      return null;
    }

    return (
      <Padder>
        <Spacer row hAlign="space-between" vAlign="flex-end" top={ 2 } bottom={ 2 }>
          { this._renderEquipmentNumber() }

          { this._renderLastServiceDate() }

          { this._renderDetailButton() }
        </Spacer>
      </Padder>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      equipmentNumber,
    } = this.state.data;

    return (
      <CardGlass
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <CardImaged
          image={ this._renderImage() }
          noImage={ !equipmentNumber }
        >
          { this._renderForm() }

          { this._renderWarrantyStatus() }
        </CardImaged>

        { this._renderDetail() }
      </CardGlass>
    );
  }

}
