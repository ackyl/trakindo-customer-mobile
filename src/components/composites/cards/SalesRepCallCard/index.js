/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Linking,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardIconed,
  CardReflected,
} from "@app-components-core/Card";
import {
  FieldH2,
} from "@app-components-core/Field";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class SalesRepCallCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  call(number:number) {
    let phoneNumber = `telprompt:${number}`;
    if (Device.isAndroid) {
      phoneNumber = `tel:${number}`;
    }

    Linking.openURL(phoneNumber);
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    return (
      <IconTrakindoSys name="sales" color={ COLOR.orange }/>
    );
  }

// ----------------------------------------

  _renderPhoneNumber() {
    const {
      phoneNumber,
      loading,
    } = this.props;

    return (
      <TextH2
        mode="light"
        loadingWidth={ Config.base(13) }
        loading={ loading }
      >
        { phoneNumber }
      </TextH2>
    );
  }

// ----------------------------------------

  _renderName() {
    const {
      name,
      loading,
    } = this.props;

    return (
      <FieldH2
        bold
        mode="light"
        label={ "Sales Rep" }
        value={ name }
        loadingWidth={ Config.base(16) }
        loading={ loading }
      />
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      loading,
      phoneNumber,
    } = this.props;

    return (
      <ButtonCompactSecondary
        onPress={ !loading ? () => this.call(phoneNumber) : null }
      >
        Call
      </ButtonCompactSecondary>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardReflected
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <CardIconed
          icon={ this._renderIcon() }
          color="transparent"
        >
          <Spacer row hAlign="space-between" vAlign="flex-end">
            <Spacer flex={ 1 }>
              { this._renderName() }

              { this._renderPhoneNumber() }
            </Spacer>

            { this._renderButton() }
          </Spacer>
        </CardIconed>
      </CardReflected>
    );
  }

}
