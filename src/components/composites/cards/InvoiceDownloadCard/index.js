/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";
import {
  FileService,
} from "@core-services";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardReflected,
} from "@app-components-core/Card";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as AuthSelector from "@app-selectors/auth";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class InvoiceDownloadCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  download(file, invoiceNumber) {
    FileService.download(
      "invoicePdf",
      file,
      `Trakindo Invoice - ${invoiceNumber}`,
      "pdf",
      {
        Authorization: this.props.authorization.data,
      },
      this.props.dispatch
    );
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTitle() {
    return (
      <TextH2
        mode="light"
      >
        Download Invoice PDF
      </TextH2>
    );
  }

// ----------------------------------------

  Description() {
    return (
      <TextBS
        mode="light2"
      >
        Download this invoice PDF file for more detail information
      </TextBS>
    );
  }

// ----------------------------------------

  _renderButtons() {
    const {
      file,
      invoiceNumber,
    } = this.props;

    return (
      <ButtonCompactSecondary
        onPress={ () => this.download(file, invoiceNumber) }
        hPadding={ Config.base(.5) }
      >
        <IconTrakindoSys name="download" size={ 24 }/>
      </ButtonCompactSecondary>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardReflected>
        <Spacer row vAlign="flex-end" hAlign="space-between">
          <Spacer flex={ 1 }>
            { this._renderTitle() }

            { this.Description() }
          </Spacer>

          <Spacer horizontal space={ 1 }/>

          { this._renderButtons() }
        </Spacer>
      </CardReflected>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    authorization: AuthSelector.authorization(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => dispatch(action),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(InvoiceDownloadCard);
