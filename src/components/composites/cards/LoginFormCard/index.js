/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Auth,
} from "aws-amplify";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  InputEmail,
  InputPassword,
} from "@app-components-core/Input";
import {
  ButtonFullPrimary,
} from "@app-components-core/Button";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardGlass,
} from "@app-components-core/Card";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class LoginFormCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      formData: {
        email: "",
        password: "",
      },

      error: {
        email: null,
        password: null,
      },
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onValueChange(key:string, value) {
    const {
      formData,
    } = this.state;

    formData[key] = value;

    this.setState({
      formData,
    });
  }

// ----------------------------------------

  login() {
    const {
      email,
      password,
    } = this.state.formData;

    const error = {
      email: this.getEmailError(email),
      password: this.getPasswordError(password),
    };

    if (Validation.hasError(error)) {
      this.setState({
        error,
      });

      return false;
    }

    const {
      onLoginStart,
      onLoginError,
    } = this.props;

    if (onLoginStart) {
      onLoginStart();
    }

    this.cognitoLogin(email, password, error)
      .then((response) => {
        this.setState({
          error: response.error,
        });

        if (Validation.hasError(error)) {
          if (onLoginError) {
            onLoginError(error);
          }

          return false;
        }

        const {
          onCognitoLoginSucces,
        } = this.props;

        if (onCognitoLoginSucces) {
          onCognitoLoginSucces(response.result.signInUserSession, response.result.attributes);
        }
      });
  }

// ----------------------------------------

  cognitoLogin(email, password, error) {
    return Auth.signIn(email, password)
      .then((response) => {
        return {
          result: response,
          error,
        };
      })
      .catch(err => {
        switch (err.code) {
          case "UserNotConfirmedException":
            error.email = "User registration incomplete";
            break;

          case "PasswordResetRequiredException":
            error.password = "Password expired";
            break;

          case "NotAuthorizedException":
            error.password = "Password invalid";
            break;

          case "UserNotFoundException":
            error.email = "User not found";
            break;

          default:
            error.email = "Cannot process user";
            // console.warn(err);
            break;
        }

        return {
          result: {},
          error,
        };
      });
  }

// ----------------------------------------

  getEmailError(email) {
    if (!Validation.isNotEmpty(email)) {
      return "Email cannot be empty";
    }

    if (!Validation.isEmail(email)) {
      return "Invalid email";
    }

    return null;
  }

// ----------------------------------------

  getPasswordError(password) {
    if (!Validation.isNotEmpty(password)) {
      return "Password cannot be empty";
    }

    if (!Validation.isMin(password, 6)) {
      return "Password length should be more or equal to 6";
    }

    return null;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

_renderEmailForm() {
  const {
    email,
  } = this.state.formData;

  return (
    <Spacer space={ 2.5 }>
      <InputEmail
        mode="light"
        label="Email Address"
        value={ email }
        error={ this.state.error.email }
        onChangeText={ (value) => this.onValueChange("email", value) }
      />
    </Spacer>
  );
}

// ----------------------------------------

_renderPasswordForm() {
  const {
    password,
  } = this.state.formData;

  return (
    <Spacer space={ 4 }>
      <InputPassword
        mode="light"
        label="Password"
        value={ password }
        error={ this.state.error.password }
        onChangeText={ (value) => this.onValueChange("password", value) }
      />
    </Spacer>
  );
}

// ----------------------------------------

_renderLoginButon() {
  return (
    <Spacer space={ 1.5 }>
      <ButtonFullPrimary
        onPress={ () => this.login() }
      >
        Login
      </ButtonFullPrimary>
    </Spacer>
  );
}


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardGlass
      >
        { this._renderEmailForm() }

        { this._renderPasswordForm() }

        { this._renderLoginButon() }
      </CardGlass>
    );
  }

}
