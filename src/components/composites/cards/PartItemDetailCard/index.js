/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBM,
} from "@app-components-core/Text";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  StatusTagPart,
} from "@app-components-core/StatusTag";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  NumberFormat,
  DateFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartItemDetailCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderDescription() {
    const {
      description,
    } = this.props;

    return (
      <Spacer space={ .5 }>
        <TextH2
          bold
        >
          { description }
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderEta() {
    const {
      eta,
      isDelayed,
    } = this.props;

    return (
      <Spacer row space={ 2 }>
        <TextBM
          bold
          color={ isDelayed ? COLOR.red : COLOR.green }
        >
          ETA
        </TextBM>

        <Spacer horizontal space={ .5 }/>

        <TextBM
        >
          <DateFormat value={ eta }/>
        </TextBM>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderStatus() {
    const {
      isDelayed,
    } = this.props;

    return (
      <StatusTagPart
        top={ -Config.base(.5) }
        right={ -Config.base(.5) }
      >
        { isDelayed ? "DELAY" : "ON_TIME" }
      </StatusTagPart>
    );
  }

// ----------------------------------------

  _renderDetails() {
    const {
      quantity,
      origin,
      destination,
    } = this.props;

    return (
      <Spacer row hAlign={ origin ? "space-between" : null }>
        <FieldH2
          label="Quantity"
          value={ <NumberFormat value={ quantity }/> }
        />

        <Spacer horizontal space={ 3 }/>

        {
          origin ?
            (
              <FieldH2
                label="Origin"
                value={ origin }
                numberOfLines={ 1 }
              />
            )
          :
            null
        }

        <Spacer horizontal space={ 3 }/>

        <FieldH2
          label="Destination"
          value={ destination }
          numberOfLines={ 1 }
        />
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardBase>

        <Spacer>
          <Spacer row hAlign="space-between">
            <Spacer>
              { this._renderDescription() }

              { this._renderEta() }
            </Spacer>

            { this._renderStatus() }
          </Spacer>

          <Spacer space={ 2 } borderBottomColor={ COLOR.grayT25 }/>

          { this._renderDetails() }
        </Spacer>

      </CardBase>
    );
  }

}
