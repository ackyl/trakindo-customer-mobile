/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardIconed,
} from "@app-components-core/Card";
import {
  StatusTagInvoice,
} from "@app-components-core/StatusTag";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  NumberFormat,
  DateFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class InvoiceCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    const {
      orderType,
      loading,
    } = this.props;

    if (loading) {
      return null;
    }

    let iconName = null;
    switch (orderType) {
      case "part":
        iconName = "parts-invoice";
        break;

      case "service":
        iconName = "service-invoice";
        break;
    }

    return (
      <IconTrakindoSys name={ iconName } color={ COLOR.orange }/>
    );
  }

// ----------------------------------------

  _renderNumberAndDue() {
    const {
      invoiceNumber,
      dueDate,
      loading,
    } = this.props;

    return (
      <Spacer space={ 1 }>
        <TextH2
          loadingWidth={ Config.base(13) }
          loading={ loading }
        >
          { invoiceNumber }
        </TextH2>

        <Spacer row>
          <TextBS
            mode="dark2"
          >
            Due date
          </TextBS>

          <Spacer horizontal space={ 1 }/>

          <TextBS
            mode="dark2"
            loadingWidth={ Config.base(8) }
            loading={ loading }
          >
            { dueDate ? <DateFormat value={ dueDate }/> : "-" }
          </TextBS>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderPo() {
    const {
      poNumbers,
      loading,
    } = this.props;

    let po = "-";
    if (poNumbers && poNumbers.length === 1) {
      po = poNumbers[0];
    } else if (poNumbers && poNumbers.length > 1) {
      po = "Multi";
    }

    return (
      <Spacer flex={ 1 }>
        <FieldH2
          label={ "PO Number" }
          value={ po }
          loadingWidth={ Config.base(10) }
          loading={ loading }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderAmount() {
    const {
      currency,
      amount,
      loading,
    } = this.props;

    return (
      <TextH2
        bold
        loadingWidth={ Config.base(14) }
        loading={ loading }
        numberOfLines={ 1 }
      >
        <NumberFormat value={ amount } currency={ currency }/>
      </TextH2>
    );
  }

// ----------------------------------------

  _renderStatus() {
    const {
      status,
      isOverdue,
      loading,
    } = this.props;

    if (loading) {
      return null;
    }

    let statusName = "OPEN";
    if (status === "CLOSED") {
      statusName = "CLOSED";
    } else if (isOverdue) {
      statusName = "OVERDUE";
    }

    return (
      <StatusTagInvoice
        absolute
        top={ -Config.base(.5) }
        right={ -Config.base(.5) }
      >
        { statusName }
      </StatusTagInvoice>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      onPress,
      loading,
    } = this.props;

    return (
      <CardIconed
        icon={ this._renderIcon() }
        onPress={ !loading ? onPress : null }
      >
        { this._renderNumberAndDue() }

        <Spacer vertical space={ 1 }/>

        <Spacer row vAlign="flex-end">
          { this._renderPo() }

          <Spacer horizontal space={ 1 }/>

          { this._renderAmount() }
        </Spacer>

        { this._renderStatus() }
      </CardIconed>
    );
  }

}
