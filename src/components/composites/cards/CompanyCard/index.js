/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardIconed,
} from "@app-components-core/Card";
import {
  FieldH2,
} from "@app-components-core/Field";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CompanyCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    return (
      <IconTrakindoSys name="account" color={ COLOR.orange }/>
    );
  }

// ----------------------------------------

  _renderName() {
    const {
      name,
      loading,
    } = this.props;

    return (
      <Spacer space={ 1 }>
        <TextH2
          loadingWidth={ Config.base(18) }
          loading={ loading }
          numberOfLines={ 1 }
        >
          { name }
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderId() {
    const {
      soldtoId,
      loading,
    } = this.props;

    return (
      <FieldH2
        label={ "Customer ID" }
        value={ (soldtoId * 1 + "") }
        loadingWidth={ Config.base(10) }
        loading={ loading }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      onPress,
      loading,
    } = this.props;

    return (
      <CardIconed
        icon={ this._renderIcon() }
        onPress={ !loading ? onPress : null }
      >
        { this._renderName() }

        { this._renderId() }
      </CardIconed>
    );
  }

}
