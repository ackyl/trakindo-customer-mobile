/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ImageLocal,
} from "@app-components-core/Image";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  StatusTagService,
} from "@app-components-core/StatusTag";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    const {
      type,
      loading,
    } = this.props;

    return (
      <ImageLocal
        name={ type === "preventive" ? "icon.preventive_service" : "icon.troubleshot_service"}
        relWidth={ 40 }
        loadingWidth={ Config.base(5) }
        loadingHeight={ Config.base(5) }
        loading={ loading }
      />
    );
  }

// ----------------------------------------

  _renderModel() {
    const {
      model,
      loading,
    } = this.props;

    return (
      <Spacer space={ 2 }>
        <FieldH2
          bold
          label="Equipment model"
          value={ model }
          loadingWidth={ Config.base(16) }
          loading={ loading }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderTop() {
    return (
      <Spacer borderBottomColor={ COLOR.blackT0 }>
        <Padder>
          <Spacer row>
            { this._renderIcon() }

            <Spacer horizontal space={ 2 }/>

            { this._renderModel() }

            { this._renderStatus() }
          </Spacer>
        </Padder>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderField(label:string, value, loadingWidth:number) {
    const {
      loading,
    } = this.props;

    return (
      <Spacer space={ 2 }>
        <FieldH2
          label={ label }
          value={ value ? value : "-" }
          loadingWidth={ Config.base(loadingWidth) }
          loading={ loading }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderStatus() {
    const {
      status,
      loading,
    } = this.props;

    if (loading || !status) {
      return null;
    }

    return (
      <StatusTagService
        absolute
        top={ -Config.base(.5) }
        right={ -Config.base(.5) }
      >
        { status }
      </StatusTagService>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      onPress,
      loading,
      equipmentNumber,
      serviceRequestNumber,
      serviceOrderNumber,
      serialNumber,
      status,
    } = this.props;

    return (
      <CardBase
        onPress={ !loading && status ? onPress : null  }
        hPadding={ 0 }
      >
        { this._renderTop() }

        <Padder>
          <Spacer row top={ 2 }>
            <Spacer>
              { this._renderField("Equipment number", equipmentNumber, 12) }

              { this._renderField("Service Request", serviceRequestNumber, 10) }
            </Spacer>

            <Spacer horizontal space={ 8 }/>

            <Spacer>
              { this._renderField("Serial Number", serialNumber, 14) }

              { this._renderField("Service Order", serviceOrderNumber, 10) }
            </Spacer>
          </Spacer>
        </Padder>
      </CardBase>
    );
  }

}
