/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  TouchableOpacity,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  FieldH2,
} from "@app-components-core/Field";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CompanyDetailCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      isExpanded: false,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  toggleExpansion(isExpanded:bool) {
    if (this.state.isExpanded === isExpanded) {
      return null;
    }

    this.setState({isExpanded});

    const {
      onExpandToggle,
    } =  this.props;

    if (onExpandToggle) {
      onExpandToggle(isExpanded);
    }
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderName() {
    const {
      name,
    } = this.props;

    return (
      <Spacer top={ 1 } space={ 2 }>
        <TextH2
          bold
        >
          { name }
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderField(label:string, value:string, loadingWidth:number, hasNoLoading:bool = false) {
    const {
      loading,
      isCompanyAllowed,
    } = this.props;

    if (!isCompanyAllowed) {
      return null;
    }

    return (
      <Spacer space={ 2 }>
        <FieldH2
          wrap
          label={ label }
          value={ value ? value : "-" }
          loadingWidth={ Config.base(loadingWidth) }
          loading={ loading && !hasNoLoading }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderIcon() {
    return (
      <WrapperCircledIcon
        absolute
        top={ Config.base(.5) }
        right={ Config.base(.5) }
      >
        <IconTrakindoSys name="account" color={ COLOR.orange }/>
      </WrapperCircledIcon>
    );
  }

// ----------------------------------------

  _renderHiddenSegment() {
    const {
      isExpanded,
    } = this.state;

    const {
      isCompanyAllowed,
    } = this.props;

    if (!isCompanyAllowed) {
      return null;
    }

    if (!isExpanded) {
      return null;
    }

    const {
      phoneNumber,
      address,
    } = this.props;

    return (
      <Spacer>
        { this._renderField("Contact No.", phoneNumber, 16, true) }
        { this._renderField("Address", address, 28, true) }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderTop() {
    const {
      owner,
    } = this.props;

    return (
      <Spacer top={ 1 }>
        <Padder>
          <Spacer row>
            { this._renderName() }

            <Spacer horizontal space={ 6 }/>
          </Spacer>

          { this._renderField("Contact Name", owner, 20) }

          { this._renderHiddenSegment() }
        </Padder>

        { this._renderIcon() }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderExpanderSegment() {
    const {
      isExpanded,
    } = this.state;

    const {
      isCompanyAllowed,
    } = this.props;

    if (!isCompanyAllowed) {
      return null;
    }

    return (
      <TouchableOpacity
        onPress={ () => this.toggleExpansion(!isExpanded) }
      >
        <Spacer hAlign="center">
          <IconTrakindoSys name={isExpanded ? "arrow-up" : "arrow-down"} size={ 24 } color={ COLOR.black }/>
        </Spacer>
      </TouchableOpacity>
    );
  }



// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardBase
        color={ COLOR.orange }
        shadowed
        shadowColor={ COLOR.orange }
        hPadding={ 0 }
        vPadding={ 0 }
      >
        <Spacer borderBottomColor={ COLOR.blueT1 }>
          { this._renderTop() }
        </Spacer>

        { this._renderExpanderSegment() }

      </CardBase>
    );
  }

}
