/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardIconed,
  CardReflected,
} from "@app-components-core/Card";
import {
  DateFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class InboxCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    const {
      type,
      loading,
    } = this.props;

    if (loading) {
      return null;
    }

    let icon = null;
    switch (type) {
      case "invoice":
        icon = "documen-due";
        break;

      case "part":
        icon = "parts-invoice";
        break;

      case "service":
        icon = "service-invoice";
        break;

      case "credit":
        icon = "money";
        break;
    }

    return (
      <IconTrakindoSys name={ icon } color={ COLOR.orange }/>
    );
  }

// ----------------------------------------

  _renderTitle() {
    const {
      title,
      loading,
    } = this.props;

    return (
      <Spacer flex={ 1 }>
        <TextH2
          mode="light"
          loadingWidth={ Config.base(15) }
          loading={ loading }
        >
          { title }
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderContent() {
    const {
      content,
      loading,
    } = this.props;

    return (
      <Spacer>
        <TextBS
          mode="light2"
          numberOfLines={ loading ? 3 : null }
          loadingWidth={ Config.base(25) }
          loading={ loading }
        >
          { content }
        </TextBS>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDate() {
    const {
      date,
      loading,
    } = this.props;

    return (
      <TextBS
        mode="light2"
        loadingWidth={ Config.base(9) }
        loading={ loading }
      >
        { <DateFormat value={ date } pattern={ "DD MMM YYYY, HH:mm" }/> }
      </TextBS>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardReflected
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <CardIconed
          icon={ this._renderIcon() }
          color="transparent"
          noTopPadding
        >
          <Spacer flex={ 1 } row hAlign="space-between">
            { this._renderTitle() }

            <Spacer horizontal space={ 1 }/>

            { this._renderDate() }
          </Spacer>

          { this._renderContent() }
        </CardIconed>
      </CardReflected>
    );
  }

}
