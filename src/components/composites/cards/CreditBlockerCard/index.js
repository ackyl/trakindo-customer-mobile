/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ButtonCompactPrimary,
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardReflected,
} from "@app-components-core/Card";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CreditBlockerCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    return (
      <IconTrakindoSys name="warning" color={ COLOR.orange } size={ 24 }/>
    );
  }

// ----------------------------------------

  _renderTitle() {
    const {
      type,
    } = this.props;

    let title = "You have overdue invoices";
    if (type === "OVERLIMIT") {
      title = "Your credit is over limit";
    }

    return (
      <TextH2
        mode="light"
      >
        { title }
      </TextH2>
    );
  }

// ----------------------------------------

  _renderDescription() {
    const {
      type,
    } = this.props;

    let desc = "Your account has been suspended because payment overdue, please pay this immedietly or call us for more information";
    if (type === "OVERLIMIT") {
      desc = "Your account has been suspended, call us to discuss more about your credit limit agreement";
    }

    return (
      <TextBS
        mode="light2"
      >
        { desc }
      </TextBS>
    );
  }

// ----------------------------------------

  _renderButtons() {
    const {
      type,
      onDetailPress,
      onCallPress,
    } = this.props;

    let callTitle = "Call Us";
    if (type === "OVERLIMIT") {
      callTitle = "Call Us to Extend";
    }

    const callButton = (
      <ButtonCompactPrimary
        onPress={ onCallPress }
      >
        { callTitle }
      </ButtonCompactPrimary>
    );

    if (type === "OVERDUE") {
      return (
        <Spacer row>
          { callButton }

          <Spacer horizontal space={ 1 }/>

          <ButtonCompactSecondary
            onPress={ onDetailPress }
          >
            Show Invoices
          </ButtonCompactSecondary>
        </Spacer>
      );
    }

    return callButton;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      onPress,
      loading,
    } = this.props;

    return (
      <CardReflected
        onPress={ !loading ? onPress : null }
      >
        <Spacer row space={ 2 }>
          <Spacer>
            { this._renderTitle() }

            { this._renderDescription() }
          </Spacer>

          <Spacer horizontal space={ 1 }/>

          { this._renderIcon() }
        </Spacer>

        { this._renderButtons() }
      </CardReflected>
    );
  }

}
