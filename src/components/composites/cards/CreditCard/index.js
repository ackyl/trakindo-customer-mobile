/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  StatusTagCredit,
} from "@app-components-core/StatusTag";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  FieldHero,
  FieldH2,
} from "@app-components-core/Field";
import {
  NumberFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CreditCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderBottom(label:string, value:string) {
    const {
      currency,
      loading,
    } = this.props;

    return (
      <Spacer flex={ 1 } top={ 2 } space={ 2 }>
        <Padder>
          <FieldH2
            bold
            label={ label }
            value={ <NumberFormat value={ value } currency={ currency }/> }
            loadingWidth={ Config.base(10) }
            loading={ loading }
          />
        </Padder>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderID() {
    const {
      payerId,
      loading,
    } = this.props;
    return (
      <Spacer space={ 1.5 }>
        <Spacer row>
          <TextBS
            bold
          >
            ACCOUNT :
          </TextBS>

          <Spacer horizontal space={ .5 }/>

          <TextBS
            bold
            loadingWidth={ Config.base(10) }
            loading={ loading }
          >
            { payerId }
          </TextBS>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderRemaining() {
    const {
      currency,
      limit,
      exposure,
      loading,
    } = this.props;

    const remaining = limit - exposure;

    return (
      <FieldHero
        label={ "Remaining Balance" }
        value={ <NumberFormat value={ remaining } currency={ currency }/> }
        loadingWidth={ Config.base(28) }
        loading={ loading }
        color={ remaining < 0 ? COLOR.red : null }
      />
    );
  }

// ----------------------------------------

  _renderStatus() {
    const {
      limit,
      exposure,
      hasOverdue,
      loading,
    } = this.props;

    const remaining = limit - exposure;

    if (loading || (remaining >= 0 && !hasOverdue)) {
      return null;
    }

    return (
      <StatusTagCredit
        absolute
        top={ Config.base(.5) }
        right={ Config.base(.5) }
      >
        BLOCKED
      </StatusTagCredit>
    );
  }

// ----------------------------------------

  _renderTop() {
    return (
      <Spacer top={ 1 } space={ 2 }>
        <Padder>
          { this._renderID() }

          { this._renderRemaining() }
        </Padder>

        { this._renderStatus() }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderBottoms() {
    const {
      limit,
      exposure,
    } = this.props;

    return (
      <Spacer row>
        { this._renderCreditBottom("Credit Limit", limit) }

        <Spacer style={ Styles.bottom.separator }/>

        { this._renderCreditBottom("Credit Exposure", exposure) }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderUnavailableAccount() {
    const {
      payerId,
    } = this.props;

    return (
      <Spacer row flex={ 1 }>
        <TextH2>
          Account :
        </TextH2>

        <Spacer horizontal space={ .5 }/>

        <TextH2
          bold
        >
          { payerId }
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderUnavailableIcon() {
    return (
      <WrapperCircledIcon>
        <IconTrakindoSys name="money" color={ COLOR.orange }/>
      </WrapperCircledIcon>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  _renderMain() {
    const {
      limit,
      exposure,
      onPress,
      loading,
    } = this.props;

    return (
      <Spacer onPress={ !loading && onPress ? onPress : null }>
        <Spacer borderBottomColor={ COLOR.blueT1 }>
          { this._renderTop() }
        </Spacer>

        <Spacer row>
          { this._renderBottom("Credit Limit", limit) }

          <Spacer horizontal borderRightColor={ COLOR.blueT1 }/>

          { this._renderBottom("Credit Exposure", exposure) }
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderUnavailable() {
    return (
      <Spacer
        vPadding={ 2 }
        hPadding={ 2 }
      >
        <Spacer row vAlign="center">
          { this._renderUnavailableAccount() }

          { this._renderUnavailableIcon() }
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  render() {
    const {
      isUnavailable,
    } = this.props;

    return (
      <CardBase
        color={ COLOR.orange }
        shadowed
        shadowColor={ COLOR.orange }
        hPadding={ 0 }
        vPadding={ 0 }
      >
        { !isUnavailable ? this._renderMain() : this._renderUnavailable() }
      </CardBase>
    );
  }

}
