/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  InputString,
} from "@app-components-core/Input";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  SalesRepDropdownFragment,
  BranchOfficeDropdownFragment,
} from "@app-components-fragments";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceRequestBranchFormCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLocationForm() {
    const {
      location,
      onLocationChange,
      locationError,
    } = this.props;

    return (
      <Spacer space={ 2.5 }>
        <InputString
          label="Equipment Location (optional)"
          value={ location }
          onChangeText={ onLocationChange }
          multiline
          error={ locationError }
        />
      </Spacer>
    );
  }

// ----------------------------------------

_renderBranchOfficeForm() {
  const {
    branchOffice,
    onBranchOfficeSelect,
    branchOfficeError,
  } = this.props;

  return (
    <Spacer space={ 2.5 }>
      <BranchOfficeDropdownFragment
        value={ branchOffice }
        onSelect={ onBranchOfficeSelect }
        mapper={ "name" }
        error={ branchOfficeError }
      />
    </Spacer>
  );
}

// ----------------------------------------

_renderSalesRepForm() {
  const {
    salesRep,
    onSalesRepSelect,
    salesRepError,
  } = this.props;

  return (
    <Spacer space={ 1.5 }>
      <SalesRepDropdownFragment
        value={ salesRep }
        onSelect={ onSalesRepSelect }
        mapper={ "name" }
        error={ salesRepError }
      />
    </Spacer>
  );
}


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardBase
      >
        { this._renderLocationForm() }

        { this._renderBranchOfficeForm() }

        { this._renderSalesRepForm() }
      </CardBase>
    );
  }

}
