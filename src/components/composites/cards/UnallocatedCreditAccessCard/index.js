/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardReflected,
} from "@app-components-core/Card";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  NumberFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class UnallocatedCreditAccessCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderAmount() {
    const {
      currency,
      unallocated,
      loading,
    } = this.props;

    return (
      <FieldH2
        mode="light"
        bold
        label={ "Payment Received" }
        loading={ loading }
        loadingWidth={ Config.base(13) }
        value={ <NumberFormat value={ unallocated } currency={ currency }/> }
      />
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      onPress,
    } = this.props;

    return (
      <ButtonCompactSecondary
        onPress={ onPress }
      >
        Learn More
      </ButtonCompactSecondary>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardReflected>
        <Spacer row vAlign="flex-end" hAlign="space-between">
          { this._renderAmount() }

          { this._renderButton() }
        </Spacer>
      </CardReflected>
    );
  }

}
