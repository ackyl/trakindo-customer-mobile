/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Linking,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
  IconFA5,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardIconed,
  CardReflected,
} from "@app-components-core/Card";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceDocumentAccessCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    return (
      <IconTrakindoSys name="documen" color={ COLOR.orange }/>
    );
  }

// ----------------------------------------

  _renderCheck() {
    const {
      isApproved,
      loading,
    } = this.props;

    if (loading || !isApproved) {
      return null;
    }

    return (
      <WrapperCircledIcon
        width={ Config.base(2.3) }
        height={ Config.base(2.3) }
        color={ COLOR.green }
      >
        <IconFA5 name="check" size={ 9 } color={ COLOR.white }/>
      </WrapperCircledIcon>
    );
  }

// ----------------------------------------

  _renderMain() {
    const {
      name,
      loading,
    } = this.props;

    return (
      <Spacer flex={ 1 } row vAlign="center">
        <TextH2
          mode="light"
          loadingWidth={ Config.base(18) }
          loading={ loading }
        >
          { name }
        </TextH2>

        <Spacer horizontal space={ 1 }/>

        { this._renderCheck() }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      onPress,
      isDisabled,
      loading,
    } = this.props;

    if (loading || isDisabled) {
      return null;
    }

    return (
      <ButtonCompactSecondary
        onPress={ onPress }
      >
        Open
      </ButtonCompactSecondary>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardReflected
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <CardIconed
          icon={ this._renderIcon() }
          color="transparent"
          noTopPadding
        >
          <Spacer flex={ 1 } row vAlign="center" hAlign="space-between">
            { this._renderMain() }

            <Spacer horizontal space={ 1 }/>

            { this._renderButton() }
          </Spacer>
        </CardIconed>
      </CardReflected>
    );
  }

}
