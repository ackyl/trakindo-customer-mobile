/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBS,
} from "@app-components-core/Text";
import {
  IconTrakindoFeature,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardGlass,
} from "@app-components-core/Card";
import {
  FieldH2,
} from "@app-components-core/Field";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class QuickAccessCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    const {
      icon,
      disabled,
    } = this.props;

    return (
      <Spacer flex={ 1 } vAlign="center" hAlign="center">
        <IconTrakindoFeature name={ icon } color={ disabled ? COLOR.whiteT4 : COLOR.orange }/>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderLabel() {
    const {
      label,
      disabled,
    } = this.props;

    return (
      <Spacer vAlign="center" hAlign="center">
        <TextBS
          mode={ !disabled ? "light" : "light2" }
        >
          { label }
        </TextBS>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    let {
      containerWidth,
      noPerRow,
      onPress,
      disabled,
    } = this.props;

    containerWidth = containerWidth ? containerWidth : Device.windows.width - Config.paddingHorizontal(2);
    noPerRow = noPerRow ? noPerRow : 3;

    return (
      <CardGlass
        onPress={ !disabled ? onPress : null }
        width={ (containerWidth - (Config.base(noPerRow))) / noPerRow }
        height={ (containerWidth - (Config.base(noPerRow))) / noPerRow }
        vPadding={ Config.base(1.5) }
        hPadding={ 0 }
      >
        { this._renderIcon() }

        { this._renderLabel() }
      </CardGlass>
    );
  }

}
