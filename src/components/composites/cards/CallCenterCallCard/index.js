/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Linking,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardIconed,
  CardReflected,
} from "@app-components-core/Card";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CallCenterCallCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  call(number:number) {
    let phoneNumber = `telprompt:${number}`;
    if (Device.isAndroid) {
      phoneNumber = `tel:${number}`;
    }

    Linking.openURL(phoneNumber);
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    return (
      <IconTrakindoSys name="help" color={ COLOR.orange }/>
    );
  }

// ----------------------------------------

  _renderPhoneNumber() {
    const {
      phoneNumber,
      loading,
    } = this.props;

    return (
      <TextH2
        mode="light"
        loadingWidth={ Config.base(13) }
        loading={ loading }
      >
        { phoneNumber }
      </TextH2>
    );
  }

// ----------------------------------------

  _renderName() {
    return (
      <TextBS
        mode="light2"
      >
        Call center helpline
      </TextBS>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      loading,
      phoneNumber,
    } = this.props;

    return (
      <ButtonCompactSecondary
        onPress={ !loading ? () => this.call(phoneNumber) : null }
      >
        Call
      </ButtonCompactSecondary>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardReflected
        vPadding={ 0 }
        hPadding={ 0 }
      >
        <CardIconed
          icon={ this._renderIcon() }
          color="transparent"
        >
          <Spacer row  vAlign="flex-end" hAlign="space-between">
            <Spacer flex={ 1 }>
              { this._renderName() }

              { this._renderPhoneNumber() }
            </Spacer>

            { this._renderButton() }
          </Spacer>
        </CardIconed>
      </CardReflected>
    );
  }

}
