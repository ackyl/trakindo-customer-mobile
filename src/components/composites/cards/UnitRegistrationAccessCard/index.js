/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardReflected,
} from "@app-components-core/Card";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class UnitRegistrationAccessCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  download(file) {
    alert("Downloading SoA File");
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTitle() {
    return (
      <TextH2
        mode="light"
      >
        Register Your Equipments
      </TextH2>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      onPress,
    } = this.props;

    return (
      <ButtonCompactSecondary
        onPress={ onPress }
        hPadding={ Config.base(.5) }
      >
        <IconTrakindoSys name="plus" size={ 24 }/>
      </ButtonCompactSecondary>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardReflected>
        <Spacer row vAlign="center" hAlign="space-between">
          <Spacer>
            { this._renderTitle() }
          </Spacer>

          { this._renderButton() }
        </Spacer>
      </CardReflected>
    );
  }

}
