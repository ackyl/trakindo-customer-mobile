/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  ScrollView,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  ServiceDetailUnitAccessFragement,
} from "@app-components-fragments";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  DateFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceRequestDetailCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {};

    this._detailScrollRef = null;
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      if (!this.props.compact && nextProps.compact) {
        this._detailScrollRef.scrollTo({y:0, animated: false});
      }

      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLabel() {
    const {
      compact,
    } = this.props;

    return (
      <Padder>
        <Spacer
          hAlign={ compact ? "center" : null }
          bottom={ 2 }
        >
          <TextH2>
            Request Detail
          </TextH2>
        </Spacer>
      </Padder>
    );
  }

// ----------------------------------------

  _renderUnitModel() {
    const {
      model,
      imageUri,
      onPress,
      visibility,
    } = this.props;

    return (
      <Spacer
        animated
        style={{
          opacity: visibility,
        }}
      >
        <Spacer borderBottomColor={ COLOR.grayT25 }/>

        <Spacer space={ 2.5 } top={ 1 } bottom={ 1 } borderBottomColor={ COLOR.grayT25 }>
          <ServiceDetailUnitAccessFragement
            model={ model }
            imageUri={ imageUri }
            onPress={ onPress }
          />
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderField(label:string, value, loadingWidth:number) {
    const {
      loading,
    } = this.props;

    return (
      <Spacer space={ 2.5 }>
        <FieldH2
          label={ label }
          value={ value ? value : "-" }
          loadingWidth={ Config.base(loadingWidth) }
          loading={ loading }
          wrap
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderUnitDetail() {
    const {
      smu,
      date,
      visibility,
    } = this.props;

    return (
      <Spacer
        animated
        space={ 2.5 }
        borderBottomColor={ COLOR.grayT25 }
        style={{
          opacity: visibility,
        }}
      >
        { this._renderField("Unit SMU Meter", smu, 8) }

        { this._renderField("Need by date", date ? <DateFormat value={ date } /> : "-", 10) }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderLocationDetail() {
    const {
      location,
      branchOffice,
      salesRep,
      visibility,
    } = this.props;

    return (
      <Spacer
        animated
        space={ 2.5 }
        borderBottomColor={ COLOR.grayT25 }
        style={{
          opacity: visibility,
        }}
      >
        { this._renderField("Equipment location", location, 20) }

        { this._renderField("Trakindo branch", branchOffice, 18) }

        { this._renderField("Sales Rep", salesRep, 16) }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderPICDetailandNote() {
    const {
      name,
      phoneNumber,
      note,
      visibility,
    } = this.props;

    return (
      <Spacer
        animated
        style={{
          opacity: visibility,
        }}
      >
        { this._renderField("PIC Name", name, 18) }

        { this._renderField("PIC Contact", phoneNumber, 16) }

        { this._renderField("Note", note, 20) }
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardBase
        hPadding={ 0 }
        vPadding={ 0 }
      >
        <Spacer>
          { this._renderLabel() }

          <ScrollView
            ref={ ref => {this._detailScrollRef = ref;} }
            nestedScrollEnabled = { true }
            bounces={ false }
          >
            <Padder>
              { this._renderUnitModel() }

              { this._renderUnitDetail() }

              { this._renderLocationDetail() }

              { this._renderPICDetailandNote() }
            </Padder>

            <Spacer space={ Device.isIphoneX ? 9 : 4 }/>
          </ScrollView>
        </Spacer>
      </CardBase>
    );
  }

}
