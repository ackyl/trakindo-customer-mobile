/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  InputString,
  InputNumber,
} from "@app-components-core/Input";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceRequestPICFormCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

_renderNameForm() {
  const {
    name,
    onNameChange,
    nameError,
  } = this.props;

  return (
    <Spacer space={ 2.5 }>
      <InputString
        label="PIC Name"
        value={ name }
        onChangeText={ onNameChange }
        error={ nameError }
      />
    </Spacer>
  );
}

// ----------------------------------------

_renderPhoneNumberForm() {
  const {
    phoneNumber,
    onPhoneNumberChange,
    phoneNumberError,
  } = this.props;

  return (
    <Spacer space={ 1.5 }>
      <InputNumber
        label="PIC Contact"
        value={ phoneNumber }
        onChangeText={ onPhoneNumberChange }
        error={ phoneNumberError }
      />
    </Spacer>
  );
}


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardBase
      >
        { this._renderNameForm() }

        { this._renderPhoneNumberForm() }
      </CardBase>
    );
  }

}
