/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ImageLocal,
} from "@app-components-core/Image";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardGlass,
} from "@app-components-core/Card";
import {
  FieldH2,
} from "@app-components-core/Field";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class VAInfoCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderBankIcon() {
    const {
      bank,
    } = this.props;

    return (
      <Spacer space={ 2 }>
        <ImageLocal name={ `bank.${bank}` }/>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderField(label:string, value:string, loadingWidth:number) {
    const {
      loading,
    } = this.props;

    return (
      <FieldH2
        mode="light"
        bold
        label={ label }
        value={ value }
        loadingWidth={ Config.base(loadingWidth) }
        loading={ loading }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      virtualAccount,
      currency,
      name,
    } = this.props;

    return (
      <CardGlass>
        { this._renderBankIcon() }

        <Spacer space={ 2 }>
          { this._renderField("Bank Account", virtualAccount, 23) }
        </Spacer>

        <Spacer space={ 2 }>
          { this._renderField("Currency", currency, 7) }
        </Spacer>

        { this._renderField("Account Name", name, 18) }
      </CardGlass>
    );
  }

}
