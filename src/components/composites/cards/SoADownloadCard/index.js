/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardReflected,
} from "@app-components-core/Card";
import {
  FieldBS,
} from "@app-components-core/Field";
import {
  DateFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class SoADownloadCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTitle() {
    return (
      <Spacer space={ 1.5 }>
        <TextH2
          mode="light"
        >
          Statement of Account
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderCutoffDate() {
    const today = moment(new Date());
    const lastMonth = today.clone().subtract(1, "month");
    let cutOffDate = (today.format("DD") * 1) <= 15 ? lastMonth.endOf("month") : moment(today.format("YYYY-MM-") + "15");
    cutOffDate = cutOffDate.format("x") * 1;

    return (
      <FieldBS
        mode="light"
        label={ "Payment cut off date" }
        value={ <DateFormat value={ cutOffDate }/> }
      />
    );
  }

// ----------------------------------------

  _renderButtons() {
    const {
      onPress,
    } = this.props;

    return (
      <ButtonCompactSecondary
        onPress={ onPress }
        hPadding={ Config.base(.5) }
      >
        <IconTrakindoSys name="download" size={ 24 }/>
      </ButtonCompactSecondary>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardReflected>
        <Spacer row vAlign="flex-end" hAlign="space-between">
          <Spacer>
            { this._renderTitle() }

            { this._renderCutoffDate() }
          </Spacer>

          { this._renderButtons() }
        </Spacer>
      </CardReflected>
    );
  }

}
