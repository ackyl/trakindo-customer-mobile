/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ImageUri,
} from "@app-components-core/Image";
import {
  IconTrakindoSys,
  IconFA5,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardImaged,
} from "@app-components-core/Card";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  DateFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class UnitCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderImage() {
    const {
      loading,
      imageUri,
    } = this.props;

    return (
      <ImageUri
        loadingWidth={ Config.base(10) }
        loadingHeight={ Config.base(10) }
        width={ Config.base(10) }
        height={ Config.base(10) }
        loading={ loading }
        uri={ imageUri }
      />
    );
  }

// ----------------------------------------

  _renderTitle() {
    const {
      model,
      loading,
    } = this.props;

    return (
      <Spacer space={ 2 }>
        <TextH2
          bold
          loadingWidth={ Config.base(13) }
          loading={ loading }
        >
          { model }
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderEquipmentNumber() {
    const {
      equipmentNumber,
      loading,
    } = this.props;

    return (
      <FieldH2
        label={ "Equipment Number" }
        value={ !isNaN(equipmentNumber) ? (equipmentNumber * 1) : equipmentNumber }
        loadingWidth={ Config.base(8) }
        loading={ loading }
      />
    );
  }

// ----------------------------------------

  _renderLastServiceDate() {
    const {
      lastServiceDate,
      loading,
    } = this.props;

    return (
      <FieldH2
        label={ "Last Service" }
        value={ lastServiceDate ? <DateFormat value={ lastServiceDate }/> : "-" }
        loadingWidth={ Config.base(8) }
        loading={ loading }
      />
    );
  }

// ----------------------------------------

  _renderWarrantyStatus() {
    const {
      isUnderWarranty,
      loading,
    } = this.props;

    if (loading || !isUnderWarranty) {
      return null;
    }

    return (
      <Spacer top={ 2 } row vAlign="center">
        <WrapperCircledIcon
          width={ Config.base(2.3) }
          height={ Config.base(2.3) }
          color={ COLOR.green }
        >
          <IconFA5 name="check" size={ 9 } color={ COLOR.white }/>
        </WrapperCircledIcon>

        <Spacer horizontal space={ .5 }/>

        <TextBS
          bold
        >
          Warranty
        </TextBS>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      onPress,
      loading,
    } = this.props;

    return (
      <CardImaged
        image={ this._renderImage() }
        onPress={ !loading ? onPress : null }
      >
        { this._renderTitle() }

        <Spacer row hAlign="space-between" vAlign="flex-end">
          { this._renderEquipmentNumber() }

          { this._renderLastServiceDate() }
        </Spacer>

        { this._renderWarrantyStatus() }
      </CardImaged>
    );
  }

}
