/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  FieldH1,
  FieldH2,
} from "@app-components-core/Field";
import {
  ImageUri,
  ImageLocal,
} from "@app-components-core/Image";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  ServiceDetailUnitAccessFragement,
} from "@app-components-fragments";
import {
  Padder,
  Spacer,
} from "@core-components-enhancers";






/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceDetailCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    const {
      compact,
    } = this.props;

    return (
      <ImageLocal
        animated
        name="icon.preventive_service"
        relWidth={ Config.base(compact ? 7 : 10) }
      />
    );
  }

// ----------------------------------------

  _renderField(label:string, value, loadingWidth:number) {
    const {
      loading,
    } = this.props;

    return (
      <Spacer space={ 2 }>
        <FieldH2
          label={ label }
          value={ value ? value : "-" }
          numberOfLines={ 1 }
          loadingWidth={ Config.base(loadingWidth) }
          loading={ loading }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderEquipmentModel(loadingWidth:number) {
    const {
      model,
      loading,
    } = this.props;

    return (
      <Spacer space={ 2 }>
        <FieldH1
          bold
          label="Equipment model"
          value={ model }
          loadingWidth={ Config.base(loadingWidth) }
          loading={ loading }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDetails() {
    const {
      equipmentNumber,
      serviceRequestNumber,
      serviceOrderNumber,
      serialNumber,
      visibility,
    } = this.props;

    return (
      <Spacer
        animated
        row
        style={{
          opacity: visibility,
        }}
      >
        <Spacer>
          { this._renderField("Equipment number", equipmentNumber, 10) }

          { this._renderField("Service Request", serviceRequestNumber, 8) }
        </Spacer>

        <Spacer horizontal space={ 2 }/>

        <Spacer>
          { this._renderField("Serial Number", serialNumber, 11) }

          { this._renderField("Service Order", serviceOrderNumber, 9) }
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderMain() {
    return (
      <Spacer>
        { this._renderEquipmentModel(20) }

        { this._renderDetails() }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderImage() {
    const {
      loading,
      imageUri,
    } = this.props;

    return (
      <ImageUri
        loadingWidth={ Config.base(10) }
        loadingHeight={ Config.base(10) }
        width={ Config.base(10) }
        height={ Config.base(10) }
        loading={ loading }
        uri={ imageUri }
      />
    );
  }

// ----------------------------------------

  _renderUnit() {
    const {
      model,
      imageUri,
      loading,
      onPress,
      visibility,
    } = this.props;

    return (
      <Spacer
        animated
        style={{
          opacity: visibility,
        }}
      >
        <Padder>
          <ServiceDetailUnitAccessFragement
            model={ model }
            imageUri={ imageUri }
            loading={ loading }
            onPress={ onPress }
          />
        </Padder>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardBase
        hPadding={ 0 }
        vPadding={ 0 }
      >
        <Spacer space={ Device.isIphoneX ? 5 : 1 }>
          <Spacer space={ 1 } borderBottomColor={ COLOR.blackT0 }>
            <Padder>
              <Spacer row>

                <Spacer flex={ 1 } top={ .5 }>
                  { this._renderMain() }
                </Spacer>

                <Spacer horizontal space={ 2 }/>

                { this._renderIcon() }

              </Spacer>
            </Padder>
          </Spacer>

          { this._renderUnit() }
        </Spacer>

      </CardBase>
    );
  }

}
