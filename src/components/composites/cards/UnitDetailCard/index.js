/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ImageUri,
} from "@app-components-core/Image";
import {
  IconTrakindoSys,
  IconFA5,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  DateFormat,
} from "@core-components-formatters";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class UnitDetailCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderImage() {
    const {
      imageUri,
    } = this.props;

    return (
      <Spacer space={ 2.5 } borderBottomColor={ COLOR.blackT0 }>
        <Spacer space={ 1 } hAlign="center">
          <ImageUri
            width={ 263 }
            height={ 210 }
            uri={ imageUri }
          />
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderModel() {
    const {
      model,
    } = this.props;

    return (
      <TextH2
        bold
      >
        { model }
      </TextH2>
    );
  }

// ----------------------------------------

  _renderField(label:string, value, hideOnEmpty:bool = false, postLabel = null) {
    if (hideOnEmpty && !value) {
      return null;
    }

    return (
      <Spacer space={ 2 }>
        <FieldH2
          label={ label }
          postLabel={ postLabel }
          value={ value ? value : "-" }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDate(label:string, value:number, hideOnEmpty:bool = false) {
    if (hideOnEmpty && !value) {
      return null;
    }

    return this._renderField(label, value ? <DateFormat value={ value }/> : "-", hideOnEmpty);
  }

// ----------------------------------------

  _renderLastServiceType() {
    const {
      lastServiceDate,
      lastServiceType,
    } = this.props;

    if (!lastServiceDate || !lastServiceType) {
      return null;
    }

    return (
      <FieldH2
        label={ "Last Service Description" }
        numberOfLines={ 3 }
        value={ lastServiceDate ? lastServiceType : "-" }
        wrap
      />
    );
  }

// ----------------------------------------

  _renderLocation() {
    const {
      location,
    } = this.props;

    return (
      <Spacer space={ 2 }>
        <FieldH2
          label={ "Location (updated)" }
          numberOfLines={ 3 }
          value={ location ? location : "-" }
          wrap
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderStatus() {
    const {
      isUnderWarranty,
    } = this.props;

    if (!isUnderWarranty) {
      return null;
    }

    return (
      <Spacer top={ 1 } row vAlign="center">
        <WrapperCircledIcon
          width={ Config.base(2.3) }
          height={ Config.base(2.3) }
          color={ COLOR.green }
        >
          <IconFA5 name="check" size={ 9 } color={ COLOR.white }/>
        </WrapperCircledIcon>

        <Spacer horizontal space={ .5 }/>

        <TextBS
          bold
        >
          Warranty
        </TextBS>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderSMUHintButton() {
    const {
      onSMUHintPressed,
    } = this.props;

    return (
      <Spacer row width={ Config.base(6) } onPress={ onSMUHintPressed }>
        <Spacer horizontal space={ .5 }/>

        <Spacer
          width={ Config.base(2) }
          height={ Config.base(2) }
          backgroundColor={ COLOR.black }
          radius={ 50 }
          vAlign="center"
          hAlign="center"
        >
          <TextBS
            mode="light"
          >
            ?
          </TextBS>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      equipmentNumber,
      serialNumber,
      smu,
      warrantyExpirationDate,
      lastServiceDate,
    } = this.props;

    return (
      <CardBase>
        { this._renderImage() }

        { this._renderModel() }

        { this._renderStatus() }

        <Spacer row top={ 2 }>
          <Spacer>
            { this._renderField("Equipment Number", !isNaN(equipmentNumber) ? (equipmentNumber * 1) : equipmentNumber) }

            { this._renderField("Unit SMU Meter", smu, false, this._renderSMUHintButton()) }
          </Spacer>

          <Spacer horizontal space={ 4 }/>

          <Spacer>
            { this._renderField("Serial Number", serialNumber) }

            { this._renderDate("Warranty Expire Date", warrantyExpirationDate) }
          </Spacer>
        </Spacer>

        { this._renderLocation() }

        { this._renderDate("Last Service", lastServiceDate, true) }

        { this._renderLastServiceType() }
      </CardBase>
    );
  }

}
