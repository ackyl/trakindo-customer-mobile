/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  FieldH1,
} from "@app-components-core/Field";
import {
  NumberFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CreditExpandedCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderBottom(label:string, value:string, isLast:bool = false) {
    const {
      currency,
    } = this.props;

    return (
      <Spacer top={ 2 } bottom={ 2 } borderBottomColor={ !isLast ? COLOR.blackT0 : null }>
        <Padder>
          <FieldH1
            bold
            label={ label }
            value={ <NumberFormat value={ value } currency={ currency }/> }
          />
        </Padder>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      limit,
      exposure,
    } = this.props;

    const remaining = limit - exposure;

    return (
      <CardBase
        color={ COLOR.orange }
        shadowed
        shadowColor={ COLOR.orange }
        hPadding={ 0 }
        vPadding={ 0 }
      >
        { this._renderBottom("Remaining Balance", remaining) }

        { this._renderBottom("Credit Limit", limit) }

        { this._renderBottom("Credit Exposure", exposure, true) }
      </CardBase>
    );
  }

}
