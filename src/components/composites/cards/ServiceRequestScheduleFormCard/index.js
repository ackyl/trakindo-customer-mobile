/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  InputNumber,
} from "@app-components-core/Input";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  CalendarDropdownFragment,
} from "@app-components-fragments";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceRequestScheduleFormCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

_renderSMUForm() {
  const {
    smu,
    onSMUChange,
    smuError,
  } = this.props;

  return (
    <Spacer space={ 2.5 }>
      <InputNumber
        label="Unit SMU Meter (optional)"
        value={ smu }
        onChangeText={ onSMUChange }
        error={ smuError }
      />
    </Spacer>
  );
}

// ----------------------------------------

_renderDateForm() {
  const {
    date,
    onDateSelect,
    dateError,
  } = this.props;

  return (
    <Spacer space={ 1.5 }>
      <CalendarDropdownFragment
        value={ date }
        onSelect={ onDateSelect }
        error={ dateError }
      />
    </Spacer>
  );
}


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <CardBase
      >
        { this._renderSMUForm() }

        { this._renderDateForm() }
      </CardBase>
    );
  }

}
