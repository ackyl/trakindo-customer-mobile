/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  StatusTagInvoice,
} from "@app-components-core/StatusTag";
import {
  FieldH1,
  FieldH2,
} from "@app-components-core/Field";
import {
  NumberFormat,
  DateFormat,
} from "@core-components-formatters";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class InvoiceDetailCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    const {
      orderType,
    } = this.props;

    let iconName = null;
    switch (orderType) {
      case "part":
        iconName = "parts-invoice";
        break;

      case "service":
        iconName = "service-invoice";
        break;
    }

    return (
      <Spacer space={ 1.5 }>
        <WrapperCircledIcon>
          <IconTrakindoSys name={ iconName } color={ COLOR.orange }/>
        </WrapperCircledIcon>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderNumber() {
    const {
      invoiceNumber,
    } = this.props;

    return (
      <Spacer space={ 2.5 }>
        <FieldH1
          bold
          label={ "Invoice Number" }
          value={ invoiceNumber }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDate(label:string, value:number) {
    return (
      <FieldH2
        label={ label }
        value={ value ? <DateFormat value={ value }/> : "-" }
      />
    );
  }

// ----------------------------------------

  _renderPoItem(po:number, index:number) {
    return (
      <Spacer row key={ index }>
        <Spacer space={ 1 } style={ Styles.poNumber.container }>
          <TextH2>
            { po }
          </TextH2>
        </Spacer>

        <Spacer horizontal space={ 1 }/>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderPos() {
    const {
      poNumbers,
    } = this.props;

    return (
      <Spacer space={ 1.5 }>
        <TextBS
          mode="dark2"
        >
          PO Number{ poNumbers.length > 1 ? "s" : "" }
        </TextBS>

        <Spacer row wrap>
          {
            poNumbers.map((po, index) => {
              return this._renderPoItem(po, index);
            })
          }
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderAmount() {
    const {
      currency,
      amount,
    } = this.props;

    return (
      <FieldH2
        label={ "Amount" }
        value={ <NumberFormat value={ amount } currency={ currency }/> }
      />
    );
  }

// ----------------------------------------

  _renderStatus() {
    const {
      status,
      isOverdue,
    } = this.props;

    let statusName = "OPEN";
    if (status === "CLOSED") {
      statusName = "CLOSED";
    } else if (isOverdue) {
      statusName = "OVERDUE";
    }

    return (
      <StatusTagInvoice
        absolute
        top={ Config.base(.5) }
        right={ Config.base(.5) }
      >
        { statusName }
      </StatusTagInvoice>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      dueDate,
      receiveDate,
    } = this.props;

    return (
      <CardBase>
        { this._renderIcon() }

        { this._renderNumber() }

        <Spacer row space={ 2.5 }>
          { this._renderDate("Due Date", dueDate) }

          <Spacer horizontal space={ 4 }/>

          { this._renderDate("Receive Date", receiveDate) }
        </Spacer>

        { this._renderPos() }

        { this._renderAmount() }

        { this._renderStatus() }
      </CardBase>
    );
  }

}
