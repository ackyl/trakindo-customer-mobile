/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH1,
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  FieldH1,
  FieldH2,
} from "@app-components-core/Field";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CardBase,
} from "@app-components-core/Card";
import {
  NumberFormat,
  DateFormat,
} from "@core-components-formatters";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PaymentDueReminderCard extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderDate() {
    const {
      loading,
    } = this.props;

    return (
      <Spacer space={ 2 }>
        <FieldH2
          bold
          label={ "Service" }
          value={ <DateFormat value={ (new Date()).getTime() } patern={ "MMM YYYY" }/> }
          loadingWidth={ Config.base(10) }
          loading={ loading }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderNumber() {
    const {
      numberOfUnits,
      loading,
    } = this.props;

    return (
      <FieldH1
        bold
        label={ "Total Unit" }
        value={ <NumberFormat value={ numberOfUnits } zeroLead={ 1 }/> }
        loadingWidth={ Config.base(8) }
        loading={ loading }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      onPress,
      loading,
    } = this.props;

    return (
      <CardBase
        onPress={ !loading ? onPress : null }
      >
        { this._renderDate() }

        { this._renderNumber() }
      </CardBase>
    );
  }

}
