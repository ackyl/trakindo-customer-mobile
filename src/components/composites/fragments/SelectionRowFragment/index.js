/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class SelectionRowFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderName() {
    const {
      name,
      loading,
      loadingWidth,
    } = this.props;

    return (
      <Spacer flex={ 1 }>
        <TextH2
          mode="light"
          loading={ loading }
          loadingWidth={ loadingWidth }
        >
          { name }
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderRadioButton() {
    const {
      isActive,
      loading,
    } = this.props;

    if (loading) {
      return null;
    }

    return (
      <Spacer
        width={ Config.base(2.5) }
        height={ Config.base(2.5) }
        radius={ 50 }
        vAlign="center"
        hAlign="center"
        borderColor={ COLOR.whiteT4 }
      >
        {
          isActive ?
            (
              <Spacer
                width={ Config.base(1) }
                height={ Config.base(1) }
                radius={ 50 }
                backgroundColor={ COLOR.whiteT4 }
              />
            )
          :
            null
        }
      </Spacer>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      onPress,
    } = this.props;

    return (
      <Spacer onPress={ onPress }>
        <Padder>
          <Spacer top={ 2 } space={ 2 } row hAlign="space-between" vAlign="flex-end">
            { this._renderName() }

            { this._renderRadioButton() }
          </Spacer>
        </Padder>
      </Spacer>
    );
  }

}
