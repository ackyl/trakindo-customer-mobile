/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  NumberFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PaymentDueItemFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  _renderMain() {
    const {
      currency,
      loading,
      type,
      amount,
    } = this.props;

    return (
      <FieldH2
        mode="light"
        bold
        preLabel={
          <Spacer row>
            <Spacer
              width={ Config.base(1) }
              height={ Config.base(1) }
              backgroundColor={ type === "overdue" ? COLOR.red : COLOR.green }
              radius={ 50 }
            />
            <Spacer horizontal space={ 1 }/>
          </Spacer>
        }
        label={ type === "overdue" ? "Overdue" : "Current" }
        value={ <NumberFormat value={ amount } currency={ currency }/> }
        loadingWidth={ Config.base(12) }
        loading={ loading }
      />
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      onPress,
    } = this.props;

    return (
      <ButtonCompactSecondary
        onPress={ onPress }
      >
        See Invoices
      </ButtonCompactSecondary>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Padder>
        <Spacer top={ 2 } space={ 2 } row hAlign="space-between" vAlign="flex-end">
          { this._renderMain() }

          { this._renderButton() }
        </Spacer>
      </Padder>
    );
  }

}
