/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  Spacer,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CalendarDateFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  isSelected() {
    const {
      selectedDate,
      date,
      month,
    } = this.props;

    if (!selectedDate) {
      return false;
    }

    return date === selectedDate;
  }

// ----------------------------------------

  isInCurrentMonth() {
    const {
      isExcluded,
    } = this.props;

    return !isExcluded;
  }


// ----------------------------------------

  isToday() {
    const {
      todayDate,
      date,
    } = this.props;

    if (!todayDate) {
      return false;
    }

    return date === todayDate;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderDate() {
    const {
      date,
    } = this.props;

    return (
      <TextH2
        mode={
          this.isInCurrentMonth() ?
            (
              this.isSelected() ?
                "dark"
              :
                "light"
            )
          :
            "light2"
        }
      >
        { date }
      </TextH2>
    );
  }

// ----------------------------------------

  _renderTodayIndicator() {
    const {
      date,
    } = this.props;

    if (!this.isToday()) {
      return null;
    }

    return (
      <Spacer
        width={ Config.base(1) }
        height={ Config.base(1) }
        backgroundColor={ COLOR.red }
        radius={ 50 }
        style={{
          position: "absolute",
          top: Config.base(.5),
          right: Config.base(.5),
        }}
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    let {
      width,
    } = this.props;

    if (!width) {
      width = Device.windows.width;
    }

    const dimension = (width - (Config.base(.5) / 6)) / 7;

    const {
      date,
      onDatePress,
    } = this.props;

    return (
      <Spacer
        onPress={ onDatePress && this.isInCurrentMonth() ? () => onDatePress(date) : null }
        width={ dimension }
        height={ dimension }
        backgroundColor={ this.isInCurrentMonth() && this.isSelected() ? COLOR.orange : null }
        radius={ 50 }
        vAlign="center"
        hAlign="center"
      >
        { this._renderDate() }

        { this._renderTodayIndicator() }
      </Spacer>
    );
  }

}
