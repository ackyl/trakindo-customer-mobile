/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  InputString,
} from "@app-components-core/Input";
import {
  Spacer,
} from "@core-components-enhancers";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  UnitSelection as UnitSelectionModal,
} from "@app-modals/Unit";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class UnitDropdownFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      isModalActive: false,

      data: props.value ? this.props.value : {},
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  openModal() {
    let {
      isModalActive,
    } = this.state;

    if (isModalActive) {
      return false;
    }

    isModalActive = true;

    this.setState({
      isModalActive,
    });
  }

// ----------------------------------------

  closeModal() {
    let {
      isModalActive,
    } = this.state;

    if (!isModalActive) {
      return false;
    }

    isModalActive = false;

    this.setState({
      isModalActive,
    });
  }

// ----------------------------------------

  select(data) {
    const {
      onChange,
    } = this.props;

    this.setState({
      data,
    });

    if (onChange) {
      onChange(data);
    }

    this.closeModal();
  }

// ----------------------------------------

  openAddNew() {
    Navigation.push("UnitNotFound");

    this.closeModal();
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      error,
    } = this.props;

    return (
      <Spacer>
        <InputString
          label="Choose Equipment"
          value={ this.state.data.model }
          rightIcon="caret-down"
          onPress={ () => this.openModal() }
          error={ error }
        />

        <UnitSelectionModal
          isActive={ this.state.isModalActive }
          onClosePress={ () => this.closeModal() }
          navData={{
            onAddNewPress: () => this.openAddNew(),
            onSelect: (item) => this.select(item),
          }}
        />
      </Spacer>
    );
  }

}
