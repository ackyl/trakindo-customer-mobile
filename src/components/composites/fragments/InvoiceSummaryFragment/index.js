/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  NumberFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class InvoiceSummaryFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderName() {
    const {
      name,
    } = this.props;

    return (
      <Spacer flex={ 1 }>
        <TextH2
          mode="light"
        >
          { name }
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderLabel() {
    const {
      type,
    } = this.props;

    const title = type === "overdue" ? "Total Overdue" : "Total Current";
    const circleColor = type === "overdue" ? COLOR.red : COLOR.green;

    return (
      <Spacer row vAlign="center">
        <Spacer
          width={ 8 }
          height={ 8 }
          radius={ 50 }
          backgroundColor={ circleColor }
        />

        <Spacer horizontal space={ 1 }/>

        <TextBS
          mode="light2"
        >
          { title }
        </TextBS>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderAmount() {
    const {
      currency,
      amount,
    } = this.props;

    return (
      <TextH2
        bold
        mode="light"
      >
        <NumberFormat value={ amount } currency={ currency }/>
      </TextH2>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      type,
    } = this.props;

    if (type !== "overdue" && type !== "active") {
      return null;
    }

    return (
      <Spacer height={ Device.isIphoneX ? Config.base(6) : Config.base(4) } backgroundColor={ COLOR.gray }>
        <Spacer
          backgroundColor={ COLOR.gray }
          width={ Device.windows.width }
          height={ Config.base(2) }
          radius={ Config.base(1) }
          style={ Styles.rounder }
        />

        <Padder>
          <Spacer row hAlign="space-between">
            { this._renderLabel() }

            { this._renderAmount() }
          </Spacer>
        </Padder>
      </Spacer>
    );
  }

}
