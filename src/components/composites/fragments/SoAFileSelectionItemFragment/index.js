/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";
import {
  FileService,
} from "@core-services";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as AuthSelector from "@app-selectors/auth";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class SoAFileSelectionItemFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  download(name, payerId, period) {
    FileService.download(
      "soaPdf",
      `${period}/${payerId}/${name}`,
      name,
      "pdf",
      {
        Authorization: this.props.authorization.data,
      },
      this.props.dispatch
    );

    const {
      onPress,
    } = this.props;

    if (onPress) {
      onPress();
    }
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderName() {
    const {
      name,
      loading,
    } = this.props;

    return (
      <Spacer flex={ 1 }>
        <TextH2
          mode="light"
          loadingWidth={ Config.base(23) }
          loading={ loading }
        >
          { name }
        </TextH2>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      name,
      payerId,
      period,
    } = this.props;

    return (
      <Spacer onPress={ () => this.download(name, payerId, period) }>
        <Padder>
          <Spacer top={ 2 } space={ 2 } row hAlign="space-between" vAlign="flex-end">
            { this._renderName() }
          </Spacer>
        </Padder>
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    authorization: AuthSelector.authorization(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => dispatch(action),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(SoAFileSelectionItemFragment);
