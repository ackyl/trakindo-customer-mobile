/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  FieldH2,
} from "@app-components-core/Field";
import {
  ImageUri,
} from "@app-components-core/Image";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceDetailUnitAccessFragement extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------
  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderModel() {
    const {
      model,
      loading,
    } = this.props;

    return (
      <Spacer flex={ 1 } vAlign="center">
        <FieldH2
          label={ "Equipment" }
          value={ model }
          numberOfLines={ 1 }
          loadingWidth={ Config.base(16) }
          loading={ loading }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderImage() {
    const {
      loading,
      imageUri,
    } = this.props;

    return (
      <ImageUri
        loadingWidth={ Config.base(10) }
        loadingHeight={ Config.base(10) }
        width={ Config.base(10) }
        height={ Config.base(10) }
        loading={ loading }
        uri={ imageUri }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      loading,
      onPress,
    } = this.props;

    return (
      <Spacer
        row
        vAlign="center"
        onPress={ !loading && onPress ? onPress : null }
      >
        { this._renderImage() }

        <Spacer horizontal space={ 2 }/>

        { this._renderModel() }

        <IconTrakindoSys name="arrow-right" color={ COLOR.black }/>
      </Spacer>
    );
  }

}
