/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Hub,
  Auth,
} from "aws-amplify";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";
import {
  AWS_COGNITO_USER_POOL_WEB_CLIENT_ID,
} from "react-native-dotenv";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullSecondary,
} from "@app-components-core/Button";
import {
  Spacer,
} from "@core-components-enhancers";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  FederatedWebview as FederatedWebviewModal,
} from "@app-modals/Auth";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class FederatedLoginFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      isWebviewOpen: false,
    };
  }

// ----------------------------------------

  componentDidMount() {
    Hub.listen("auth", ({ payload: { event, data } }) => {
      switch (event) {
        case "signIn":
          this.handleSuccess();
          break;

        case "signIn_failure":
          this.handleError();
          break;
      }
    });
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  handleSuccess() {
    const {
      onLoginStart,
      onLoginError,
      onCognitoLoginSucces,
    } = this.props;

    this.setState({isWebviewOpen: false});

    setTimeout(() => {
      onLoginStart();
    }, 100);

    if (onLoginError) {
      Auth.currentAuthenticatedUser()
        .then((response) => {
          onCognitoLoginSucces(response.signInUserSession, response.attributes);
        })
        .catch(err => {
          onLoginError(err);
        });
    }
  }

// ----------------------------------------

  handleError() {
    const {
      onLoginError,
    } = this.props;

    if (onLoginError) {
      onLoginError();
    }
  }

// ----------------------------------------

  login() {
    this.setState({
      isWebviewOpen: true,
    });

    // const {
    //   onLoginStart,
    // } = this.props;

    // if (onLoginStart) {
    //   onLoginStart();
    // }

    // this.cognitoLogin();
  }

// ----------------------------------------

  cognitoLogin() {
    return Auth.federatedSignIn({
      provider: "TMT-Providers",
    });
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

_renderLoginButon() {
  return (
    <Spacer space={ 1.5 }>
      <ButtonFullSecondary
        onPress={ () => this.login() }
      >
        Login with Trakindo ID
      </ButtonFullSecondary>
    </Spacer>
  );
}


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer overflowable>
        { this._renderLoginButon() }

        <FederatedWebviewModal
          isActive={ this.state.isWebviewOpen }
          onClosePress={ () => this.setState({isWebviewOpen: false}) }
          navData={{
            uri: `https://tmt-trakindo.auth.ap-southeast-1.amazoncognito.com/login?redirect_uri=trakindomobileApp%3A%2F%2FCompanyList&response_type=code&client_id=${AWS_COGNITO_USER_POOL_WEB_CLIENT_ID}`
          }}
        />
      </Spacer>
    );
  }

}
