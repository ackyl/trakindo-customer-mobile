/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBS,
} from "@app-components-core/Text";
import {
  Spacer,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  DateFormat,
} from "@core-components-formatters";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceProgressTrackingItemFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  getStatusText() {
    const {
      status,
    } = this.props;

    switch (status) {
      case "QUOTED":
        return "Confirm Order";

      case "SCHEDULED":
        return "Scheduling";

      case "DISPATCHED":
        return "On the Way";

      case "IN_PROGRESS":
        return "In Progress";

      case "COMPLETED":
        return "Complete";
    }
  }

// ----------------------------------------

  getStatusIcon() {
    const {
      status,
    } = this.props;

    switch (status) {
      case "QUOTED":
        return "parts-invoice";

      case "SCHEDULED":
        return "delivery";

      case "DISPATCHED":
      case "IN_PROGRESS":
      case "COMPLETED":
        return "location";
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLine(isNext:bool = false) {
    const {
      isLast,
    } = this.props;

    if (isLast) {
      return null;
    }

    return (
      <Spacer borderBottomColor={ COLOR.whiteT4 } width={ Config.base(6) }/>
    );
  }

// ----------------------------------------

  _renderIcon() {
    const {
      isCurrent,
    } = this.props;

    return (
      <IconTrakindoSys
        name={ this.getStatusIcon() }
        size={ 24 }
        color={ isCurrent ? COLOR.black : null }
      />
    );
  }

// ----------------------------------------

  _renderTracking() {
    const {
      isCurrent,
    } = this.props;

    return (
      <Spacer row vAlign="center">
        <WrapperCircledIcon
          width={ Config.base(6) }
          height={ Config.base(6) }
          color={ !isCurrent ? COLOR.whiteT1 : COLOR.orange }
          borderColor={ !isCurrent ? COLOR.whiteT4 : COLOR.orange }
        >
          { this._renderIcon() }
        </WrapperCircledIcon>

        { this._renderLine(true) }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDetail() {
    const {
      updatedAt,
      isCurrent,
    } = this.props;

    return (
      <Spacer top={ 1 }>
        <TextBS
          mode="light"
          bold={ isCurrent }
          numberOfLines={ 1 }
          color={ isCurrent ? COLOR.orange : null }
        >
          { this.getStatusText() }
        </TextBS>

        <Spacer>
          <TextBS
            mode="light2"
          >
            {/* updatedAt ? <DateFormat value={ updatedAt } pattern="DD MMM YYYY"/> : "" */}
          </TextBS>
        </Spacer>
      </Spacer>
    );
  }



// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      order,
    } = this.props;

    return (
      <Spacer height={ Config.base(12) }>
        { this._renderTracking() }

        <Spacer flex={ 1 }>
          <HorizontalAnimator order={ order }>
            { this._renderDetail() }
          </HorizontalAnimator>
        </Spacer>
      </Spacer>
    );
  }

}
