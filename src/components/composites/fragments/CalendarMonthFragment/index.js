/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CalendarWeekFragment,
} from "@app-components-fragments";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CalendarMonthFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  getBaseDate() {
    let {
      year,
      month,
    } = this.props;

    if (!year) {
      year = moment().format("YYYY") * 1;
    }

    if (!month) {
      month = moment().format("M") * 1;
    }

    return moment(`${month}-${year}`, "M-YYYY");
  }

// ----------------------------------------

  onDatePress(date) {
    let {
      year,
      month,
      onDatePress,
    } = this.props;

    if (!onDatePress) {
      return false;
    }

    if (!year) {
      year = moment().format("YYYY") * 1;
    }

    if (!month) {
      month = moment().format("M") * 1;
    }

    onDatePress(moment(`${date}-${month}-${year}`, "D-M-YYYY").format("x") * 1);
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderWeekItem(date, current, isCurrentMonth, isSelectedMonth, maxLastMonth, isZeroFirst, index) {
    const {
      min,
    } = this.props;

    let minSelectable = true;
    if (min) {
      minSelectable = moment(date).week() >= moment(min).week();
    }

    return (
      <Spacer space={ 2 } key={ index }>
        <CalendarWeekFragment
          { ... this.props }
          date={ date }
          current={ current }
          week={ index + (isZeroFirst ? 2 : 1) }
          maxLastMonth={ maxLastMonth }
          isZeroFirst={ isZeroFirst }
          isCurrentMonth={ isCurrentMonth }
          isSelectedMonth={ isSelectedMonth }
          onDatePress={ (dateItem) => this.onDatePress(dateItem) }
          isExcluded={ !minSelectable }
          min={ minSelectable ? min : null }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderWeeks() {
    const {
      selected,
    } = this.props;

    const date = this.getBaseDate();
    const isZeroFirst = date.day() === 0;

    const startWeek = date.clone().startOf("month").subtract(isZeroFirst ? 2 : 1, "week");
    let startWeekNo = startWeek.week() - 1;
    let endWeekNo = date.clone().endOf("month").week() - 1;
    if (!endWeekNo) {
      endWeekNo = date.clone().endOf("month").subtract(1, "week").week();
    }

    if (endWeekNo < startWeekNo) {
      startWeekNo = 1;
      endWeekNo += 2;
    }

    const current = date.clone().format("x") * 1;

    const isCurrentMonth = this.getBaseDate().isSame(moment(), "month");
    const isSelectedMonth = this.getBaseDate().isSame(moment(selected), "month");

    const maxLastMonth = date.clone().subtract(1, "month").endOf("month").format("D");

    const dates = [];
    let y = 0;
    for (let x = startWeekNo; x < endWeekNo; x++) {
      const weekDateStart = startWeek.add(1, "week");
      if (y > 1 && weekDateStart.startOf("week").add(1, "day").format("D") * 1 <= 7) {
        continue;
      }

      dates.push(this._renderWeekItem(weekDateStart.format("x") * 1, current, isCurrentMonth, isSelectedMonth, maxLastMonth, isZeroFirst, y));
      y++;
    }

    return dates;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        { this._renderWeeks() }
      </Spacer>
    );
  }

}
