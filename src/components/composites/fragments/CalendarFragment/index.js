/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBM,
} from "@app-components-core/Text";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CalendarMonthFragment,
} from "@app-components-fragments";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CalendarFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    const today = moment();

    this.state = {
      month: props.month ? props.month : today.format("M") * 1,
      year: props.year ? props.year : today.format("YYYY") * 1,
      selected: props.selected ? props.selected : 0,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  isLeftable() {
    const {
      min,
    } = this.props;

    if (!min) {
      return true;
    }

    let {
      year,
      month,
    } = this.state;

    if (!year) {
      year = moment().format("YYYY") * 1;
    }

    if (!month) {
      month = moment().format("MM") * 1;
    }

    const minDate = moment(min);
    const minYear = minDate.format("YYYY") * 1;
    if (year < minYear) {
      return false;
    } else if (year === minYear && month <= (minDate.format("MM") * 1)) {
      return false;
    }

    return true;
  }

// ----------------------------------------

  onSelectionChange(selected) {
    this.setState({
      selected,
    });

    const {
      onSelectionChange,
    } = this.props;

    if (onSelectionChange) {
      onSelectionChange(selected);
    }
  }

// ----------------------------------------

  moveMonthUp() {
    let {
      month,
      year,
    } = this.state;

    if (month === 12) {
      month = 1;
      year++;
    } else {
      month++;
    }

    this.setState({
      month,
      year,
    });
  }

// ----------------------------------------

  moveMonthDown() {
    let {
      month,
      year,
    } = this.state;

    if (month === 1) {
      month = 12;
      year--;
    } else {
      month--;
    }

    this.setState({
      month,
      year,
    });
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderMonthControl() {
    let {
      year,
      month,
    } = this.state;

    if (!year) {
      year = moment().format("YYYY") * 1;
    }

    if (!month) {
      month = moment().format("MM") * 1;
    }

    const leftAble = this.isLeftable();

    return (
      <Spacer row space={ 5 } vAlign="center">
        <Spacer
          width={ 30 }
          height={ 30 }
          hAlign="center"
          vAlign="center"
          onPress={ leftAble ? () => this.moveMonthDown() : null }
        >
          <IconTrakindoSys name="arrow-left" color={ !leftAble ? COLOR.whiteT4 : null }/>
        </Spacer>


        <Spacer width={ Config.base(20) } hAlign="center">
          <TextH2
            mode="light"
          >
            { moment(`${month}-${year}`, "M-YYYY").format("MMMM YYYY") }
          </TextH2>
        </Spacer>


        <Spacer
          width={ 30 }
          height={ 30 }
          hAlign="center"
          vAlign="center"
          onPress={ () => this.moveMonthUp() }
        >
          <IconTrakindoSys name="arrow-right"/>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderWeekDayItem(day:string) {
    let {
      width,
    } = this.props;

    if (!width) {
      width = Device.windows.width;
    }

    const dimension = (width - (Config.base(.5) / 6)) / 7;

    return (
      <Spacer
        width={ dimension }
        hAlign="center"
      >
        <TextBM
          mode="light2"
        >
          { day }
        </TextBM>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderWeekDays() {
    let {
      width,
    } = this.props;

    if (!width) {
      width = Device.windows.width;
    }

    return (
      <Spacer
        space={ 2 }
        width={ width }
        row
        hAlign="space-between"
        borderBottomColor={ COLOR.whiteT4 }
        top={ 1.5 }
        bottom={ 1.5 }
      >
        { this._renderWeekDayItem("Mon") }
        { this._renderWeekDayItem("Tue") }
        { this._renderWeekDayItem("Wed") }
        { this._renderWeekDayItem("Thu") }
        { this._renderWeekDayItem("Fri") }
        { this._renderWeekDayItem("Sat") }
        { this._renderWeekDayItem("Sun") }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderMonth() {
    const {
      min,
    } = this.props;
    const leftAble = this.isLeftable();

    return (
      <CalendarMonthFragment
        { ... this.state }
        onDatePress={ (date) => this.onSelectionChange(date) }
        min={ !leftAble ? min : null }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer hAlign="center">
        { this._renderMonthControl() }

        { this._renderWeekDays() }

        { this._renderMonth() }
      </Spacer>
    );
  }

}
