/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  NumberFormat,
  DateFormat,
} from "@core-components-formatters";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartItemGroupItemFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  getEtaStatus() {
    let {
      pickupStatus,
    } = this.props;

    switch (pickupStatus) {
      case "outstanding":
        return "ETA";

      case "ready":
        return "Arrived";

      case "taken":
        return "Taken";
    }
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderEta() {
    const {
      eta,
      isDelayed,
      pickupStatus,
    } = this.props;

    return (
      <Spacer row>
        <TextH2
          bold={ pickupStatus === "outstanding" }
          mode="light"
          color={ pickupStatus === "outstanding" ? (isDelayed ? COLOR.red : COLOR.green) : null }
        >
          { this.getEtaStatus() }
        </TextH2>

        <Spacer horizontal space={ .5 }/>

        <TextH2
          mode="light"
        >
          { eta ? <DateFormat value={ eta }/> : "" }
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderQuantity() {
    const {
      quantity,
    } = this.props;

    return (
      <Spacer row>
        <TextBS
          mode="light2"
        >
          Quantity:
        </TextBS>

        <Spacer horizontal space={ .4 }/>

        <TextBS
          mode="light2"
        >
          { <NumberFormat value={ quantity }/> }
        </TextBS>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      onPress,
      pickupStatus,
      caseNumber,
      eta,
      isTrackable,
    } = this.props;

    if (pickupStatus !== "outstanding" || !caseNumber || !eta || !isTrackable) {
      return null;
    }

    return (
      <ButtonCompactSecondary
        onPress={ onPress }
      >
        Track
      </ButtonCompactSecondary>
    );
  }

// ----------------------------------------

  _renderOrderNumber() {
    const {
      pickupStatus,
      caseNumber,
    } = this.props;

    if (pickupStatus !== "ready" || !caseNumber) {
      return null;
    }

    return (
      <Spacer vAlign="space-between">
        <TextBS
          mode="light2"
        >
          Order Number
        </TextBS>

        <TextBS
          mode="light2"
        >
          { caseNumber }
        </TextBS>
      </Spacer>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Padder>
        <Spacer top={ 2 } space={ 2 } row hAlign="space-between" vAlign="flex-end">
          <Spacer flex={ 1 }>
            { this._renderEta() }

            { this._renderQuantity() }
          </Spacer>

          { this._renderButton() }

          {/* this._renderOrderNumber() */}
        </Spacer>
      </Padder>
    );
  }

}
