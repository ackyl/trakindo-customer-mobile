import FederatedLoginFragment from "./FederatedLoginFragment";

import InvoiceSummaryFragment from "./InvoiceSummaryFragment";
import PaymentDueItemFragment from "./PaymentDueItemFragment";
import SoAFileSelectionItemFragment from "./SoAFileSelectionItemFragment";

import PartItemGroupItemFragment from "./PartItemGroupItemFragment";
import PartShippingTrackingItemFragment from "./PartShippingTrackingItemFragment";

import ServiceProgressTrackingItemFragment from "./ServiceProgressTrackingItemFragment";
import ServiceDetailUnitAccessFragement from "./ServiceDetailUnitAccessFragement";

import SegmentTitleFragment from "./SegmentTitleFragment";
import ResourceVersionInfoFragment from "./ResourceVersionInfoFragment";
import SearchBarFragment from "./SearchBarFragment";
import FilterBarFragment from "./FilterBarFragment";
import TabHeadFragment from "./TabHeadFragment";
import SelectionRowFragment from "./SelectionRowFragment";
import ProceesInformationHeadFragment from "./ProceesInformationHeadFragment";
import TechnicianCallFragment from "./TechnicianCallFragment";
import RatingStarFragment from "./RatingStarFragment";
import AddUnitAccessFragment from "./AddUnitAccessFragment";

import UnitDropdownFragment from "./UnitDropdownFragment";
import CalendarDropdownFragment from "./CalendarDropdownFragment";
import BranchOfficeDropdownFragment from "./BranchOfficeDropdownFragment";
import SalesRepDropdownFragment from "./SalesRepDropdownFragment";

import PDFViewerFragment from "./PDFViewerFragment";
import PDFScrollInstructionFragment from "./PDFScrollInstructionFragment";
import OrderConfirmationFormFragment from "./OrderConfirmationFormFragment";
import QuotationApprovalActionFragment from "./QuotationApprovalActionFragment";
import TnCApprovalActionFragment from "./TnCApprovalActionFragment";

import CalendarDateFragment from "./CalendarDateFragment";
import CalendarWeekFragment from "./CalendarWeekFragment";
import CalendarMonthFragment from "./CalendarMonthFragment";
import CalendarFragment from "./CalendarFragment";

import LoadSpinnerFragment from "./LoadSpinnerFragment";

export {
  FederatedLoginFragment,

  InvoiceSummaryFragment,
  PaymentDueItemFragment,
  SoAFileSelectionItemFragment,

  PartItemGroupItemFragment,
  PartShippingTrackingItemFragment,

  ServiceProgressTrackingItemFragment,
  ServiceDetailUnitAccessFragement,

  SegmentTitleFragment,
  ResourceVersionInfoFragment,
  SearchBarFragment,
  FilterBarFragment,
  TabHeadFragment,
  SelectionRowFragment,
  ProceesInformationHeadFragment,
  TechnicianCallFragment,
  RatingStarFragment,
  AddUnitAccessFragment,

  UnitDropdownFragment,
  CalendarDropdownFragment,
  BranchOfficeDropdownFragment,
  SalesRepDropdownFragment,

  PDFViewerFragment,
  PDFScrollInstructionFragment,
  OrderConfirmationFormFragment,
  QuotationApprovalActionFragment,
  TnCApprovalActionFragment,

  CalendarDateFragment,
  CalendarWeekFragment,
  CalendarMonthFragment,
  CalendarFragment,

  LoadSpinnerFragment,
};
