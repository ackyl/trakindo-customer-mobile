/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  InputString,
} from "@app-components-core/Input";
import {
  Spacer,
} from "@core-components-enhancers";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  CalendarSelection as CalendarSelectionModal,
} from "@app-modals/Service";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CalendarDropdownFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      isModalActive: false,

      value: "",
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  openModal() {
    let {
      isModalActive,
    } = this.state;

    if (isModalActive) {
      return false;
    }

    isModalActive = true;

    this.setState({
      isModalActive,
    });
  }

// ----------------------------------------

  closeModal() {
    let {
      isModalActive,
    } = this.state;

    if (!isModalActive) {
      return false;
    }

    isModalActive = false;

    this.setState({
      isModalActive,
    });
  }

// ----------------------------------------

  select(date) {
    const {
      onSelect,
    } = this.props;

    this.setState({
      value: date,
    });

    if (onSelect) {
      onSelect(date);
    }

    this.closeModal();
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    let value = this.state.value;
    if (value) {
      value = moment(value).format("DD MMM YYYY");
    }

    const {
      error,
    } = this.props;

    return (
      <Spacer>
        <InputString
          label="Need by Date"
          value={ value }
          rightIcon="calendar"
          onPress={ () => this.openModal() }
          error={ error }
        />

        <CalendarSelectionModal
          isActive={ this.state.isModalActive }
          onClosePress={ () => this.closeModal() }
          navData={{
            selected: this.state.value,
            onSelect: (date) => this.select(date),
          }}
        />
      </Spacer>
    );
  }

}
