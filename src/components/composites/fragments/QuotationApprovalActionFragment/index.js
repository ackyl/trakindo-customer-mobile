/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullPrimary,
  ButtonFullSecondary,
} from "@app-components-core/Button";
import {
  Padder,
  Spacer,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class QuotationConfirmationFormFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderMainButton() {
    const {
      onSubmit,
      isApprovable,
    } = this.props;

    if (!isApprovable) {
      return null;
    }

    return (
      <Spacer space={ 2 }>
        <ButtonFullPrimary
          onPress={ onSubmit }
        >
          Approve
        </ButtonFullPrimary>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderCallButton() {
    const {
      onCallPress,
    } = this.props;

    return (
      <ButtonFullSecondary
        onPress={ onCallPress }
      >
        Call for Clarification
      </ButtonFullSecondary>
    );
  }


// ----------------------------------------
// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer top={ 1.5 } bottom={ Device.isIphoneX ? 5 : 2 } backgroundColor={ COLOR.gray }>
        <Spacer
          backgroundColor={ COLOR.gray }
          width={ Device.windows.width }
          height={ Config.base(2) }
          radius={ 50 }
          style={{
            position: "absolute",
            top: -Config.base(1),
          }}
        />

        <Padder>
          { this._renderMainButton() }

          { this._renderCallButton() }
        </Padder>
      </Spacer>
    );
  }

}
