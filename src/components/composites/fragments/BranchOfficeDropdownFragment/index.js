/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  InputString,
} from "@app-components-core/Input";
import {
  Spacer,
} from "@core-components-enhancers";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  BranchOfficeSelection as BranchOfficeSelectionModal,
} from "@app-modals/Company";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class BranchOfficeDropdownFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      isModalActive: false,

      activeIndex: -1,

      value: "",
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  openModal() {
    let {
      isModalActive,
    } = this.state;

    if (isModalActive) {
      return false;
    }

    isModalActive = true;

    this.setState({
      isModalActive,
    });
  }

// ----------------------------------------

  closeModal() {
    let {
      isModalActive,
    } = this.state;

    if (!isModalActive) {
      return false;
    }

    isModalActive = false;

    this.setState({
      isModalActive,
    });
  }

// ----------------------------------------

  select(item, index) {
    const {
      onSelect,
      mapper,
    } = this.props;

    this.setState({
      value: item[mapper ? mapper : "name"],
      activeIndex: index,
    });

    if (onSelect) {
      onSelect(item, index);
    }

    this.closeModal();
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      error,
    } = this.props;

    return (
      <Spacer>
        <InputString
          label="Trakindo Nearest Branch to Equipment"
          value={ this.state.value }
          rightIcon="caret-down"
          onPress={ () => this.openModal() }
          error={ error }
        />

        <BranchOfficeSelectionModal
          isActive={ this.state.isModalActive }
          onClosePress={ () => this.closeModal() }
          navData={{
            activeIndex: this.state.activeIndex,
            onSelect: (item, index) => this.select(item, index),
          }}
        />
      </Spacer>
    );
  }

}
