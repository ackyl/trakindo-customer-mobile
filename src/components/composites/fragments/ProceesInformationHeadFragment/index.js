/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Animated,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  IconTrakindoFeature,
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  TextBM,
  TextHero,
} from "@app-components-core/Text";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ProceesInformationHeadFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  getNewHeadPosition(newIndex) {
    let {
      tabs,
    } = this.props;

    tabs = tabs ? tabs : [];

    return ((Device.windows.width - Config.paddingHorizontal(2)) / tabs.length) * newIndex;
  }

// ----------------------------------------

  switchTab(newIndex) {
    const {
      activeIndex,
    } = this.state;

    if (activeIndex !== newIndex) {
      this.setState({
        activeIndex: newIndex,
      });

      Animated.timing(this.state.headPosition, {
        toValue: this.getNewHeadPosition(newIndex),
        duration: 100,
        delay: 0,
      }).start();

      const {
        onTabSwitched,
      } = this.props;

      if (onTabSwitched) {
        onTabSwitched(newIndex);
      }
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderIcon() {
    const {
      icon,
      iconFetured,
      iconColor,
    } = this.props;

    if (iconFetured) {
      return (
        <IconTrakindoFeature size={ Config.base(10) } name={ icon } color={ iconColor ? iconColor :  COLOR.orange }/>
      );
    }

    return (
      <IconTrakindoSys size={ Config.base(10) } name={ icon } color={ iconColor ? iconColor :  COLOR.orange }/>
    );
  }

// ----------------------------------------

  _renderTitle() {
    const {
      title,
    } = this.props;

    return (
      <Spacer space={ 1 }>
        <TextHero
        mode="light"
        align="center"
        >
          { title }
        </TextHero>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDescription() {
    const {
      description,
    } = this.props;

    return (
      <TextBM
        mode="light"
        align="center"
      >
        { description }
      </TextBM>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer hAlign="center">
        <Spacer space={ 2.5 }>
          { this._renderIcon() }
        </Spacer>

        { this._renderTitle() }

        { this._renderDescription() }
      </Spacer>
    );
  }

}
