/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import {
  COLOR,
} from "./consts";


export default {
  container: {
    position: "absolute",
    height: Config.base(4),
    width: Device.windows.width - Config.paddingHorizontal(2),
  },

  block: {
    height: Config.base(4),
    backgroundColor: COLOR.whiteT1,
    borderRadius: Config.base(1),
  },
};
