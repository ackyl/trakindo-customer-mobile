/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBM,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class TabHeadFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    const tabIndex = props.activeIndex ? props.activeIndex : 0;

    this.state = {
      activeIndex: tabIndex ? tabIndex : 0,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      // if (nextProps.activeIndex !== nextState.activeIndex) {
      //   this.switchTab(nextProps.activeIndex);
      // }

      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  getNewHeadPosition(newIndex) {
    let {
      tabs,
    } = this.props;

    tabs = tabs ? tabs : [];

    return ((Device.windows.width - Config.paddingHorizontal(2)) / tabs.length) * newIndex;
  }

// ----------------------------------------

  switchTab(newIndex, byTap:bool = false) {
    const {
      activeIndex,
    } = this.props;

    if (activeIndex !== newIndex) {
      // this.setState({
      //   activeIndex: newIndex,
      // });

      const {
        onTabSwitched,
      } = this.props;

      if (onTabSwitched) {
        onTabSwitched(newIndex, byTap);
      }
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTabHeadItem(label, index) {
    const activeIndex = this.props.activeIndex;

    const isActive = activeIndex === index;

    return (
      <Spacer
        flex={ 1 }
        hAlign="center"
        onPress={ () => this.switchTab(index, true) }
        backgroundColor={ isActive ? COLOR.whiteT1 : null }
        radius={ Config.base(1) }
        top={ 1 }
        bottom={ 1 }
        key={ index }
      >
        <Padder pad={ .5 }>
          <TextBM
            mode={ isActive ? "light" : "light2" }
            numberOfLines={ 1 }
          >
            { label }
          </TextBM>
        </Padder>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderTabs() {
    let {
      tabs,
    } = this.props;

    tabs = tabs ? tabs : [];

    return tabs.map(tab => {
      const {
        label,
        index,
      } = tab;

      return this._renderTabHeadItem(label, index);
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer
        row
        space={ 1 }
        backgroundColor={ COLOR.whiteT1 }
        radius={ Config.base(1) }
        vAlign={ "center" }
        hAlign={ "center" }
      >
        { this._renderTabs() }
      </Spacer>
    );
  }

}
