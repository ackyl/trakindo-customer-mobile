/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  InputString,
} from "@app-components-core/Input";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class FilterBarFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      value: props.value ? props.value : "",
      timedAction: null,
    };

    this._mainRef = null;
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      if (
        nextProps.value !== undefined &&
        nextProps.value !== null &&
        nextProps.value !== this.state.value
      ) {
        this.updateValue(nextProps.value);
      }

      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  updateValue(value:string) {
    if (this.state.timedAction) {
      clearTimeout(this.state.timedAction);
    }

    const {
      onSearchTriggered,
      bounceTimeout,
      minCharTrigger,
    } = this.props;

    this.setState({
      value,
      timedAction: (
        value !== undefined &&
        value !== null &&
        (value.trim().length === 0 ||
        value.trim().length > (minCharTrigger ? minCharTrigger : 2))
      ) ? setTimeout(
        () => {
          if (onSearchTriggered) {
            onSearchTriggered(value);
          }
        },
        (bounceTimeout ? bounceTimeout : 1000)
      ) : null,
    });
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderMain() {
    const {
      placeholder,
    } = this.props;

    return (
      <Spacer row vAlign="center">
        <IconTrakindoSys name="search" size={ 16 } color={ COLOR.whiteT4 }/>

        <Spacer horizontal space={ 1 }/>

        <Spacer flex={ 1 }>
          <InputString
            mode="light"
            cref={ (ref) => {this._mainRef = ref;} }
            noBorder
            placeholder={ placeholder ? placeholder : "Search" }
            onChangeText={ (value) => this.updateValue(value) }
            value={ this.state.value }
          />
        </Spacer>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer
        onPress={ () => this._mainRef.focus() }
        top={ 1 }
        space={ 1 }
        bottom={ 1 }
        backgroundColor={ COLOR.whiteT1 }
        radius={ Config.base(1) }
      >
        <Padder>
          { this._renderMain() }
        </Padder>
      </Spacer>
    );
  }

}
