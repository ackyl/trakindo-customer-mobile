/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBM,
  TextBS,
} from "@app-components-core/Text";
import {
  Spacer,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  DateFormat,
} from "@core-components-formatters";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  WrapperCircledIcon,
} from "@app-components-core/Wrapper";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartShippingTrackingItemFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  getStatusText() {
    const {
      statusCode,
    } = this.props;

    switch (statusCode) {
      case "PUP":
      case "OHP":
        return "In Process for Shipment";

      case "TRI":
        return "In Transit";

      case "SOP":
        return "Left Origin Facility";

      case "POD":
        return "Delivered";

      case "DLY":
        return "Delayed";
    }
  }

// ----------------------------------------

  getStatusIcon() {
    const {
      statusCode,
    } = this.props;

    switch (statusCode) {
      case "PUP":
      case "OHP":
        return "branch";

      case "TRI":
        return "delivery";

      case "SOP":
        return "branch";

      case "POD":
        return "prepare-item";

      case "DLY":
        return "delay";
    }
  }

// ----------------------------------------

  getDelayReasonText() {
    const {
      delayCode,
    } = this.props;

    switch (delayCode) {
      case 1:
        return "Waiting Next Flight";

      case 2:
        return "Delivery Process";

      case 3:
        return "Force Majeure";

      case 4:
        return "Customs Processing";

      case 5:
        return "Delivery Process";

      case 6:
        return "Delivery Process";

      case 7:
        return "Delivery Process";

      case 8:
        return "Customs Processing";

      case 9:
        return "Route Adjustment";

      case 10:
        return "Waiting Next Shipment";

      case 11:
        return "Administrative Processing";

      case 12:
        return "Delivery Process";

      case 13:
        return "Waiting Next Flight";
    }
  }

// ----------------------------------------

  isDelay() {
    const {
      statusCode,
    } = this.props;

    return statusCode === "DLY";
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLine(isNext:bool = false) {
     const {
       isFirst,
       isLast,
     } = this.props;

    if (isFirst && !isNext) {
      return null;
    }

    if (isLast && isNext) {
      return null;
    }

    const isDelay = this.isDelay();

    return (
      <Spacer borderRightColor={ COLOR.whiteT4 } height={ isDelay && isNext ? Config.base(5) : Config.base(3) }/>
    );
  }

// ----------------------------------------

  _renderIcon() {
    const {
      isLast,
      loading,
    } = this.props;

    if (loading) {
      return null;
    }

    const isDelay = this.isDelay();

    return (
      <IconTrakindoSys
        name={ this.getStatusIcon() }
        size={ 24 }
        color={ isLast && !isDelay ? COLOR.black : null }
      />
    );
  }

// ----------------------------------------

  _renderTracking() {
    const {
      isLast,
      loading,
    } = this.props;

    const isDelay = this.isDelay();

    return (
      <Spacer width={ Config.base(6) } hAlign="center">
        { this._renderLine() }

        <WrapperCircledIcon
          width={ Config.base(6) }
          height={ Config.base(6) }
          color={ (isDelay && !loading) ? COLOR.red : (!isLast || loading ? COLOR.whiteT1 : COLOR.orange) }
          borderColor={ (isDelay && !loading) ? COLOR.red : (!isLast || loading ? COLOR.whiteT4 : COLOR.orange) }
        >
          { this._renderIcon() }
        </WrapperCircledIcon>

        { this._renderLine(true) }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDetail() {
    const {
      isFirst,
      isLast,
      location,
      lastUpdatedAt,
      loading,
    } = this.props;

    return (
      <Spacer top={ !isFirst ? 3.5 : .5 } flex={ 1 }>
        <TextBM
          mode="light"
          bold
          loadingWidth={ Config.base(10) }
          loading={ loading }
          color={ isLast ? COLOR.orange : null }
          numberOfLines={ 1 }
        >
          { location }
        </TextBM>

        {/*<Spacer horizontal space={ .5 }/>

          <TextBM
            mode="light"
            loadingWidth={ 0 }
            loading={ loading }
          >
            -
          </TextBM>

        <Spacer horizontal space={ .5 }/>*/}

        <Spacer>
          <TextBM
            mode="light"
            loadingWidth={ Config.base(15) }
            loading={ loading }
          >
            { <DateFormat value={ lastUpdatedAt } pattern="DD MMM YYYY | HH:mm "/> }
          </TextBM>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDelayReason() {
    const {
      loading,
    } = this.props;

    const isDelay = this.isDelay();

    if (!isDelay || loading) {
      return null;
    }

    return (
      <TextBS
        mode="light2"
        numberOfLines={ 1 }
      >
        { this.getDelayReasonText() }
      </TextBS>
    );
  }

// ----------------------------------------

  _renderStatus() {
    const {
      loading,
    } = this.props;

    const isDelay = this.isDelay();

    return (
      <Spacer>
        <TextBS
          mode={ !isDelay ? "light2" : "light" }
          loadingWidth={ Config.base(15) }
          loading={ loading }
        >
          { this.getStatusText() }
        </TextBS>

        { this._renderDelayReason(isDelay) }
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      order,
    } = this.props;

    return (
      <Spacer row>
        { this._renderTracking() }

        <Spacer horizontal space={ 3 }/>

        <Spacer flex={ 1 }>
          <HorizontalAnimator order={ order }>
            { this._renderDetail() }

            { this._renderStatus() }
          </HorizontalAnimator>
        </Spacer>
      </Spacer>
    );
  }

}
