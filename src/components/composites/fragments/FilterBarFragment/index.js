/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  FamilyFilter as FamilyFilterModal,
} from "@app-modals/Unit";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class FilterBarFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      isModalActive: false,

      activeIndex: 0,
      selectedLabel: "All Equipment",
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  openModal() {
    this.setState({
      isModalActive: true,
    });
  }

// ----------------------------------------

  closeModal() {
    this.setState({
      isModalActive: false,
    });
  }

// ----------------------------------------

  onSelect(item, index) {
    if (this.state.activeIndex === index) {
      return false;
    }

    this.setState({
      activeIndex: index,
      selectedLabel: item.name,
    });

    const {
      onChange,
    } = this.props;

    if (onChange) {
      onChange(item, index);
    }

    this.closeModal();
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderMain() {
    const {
      selectedLabel,
    } = this.state;

    return (
      <Spacer row vAlign="center">
        <TextH2
          flex={ 1 }
          mode="light"
        >
          { selectedLabel }
        </TextH2>

        <IconTrakindoSys name="arrow-down-2" color={ COLOR.whiteT4 }/>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        <Spacer
          space={ 1 }
          backgroundColor={ COLOR.whiteT1 }
          top={ 1 }
          bottom={ 1 }
          radius={ Config.base(1) }
          onPress={ () => this.openModal() }
        >
          <Padder>
            { this._renderMain() }
          </Padder>
        </Spacer>

        <FamilyFilterModal
          isActive={ this.state.isModalActive }
          onClosePress={ () => this.closeModal() }
          navData={{
            activeIndex: this.state.activeIndex,
            onSelect: (item, index) => this.onSelect(item, index),
          }}
        />
      </Spacer>
    );
  }

}
