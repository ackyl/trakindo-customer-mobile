/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import DeviceInfo from "react-native-device-info";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBS,
} from "@app-components-core/Text";
import {
  Spacer,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ResourceVersionInfoFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      version: null,
    };
  }

// ----------------------------------------

  componentDidMount() {
    DeviceInfo.getReadableVersion()
      .then(version => {
        this.setState({
          version,
        });
      });
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderVersion() {
    return (
      <Spacer row vAlign="center" hAlign="center">
        <TextBS
          mode="light2"
        >
          Trakindo Customer App | Version
        </TextBS>

        <Spacer horizontal space={ .4 }/>

        <TextBS
          mode="light2"
        >
          { this.state.version }
        </TextBS>

      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer height={ Device.isIphoneX ? Config.base(6) : Config.base(4) } backgroundColor={ COLOR.gray }>
        <Spacer
          backgroundColor={ COLOR.gray }
          width={ Device.windows.width }
          height={ Config.base(2) }
          radius={ Config.base(1) }
          style={ Styles.rounder }
        />

        { this._renderVersion() }
      </Spacer>
    );
  }

}
