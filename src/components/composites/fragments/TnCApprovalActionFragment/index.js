/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBS,
} from "@app-components-core/Text";
import {
  ButtonFullPrimary,
} from "@app-components-core/Button";
import {
  Padder,
  Spacer,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class TnCApprovalActionFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderDisclaimer() {
    return (
      <Spacer space={ 2 }>
        <TextBS
          mode="light"
        >
          By clicking Agree button, you have read and agreed to these terms and conditions
        </TextBS>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderMainButton() {
    const {
      onSubmit,
      isApprovable,
    } = this.props;

    if (!isApprovable) {
      return null;
    }

    return (
      <ButtonFullPrimary
        onPress={ onSubmit }
      >
        Agree
      </ButtonFullPrimary>
    );
  }


// ----------------------------------------
// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer top={ 1.5 } bottom={ Device.isIphoneX ? 5 : 2 } backgroundColor={ COLOR.gray }>
        <Spacer
          backgroundColor={ COLOR.gray }
          width={ Device.windows.width }
          height={ Config.base(2) }
          radius={ 50 }
          style={{
            position: "absolute",
            top: -Config.base(1),
          }}
        />

        <Padder>
          { this._renderDisclaimer() }

          { this._renderMainButton() }
        </Padder>
      </Spacer>
    );
  }

}
