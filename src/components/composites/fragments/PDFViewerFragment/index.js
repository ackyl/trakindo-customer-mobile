/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";
import Pdf from "react-native-pdf";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  ErrorAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class PDFViewerFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      isErrorShown: false,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onLoadComplete(noOfPages, path) {
    const {
      onLoadComplete,
    } = this.props;

    if (onLoadComplete) {
      onLoadComplete(noOfPages, path);
    }
  }

// ----------------------------------------

  onPageChanged(page, noOfPages) {
    const {
      onPageChanged,
    } = this.props;

    if (onPageChanged) {
      onPageChanged(page, noOfPages);
    }
  }

// ----------------------------------------

  onError(error) {
    const {
      onError,
    } = this.props;

    const {
      isErrorShown,
    } = this.state;

    if (!isErrorShown) {
      this.props.setError(error);
    }

    this.setState({
      isErrorShown: true,
    });

    if (onError) {
      onError(error);
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      file,
      headers,
      shouldCache,
    } = this.props;

    return (
      <Pdf
        source={{
          uri: file,
          cache: true,
          expiration: 60 * (!shouldCache ? 5 : (24 * 30)),
          headers: headers ? headers : {},
        }}
        onLoadComplete={ (numberOfPages, filePath) => this.onLoadComplete(numberOfPages, filePath) }
        onPageChanged={ (page, numberOfPages) => this.onPageChanged(page, numberOfPages) }
        onError={ (error) => this.onError(error) }
        style={ Styles.main }
      />
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {

  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    setError: (data, onComplete) => dispatch(ErrorAction.setErrorFile(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(PDFViewerFragment);



