/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullPrimary,
} from "@app-components-core/Button";
import {
  Padder,
  Spacer,
} from "@core-components-enhancers";
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  InputNumber,
} from "@app-components-core/Input";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class OrderConfirmationFormFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      formData: {
        serviceAgreementNumber: "",
      },

      error: {
        serviceAgreementNumber: null,
      },
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onValueChange(value) {
    const {
      formData,
    } = this.state;

    formData.serviceAgreementNumber = value;

    this.setState({
      formData,
    });
  }

// ----------------------------------------

  submit() {
    const {
      serviceAgreementNumber,
    } = this.state.formData;

    const error = {
      serviceAgreementNumber: this.getServiceAgreementNumberError(serviceAgreementNumber),
    };

    this.setState({
      error,
    });

    if (Validation.hasError(error)) {
      return false;
    }

    const {
      onSubmit,
    } = this.props;

    if (onSubmit) {
      onSubmit(serviceAgreementNumber);
    }
  }

// ----------------------------------------

  getServiceAgreementNumberError(serviceAgreementNumber) {
    if (!Validation.isNotEmpty(serviceAgreementNumber)) {
      return "Service Quotation cannot be empty";
    }

    const {
      localCheckValue,
    } = this.props;

    if ((serviceAgreementNumber + "").trim() !== (localCheckValue + "").trim()) {
      return "Invalid Service Quotation Number";
    }

    return null;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderBottomInstruction() {
    return (
      <Spacer space={ 2 }>
        <TextH2
          mode="light"
        >
          Input service quotation/reference number to confirm
        </TextH2>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderNumberForm() {
    const {
      isApprovable,
    } = this.props;

    if (!isApprovable) {
      return null;
    }

    return (
      <Spacer space={ 2 }>
        <InputNumber
          mode="light"
          label="Service quotation number (reference number)"
          value={ this.state.formData.serviceAgreementNumber }
          onChangeText={ (data) => this.onValueChange(data) }
          error={ this.state.error.serviceAgreementNumber }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDisclaimer() {
    return (
      <Spacer space={ 1 }>
        <TextBS
          mode="light"
        >
          By clicking Agree button, you have read and agreed to these terms and conditions
        </TextBS>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      isApprovable,
    } = this.props;

    if (!isApprovable) {
      return null;
    }

    return (
      <ButtonFullPrimary
        onPress={ () => this.submit() }
      >
        Agree
      </ButtonFullPrimary>
    );
  }

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer top={ 1.5 } bottom={ Device.isIphoneX ? 5 : 2 } backgroundColor={ COLOR.gray }>
        <Spacer
          backgroundColor={ COLOR.gray }
          width={ Device.windows.width }
          height={ Config.base(2) }
          radius={ 50 }
          style={{
            position: "absolute",
            top: -Config.base(1),
          }}
        />

        <Padder>
          { this._renderBottomInstruction() }

          { this._renderNumberForm() }

          { this._renderDisclaimer() }

          { this._renderButton() }
        </Padder>
      </Spacer>
    );
  }

}
