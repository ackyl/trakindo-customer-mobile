/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import {
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  IconTrakindoSys,
} from "@app-components-core/Icon";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class AddUnitAccessFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderRounder() {
    return (
      <Spacer
        width={ Device.windows.width }
        height={ Config.base(2) }
        backgroundColor={ COLOR.gray }
        radius={ Config.base(1) }
        style={{
          position: "absolute",
          top: -Config.base(1),
        }}
      />
    );
  }

// ----------------------------------------

  _renderText() {
    return (
      <TextH2
        mode="light"
      >
        Can’t find my equipment
      </TextH2>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      onPress,
    } = this.props;

    return (
      <ButtonCompactSecondary
        onPress={ onPress }
        hPadding={ Config.base(1) }
      >
        <IconTrakindoSys name="arrow-right"/>
      </ButtonCompactSecondary>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer top={ 1 }>
        <Spacer
          backgroundColor={ COLOR.gray}
          top={ 1 }
          bottom={ Device.isIphoneX ? 4 : 2 }
        >
          { this._renderRounder() }

          <Padder>
            <Spacer row hAlign="space-between">
              { this._renderText() }

              { this._renderButton() }
            </Spacer>
          </Padder>
        </Spacer>
      </Spacer>
    );
  }

}
