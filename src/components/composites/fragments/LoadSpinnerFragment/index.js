/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  IMPORTS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Animated,
  Easing,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  IconFA5,
} from "@app-components-core/Icon";




/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  MAIN CLASS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/
export default class LoadSpinnerFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
    };

    this.spinValue = new Animated.Value(0);
  }

// ----------------------------------------

  componentDidMount() {
    this.rotateSpinner();
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      if (!this.props.isActive && nextProps.isActive) {
        this.rotateSpinner();
      }

      if (this.props.isActive && !nextProps.isActive) {
        this.resetSpinner();
      }

      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  rotateSpinner() {
    Animated.loop(
      Animated.timing(
          this.spinValue,
        {
          toValue: 1,
          duration: 1600,
          easing: Easing.ease,
        }
      ))
    .start();
  }

// ----------------------------------------

  resetSpinner() {
    this.spinValue = new Animated.Value(0);
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      size,
    } = this.props;

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", "360deg"],
    });

    return (
      <Animated.View style={{
        transform: [{rotate: spin}],
      }}>
        <IconFA5
          name="circle-notch"
          color={ COLOR.orange }
          size={ size ? size : 35 }
        />
      </Animated.View>
    );

  }

}
