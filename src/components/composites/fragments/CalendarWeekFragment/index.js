/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
} from "@core-components-enhancers";
import {
  CalendarDateFragment,
} from "@app-components-fragments";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class CalendarWeekFragment extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderDateItem(date, maxLastMonth, maxDate, todayDate, selectedDate, index) {
    const {
      week,
      min,
      isZeroFirst,
      isExcluded,
    } = this.props;

    let isInCurrentMonth = true;
    if (week > (isZeroFirst ? 2 : 1)) {
      if (date > maxDate) {
        date -= maxDate;
        isInCurrentMonth = false;
      }
    } else {
      if (date > maxLastMonth) {
        date -= maxLastMonth;
      } else if (date > 7) {
        isInCurrentMonth = false;
      }
    }

    let minSelectable = true;
    const minDate = moment(min).format("D") * 1;
    if (min) {
      minSelectable = date >= minDate;
    }

    return (
      <CalendarDateFragment
        { ... this.props }
        date={ date }
        isExcluded={ isExcluded || !isInCurrentMonth || !minSelectable }
        key={ index }
        todayDate={ todayDate }
        selectedDate={ selectedDate }
      />
    );
  }

// ----------------------------------------

  _renderDates() {
    const {
      date,
      isCurrentMonth,
      isSelectedMonth,
      selected,
      maxLastMonth,
    } = this.props;

    const index = moment(date).day();
    const startDate = moment(date).subtract(index, "day");

    const weekDate = startDate.add(1, "day").format("D") * 1;

    const maxDate = moment(date).endOf("month").format("D") * 1;

    let todayDate = 0;
    if (isCurrentMonth) {
      todayDate = moment().format("D") * 1;
    }

    let selectedDate = 0;
    if (isSelectedMonth) {
      selectedDate = moment(selected).format("D") * 1;
    }

    const dates = [];
    for (let x = 0; x < 7; x++) {
      dates.push(this._renderDateItem(weekDate + x, maxLastMonth, maxDate, todayDate, selectedDate, x));
    }

    return dates;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    let {
      width,
    } = this.props;

    if (!width) {
      width = Device.windows.width;
    }

    return (
      <Spacer
        width={ width }
        row
        hAlign="space-around"
      >
        { this._renderDates() }
      </Spacer>
    );
  }

}
