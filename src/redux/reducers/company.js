/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { fromJS } from "immutable";
import * as ReducerService from "@core-services/ReducerService";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  AUTH,
  COMPANY,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  INITAL STATE
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
const initialState = fromJS({
  list: ReducerService.emptyList(),
  searchedList: ReducerService.emptyList(),
  base: ReducerService.emptyMap(),
  activeSearch: "",
  salesRepList: ReducerService.emptyList(),
  contactList: ReducerService.emptyList(),
  virtualAccountList: ReducerService.emptyList(),
});


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default (state = initialState, action) => {
  const data = action.data;

  switch (action.type) {

    case COMPANY.SET_LIST:
      return ReducerService.setPagedListState(state, "list", data.page, data.companies);

// ----------------------------------------

    case COMPANY.SET_SEARCHED_LIST:
      return ReducerService.setPagedListState(state, "searchedList", data.page, data.companies);
        // .set("activeSearch", data.activeSearch);

// ----------------------------------------

    case COMPANY.RESET_SEARCHED_LIST:
      return state.set("searchedList", ReducerService.emptyList());

// ----------------------------------------

    case COMPANY.SET_BASE:
      return state.set("base", fromJS(data.base))
        .set("salesRepList", ReducerService.emptyList())
        .set("contactList", ReducerService.emptyList())
        .set("virtualAccountList", ReducerService.emptyList());

// ----------------------------------------

    case COMPANY.SET_SALES_REP_LIST:
      return ReducerService.setStaticListState(state, "salesRepList", data.salesReps);

// ----------------------------------------

    case COMPANY.SET_CONTACT_LIST:
      return ReducerService.setStaticListState(state, "contactList", data.contacts);

// ----------------------------------------

    case COMPANY.SET_VA_LIST:
      return ReducerService.setStaticListState(state, "virtualAccountList", data.virtualAccounts);

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    case AUTH.LOGOUT:
      return initialState;

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    default:
      return state;
  }
};
