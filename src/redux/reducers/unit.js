/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { fromJS } from "immutable";
import * as ReducerService from "@core-services/ReducerService";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  AUTH,
  COMPANY,
  SERVICE,
  UNIT,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  INITAL STATE
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
const initialState = fromJS({
  list: ReducerService.emptyList(),
  filteredList: ReducerService.emptyList(),
  base: ReducerService.emptyMap(),
  familyList: ReducerService.emptyList(),
  activeFamily: "",
});


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default (state = initialState, action) => {
  const data = action.data;

  switch (action.type) {

    case UNIT.SET_LIST:
      return ReducerService.setPagedListState(state, "list", data.page, data.units);

// ----------------------------------------

    case UNIT.SET_FILTERED_LIST:
      return ReducerService.setPagedListState(state, "filteredList", data.page, data.units)
        .set("activeFamily", data.activeFamily);

// ----------------------------------------

    case UNIT.SET_BASE:
      return state.set("base", fromJS(data.base));

// ----------------------------------------

    case UNIT.RESET_FILTERED_LIST:
      return state.set("filteredList", ReducerService.emptyList());

// ----------------------------------------

    case UNIT.SET_FAMILY_LIST:
      return ReducerService.setStaticListState(state, "familyList", data.unitFamilies);

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    case SERVICE.SET_BASE:
      return initialState
        .set("list", state.get("list"))
        .set("familyList", state.get("familyList"));

// ----------------------------------------

    case AUTH.LOGOUT:
    case COMPANY.SET_BASE:
      return initialState
        .set("familyList", state.get("familyList"));

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    default:
      return state;
  }
};
