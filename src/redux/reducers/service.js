/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { fromJS } from "immutable";
import * as ReducerService from "@core-services/ReducerService";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  AUTH,
  COMPANY,
  SERVICE,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  INITAL STATE
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
const initialState = fromJS({
  list: ReducerService.emptyList(),
  brief: ReducerService.emptyMap(),
  progress: ReducerService.emptyMap(),
  technicianList: ReducerService.emptyList(),
  ratingList: ReducerService.emptyList(),
  base: ReducerService.emptyMap(),
  activeTicket: "",
  quotationList: ReducerService.emptyList(),
});


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default (state = initialState, action) => {
  const data = action.data;

  switch (action.type) {

    case SERVICE.SET_LIST:
      return ReducerService.setPagedListState(state, "list", data.page, data.services);

// ----------------------------------------

    case SERVICE.SET_BRIEF:
      return state.set("brief", fromJS(data.serviceBrief));

// ----------------------------------------

    case SERVICE.SET_PROGRESS_DETAIL:
      state =  state.set("progress", fromJS(data.progress));
      state = ReducerService.setStaticListState(state, "technicianList", data.technicians);

      return state;

// ----------------------------------------

    case SERVICE.SET_PROGRESS_RATING:
      return ReducerService.setStaticListState(state, "ratingList", data.ratings);

// ----------------------------------------

    case SERVICE.SET_BASE:
      return state
        .set("base", fromJS(data.base))
        .set("progress", ReducerService.emptyMap())
        .set("technicianList", ReducerService.emptyList())
        .set("ratingList", ReducerService.emptyList())
        .set("activeTicket", "")
        .set("quotationList", ReducerService.emptyList());

// ----------------------------------------

    case SERVICE.CREATE_REQUEST:
      return state
        .set("activeTicket", data.ticketNumber)
        .set("list", ReducerService.emptyList());

// ----------------------------------------

    case SERVICE.SET_QUOTATION_LIST:
      return ReducerService.setStaticListState(state, "quotationList", data.quotations);

// ----------------------------------------

    case SERVICE.RESET_ACTIVE_TICKET:
      return state
        .set("activeTicket", "");

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    case AUTH.LOGOUT:
    case COMPANY.SET_BASE:
      return initialState;

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    default:
      return state;
  }
};
