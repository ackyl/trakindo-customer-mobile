/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { fromJS } from "immutable";
import * as ReducerService from "@core-services/ReducerService";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  AUTH,
  COMPANY,
  INVOICE,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  INITAL STATE
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
const initialState = fromJS({
  overdueList: ReducerService.emptyList(),
  activeList: ReducerService.emptyList(),
  historyList: ReducerService.emptyList(),
  brief: ReducerService.emptyMap(),
});


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default (state = initialState, action) => {
  const data = action.data;

  switch (action.type) {

    case INVOICE.SET_LIST:
      return ReducerService.setPagedListState(state, "activeList", data.page, data.invoices);

// ----------------------------------------

    case INVOICE.SET_OVERDUE_LIST:
      return ReducerService.setPagedListState(state, "overdueList", data.page, data.invoices);

// ----------------------------------------

    case INVOICE.SET_HISTORY_LIST:
      return ReducerService.setPagedListState(state, "historyList", data.page, data.invoices);

// ----------------------------------------

    case INVOICE.SET_BRIEF:
      return state.set("brief", fromJS(data.invoiceBrief));


// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    case AUTH.LOGOUT:
    case COMPANY.SET_BASE:
      return initialState;

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    default:
      return state;
  }
};
