import core from "./core";
import error from "./error";
import auth from "./auth";
import company from "./company";
import invoice from "./invoice";
import credit from "./credit";
import unit from "./unit";
import service from "./service";
import part from "./part";
import partItem from "./partItem";
import partShipping from "./partShipping";
import notification from "./notification";
import misc from "./misc";
import cache from "./cache";

export default {
  core,
  error,
  auth,
  company,
  invoice,
  credit,
  unit,
  service,
  part,
  partItem,
  partShipping,
  notification,
  misc,
  cache,
};
