/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { fromJS } from "immutable";
import * as ReducerService from "@core-services/ReducerService";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  AUTH,
  COMPANY,
  SERVICE,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  INITAL STATE
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
const initialState = fromJS({
  service: {
    approvedDocumentList: ReducerService.emptyList(),
    confirmedScheduleList: ReducerService.emptyList(),
    completedList: ReducerService.emptyList(),
  },

  timestampToClean: (new Date()).getTime() + (60 * 60 * 1000),
});


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default (state = initialState, action) => {
  const data = action.data;

  switch (action.type) {

    case SERVICE.CONFIRM_DOCUMENTS:
      return state
        .set("service",
          ReducerService.pushListState(state.get("service"), "approvedDocumentList", data.serviceRequestNumber)
        )
        .set("timestampToClean", (new Date()).getTime() + (60 * 60 * 1000));

// ----------------------------------------

    case SERVICE.CONFIRM_SCHEDULE:
      return state
        .set("service",
          ReducerService.pushListState(state.get("service"), "confirmedScheduleList", data.serviceOrderNumber)
        )
        .set("timestampToClean", (new Date()).getTime() + (60 * 60 * 1000));

// ----------------------------------------

    case SERVICE.SET_COMPLETED:
      return state
        .set("service",
          ReducerService.pushListState(state.get("service"), "completedList", data.serviceRequestNumber)
        )
        .set("timestampToClean", (new Date()).getTime() + (60 * 60 * 1000));

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    case AUTH.LOGOUT:
      return initialState;

// ----------------------------------------
    case COMPANY.SET_BASE:
      if (state.timestampToClean < ((new Date()).getTime() - (60 * 60 * 1000))) {
        return initialState;
      } else {
        return state;
      }

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    default:
      return state;
  }
};
