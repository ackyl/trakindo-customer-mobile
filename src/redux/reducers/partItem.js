/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { fromJS } from "immutable";
import * as ReducerService from "@core-services/ReducerService";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  AUTH,
  COMPANY,
  PART,
  PART_ITEM,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  INITAL STATE
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
const initialState = fromJS({
  outstandingList: ReducerService.emptyList(),
  readyList: ReducerService.emptyList(),
  takenList: ReducerService.emptyList(),
  base: ReducerService.emptyMap(),
  brief: ReducerService.emptyMap(),
});


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default (state = initialState, action) => {
  const data = action.data;

  switch (action.type) {

    case PART_ITEM.SET_LIST:
      return ReducerService.setPagedListState(state, "readyList", data.page, data.itemGroups);

// ----------------------------------------

    case PART_ITEM.SET_OUTSTANDING_LIST:
      return ReducerService.setPagedListState(state, "outstandingList", data.page, data.itemGroups);

// ----------------------------------------

    case PART_ITEM.SET_TAKEN_LIST:
      return ReducerService.setPagedListState(state, "takenList", data.page, data.itemGroups);

// ----------------------------------------

    case PART_ITEM.SET_BASE:
      return state.set("base", fromJS(data.base));

// ----------------------------------------

    case PART_ITEM.SET_BRIEF:
      return state.set("brief", fromJS(data.itemGroupBrief));

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    case AUTH.LOGOUT:
    case COMPANY.SET_BASE:
    case PART.SET_BASE:
      return initialState;

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

    default:
      return state;
  }
};
