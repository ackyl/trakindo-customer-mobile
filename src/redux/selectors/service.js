/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import * as SelectorService from "@core-services/SelectorService";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  STATES
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

const services = (state, props) => state.service.get("list");

// ----------------------------------------

const serviceBrief = (state, props) => state.service.get("brief");

// ----------------------------------------

const serviceProgress = (state, props) => state.service.get("progress");

// ----------------------------------------

const serviceTechnicians = (state, props) => state.service.get("technicianList");

// ----------------------------------------

const serviceRatings = (state, props) => state.service.get("ratingList");

// ----------------------------------------

const selectedService = (state, props) => state.service.get("base");

// ----------------------------------------

const ticket = (state, props) => state.service.get("activeTicket");

// ----------------------------------------

const serviceQuotations = (state, props) => state.service.get("quotationList");

// ----------------------------------------

const confirmedServiceSchedules = (state, props) => state.cache.getIn(["service", "confirmedScheduleList"]);

// ----------------------------------------

const approvedServiceDocuments = (state, props) => state.cache.getIn(["service", "approvedDocumentList"]);

// ----------------------------------------

const completeds = (state, props) => state.cache.getIn(["service", "completedList"]);




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  SELECTORS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const list = SelectorService.select(
  [services, completeds],
  (items, completedServices) => (items ? items.toJS()
      .map(serv => {
        if (serv.status !== "IN_PROGRESS") {
          return serv;
        }

        return {
          ... serv,
          status: completedServices.indexOf(serv.serviceRequestNumber) > -1 ? "COMPLETED" : serv.status,
        };
      })
    :
      []
    )
);

// ----------------------------------------

export const brief = SelectorService.select(
  [serviceBrief],
  (item) => (item ? item.toJS() : {})
);

// ----------------------------------------

export const progress = SelectorService.select(
  [serviceProgress],
  (item) => (item ? item.toJS() : {})
);

// ----------------------------------------

export const technicians = SelectorService.select(
  [serviceTechnicians],
  (items) => (items ? items.toJS() : [])
);

// ----------------------------------------

export const ratings = SelectorService.select(
  [serviceRatings],
  (items) => (items ? items.toJS()
    .filter((a) =>  a.type === "daily")
    .sort((a, b) => a.date > b.date)
      :
    []
  )
);

// ----------------------------------------

export const endRating = SelectorService.select(
  [serviceRatings],
  (items) => {
    const endRatingList = (items ? items.toJS()
      .filter((a) =>  a.type === "overall")
      .sort((a, b) => a.date < b.date)
        :
      []
    );

    if (endRatingList.length < 1) {
      return null;
    }

    return endRatingList[0];
  }
);

// ----------------------------------------

export const selected = SelectorService.select(
  [selectedService],
  (item) => item.toJS()
);

// ----------------------------------------

export const activeTicket = SelectorService.select(
  [ticket],
  (item) => (item ? item : "")
);

// ----------------------------------------

export const quotation = SelectorService.select(
  [serviceQuotations],
  (items) => {
    const quotations = items ?
      items.toJS()
        .filter(a => a.shouldShowQuotation)
        .sort((a, b) => a.expirationDate < b.expirationDate)
      :
        [];

    if (quotations.length < 1) {
      return null;
    }

    return quotations[0];
  }
);

// ----------------------------------------

export const confirmedSchedules = SelectorService.select(
  [confirmedServiceSchedules],
  (items) => (items ? items.toJS() : [])
);

// ----------------------------------------

export const approvedDocuments = SelectorService.select(
  [approvedServiceDocuments],
  (items) => (items ? items.toJS() : [])
);


