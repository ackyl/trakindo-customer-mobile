/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import * as SelectorService from "@core-services/SelectorService";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  STATES
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

const companies = (state, props) => state.company.get("list");

// ----------------------------------------

const searchedCompanies = (state, props) => state.company.get("searchedList");

// ----------------------------------------

const selectedCompany = (state, props) => state.company.get("base");

// ----------------------------------------

const activeSearch = (state, props) => state.company.get("activeSearch");

// ----------------------------------------

const companySalesReps = (state, props) => state.company.get("salesRepList");

// ----------------------------------------

const contacts = (state, props) => state.company.get("contactList");

// ----------------------------------------

const companyVAs = (state, props) => state.company.get("virtualAccountList");




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  SELECTORS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const list = SelectorService.select(
  [companies],
  (items) => items.toJS()
    .sort((a, b) => a.soldtoId > b.soldtoId)
);

// ----------------------------------------

export const searchedList = SelectorService.select(
  [searchedCompanies, activeSearch],
  (items, search) => {
    return ((items && search || items) ? items.toJS() : []);
  }
);

// ----------------------------------------

export const selected = SelectorService.select(
  [selectedCompany],
  (item) => item.toJS()
);

// ----------------------------------------

export const salesReps = SelectorService.select(
  [companySalesReps],
  (items) => {
    return items
      .filter((a) => (a.get("role") === "ZPSSR" || a.get("role") === "ZISR"))
      .toJS();
  }
);

// ----------------------------------------

export const selectedContact = SelectorService.select(
  [contacts],
  (items) => {
    const item = items
    .filter((a) => a.get("isPrimary"))
    .get(0);

    return (item ? item.toJS() : {});
  }
);

// ----------------------------------------

export const virtualAccounts = SelectorService.select(
  [companyVAs],
  (items) => (items ? items.toJS()
    .sort((a, b) =>
        a.currency !== "IDR"
      )
    :
      [])
);



