/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import * as SelectorService from "@core-services/SelectorService";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  STATES
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

const trackingStatuses = (state, props) => state.partShipping.get("trackingList");





/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  SELECTORS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const trackingList = SelectorService.select(
  [trackingStatuses],
  (items) => {
    return ((items) ? items.toJS()
        .filter((item) => {
          const allowedStatusCodes = [
            "PUP",
            "OHP",
            "TRI",
            "SOP",
            "POD",
            "DLY",
          ];

          return allowedStatusCodes.indexOf(item.statusCode) > -1;
        })
      :
        []
    );
  }
);
