import error from "./error";
import auth from "./auth";
import company from "./company";
import credit from "./credit";
import invoice from "./invoice";
import unit from "./unit";
import service from "./service";
import part from "./part";
import partItem from "./partItem";
import partShipping from "./partShipping";
import notification from "./notification";
import misc from "./misc";

export default {
	error,
  auth,
  company,
  credit,
  invoice,
  unit,
  service,
  part,
  partItem,
  partShipping,
  notification,
  misc,
};
