/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import * as SelectorService from "@core-services/SelectorService";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  STATES
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

const outstandings = (state, props) => state.partItem.get("outstandingList");

// ----------------------------------------

const readies = (state, props) => state.partItem.get("readyList");

// ----------------------------------------

const takens = (state, props) => state.partItem.get("takenList");

// ----------------------------------------

const selectedPartItem = (state, props) => state.partItem.get("base");

// ----------------------------------------

const briefData = (state, props) => state.partItem.get("brief");

// ----------------------------------------

const selectedPart = (state, props) => state.part.get("base");



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  SELECTORS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const outstandingList = SelectorService.select(
  [outstandings, selectedPart],
  (items, part) => (items ? items.toJS()
      .map((partItem) => {
        return {
          ... partItem,
          items: partItem.items.map((detail) => {
            return {
              ... detail,
              isDelayed: detail.pickupStatus === "outstanding" && detail.eta > part.get("committedDate"),
            };
          }),
        };
      })
    :
      []
   )
);

// ----------------------------------------

export const readyList = SelectorService.select(
  [readies],
  (items) => (items ? items.toJS() : [])
);

// ----------------------------------------

export const takenList = SelectorService.select(
  [takens],
  (items) => (items ? items.toJS() : [])
);

// ----------------------------------------

export const selected = SelectorService.select(
  [selectedPartItem],
  (item) => item.toJS()
);

// ----------------------------------------

export const brief = SelectorService.select(
  [briefData],
  (item) => (item ? item.toJS() : null)
);

// ----------------------------------------


