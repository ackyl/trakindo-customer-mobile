/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import * as SelectorService from "@core-services/SelectorService";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  STATES
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

const overdues = (state, props) => state.invoice.get("overdueList");

// ----------------------------------------

const actives = (state, props) => state.invoice.get("activeList");

// ----------------------------------------

const histories = (state, props) => state.invoice.get("historyList");

// ----------------------------------------

const briefData = (state, props) => state.credit.getIn(["payerList", 0]);



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  SELECTORS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const overdueList = SelectorService.select(
  [overdues],
  (items) => (items ? items.toJS()
    // .sort((a, b) => a.invoiceNumber > b.invoiceNumber)
    // .sort((a, b) => a.amount > b.amount)
    // .sort((a, b) => a.dueDate < b.dueDate) 
    : [])
);

// ----------------------------------------

export const activeList = SelectorService.select(
  [actives],
  (items) => (items ? items.toJS()
    // .sort((a, b) => a.invoiceNumber > b.invoiceNumber)
    // .sort((a, b) => a.amount > b.amount)
    // .sort((a, b) => a.dueDate < b.dueDate) 
    : [])
);

// ----------------------------------------

export const historyList = SelectorService.select(
  [histories],
  (items) => (items ? items.toJS()
    // .sort((a, b) => a.invoiceNumber > b.invoiceNumber)
    // .sort((a, b) => a.amount > b.amount)
    // .sort((a, b) => a.dueDate < b.dueDate) 
    : [])
);

// ----------------------------------------

export const brief = SelectorService.select(
  [briefData],
  (item) => (item ? item.toJS() : null)
);

// ----------------------------------------


