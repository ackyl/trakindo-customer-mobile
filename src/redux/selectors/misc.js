/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import * as SelectorService from "@core-services/SelectorService";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  STATES
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

const branches = (state, props) => state.misc.get("branchOfficeList");

// ----------------------------------------

const appSetting = (state, props) => state.misc.get("setting");



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  SELECTORS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const branchList = SelectorService.select(
  [branches],
  (items) => (items ? items.toJS()
      .sort((a, b) => (a.name > b.name))
    :
      []
  )
);

// ----------------------------------------

export const setting = SelectorService.select(
  [appSetting],
  (item) => (item ? item.toJS() : {})
);


