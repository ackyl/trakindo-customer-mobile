/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import * as SelectorService from "@core-services/SelectorService";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  STATES
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

const payers = (state, props) => state.credit.get("payerList");

// ----------------------------------------

const selectedCredit = (state, props) => state.credit.get("data");

// ----------------------------------------

const payerSoaFiles = (state, props) => state.credit.get("soaFileList");



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  SELECTORS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const payer = SelectorService.select(
  [payers],
  (item) => {
    const payerList = item ? item.toJS()
      .sort((a, b) =>
        a.payerId.trim().toLowerCase().indexOf("P00") !== 0 ||
        a.currency !== "IDR" ||
        (a.overdueAmount === b.overdueAmount && a.currentAmount < b.currentAmount) ||
        a.overdueAmount < b.overdueAmount
      )
    :
      [];

    return payerList[0] ? payerList[0] : null;
  }
);

// ----------------------------------------

export const selected = SelectorService.select(
  [selectedCredit],
  (item) => (item ? item.toJS() : null)
);

// ----------------------------------------

export const isOverlimit = SelectorService.select(
  [selected],
  (item) => {
    const remaining = item ? item.data.remaining : 0;
    return remaining !== null && remaining !== undefined && remaining < 0;
  }
);

// ----------------------------------------

export const hasOverdue = SelectorService.select(
  [payer],
  (item) => {
    return item && item.data.overdueAmount > 0;
  }
);

// ----------------------------------------

export const soaFiles = SelectorService.select(
  [payerSoaFiles],
  (items) => (items ? items.toJS() : [])
);

