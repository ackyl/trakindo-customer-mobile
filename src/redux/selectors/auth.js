/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import * as SelectorService from "@core-services/SelectorService";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  STATES
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

const currentAuth = (state, props) => state.auth;

// ----------------------------------------

const awsAccessToken = (state, props) => state.auth.get("accessToken");

// ----------------------------------------

const awsRefreshToken = (state, props) => state.auth.get("refreshToken");

// ----------------------------------------

const oneSignalDeviceId = (state, props) => state.auth.get("deviceId");

// ----------------------------------------

const loggedInSalesRepId = (state, props) => state.auth.get("salesRepId");

// ----------------------------------------

const roleId = (state, props) => state.auth.get("roleId");




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  SELECTORS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const isLoggedIn = SelectorService.select(
  [awsAccessToken],
  (item) => item !== null && item !== undefined && item.length > 0
);

// ----------------------------------------

export const authorization = SelectorService.select(
  [isLoggedIn, awsAccessToken],
  (isLoggin, token) => isLoggin ? `Bearer ${token}` : ""
);

// ----------------------------------------

export const refreshToken = SelectorService.select(
  [isLoggedIn, awsRefreshToken],
  (isLoggin, token) => isLoggin ? token : ""
);

// ----------------------------------------

export const deviceId = SelectorService.select(
  [isLoggedIn, oneSignalDeviceId],
  (isLoggin, id) => isLoggin ? id : ""
);

// ----------------------------------------

export const auth = SelectorService.select(
  [isLoggedIn, currentAuth],
  (isLoggin, thisAuth) => isLoggin && thisAuth ? thisAuth.toJS() : null
);

// ----------------------------------------

export const salesRepId = SelectorService.select(
  [isLoggedIn, roleId, loggedInSalesRepId],
  (isLoggin, role, salesId) => {
    if (!isLoggin) {
      return "";
    }

    role = role * 1;

    if (role <= 5) {
      return "";
    }

    if (role === 666) {
      return "";
    }

    return salesId;
  }
);

// ----------------------------------------

export const accessibleModules = SelectorService.select(
  [roleId],
  (role) => {
    const basicModules = [
      "COMPANY",
      "HELP",
    ];

    const customerModules = [
      ... basicModules,
      "NOTIFICATION",
    ];

    const mainModules = [
      "FINANCE",
      "PART",
      "SERVICE",
    ];

    role = role * 1;

    if (role <= 1) {
      return [];
    }

    if (role === 666) {
      return [
        ... customerModules,
        ... mainModules,
      ];
    }


    if (role > 4) {
      return [
        ... basicModules,
        ... mainModules,
      ];
    }

    switch (role) {
      case 2:
        return [
          ... customerModules,
          ... mainModules,
        ];

      case 3:
        return [
          ... customerModules,
          "FINANCE",
        ];

      case 4:
        return [
          ... customerModules,
          "PART",
          "SERVICE",
        ];
    }
  }
);

