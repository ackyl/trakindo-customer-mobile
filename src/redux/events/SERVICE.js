export const SET_LIST = "SERVICE_SET_LIST";
export const SET_BRIEF = "SERVICE_SET_BRIEF";
export const SET_PROGRESS_DETAIL = "SERVICE_SET_PROGRESS_DETAIL";
export const SET_PROGRESS_RATING = "SERVICE_SET_PROGRESS_RATING";
export const SET_BASE = "SERVICE_SET_BASE";
export const CREATE_REQUEST = "SERVICE_CREATE_REQUEST";
export const SET_QUOTATION_LIST = "SERVICE_SET_QUOTATION_LIST";
export const RESET_ACTIVE_TICKET = "SERVICE_RESET_ACTIVE_TICKET";
export const CONFIRM_DOCUMENTS = "SERVICE_CONFIRM_DOCUMENTS";
export const CONFIRM_SCHEDULE = "SERVICE_CONFIRM_SCHEDULE";
export const SET_COMPLETED = "SERVICE_SET_COMPLETED";
