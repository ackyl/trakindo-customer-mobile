export const LOGIN = "AUTH_LOGIN";
export const LOGOUT = "AUTH_LOGOUT";
export const SET_DEVICE_ID = "AUTH_SET_DEVICE_ID";
