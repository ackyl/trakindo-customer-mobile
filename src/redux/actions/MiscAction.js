/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  DataService,
  MapperService,
} from "@core-services";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  MISC,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function getBranchOfficeList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchBranchOfficeList",
    data,
    (response, dispatch) => {
      dispatch({
        type: MISC.SET_BRANCH_OFFICE_LIST,
        data: {
          branchOffices: MapperService.mapResponseList("branchOfficeDetail", response.branches),
        },
      });
    },
  );
}

// ----------------------------------------

export function getSettings(data = {}, onSuccess:func) {
  return DataService.request(
    "getSetting",
    data,
    (response, dispatch) => {
      dispatch({
        type: MISC.SET_SETTING,
        data: {
          setting: MapperService.mapResponseData("settingDetail", response.setting),
        },
      });
    },
  );
}






