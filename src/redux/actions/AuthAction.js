/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  DataService,
  MapperService,
} from "@core-services";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  COMPANY,
  AUTH,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export function loginCustomer(data = {}, onSuccess:func, preFunction:func = null, postFunction:func = null) {
  return DataService.request(
    "loginCustomer",
    data,
    (response, dispatch) => {
      dispatch({
        type: AUTH.LOGIN,
        data: data,
      });

      if (!data.noBaseSet) {
        dispatch({
          type: COMPANY.SET_BASE,
          data: {
            base: MapperService.mapResponseData("companyDetail", response.company_detail),
          },
        });
      }

      onSuccess();
    },
    preFunction,
    postFunction,
  );
}

// ----------------------------------------

export function setDeviceId(data = {}, onSuccess:func) {
  return DataService.direct(
    "",
    {},
    (response, dispatch) => {
      dispatch({
        type: AUTH.SET_DEVICE_ID,
        data: {
          deviceId: data.deviceId,
        },
      });
    }
  );
}

// ----------------------------------------

export function logout(data = {}, onSuccess:func) {
  return DataService.request(
    "logout",
    data,
    (response, dispatch) => {
      dispatch({
        type: AUTH.LOGOUT,
        data: {},
      });

      onSuccess();
    }
  );
}

// ----------------------------------------

export function directLogout(data = {}, onSuccess:func) {
  return DataService.direct(
    "logout",
    {},
    (response, dispatch) => {
      dispatch({
        type: AUTH.LOGOUT,
        data: {},
      });

      onSuccess();
    }
  );
}

// ----------------------------------------
