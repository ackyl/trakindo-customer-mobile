/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  DataService,
  MapperService,
} from "@core-services";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  PART_ITEM,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function getOutstandingItemGroupList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchPartItemGroupList",
    {
      ... data,
      status: "OUTSTANDING",
    },
    (response, dispatch) => {
      dispatch({
        type: PART_ITEM.SET_OUTSTANDING_LIST,
        data: {
          page: data.page,
          itemGroups: MapperService.mapResponseList("partItemGroupDetail", response.items),
        },
      });
    }
  );
}

// ----------------------------------------

export function getReadyItemGroupList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchPartItemGroupList",
    {
      ... data,
      status: "READY",
    },
    (response, dispatch) => {
      dispatch({
        type: PART_ITEM.SET_LIST,
        data: {
          page: data.page,
          itemGroups: MapperService.mapResponseList("partItemGroupDetail", response.items),
        },
      });
    }
  );
}

// ----------------------------------------

export function getTakenItemGroupList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchPartItemGroupList",
    {
      ... data,
      status: "TAKEN",
    },
    (response, dispatch) => {
      dispatch({
        type: PART_ITEM.SET_TAKEN_LIST,
        data: {
          page: data.page,
          itemGroups: MapperService.mapResponseList("partItemGroupDetail", response.items),
        },
      });
    }
  );
}

// ----------------------------------------

export function setBaseFromList(data = {}, onSuccess:func) {
  return DataService.direct(
    "fetchPartItemGroupList",
    {},
    (response, dispatch) => {
      dispatch({
        type: PART_ITEM.SET_BASE,
        data: {
          base: data,
        },
      });
    }
  );
}

// ----------------------------------------

export function getBrief(data = {}, onSuccess:func) {
  return DataService.request(
    "getItemGroupBrief",
    data,
    (response, dispatch) => {
      dispatch({
        type: PART_ITEM.SET_BRIEF,
        data: {
          itemGroupBrief: MapperService.mapResponseData("partItemGroupBrief", response.item_counter),
        },
      });
    }
  );
}
