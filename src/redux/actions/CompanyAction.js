/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  DataService,
  MapperService,
} from "@core-services";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  COMPANY,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function getList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchCompanyList",
    data,
    (response, dispatch) => {
      dispatch({
        type: data && data.search ? COMPANY.SET_SEARCHED_LIST : COMPANY.SET_LIST,
        data: {
          page: data.page,
          activeSearch: data.search ? data.search : "",
          companies: MapperService.mapResponseList("companyDetail", response.companies),
        },
      });
    },
    (dispatch) => {
      if (data.search && (!data.page || data.page < 2)) {
        dispatch({
          type: COMPANY.RESET_SEARCHED_LIST,
        });
      }
    }
  );
}

// ----------------------------------------

export function setBaseFromList(data = {}, onSuccess:func) {
  return DataService.direct(
    "fetchCompanyList",
    {},
    (response, dispatch) => {
      dispatch({
        type: COMPANY.SET_BASE,
        data: {
          base: data,
        },
      });
    }
  );
}

// ----------------------------------------

export function getSalesRepList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchSalesRepList",
    data,
    (response, dispatch) => {
      dispatch({
        type: COMPANY.SET_SALES_REP_LIST,
        data: {
          salesReps: MapperService.mapResponseList("salesRepDetail", response.sales_reps),
        },
      });
    }
  );
}

// ----------------------------------------

export function getContactList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchContactList",
    data,
    (response, dispatch) => {
      dispatch({
        type: COMPANY.SET_CONTACT_LIST,
        data: {
          contacts: MapperService.mapResponseList("contactDetail", response.contacts),
        },
      });
    }
  );
}

// ----------------------------------------

export function getVirtualAccountList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchVirtualAccountList",
    data,
    (response, dispatch) => {
      dispatch({
        type: COMPANY.SET_VA_LIST,
        data: {
          virtualAccounts: MapperService.mapResponseList("virtualAccountDetail", response.accounts),
        },
      });
    }
  );
}
