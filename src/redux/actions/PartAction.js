/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  DataService,
  MapperService,
} from "@core-services";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  PART,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function getList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchPartList",
    data,
    (response, dispatch) => {
      dispatch({
        type: data && data.search ? PART.SET_SEARCHED_LIST : PART.SET_LIST,
        data: {
          page: data.page,
          activeSearch: data.search ? data.search : "",
          parts: MapperService.mapResponseList("partDetail", response.part_orders),
        },
      });
    },
    (dispatch) => {
      if (data.search && (!data.page || data.page < 2)) {
        dispatch({
          type: PART.RESET_SEARCHED_LIST,
        });
      }
    }
  );
}

// ----------------------------------------

export function setBaseFromList(data = {}, onSuccess:func) {
  return DataService.direct(
    "fetchPartList",
    {},
    (response, dispatch) => {
      dispatch({
        type: PART.SET_BASE,
        data: {
          base: data,
        },
      });
    }
  );
}
