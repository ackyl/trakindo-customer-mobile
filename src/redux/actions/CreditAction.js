/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  DataService,
  MapperService,
} from "@core-services";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  CREDIT,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function getPayerList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchPayerList",
    data,
    (response, dispatch) => {
      dispatch({
        type: CREDIT.SET_PAYER_LIST,
        data: {
          payers: MapperService.mapResponseList("payerDetail", response.payers),
        },
      });
    }
  );
}

// ----------------------------------------

export function getCredit(data = {}, onSuccess:func) {
  return DataService.request(
    "getCredit",
    data,
    (response, dispatch) => {
      dispatch({
        type: CREDIT.SET_DATA,
        data: {
          credit: MapperService.mapResponseData("creditDetail", response.credit_limit),
        },
      });
    }
  );
}

// ----------------------------------------

export function getSoaFileList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchSoaFileList",
    data,
    (response, dispatch) => {
      dispatch({
        type: CREDIT.SET_SOA_FILE_LIST,
        data: {
          files: MapperService.mapResponseList("soaFileDetail", response.files),
        },
      });
    }
  );
}


