/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  DataService,
  MapperService,
} from "@core-services";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  SERVICE,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function getList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchServiceList",
    data,
    (response, dispatch) => {
      dispatch({
        type: SERVICE.SET_LIST,
        data: {
          page: data.page,
          services: MapperService.mapResponseList("serviceDetail", response.orders),
        },
      });
    }
  );
}

// ----------------------------------------

export function getBrief(data = {}, onSuccess:func) {
  return DataService.request(
    "getServiceBrief",
    data,
    (response, dispatch) => {
      dispatch({
        type: SERVICE.SET_BRIEF,
        data: {
          serviceBrief: MapperService.mapResponseData("serviceBrief", response.brief),
        },
      });
    }
  );
}

// ----------------------------------------

export function setBaseFromList(data = {}, onSuccess:func) {
  return DataService.direct(
    "fetchServiceList",
    {},
    (response, dispatch) => {
      dispatch({
        type: SERVICE.SET_BASE,
        data: {
          base: data,
        },
      });
    }
  );
}

// ----------------------------------------

export function createRequest(data = {}, onSuccess:func, preFunction:func = null, postFunction:func = null) {
  return DataService.request(
    "createServiceRequest",
    data,
    (response, dispatch) => {
      dispatch({
        type: SERVICE.CREATE_REQUEST,
        data: {
          ticketNumber: response.service_request_number,
        },
      });

      onSuccess();
    },
    (dispatch) => {
      if (preFunction) {
        preFunction();
      }

      dispatch({
        type: SERVICE.RESET_ACTIVE_TICKET,
        data: {},
      });
    },
    postFunction
  );
}

// ----------------------------------------

export function getQuotationList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchServiceQuotationList",
    data,
    (response, dispatch) => {
      dispatch({
        type: SERVICE.SET_QUOTATION_LIST,
        data: {
          quotations: MapperService.mapResponseList("serviceQuotationDetail", response.quotations),
        },
      });
    }
  );
}

// ----------------------------------------

export function getProgressDetail(data = {}, onSuccess:func) {
  return DataService.request(
    "getServiceScheduledDetail",
    data,
    (response, dispatch) => {
      let responseData = response.order_technicians;

      responseData = Array.isArray(responseData) ? responseData[0] : responseData;

      dispatch({
        type: SERVICE.SET_PROGRESS_DETAIL,
        data: {
          progress: MapperService.mapResponseData("serviceProgressDetail", responseData),
          technicians: MapperService.mapResponseList("serviceTechnicianDetail", responseData.technicians),
        },
      });
    }
  );
}

// ----------------------------------------

export function getProgressRatings(data = {}, onSuccess:func) {
  return DataService.request(
    "getServiceOnProgressDetail",
    data,
    (response, dispatch) => {
      dispatch({
        type: SERVICE.SET_PROGRESS_RATING,
        data: {
          ratings: MapperService.mapResponseList("serviceRatingDetail", response.orders.ratings),
        },
      });
    }
  );
}

// ----------------------------------------

export function approveDocuments(data = {}, onSuccess:func, preFunction:func = null, postFunction:func = null) {
  return DataService.request(
    "confirmServiceDocuments",
    data,
    (response, dispatch) => {
      dispatch({
        type: SERVICE.CONFIRM_DOCUMENTS,
        data: {
          serviceRequestNumber: `${data.serviceRequestNumber}|||${data.quotationNumber}`,
        },
      });

      onSuccess();
    },
    preFunction,
    postFunction
  );
}

// ----------------------------------------

export function confirmSchedule(data = {}, onSuccess:func, preFunction:func = null, postFunction:func = null) {
  return DataService.request(
    "confirmServiceSchedule",
    data,
    (response, dispatch) => {
      dispatch({
        type: SERVICE.CONFIRM_SCHEDULE,
        data: {
          serviceOrderNumber: `${data.serviceOrderNumber}|||${data.eta}`,
        },
      });

      onSuccess();
    },
    preFunction,
    postFunction
  );
}

// ----------------------------------------

export function setCompleted(data = {}, onSuccess:func, preFunction:func = null, postFunction:func = null) {
  return DataService.direct(
    "",
    data,
    (response, dispatch) => {
      dispatch({
        type: SERVICE.SET_COMPLETED,
        data: {
          serviceRequestNumber: data.serviceRequestNumber,
        },
      });

      onSuccess();
    },
    preFunction,
    postFunction
  );
}
