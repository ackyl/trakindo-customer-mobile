/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  DataService,
  MapperService,
  ErrorReportingService,
} from "@core-services";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  ERROR,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function set(data = {}, onSuccess:func) {
  return DataService.direct(
    "",
    {},
    (response, dispatch) => {
      dispatch({
          type: ERROR.SET,
          data,
        });
    },
  );
}

// ----------------------------------------

export function setErrorFile(data = {}, onSuccess:func) {
  return DataService.direct(
    "",
    {},
    (response, dispatch) => {
      const additionals = {
        errorFile: JSON.stringify(data),
      };

      const error = {
        type: "FileNotFoundError",
        priority: 3,
        code: "404",
        message: "Getting File Failed",
        additionals,
      };

      ErrorReportingService.reportApiError(error);

      dispatch({
        type: ERROR.SET,
        data: error,
      });
    },
  );
}

// ----------------------------------------

export function reset(data = {}, onSuccess:func) {
  return DataService.direct(
    "",
    {},
    (response, dispatch) => {
      dispatch({
          type: ERROR.RESET,
          data,
        });
    },
  );
}

