/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  DataService,
  MapperService,
} from "@core-services";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  UNIT,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function getList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchUnitList",
    data,
    (response, dispatch) => {
      dispatch({
        type: data && data.family ? UNIT.SET_FILTERED_LIST : UNIT.SET_LIST,
        data: {
          page: data.page,
          activeFamily: data.family,
          units: MapperService.mapResponseList("unitDetail", response.units),
        },
      });
    },
    (dispatch) => {
      if (data.family && (!data.page || data.page < 2)) {
        dispatch({
          type: UNIT.RESET_FILTERED_LIST,
        });
      }
    }
  );
}

// ----------------------------------------

export function getDetail(data = {}, onSuccess:func) {
  return DataService.request(
    "getUnitDetail",
    data,
    (response, dispatch) => {
      dispatch({
        type: UNIT.SET_BASE,
        data: {
          base: MapperService.mapResponseData("unitDetail", response.unit),
        },
      });
    }
  );
}

// ----------------------------------------

export function getFamilyList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchUnitFamilyList",
    data,
    (response, dispatch) => {
      dispatch({
        type: UNIT.SET_FAMILY_LIST,
        data: {
          unitFamilies: MapperService.mapResponseList("unitFamilyDetail", response.unit_families),
        },
      });
    }
  );
}




