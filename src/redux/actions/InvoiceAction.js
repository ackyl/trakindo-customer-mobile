/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  DataService,
  MapperService,
} from "@core-services";

// ----------------------------------------
// EVENT IMPORTS
// ----------------------------------------
import {
  INVOICE,
} from "@app-events";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function getOverdueList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchInvoiceList",
    {
      ... data,
      status: "OPEN",
      isOverdue: true,
    },
    (response, dispatch) => {
      dispatch({
        type: INVOICE.SET_OVERDUE_LIST,
        data: {
          page: data.page,
          invoices: MapperService.mapResponseList("invoiceDetail", response.invoices),
        },
      });
    }
  );
}

// ----------------------------------------

export function getActiveList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchInvoiceList",
    {
      ... data,
      status: "OPEN",
      isOverdue: false,
    },
    (response, dispatch) => {
      dispatch({
        type: INVOICE.SET_LIST,
        data: {
          page: data.page,
          invoices: MapperService.mapResponseList("invoiceDetail", response.invoices),
        },
      });
    }
  );
}

// ----------------------------------------

export function getHistoryList(data = {}, onSuccess:func) {
  return DataService.request(
    "fetchInvoiceList",
    {
      ... data,
      status: "CLOSED",
    },
    (response, dispatch) => {
      dispatch({
        type: INVOICE.SET_HISTORY_LIST,
        data: {
          page: data.page,
          invoices: MapperService.mapResponseList("invoiceDetail", response.invoices),
        },
      });
    }
  );
}

// ----------------------------------------

export function getBrief(data = {}, onSuccess:func) {
  return DataService.request(
    "getInvoiceBrief",
    data,
    (response, dispatch) => {
      dispatch({
        type: INVOICE.SET_BRIEF,
        data: {
          invoiceBrief: MapperService.mapResponseData("invoiceBrief", response.invoice_brief),
        },
      });
    }
  );
}



