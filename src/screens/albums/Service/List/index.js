/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  ServiceList,
} from "@app-components-lists";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class List extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Default</TextH1>
            </Spacer>

            <ServiceList
              data={[
                {
                  type: "preventive",
                  model: "336 GC Excavator",
                  equipmentNumber: 5180334909,
                  serviceRequestNumber: 33449988,
                  serviceOrderNumber: 24890488,
                  serialNumber: "WE 12346783",
                  status: "QUOTED",
                },
                {
                  type: "preventive",
                  model: "336 GC Excavator",
                  equipmentNumber: 5180334909,
                  serviceRequestNumber: 33449988,
                  serviceOrderNumber: 24890488,
                  serialNumber: "WE 12346783",
                  status: "SCHEDULED",
                },
                {
                  type: "troubleshooting",
                  model: "336 GC Excavator",
                  equipmentNumber: 5180334909,
                  serviceRequestNumber: 33449988,
                  serviceOrderNumber: 24890488,
                  serialNumber: "WE 12346783",
                  status: "DISPATCHED",
                },
                {
                  type: "preventive",
                  model: "336 GC Excavator",
                  equipmentNumber: 5180334909,
                  serviceRequestNumber: 33449988,
                  serviceOrderNumber: 24890488,
                  serialNumber: "WE 12346783",
                  status: "IN_PROGRESS",
                },
                {
                  type: "troubleshooting",
                  model: "336 GC Excavator",
                  equipmentNumber: 5180334909,
                  serviceRequestNumber: 33449988,
                  serviceOrderNumber: 24890488,
                  serialNumber: "WE 12346783",
                  status: "COMPLETED",
                },
              ]}
              onItemPress={ (item, index) => alert(`Pressed on Service Card #${(index+1)}`) }
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading</TextH1>
            </Spacer>

            <ServiceList
              loading
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Empty</TextH1>
            </Spacer>

            <ServiceList
              order={ 1 }
            />
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
