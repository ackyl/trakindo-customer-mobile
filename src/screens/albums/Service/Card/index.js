/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  ServiceCard,
} from "@app-components-cards";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Card extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderServiceTracking( type:string = "preventive", loading:bool = false, status:string = "QUOTED") {
    return (
      <ServiceCard
        type={ type }
        loading={ loading }
        model={ "336 GC Excavator" }
        equipmentNumber={ 5180334909 }
        serviceRequestNumber={ 33449988 }
        serviceOrderNumber={ 24890488 }
        serialNumber={ "WE 12346783" }
        status={ status }
        onPress={ () => alert("Pressed on Service Card") }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading State</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderServiceTracking("preventive",true) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderServiceTracking() }
            </Spacer>
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Size</TextH1>
            </Spacer>

            <Spacer row>
              <Spacer flex={ 4 }>
                <Spacer space={ 2 }>
                  { this._renderServiceTracking("preventive",true) }
                </Spacer>
              <Spacer space={ 2 }>
                { this._renderServiceTracking() }
              </Spacer>
            </Spacer>

            <Spacer flex={ 1 }/>
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Status</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderServiceTracking("preventive", false, "QUOTED") }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderServiceTracking("preventive", false, "SCHEDULED") }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderServiceTracking("preventive", false, "DISPATCHED") }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderServiceTracking("preventive", false, "IN_PROGRESS") }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderServiceTracking("preventive", false, "COMPLETED") }
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Type</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderServiceTracking() }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderServiceTracking("troubleshooting") }
            </Spacer>
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
