/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  ServiceDetailCard,
} from "@app-components-cards";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class ServiceDetail extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderServiceTracking( loading:bool = false) {
    return (
      <ServiceDetailCard
        model={ "336 GC Excavator" }
        committedDate={ 1564854675000 }
        equipmentNumber={ 5180334909 }
        serviceRequestNumber={ 33449988 }
        serviceOrderNumber={ 24890488 }
        serialNumber={ "WE 12346783" }
        loading={ loading }
        onPress={ () => alert("Pressed on Service Detail Card") }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading State</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderServiceTracking(true) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderServiceTracking() }
            </Spacer>
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Size</TextH1>
            </Spacer>

            <Spacer row>
              <Spacer flex={ 4 }>
                <Spacer space={ 2 }>
                  { this._renderServiceTracking(true) }
                </Spacer>
              <Spacer space={ 2 }>
                { this._renderServiceTracking() }
              </Spacer>
            </Spacer>

            <Spacer flex={ 1 }/>
            </Spacer>
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
