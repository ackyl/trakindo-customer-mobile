/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  ServiceCompleteRatingList,
} from "@app-components-lists";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class SCRList extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Default</TextH1>
            </Spacer>

            <ServiceCompleteRatingList
              width={ Device.windows.width - Config.paddingHorizontal(2) }
              data={[
                {
                  stage: "Day 01",
                  status: "Service Completed",
                  date: 1570265036000,
                  rating: 3,
                },
                {
                  stage: "Day 02",
                  status: "Service Completed",
                  date: 1570265036000,
                  rating: 3,
                },
                {
                  stage: "Day 03",
                  status: "Service Completed",
                  date: 1570265036000,
                  rating: 3,
                },
                {
                  stage: "Day 04",
                  status: "Service Completed",
                  date: 1570265036000,
                  rating: 3,
                },
              ]}
              onItemPress={ (item, index) => alert(`Pressed on Service Rating Card #${(index+1)}`) }
              order={ 1 }
            />
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Size</TextH1>
            </Spacer>

            <Spacer width={ Device.windows.width - Config.paddingHorizontal(8) }>
              <ServiceCompleteRatingList
                width={ Device.windows.width - Config.paddingHorizontal(8) }
                data={[
                  {
                    stage: "Day 01",
                    status: "Service Completed",
                    date: 1570265036000,
                    rating: 3,
                  },
                  {
                    stage: "Day 02",
                    status: "Service Completed",
                    date: 1570265036000,
                    rating: 3,
                  },
                  {
                    stage: "Day 03",
                    status: "Service Completed",
                    date: 1570265036000,
                    rating: 3,
                  },
                  {
                    stage: "Day 04",
                    status: "Service Completed",
                    date: 1570265036000,
                    rating: 3,
                  },
                ]}
                onItemPress={ (item, index) => alert(`Pressed on Service Rating Card #${(index+1)}`) }
                order={ 1 }
              />
            </Spacer>
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading</TextH1>
            </Spacer>

            <ServiceCompleteRatingList
              width={ Device.windows.width - Config.paddingHorizontal(2) }
              loading
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Empty</TextH1>
            </Spacer>

            <ServiceCompleteRatingList
              width={ Device.windows.width - Config.paddingHorizontal(2) }
              order={ 1 }
            />
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
