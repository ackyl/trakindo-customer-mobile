/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  PartItemGroupList,
} from "@app-components-lists";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class List extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Default</TextH1>
            </Spacer>

            <PartItemGroupList
              data={[
                {
                  partNumber: "3051765",
                  description: "Cat DEO 15W-40 (5 L)",
                  originalEta: 1560189075000,
                  items: [
                    {
                      eta: 1559670675000,
                      quantity: 12,
                      trackable: false,
                      isDelayed: false,
                    },
                    {
                      eta: 1560361875000,
                      quantity: 67,
                      trackable: true,
                      isDelayed: true,
                    },
                    {
                      eta: 1559670675000,
                      quantity: 5,
                      trackable: true,
                      isDelayed: false,
                    },
                  ],
                },
                {
                  partNumber: "3051765",
                  description: "Cat DEO 15W-40 (5 L)",
                  originalEta: 1560189075000,
                  items: [
                    {
                      eta: 1559670675000,
                      quantity: 12,
                      pickingStatus: "ready",
                      isDelayed: false,
                    },
                    {
                      eta: 1560361875000,
                      quantity: 67,
                      pickingStatus: "outstanding",
                      isDelayed: false,
                    },
                    {
                      eta: 1559670675000,
                      quantity: 5,
                      pickingStatus: "taken",
                      isDelayed: false,
                    },
                  ],
                },
                {
                  partNumber: "3051765",
                  description: "Cat DEO 15W-40 (5 L)",
                  originalEta: 1560189075000,
                  items: [
                    {
                      eta: 1559670675000,
                      quantity: 12,
                      pickingStatus: "ready",
                      isDelayed: false,
                    },
                    {
                      eta: 1560361875000,
                      quantity: 67,
                      pickingStatus: "outstanding",
                      isDelayed: true,
                    },
                    {
                      eta: 1559670675000,
                      quantity: 5,
                      pickingStatus: "taken",
                      isDelayed: true,
                    },
                  ],
                },
              ]}
              onItemPress={ (item, groupIndex, index) => alert(`Press Track on item #${index} from group #${groupIndex}`) }
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading</TextH1>
            </Spacer>

            <PartItemGroupList
              loading
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Empty</TextH1>
            </Spacer>

            <PartItemGroupList
              order={ 1 }
            />
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
