/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  PartShippingTrackingCard,
} from "@app-components-cards";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartShippingTracking extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderPart(loading:bool = false) {
    return (
      <PartShippingTrackingCard
        statuses={[
          {
            location: "Singapore",
            lastUpdatedAt: 1569926932000,
            lastEta: 1569926932000,
            status: "Pick Up",
            statusCode: "PUP",
          },
          {
            location: "Singapore",
            lastUpdatedAt: 1569926932000,
            lastEta: 1569926932000,
            status: "Station Outbound Package",
            statusCode: "SOP",
          },
          {
            location: "Singapore",
            lastUpdatedAt: 1569926932000,
            lastEta: 1569926932000,
            status: "Transit Inbound",
            statusCode: "TRI",
          },
          {
            location: "Singapore",
            lastUpdatedAt: 1569926932000,
            lastEta: 1569926932000,
            status: "Delay (01)",
            statusCode: "DLY",
          },
          {
            location: "Singapore",
            lastUpdatedAt: 1569926932000,
            lastEta: 1569926932000,
            status: "Proof of Delivery",
            statusCode: "POD",
          },
        ]}
        loading={ loading }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading State</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart(true) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart() }
            </Spacer>
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Size</TextH1>
            </Spacer>

            <Spacer row>
              <Spacer flex={ 4 }>
                <Spacer space={ 2 }>
                  { this._renderPart(true) }
                </Spacer>

                <Spacer space={ 2 }>
                  { this._renderPart() }
                </Spacer>
              </Spacer>

              <Spacer flex={ 1 }/>
            </Spacer>
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
