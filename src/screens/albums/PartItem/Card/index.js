/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  PartItemGroupCard,
} from "@app-components-cards";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Card extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderPart(hasDelay:bool = true, loading:bool = false) {
    return (
      <PartItemGroupCard
        partNumber={ "3051765" }
        description={ "Cat DEO 15W-40 (5 L)" }
        originalEta={ 1560189075000 }
        items={[
          {
            eta: 1559670675000,
            quantity: 12,
            trackable: false,
            status: "ON_TIME",
          },
          {
            eta: 1560361875000,
            quantity: 67,
            trackable: true,
            status: hasDelay ? "DELAY" : "ON_TIME",
          },
          {
            eta: 1559670675000,
            quantity: 5,
            trackable: true,
            status: "ON_TIME",
          },
        ]}
        onItemPress={ (item, index) => alert(`Press Track on item #${index}`) }
        loading={ loading }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading State</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart(true, true) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart() }
            </Spacer>
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Size</TextH1>
            </Spacer>

            <Spacer row>
              <Spacer flex={ 4 }>
                <Spacer space={ 2 }>
                  { this._renderPart(true, true) }
                </Spacer>

                <Spacer space={ 2 }>
                  { this._renderPart() }
                </Spacer>
              </Spacer>

              <Spacer flex={ 1 }/>
            </Spacer>
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Delay Status</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart() }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart(false) }
            </Spacer>
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
