/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  PartItemDetailCard,
} from "@app-components-cards";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PartItemDetail extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderPart(isDelayed:bool = false, isOriginEmpty:bool = false) {
    return (
      <PartItemDetailCard
        description={ "Cat DEO 15W-40 (5 L)" }
        eta={ 1564854675000 }
        quantity={ 12 }
        origin={ !isOriginEmpty ? "Singapore" : null }
        destination={ "Manado" }
        isDelayed={ isDelayed }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Default</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart() }
            </Spacer>
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Size</TextH1>
            </Spacer>

            <Spacer row>
              <Spacer flex={ 4 }>
                <Spacer space={ 2 }>
                  { this._renderPart() }
                </Spacer>
              </Spacer>

              <Spacer flex={ 1 }/>
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Origin</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart(false, true) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart(false) }
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Status</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart(false) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart(true) }
            </Spacer>
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
