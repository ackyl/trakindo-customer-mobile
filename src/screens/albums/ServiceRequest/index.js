/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  ScrollView,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";

// ----------------------------------------
// ALBUM IMPORTS
import ServiceRequestCard from "./ServiceRequest";
import ServiceRequestUnitFormCard from "./ServiceRequestUnitForm";
import ServiceRequestScheduleFormCard from "./ServiceRequestScheduleForm";
import ServiceRequestBranchFormCard from "./ServiceRequestBranchForm";
import ServiceRequestPICFormCard from "./ServiceRequestPICForm";
import ServiceRequestNoteFormCard from "./ServiceRequestNoteForm";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Main extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderMain(name:string, component) {
    return (
      <Spacer style={{
        width: Device.windows.width,
      }}>
        <Spacer>
          <Padder>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold>{ name }</TextH1>
            </Spacer>
          </Padder>

          <Spacer style={{
            height: Config.base(.5),
            width: Config.base(7),
            backgroundColor: COLOR.orange,
          }}/>

        </Spacer>

        <ScrollView style={{
          flex: 0,
        }}>

          { component }

        </ScrollView>
      </Spacer>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer top={ 6 }>
        <ScrollView
          horizontal
          style={{
            flex: -1,
            width: Device.windows.width,
          }}
          pagingEnabled
        >
          { this._renderMain("ServiceRequestCard", <ServiceRequestCard/>) }
          { this._renderMain("ServiceRequestUnitFormCard", <ServiceRequestUnitFormCard/>) }
          { this._renderMain("ServiceRequestScheduleFormCard", <ServiceRequestScheduleFormCard/>) }
          { this._renderMain("ServiceRequestBranchFormCard", <ServiceRequestBranchFormCard/>) }
          { this._renderMain("ServiceRequestPICFormCard", <ServiceRequestPICFormCard/>) }
          { this._renderMain("ServiceRequestNoteFormCard", <ServiceRequestNoteFormCard/>) }
        </ScrollView>
      </Spacer>
    );
  }
}
