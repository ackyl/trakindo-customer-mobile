/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  InboxList,
} from "@app-components-lists";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class IList extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Default</TextH1>
            </Spacer>

            <InboxList
              data={[
                {
                  type: "invoice",
                  content: "Your account has been suspended because payment overdue, please pay this immedietly or call us for more information",
                  title: "You have overdue invoices",
                  date: 1570948595000,
                },
                {
                  type: "part",
                  content: "Your parts  SEAL-LIP TYP, from PO ZA_A304509 already left from origin facility to jakarta.",
                  title: "Parts left origin facility",
                  date: 1570948595000,
                },
                {
                  type: "service",
                  content: "Your parts  SEAL-LIP TYP already left from origin facility to jakarta.",
                  title: "Your service quotation is ready",
                  date: 1570948595000,
                },
                {
                  type: "credit",
                  content: "Your account has been suspended, call us to discuss more about your credit limit agreement",
                  title: "Your credit is over limit",
                  date: 1570948595000,
                },
              ]}
              onItemPress={ (item, index) => alert(`Pressed on Inbox Item Card #${(index+1)}`) }
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading</TextH1>
            </Spacer>

            <InboxList
              loading
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Empty</TextH1>
            </Spacer>

            <InboxList
              order={ 1 }
            />
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
