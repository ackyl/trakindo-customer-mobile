/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import moment from "moment";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  CalendarFragment,
} from "@app-components-fragments";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Calendar extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderPart(width = null) {
    return (
      <CalendarFragment
        width={ width ? width : Device.windows.width - Config.paddingHorizontal(2) }
        month={ 12 }
        year={ 2018 }
        selected={ 1571370596000 }
        // onSelectionChange={ (date) => alert(moment(date).format("DD-MM-YYYY")) }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Default</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderPart() }
            </Spacer>
          </Spacer>

          {/*<Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Size</TextH1>
            </Spacer>

            <Spacer row>
              <Spacer flex={ 4 }>
                <Spacer space={ 2 }>
                  { this._renderPart(Device.windows.width - Config.paddingHorizontal(4)) }
                </Spacer>
              </Spacer>

              <Spacer flex={ 1 }/>
            </Spacer>
          </Spacer>*/}
        </Padder>
      </Spacer>
    );
  }
}
