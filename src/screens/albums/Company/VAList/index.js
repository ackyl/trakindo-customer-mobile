/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  VAInfoList,
} from "@app-components-lists";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class VAList extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading State</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              <VAInfoList
                loading
                order={ 1 }
              />
            </Spacer>

            <Spacer space={ 2 }>
              <VAInfoList
                data={[
                  {
                    bank: "citibank",
                    virtualAccount: "9897 9117 9875 1235",
                    currency: "IDR",
                  },
                  {
                    bank: "citibank",
                    virtualAccount: "9897 9117 9875 1235",
                    currency: "IDR",
                  },
                  {
                    bank: "citibank",
                    virtualAccount: "9897 9117 9875 1235",
                    currency: "IDR",
                  },
                ]}
                order={ 1 }
              />
            </Spacer>
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
