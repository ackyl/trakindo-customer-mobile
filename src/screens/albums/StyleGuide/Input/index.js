/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  InputString,
} from "@app-components-core/Input";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Input extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderMain(mode:string = "dark") {
    return (
      <Padder>
        <Spacer space={ 2 }>
          <InputString
            mode={ mode }
            label={ "Input String" }
          />

          <Spacer space={ 1 }/>

          <InputString
            mode={ mode }
            value={ "Has Value" }
          />

          <Spacer space={ 1 }/>

          <InputString
            mode={ mode }
            error={ "Error Message" }
          />
        </Spacer>
      </Padder>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        <Spacer top={ 2 } backgroundColor={ COLOR.black }>
          { this._renderMain() }
        </Spacer>

        <Spacer top={ 2 } space={ 2 } backgroundColor={ COLOR.white }>
          { this._renderMain("light") }
        </Spacer>
      </Spacer>
    );
  }
}
