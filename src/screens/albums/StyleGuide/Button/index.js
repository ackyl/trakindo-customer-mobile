/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  ButtonFullPrimary,
  ButtonFullSecondary,
  ButtonCompactPrimary,
  ButtonCompactSecondary,
} from "@app-components-core/Button";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Button extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderMain(mode:string = "dark") {
    return (
      <Padder>
        <Spacer space={ 2 }>
          <ButtonFullPrimary mode={ mode }>Full Primary</ButtonFullPrimary>

          <Spacer space={ 1 }/>

          <ButtonFullPrimary mode={ mode } disabled>Full Primary Disabled</ButtonFullPrimary>
        </Spacer>

        <Spacer space={ 2 }>
          <ButtonFullSecondary mode={ mode }>Full Secondary</ButtonFullSecondary>

          <Spacer space={ 1 }/>

          <ButtonFullSecondary mode={ mode } disabled>Full Secondary Disabled</ButtonFullSecondary>
        </Spacer>

        <Spacer space={ 2 }>
          <ButtonCompactPrimary mode={ mode }>Compact Primary</ButtonCompactPrimary>

          <Spacer space={ 1 }/>

          <ButtonCompactPrimary mode={ mode } disabled>Compact Primary Disabled</ButtonCompactPrimary>
        </Spacer>

        <Spacer space={ 2 }>
          <ButtonCompactSecondary mode={ mode }>Compact Secondary</ButtonCompactSecondary>

          <Spacer space={ 1 }/>

          <ButtonCompactSecondary mode={ mode } disabled>Compact Secondary Disabled</ButtonCompactSecondary>
        </Spacer>

      </Padder>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        <Spacer top={ 2 } backgroundColor={ COLOR.black }>
          { this._renderMain() }
        </Spacer>

        <Spacer top={ 2 } space={ 2 } backgroundColor={ COLOR.white }>
          { this._renderMain("light") }
        </Spacer>
      </Spacer>
    );
  }
}
