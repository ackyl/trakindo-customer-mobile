/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  InvoiceList,
} from "@app-components-lists";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class List extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Default</TextH1>
            </Spacer>

            <InvoiceList
              data={[
                {
                  invoiceNumber: "5461278965",
                  dueDate: 1562979224000,
                  poNumbers: ["ZA_A30"],
                  orderType: "part",
                  currency: "IDR",
                  amount: 50000000,
                  status: "OPEN",
                  isOverdue: true,
                },
                {
                  invoiceNumber: "5461278965",
                  dueDate: 1562979224000,
                  poNumbers: ["ZA_A30", "ZA_A27"],
                  orderType: "part",
                  currency: "IDR",
                  amount: 50000000,
                  status: "OPEN",
                  isOverdue: false,
                },
                {
                  invoiceNumber: "5461278965",
                  dueDate: 1562979224000,
                  poNumbers: ["ZA_A30"],
                  orderType: "service",
                  currency: "IDR",
                  amount: 50000000,
                  status: "CLOSED",
                  isOverdue: false,
                },
              ]}
              onItemPress={ (item, index) => alert(`Pressed on Invoice Card #${(index+1)}`) }
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading</TextH1>
            </Spacer>

            <InvoiceList
              loading
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Empty</TextH1>
            </Spacer>

            <InvoiceList
              order={ 1 }
            />
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
