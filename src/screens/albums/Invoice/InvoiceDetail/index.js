/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  InvoiceDetailCard,
} from "@app-components-cards";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class InvoiceDetail extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderInvoice(orderType:string, status:string = "OPEN", isOverdue:bool = false, poNumbers:array = ["ZA_A30"]) {
    return (
      <InvoiceDetailCard
        invoiceNumber={ "5461278965" }
        dueDate={ 1562979224000 }
        poNumbers={ poNumbers }
        orderType={ orderType }
        currency={ "IDR" }
        amount={ 50000000 }
        status={ status }
        isOverdue={ isOverdue }
        onPress={ () => alert("Pressed on Invoice Card") }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Default</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderInvoice("part") }
            </Spacer>
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Size</TextH1>
            </Spacer>

            <Spacer row>
              <Spacer flex={ 4 }>
                <Spacer space={ 2 }>
                  { this._renderInvoice("part") }
                </Spacer>
              </Spacer>

              <Spacer flex={ 1 }/>
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">PO Numbers</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderInvoice("part", "OPEN", false) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderInvoice("part", "OPEN", false, [
                "ZA_A30",
                "ZA_A41",
                "ZA_A25",
                "ZA_A17",
                "ZA_A28",
              ]) }
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Order Type</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderInvoice("part") }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderInvoice("service") }
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Status</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderInvoice("part", "OPEN", true) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderInvoice("part", "OPEN", false) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderInvoice("part", "CLOSED") }
            </Spacer>
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
