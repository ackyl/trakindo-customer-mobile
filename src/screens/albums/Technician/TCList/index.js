/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  TechnicianCallList,
} from "@app-components-lists";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class TCList extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Default</TextH1>
            </Spacer>

            <TechnicianCallList
              data={[
                {
                  name: "Budiono Susanto",
                  phoneNumber: "081385217242",
                  imageUri: "https://imgd.aeplcdn.com/1280x720/n/bw/models/colors/suzuki-burgman-street-pearl-mirage-white-1531998385099.jpg?20190103151915&q=80",
                },
                {
                  name: "Bambang Prayono",
                  phoneNumber: "0819678567",
                  imageUri: "https://imgd.aeplcdn.com/1280x720/n/bw/models/colors/suzuki-burgman-street-pearl-mirage-white-1531998385099.jpg?20190103151915&q=80",
                },
                {
                  name: "Budiono Susanto",
                  phoneNumber: "0819678567",
                  imageUri: "https://imgd.aeplcdn.com/1280x720/n/bw/models/colors/suzuki-burgman-street-pearl-mirage-white-1531998385099.jpg?20190103151915&q=80",
                },
              ]}
              onItemPress={ (item, index) => alert(`Pressed on Technician Call Card #${(index+1)}`) }
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading</TextH1>
            </Spacer>

            <TechnicianCallList
              loading
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Empty</TextH1>
            </Spacer>

            <TechnicianCallList
              order={ 1 }
            />
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
