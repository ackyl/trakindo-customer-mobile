/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  CreditCard,
} from "@app-components-cards";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Card extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderCredit(exposure:number = 500000000, hasOverdue:bool = false, isUnavailable:bool = false, loading:bool = false) {
    return (
      <CreditCard
        payerId={ "P0004742" }
        currency={ "IDR" }
        remaining={ 250000000 }
        limit={ 750000000 }
        exposure={ exposure }
        hasOverdue={ hasOverdue }
        isUnavailable={ isUnavailable }
        loading={ loading }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading State</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderCredit(500000000, false, false, true) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderCredit() }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderCredit(0, false, true) }
            </Spacer>
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Size</TextH1>
            </Spacer>

            <Spacer row>
              <Spacer flex={ 4 }>
                <Spacer space={ 2 }>
                  { this._renderCredit(500000000, false, false, true) }
                </Spacer>

                <Spacer space={ 2 }>
                  { this._renderCredit(500000000, true) }
                </Spacer>

                <Spacer space={ 2 }>
                  { this._renderCredit(0, false, true) }
                </Spacer>
              </Spacer>

              <Spacer flex={ 1 }/>
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Has Overdue Status</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderCredit(500000000, false) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderCredit(500000000, true) }
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Overlimit Status</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderCredit(500000000) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderCredit(800000000) }
            </Spacer>
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
