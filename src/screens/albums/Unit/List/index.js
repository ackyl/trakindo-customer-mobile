/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  UnitList,
} from "@app-components-lists";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class List extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Default</TextH1>
            </Spacer>

            <UnitList
              data={[
                {
                  model: "140M/140M AWD",
                  equipmentNumber: "1500228",
                  imageUri: "https://imgd.aeplcdn.com/1280x720/n/bw/models/colors/suzuki-burgman-street-pearl-mirage-white-1531998385099.jpg?20190103151915&q=80",
                  lastServiceDate: 1562979224000,
                  isUnderWarranty: true,
                },
                {
                  model: "140M/140M AWD",
                  equipmentNumber: "1500228",
                  imageUri: "https://imgd.aeplcdn.com/1280x720/n/bw/models/colors/suzuki-burgman-street-pearl-mirage-white-1531998385099.jpg?20190103151915&q=80",
                  lastServiceDate: 1562979224000,
                  isUnderWarranty: false,
                },
                {
                  model: "140M/140M AWD",
                  equipmentNumber: "1500228",
                  imageUri: "https://imgd.aeplcdn.com/1280x720/n/bw/models/colors/suzuki-burgman-street-pearl-mirage-white-1531998385099.jpg?20190103151915&q=80",
                  lastServiceDate: 0,
                  isUnderWarranty: true,
                },
              ]}
              onItemPress={ (item, index) => alert(`Pressed on Unit Card #${(index+1)}`) }
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading</TextH1>
            </Spacer>

            <UnitList
              loading
              order={ 1 }
            />
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Empty</TextH1>
            </Spacer>

            <UnitList
              order={ 1 }
            />
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
