/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  UnitCard,
} from "@app-components-cards";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Card extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderUnit(lastServiceDate:number =  1562979224000, isUnderWarranty:bool = true, loading:bool = false) {
    return (
      <UnitCard
        model={ "140M/140M AWD" }
        equipmentNumber={ "1500228" }
        imageUri={ "https://imgd.aeplcdn.com/1280x720/n/bw/models/colors/suzuki-burgman-street-pearl-mirage-white-1531998385099.jpg?20190103151915&q=80" }
        lastServiceDate={ lastServiceDate }
        isUnderWarranty={ isUnderWarranty }
        loading={ loading }
        onPress={ () => alert("Pressed on Unit Card") }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Loading State</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderUnit(1562979224000, true, true) }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderUnit() }
            </Spacer>
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Size</TextH1>
            </Spacer>

            <Spacer row>
              <Spacer flex={ 4 }>
                <Spacer space={ 2 }>
                  { this._renderUnit(1562979224000, true, true) }
                </Spacer>

                <Spacer space={ 2 }>
                  { this._renderUnit() }
                </Spacer>
              </Spacer>

              <Spacer flex={ 1 }/>
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Last Service</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderUnit() }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderUnit(0) }
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Warranty Status</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderUnit() }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderUnit(1562979224000, false) }
            </Spacer>
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
