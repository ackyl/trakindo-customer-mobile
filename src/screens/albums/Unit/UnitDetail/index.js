/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
const COLOR = Config.theme("Color.BASE");

// ----------------------------------------
// COMPONENTS IMPORTS
// ----------------------------------------
import {
  TextH1,
} from "@app-components-core/Text";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  UnitDetailCard,
} from "@app-components-cards";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class UnitDetail extends Component {

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderUnit(isUnderWarranty:bool = true, hasLastService:bool = true) {
    const {
      lastServiceDate,
      lastServiceType,
    } = hasLastService ? {
      lastServiceDate: 1562979224000,
      lastServiceType: "Preventive Maintenance",
    } : {};

    return (
      <UnitDetailCard
        model={ "D3K2 Tier 4 Final" }
        equipmentNumber={ "1500228" }
        imageUri={ "https://imgd.aeplcdn.com/1280x720/n/bw/models/colors/suzuki-burgman-street-pearl-mirage-white-1531998385099.jpg?20190103151915&q=80" }
        serialNumber={ "IDC BJ600838" }
        smu={ "IDC BJ600838" }
        warantyExpirationDate={ 1562979224000 }
        lastServiceDate={ lastServiceDate }
        lastServiceType={ lastServiceType }
        location={ "Jl. Cipendawa Baru, Bojong Menteng, Rawalumbu, Kota Bks, Jawa Barat 17117" }
        isUnderWarranty={ isUnderWarranty }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer backgroundColor={ COLOR.black }>
        <Padder>
          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Default</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderUnit() }
            </Spacer>
          </Spacer>

          <Spacer top={ 1 } space={ 1 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Size</TextH1>
            </Spacer>

            <Spacer row>
              <Spacer flex={ 4 }>
                <Spacer space={ 2 }>
                  { this._renderUnit() }
                </Spacer>
              </Spacer>

              <Spacer flex={ 1 }/>
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Last Service</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderUnit() }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderUnit(true, false) }
            </Spacer>
          </Spacer>

          <Spacer space={ 2 }>
            <Spacer top={ 1 } space={ 1 }>
              <TextH1 bold mode="light2">Warranty Status</TextH1>
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderUnit() }
            </Spacer>

            <Spacer space={ 2 }>
              { this._renderUnit(false) }
            </Spacer>
          </Spacer>
        </Padder>
      </Spacer>
    );
  }
}
