/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";
import {
  Keyboard,
} from "react-native";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextHero,
  TextBM,
} from "@app-components-core/Text";
import {
  ImageLocal,
} from "@app-components-core/Image";
import {
  LoginFormCard,
} from "@app-components-cards";
import {
  FederatedLoginFragment,
} from "@app-components-fragments";
import BaseContainer from "@app-components-containers/BasePatterned";
import {
  Spacer,
  Padder,
  VerticalAnimator,
} from "@core-components-enhancers";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  LoadBlocker as LoadBlockerModal,
} from "@app-modals/Misc";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as AuthSelector from "@app-selectors/auth";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  AuthAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Login extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),

      isKeyboarUp: false,
    };

    if (!Device.isAndroid){
      Keyboard.addListener("keyboardWillShow", () => {
        this.setState({isKeyboarUp: true});
      });

      Keyboard.addListener("keyboardWillHide", () => {
        this.setState({isKeyboarUp: false});
      });
    } else {
      Keyboard.addListener("keyboardDidShow", () => {
        this.setState({isKeyboarUp: true});
      });

      Keyboard.addListener("keyboardDidHide", () => {
        this.setState({isKeyboarUp: false});
      });
    }
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("deviceId", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  setAuthTokens(tokens, attributes) {
    const roleId = 666;
    // const roleId = attributes["custom:role"] ? attributes["custom:role"] : 1;

    const data = {
      accessToken: tokens.accessToken.jwtToken,
      refreshToken: tokens.refreshToken.token,
      idToken: tokens.idToken.jwtToken,

      userId: attributes.sub,
      roleId,
      soldtoId: attributes["custom:soldto_id"] ? attributes["custom:soldto_id"] : "",
      salesRepId: attributes["custom:salesRepId"] ? attributes["custom:salesRepId"] : "",

      deviceId: this.Base.data("deviceId"),
    };

    if (roleId > 1) {
      this.props.login(
        data,
        () => {
          if (roleId > 4) {
            Navigation.replace("CompanySelection");
          } else {
            Navigation.replace("Home");
          }
        },
        null,
        () => this.Base.closeModal("LoadBlocker"),
      );
    } else {
      this.Base.closeModal("LoadBlocker");
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderMainLogo() {
    return (
      <VerticalAnimator order={ 1 }>
        <Spacer space={ 4 } top={ 8 } hAlign="center">
          <ImageLocal name="main.app_logo"/>
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderWelcomeText() {
    return (
      <Spacer space={ 3 } hAlign="center">
        <VerticalAnimator order={ 2 }>
          <Spacer space={ 1 }>
            <TextHero
              mode="light"
            >
              Welcome
            </TextHero>
          </Spacer>
        </VerticalAnimator>

        <VerticalAnimator order={ 3 }>
          <TextBM
            mode="light"
          >
            Login to Trakindo Customer App
          </TextBM>
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderForm() {
    return (
      <VerticalAnimator order={ 4 }>
        <LoginFormCard
          onLoginStart={ () => this.Base.openModal("LoadBlocker") }
          onLoginError={ () => this.Base.closeModal("LoadBlocker") }
          onCognitoLoginSucces={ (tokens, attributes) => this.setAuthTokens(tokens, attributes) }
        />
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderFederatedButton() {
    return (
      <VerticalAnimator order={ 6 }>
        <FederatedLoginFragment
          onLoginStart={ () => this.Base.openModal("LoadBlocker") }
          onLoginError={ () => this.Base.closeModal("LoadBlocker") }
          onCognitoLoginSucces={ (tokens, attributes) => this.setAuthTokens(tokens, attributes) }
        />
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderDivider() {
    return (
      <VerticalAnimator order={ 5 }>
        <Spacer row top={ 4 } bottom={ 4 } vAlign="center">
          <Spacer flex={ 1 } borderBottomColor={ COLOR.whiteT4 }/>

          <Spacer horizontal space={ 1 }/>

          <TextBM
            mode="light"
          >
            OR
          </TextBM>

          <Spacer horizontal space={ 1 }/>

          <Spacer flex={ 1 } borderBottomColor={ COLOR.whiteT4 }/>
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderBackgroundImage() {
    return (
      <ImageLocal relWidth={ Device.windows.width + 2 } name="login.background"/>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <BaseContainer
        backgroundImage={ this._renderBackgroundImage() }
        noBounce
      >
        <Padder>
          { this._renderMainLogo() }

          { this._renderWelcomeText() }

          { this._renderForm() }

          { this._renderDivider() }

          { this._renderFederatedButton() }
        </Padder>

        <LoadBlockerModal
          isActive={ this.Base.isModalActive("LoadBlocker") }
        />
      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    deviceId: AuthSelector.deviceId(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    login: (data, onComplete, preFunc, postFunc) => dispatch(AuthAction.loginCustomer(data, onComplete, preFunc, postFunc)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Login);

