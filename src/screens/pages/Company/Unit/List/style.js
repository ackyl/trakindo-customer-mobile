/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";
import {
  COLOR,
  TABS,
} from "./consts";


export default {
  tabHead: {
    container: {
      backgroundColor: COLOR.gray,
    },

    innerContainer: {
      justifyContent: "center",
      alignItems: "center",
      overflow: "hidden",
    },

    item: {
      container: {
        flex: 1,
        paddingHorizontal: Config.base(1.5),
        paddingVertical: Config.base(1),
        justifyContent: "center",
        alignItems: "center",
        borderRadius: Config.base(1),
      },
    },
  },

  list: {
    container: {
      flex: 1,
    },

    contentContainer: {
      paddingVertical: Config.base(2),
    },
  },
};
