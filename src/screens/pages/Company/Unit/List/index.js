/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import BaseContainer from "@app-components-containers/Base";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  UnitList,
} from "@app-components-lists";
import {
  FilterBarFragment,
} from "@app-components-fragments";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as UnitSelector from "@app-selectors/unit";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  UnitAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class List extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),

      activeIndex: {
        familyFilter: 0,
      },
    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getUnitList(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("familyList", nextProps, nextState);

    this.Base.setData("list", nextProps, nextState);

    if (nextState.activeIndex.familyFilter > 0) {
      this.Base.setData("filteredList", nextProps, nextState);
    }

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getUnitList(newPage:number = 1, family:string = "", dataType:string = "list") {
    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getUnitList({
      soldtoId,
      page: newPage,
      family,
    });

    this.Base.setPage(dataType, newPage);
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  getSelectedFamily() {
    const families = this.Base.data("familyList");
    const selectedFamily = families[this.state.activeIndex.familyFilter - 1];

    return selectedFamily ? selectedFamily : {
      name: "All Equipment",
      slug: "",
    };
  }

// ----------------------------------------

  onUnitSelected(data) {
    Navigation.push("UnitDetail", {unitDetail: data});
  }

// ----------------------------------------

  onFilterSelected(item, index) {
    const {
      activeIndex,
    } = this.state;

    if (activeIndex.familyFilter === index) {
      return null;
    }

    activeIndex.familyFilter = index;

    this.Base.resetList("filteredList", {activeIndex});

    const selectedFamily = this.getSelectedFamily();

    const dataType = index === 0 ? "list" : "filteredList";

    this.getUnitList(1, selectedFamily.slug, dataType);

    this.Base.closeModal("FamilyFilter");
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderFilterHead() {
    return (
      <Spacer top={ 1 } backgroundColor={ COLOR.gray }>
        <Padder>
          <FilterBarFragment
            onChange={ (item, index) => this.onFilterSelected(item, index) }
          />
        </Padder>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderList() {
    const index = this.state.activeIndex.familyFilter;

    const dataType = index === 0 ? "list" : "filteredList";
    const selectedFamily = this.getSelectedFamily();
    const dataName = selectedFamily.name;

    const data = this.Base.data(dataType);

    return (
      <Spacer flex={ 1 }>
        <UnitList
          data={ data }
          onItemPress={ (itemData) => this.onUnitSelected(itemData) }
          loading={ this.Base.isLoading(dataType) }
          page={ this.Base.currentPage(dataType) }
          emptyText={ `No ${dataName} Found` }
          loadNext={ (page) => this.getUnitList(page, selectedFamily.slug, dataType) }
          order={ 2 }
          key={ index }
          padded
        />
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <BaseContainer
        title="Registered Equipments"
        leftHeaderMode="back"
        onLeftHeaderPress={ () => Navigation.pop() }
        onBackPress={ () => Navigation.pop() }
        solidHeader
        static
      >

        { this._renderFilterHead() }

        { this._renderList() }

      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    familyList: UnitSelector.familyList(state, props),

    list: UnitSelector.list(state, props),
    filteredList: UnitSelector.filteredList(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getUnitList: (data, onComplete) => dispatch(UnitAction.getList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(List);

