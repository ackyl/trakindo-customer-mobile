/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
} from "@core-utils";
import {
  COLOR,
} from "./consts";



export default {
  companyList: {
    item: {
      container: {
        flex: -1,
        flexDirection: "row",
        borderRadius: 8,
        backgroundColor: COLOR.grayLight,
        paddingHorizontal: Config.base(1.5),
        paddingVertical: Config.base(2),
      },

      icon: {
        container: {
          flex: -1,
          height: Config.base(5),
          width: Config.base(5),
          borderRadius: 50,
          backgroundColor: COLOR.black,
          marginRight: Config.base(2),
        },
      },
    },
  },
};
