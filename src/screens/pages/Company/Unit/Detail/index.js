/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullPrimary,
} from "@app-components-core/Button";
import BaseContainer from "@app-components-containers/Base";
import {
  Padder,
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  UnitDetailCard,
} from "@app-components-cards";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  SMUExplanation as SMUExplanationModal,
} from "@app-modals/Unit";
import {
  RequestTypeSelection as RequestTypeSelectionModal,
} from "@app-modals/Service";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as AuthSelector from "@app-selectors/auth";
import * as CreditSelector from "@app-selectors/credit";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  CreditAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Detail extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const basedData = this.Base.initBaseState(DATA, MODAL);
    basedData.data.unitDetail = props.navData.unitDetail;
    basedData.data.isFromService = props.navData.isFromService ? props.navData.isFromService : false;

    this.state = {
      ... basedData,
    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getPayerList(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("modules", nextProps, nextState);

    this.Base.setData("payer", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getPayerList() {
    if (this.Base.data("payer")) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getPayerList({
      soldtoId,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  isServiceAllowed() {
    const modules = this.Base.data("modules");

    return modules.indexOf("SERVICE") > -1 && !this.Base.data("isFromService");
  }

// ----------------------------------------

  openServiceOption() {
    if (!this.Base.data("payer")) {
      return null;
    }

    this.Base.openModal("RequestTypeSelection");
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderDetail() {
    const data = this.Base.data("unitDetail");

    return (
      <Spacer space={ 2.5 }>
        <VerticalAnimator order={ 1 }>
          <UnitDetailCard
            { ... data }
            onSMUHintPressed={ () => this.Base.openModal("SMUExplanation") }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButton() {
    if (!this.isServiceAllowed()) {
      return null;
    }

    return (
      <VerticalAnimator order={ 2 }>
        <Spacer space={ 1 }>
          <ButtonFullPrimary
            onPress={ () => this.openServiceOption() }
          >
            Service Equipment
          </ButtonFullPrimary>
        </Spacer>
      </VerticalAnimator>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <BaseContainer
        title="Equipment"
        leftHeaderMode="back"
        onLeftHeaderPress={ () => Navigation.pop() }
      >

        <Spacer top={ 1 } overflowable>
          <Padder overflowable>
            { this._renderDetail() }

            { this._renderButton() }
          </Padder>
        </Spacer>

        <SMUExplanationModal
          isActive={ this.Base.isModalActive("SMUExplanation") }
          onClosePress={ () => this.Base.closeModal("SMUExplanation") }
        />

        <RequestTypeSelectionModal
          isActive={ this.Base.isModalActive("RequestTypeSelection") }
          onClosePress={ () => this.Base.closeModal("RequestTypeSelection") }
          navData={{
            unitDetail: this.Base.data("unitDetail"),
          }}
        />
      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    modules: AuthSelector.accessibleModules(state, props),

    payer: CreditSelector.payer(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getPayerList: (data, onComplete) => dispatch(CreditAction.getPayerList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Detail);

