/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";
import {
  ScrollView,
  PanResponder,
  Animated,
} from "react-native";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import BaseContainer from "@app-components-containers/BasePatterned";
import {
  Padder,
  Spacer,
} from "@core-components-enhancers";
import {
  ServiceDetailCard,
} from "@app-components-cards";
import {
  ServiceCompleteRatingList,
  ServiceProgressTrackingItemList,
} from "@app-components-lists";
import Scheduling from "./Scheduling";
import OnTheWay from "./OnTheWay";
import InProgress from "./InProgress";
import Completed from "./Completed";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as ServiceSelector from "@app-selectors/service";
import * as UnitSelector from "@app-selectors/unit";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  UnitAction,
} from "@app-actions";

const BASE_PAN = Device.windows.height - Config.base(35);
const START_PAN = -Config.base(27);
const END_PAN = Config.base(0);




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Detail extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const basedData = this.Base.initBaseState(DATA, MODAL);
    basedData.data.serviceData = this.props.navData.serviceData;

    this.state = {
      ... basedData,

      status: this.props.navData.serviceData ? this.props.navData.serviceData.status : null,

      detailPosition: new Animated.Value(START_PAN),
      detailVisibility: new Animated.Value(0),
      detailMode: "compact",
    };

    this._panResponsder = PanResponder.create({
      onStartShouldSetPanResponder: (event, panState) => true,
      onPanResponderMove: (event, panState) => this.onPanResponsderMove(event, panState),
      onPanResponderRelease: (event, panState) => this.onPanResponderRelease(event, panState),
    });
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getUnit(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);

    this.Base.setData("unit", nextProps, nextState);
    this.Base.setData("ratingList", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getUnit() {
    if (this.Base.data("unit")) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    const {
      serialNumber,
    } = this.Base.data("serviceData");

    this.props.getUnit({
      soldtoId,
      serialNumber,
    });
  }

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  getProcessIndex() {
    const index = 4;

    return index;
  }

// ----------------------------------------

  onPanResponsderMove(event, panState) {
    let newValue = BASE_PAN - panState.moveY;

    if (newValue <= START_PAN) {
      newValue = START_PAN;
    } else if (newValue >= 0) {
      newValue = END_PAN;
    }

    Animated.timing(this.state.detailPosition, {
      toValue: newValue,
      duration: 0,
      delay: 0,
    }).start();

    Animated.timing(this.state.detailVisibility, {
      toValue: (newValue + (-START_PAN)) / (-START_PAN),
      duration: 0,
      delay: 0,
    }).start();

    if (newValue < -Config.base(24)) {
      this.setState({
        detailMode: "compact",
      });
    } else {
      this.setState({
        detailMode: "full",
      });
    }
  }

// ----------------------------------------

  onPanResponderRelease(event, panState) {
    let newValue = BASE_PAN - panState.moveY;

    if (newValue <= START_PAN) {
      newValue = START_PAN;
    } else if (newValue >= 0) {
      newValue = END_PAN;
    }

    if (newValue > (START_PAN * (1 / 2))) {
      Animated.timing(this.state.detailPosition, {
        toValue: 0,
        duration: 0,
        delay: 0,
      }).start();

      Animated.timing(this.state.detailVisibility, {
        toValue: 1,
        duration: 0,
        delay: 0,
      }).start();

      this.setState({
        detailMode: "full",
      });
    } else {
      Animated.timing(this.state.detailPosition, {
        toValue: START_PAN,
        duration: 0,
        delay: 0,
      }).start();

      Animated.timing(this.state.detailVisibility, {
        toValue: END_PAN,
        duration: 0,
        delay: 0,
      }).start();

      this.setState({
        detailMode: "compact",
      });
    }
  }

// ----------------------------------------

  switchProcess(process) {
    this.setState({
      status: process,
    });
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTop() {
    const {
      status,
    } = this.state;

    return (
      <Spacer top={ 1 }>
        <ServiceProgressTrackingItemList
          status={ status }
          order={ 1 }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderRatings() {
    const {
      status,
    } = this.state;

    if (status !== "IN_PROGRESS") {
      return null;
    }

    const data = this.Base.data("ratingList");

    return (
      <Spacer>
        <ServiceCompleteRatingList
          data={ data }
          loading={ this.Base.isLoading("ratingList") }
          order={ 1 }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderContent() {
    const {
      status,
    } = this.state;

    switch (status) {
      case "SCHEDULED":
        return (
          <Scheduling
            navData={ this.props.navData }
            switchProcess={ (process) => this.switchProcess(process) }
          />
        );

      case "DISPATCHED":
        return (
          <OnTheWay
            navData={ this.props.navData }
          />
        );

      case "IN_PROGRESS":
        return (
          <InProgress
            navData={ this.props.navData }
            switchProcess={ (process) => this.switchProcess(process) }
          />
        );

      case "COMPLETED":
        return (
          <Completed
            navData={ this.props.navData }
          />
        );
    }
  }

// ----------------------------------------

  _renderDetail() {
    const data = this.Base.data("serviceData");
    const unit = this.Base.data("unit") ? this.Base.data("unit") : {};

    return (
      <Animated.View
        style={{
          position: "absolute",
          bottom: this.state.detailPosition,
        }}
        { ...this._panResponsder.panHandlers }
      >
        <Spacer
          animated
          backgroundColor={ COLOR.white }
          radius={ Config.base(1) }
          width={ Device.windows.width }
        >
          <Spacer
            height={ Config.base(2) }
            hAlign="center"
            vAlign="center"
          >
            <Spacer
              backgroundColor={ COLOR.grayT4 }
              width={ Config.base(7) }
              height={ Config.base(.5) }
              radius={ 50 }
            />
          </Spacer>

          <ServiceDetailCard
            { ... unit }
            { ... data }
            loading={ this.Base.isLoading("unit") }
            compact={ this.state.detailMode === "compact" }
            visibility={ this.state.detailVisibility }
            onPress={ () => Navigation.push("UnitDetail", {
              unitDetail: unit,
              isFromService: true,
            }) }
          />
        </Spacer>
      </Animated.View>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      type,
    } = this.Base.data("serviceData");

    return (
      <BaseContainer
        static
        title="Service Detail"
        // title={ type === "preventive" ? "Preventive Maintenance" : "Troubleshooting" }
        leftHeaderMode="back"
        onLeftHeaderPress={ () => Navigation.pop() }
      >
        { this._renderTop() }

        { this._renderRatings() }

        <Spacer flex={ 1 } bottom={ 12 }>
          <Padder flex={ 1 }>
            <ScrollView>
              { this._renderContent() }
            </ScrollView>
          </Padder>
        </Spacer>

        { this._renderDetail() }
      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),

    unit: UnitSelector.selected(state, props),
    ratingList: ServiceSelector.ratings(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getUnit: (data, onComplete) => dispatch(UnitAction.getDetail(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
