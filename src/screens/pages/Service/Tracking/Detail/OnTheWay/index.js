/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  EstimatedArrivalCard,
} from "@app-components-cards";
import {
  SegmentTitleFragment,
} from "@app-components-fragments";
import {
  TechnicianCallList,
} from "@app-components-lists";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as ServiceSelector from "@app-selectors/service";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  ServiceAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class OnTheWay extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const basedData = this.Base.initBaseState(DATA, MODAL);
    basedData.data.serviceData = this.props.navData.serviceData;

    this.state = {
      ... basedData,

    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getProgress(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);

    this.Base.setData("progress", nextProps, nextState);
    this.Base.setData("technicianList", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getProgress() {
    if (this.Base.data("progress")) {
      return false;
    }

    const {
      serviceOrderNumber,
      status,
    } = this.Base.data("serviceData");

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getProgress({
      soldtoId,
      serviceOrderNumber,
      status,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderETA() {
    const data = this.Base.data("progress");

    return (
      <Spacer space={ 2.5 }>
        <VerticalAnimator order={ 1 }>
          <EstimatedArrivalCard
            { ... data }
            loading={ this.Base.isLoading("progress") }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderTechnicians() {
    const data = this.Base.data("technicianList");

    return (
      <Spacer space={ 2.5 }>
        <SegmentTitleFragment order={ 2 }>
          Technicians
        </SegmentTitleFragment>

        <VerticalAnimator order={ 3 }>
          <TechnicianCallList
            data={ data }
            loading={ this.Base.isLoading("technicianList") }
            order={ 2 }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        { this._renderETA() }

        { this._renderTechnicians() }
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),

    progress: ServiceSelector.progress(state, props),
    technicianList: ServiceSelector.technicians(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getProgress: (data, onComplete) => dispatch(ServiceAction.getProgressDetail(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(OnTheWay);
