/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  SegmentTitleFragment,
} from "@app-components-fragments";
import {
  TechnicianCallList,
} from "@app-components-lists";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as ServiceSelector from "@app-selectors/service";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  ServiceAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class InProgress extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const basedData = this.Base.initBaseState(DATA, MODAL);
    basedData.data.serviceData = this.props.navData.serviceData;

    this.state = {
      ... basedData,

    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getProgress(),
      () => this.getRatings(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("endRating", nextProps, nextState);

    if (nextProps.endRating.data) {
      this.setAsComplete();
    }

    this.Base.setData("technicianList", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  setAsComplete() {
    const {
      serviceRequestNumber,
    } = this.Base.data("serviceData");

    this.props.setCompleted({
      serviceRequestNumber,
    },
    () => this.props.switchProcess("COMPLETED"));
  }

// ----------------------------------------

  getProgress() {
    if (this.Base.data("progress")) {
      return false;
    }

    const {
      serviceOrderNumber,
    } = this.Base.data("serviceData");

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getProgress({
      soldtoId,
      serviceOrderNumber,
    });
  }

// ----------------------------------------

  getRatings() {
    if (this.Base.data("progress")) {
      return false;
    }

    const {
      serviceOrderNumber,
    } = this.Base.data("serviceData");

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getRatings({
      soldtoId,
      serviceOrderNumber,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTechnicians() {
    const data = this.Base.data("technicianList");

    return (
      <Spacer space={ 2.5 }>
        <SegmentTitleFragment order={ 2 }>
          Technicians
        </SegmentTitleFragment>

        <VerticalAnimator order={ 3 }>
          <TechnicianCallList
            data={ data }
            loading={ this.Base.isLoading("technicianList") }
            order={ 2 }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        { this._renderTechnicians() }
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    endRating: ServiceSelector.endRating(state, props),

    technicianList: ServiceSelector.technicians(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getProgress: (data, onComplete) => dispatch(ServiceAction.getProgressDetail(data, onComplete)),
    getRatings: (data, onComplete) => dispatch(ServiceAction.getProgressRatings(data, onComplete)),
    setCompleted: (data, onComplete) => dispatch(ServiceAction.setCompleted(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(InProgress);
