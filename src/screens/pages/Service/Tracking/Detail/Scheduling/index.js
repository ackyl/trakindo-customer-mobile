/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullSecondary,
  ButtonFullPrimary,
} from "@app-components-core/Button";
import {
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  EstimatedArrivalCard,
} from "@app-components-cards";
import {
  SegmentTitleFragment,
} from "@app-components-fragments";
import {
  TechnicianCallList,
} from "@app-components-lists";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  Contacts as ContactsModal,
} from "@app-modals/Company";
import {
  LoadBlocker as LoadBlockerModal,
} from "@app-modals/Misc";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as ServiceSelector from "@app-selectors/service";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  ServiceAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Scheduling extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const basedData = this.Base.initBaseState(DATA, MODAL);
    basedData.data.serviceData = props.navData.serviceData;

    this.state = {
      ... basedData,

      isConfirmed: false,
    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getProgress(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.progress.updatedAt !== nextProps.progress.updatedAt && nextProps.progress.data) {
      const approveds = this.Base.data("confirmedScheduleList");

      for (let x in approveds) {
        if (approveds[x].indexOf(`${this.Base.data("serviceData").serviceOrderNumber}|||${nextProps.progress.data.eta}`) > -1) {
          this.setState({
            isConfirmed: true,
          });
        }
      }
    }

    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("confirmedScheduleList", nextProps, nextState);

    this.Base.setData("progress", nextProps, nextState);
    this.Base.setData("technicianList", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getProgress() {
    const {
      serviceOrderNumber,
      status,
    } = this.Base.data("serviceData");

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getProgress({
      soldtoId,
      serviceOrderNumber,
      status,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  confirm() {
    const {
      soldtoId,
    } = this.Base.data("company");

    const {
      serviceOrderNumber,
    } = this.Base.data("serviceData");

    const {
      eta,
    } = this.Base.data("progress");


    this.props.confirmSchedule(
      {
        soldtoId,
        serviceOrderNumber,
        eta,
      },
      () => this.setState({
        isConfirmed: true,
      }),
      () => this.Base.openModal("LoadBlocker"),
      () => this.Base.closeModal("LoadBlocker")
    );
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderETA() {
    const data = this.Base.data("progress");

    return (
      <Spacer space={ 2.5 }>
        <VerticalAnimator order={ 1 }>
          <EstimatedArrivalCard
            { ... data }
            loading={ this.Base.isLoading("progress") }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderTechnicians() {
    const data = this.Base.data("technicianList");

    return (
      <Spacer space={ 2.5 }>
        <SegmentTitleFragment order={ 2 }>
          Technicians
        </SegmentTitleFragment>

        <VerticalAnimator order={ 3 }>
          <TechnicianCallList
            data={ data }
            loading={ this.Base.isLoading("technicianList") }
            order={ 2 }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButtons() {
    if (this.state.isConfirmed || this.Base.isLoading("progress") || this.Base.data("serviceData").isScheduleConfirmed) {
      return null;
    }

    return (
      <Spacer>
        <VerticalAnimator order={ 4 }>
          <Spacer space={ 2 }>
            <ButtonFullPrimary
              onPress={ () => this.confirm() }
            >
              Confirm Schedule
            </ButtonFullPrimary>
          </Spacer>
        </VerticalAnimator>

        <VerticalAnimator order={ 2 }>
          <ButtonFullSecondary
            onPress={ () => this.Base.openModal("Contacts") }
          >
            Call to reschedule
          </ButtonFullSecondary>
        </VerticalAnimator>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        { this._renderETA() }

        { this._renderTechnicians() }

        { this._renderButtons() }

        <ContactsModal
          isActive={ this.Base.isModalActive("Contacts") }
          onClosePress={ () => this.Base.closeModal("Contacts") }
        />

        <LoadBlockerModal
          isActive={ this.Base.isModalActive("LoadBlocker") }
        />
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    confirmedScheduleList: ServiceSelector.confirmedSchedules(state, props),

    progress: ServiceSelector.progress(state, props),
    technicianList: ServiceSelector.technicians(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getProgress: (data, onComplete) => dispatch(ServiceAction.getProgressDetail(data, onComplete)),
    confirmSchedule: (data, onComplete, preFunction, postFunction) => dispatch(ServiceAction.confirmSchedule(data, onComplete, preFunction, postFunction)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Scheduling);
