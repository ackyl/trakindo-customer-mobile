/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Padder,
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  RatingStarList,
} from "@app-components-lists";
import {
  ProceesInformationHeadFragment,
} from "@app-components-fragments";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as ServiceSelector from "@app-selectors/service";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  ServiceAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Completed extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const basedData = this.Base.initBaseState(DATA, MODAL);
    basedData.data.serviceData = this.props.navData.serviceData;

    this.state = {
      ... basedData,

    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getRatings(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);

    this.Base.setData("endRating", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getRatings() {
    if (this.Base.data("endRating")) {
      return false;
    }

    const {
      serviceOrderNumber,
    } = this.Base.data("serviceData");

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getRatings({
      soldtoId,
      serviceOrderNumber,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTop() {
    return (
      <Spacer space={ 5 }>
        <VerticalAnimator order={ 1 }>
          <ProceesInformationHeadFragment
            icon="done"
            iconColor={ COLOR.green }
            title="Service Order Completed"
            description={ "All work for the service order has been completed.\nThank you for using our service." }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderRatings() {
    const {
      rating,
    } = this.Base.data("endRating") ? this.Base.data("endRating") : {};

    return (
      <Spacer hAlign="center">
        <RatingStarList
          rating={ rating }
          size={ 32 }
          loading={ this.Base.isLoading("endRating") }
          order={ 2 }
        />
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer top={ 8 }>
        <Padder>
          { this._renderTop() }

          { this._renderRatings() }
        </Padder>
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),

    endRating: ServiceSelector.endRating(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getRatings: (data, onComplete) => dispatch(ServiceAction.getProgressRatings(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Completed);
