/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  IMPORTS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullPrimary,
} from "@app-components-core/Button";
import BaseContainer from "@app-components-containers/BasePatterned";
import {
  Padder,
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  ServiceDocumentAccessCard,
} from "@app-components-cards";
import {
  ProceesInformationHeadFragment,
} from "@app-components-fragments";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  LoadBlocker as LoadBlockerModal,
} from "@app-modals/Misc";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as ServiceSelector from "@app-selectors/service";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  ServiceAction,
} from "@app-actions";




/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  MAIN CLASS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/
class DocumentApproval extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const basedData = this.Base.initBaseState(DATA, MODAL);
    basedData.data.serviceData = props.navData.serviceData;

    this.state = {
      ... basedData,

      isDocumentApproved: {
        quotation: false,
        serviceAgreement: false,
        tnc: false,
      },

      serviceRequestNumber: "",
    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getQuotation(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.quotation.updatedAt !== nextProps.quotation.updatedAt && nextProps.quotation.data) {
      const approveds = this.Base.data("approvedDocumentList");

      for (let x in approveds) {
        if (approveds[x].indexOf(`${this.Base.data("serviceData").serviceRequestNumber}|||${nextProps.quotation.data.quotationNumber}`) > -1) {
          Navigation.replace("OrderApproved");
        }
      }
    }

    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("approvedDocumentList", nextProps, nextState);

    this.Base.setData("quotation", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getQuotation() {
    const {
      serviceRequestNumber,
    } = this.Base.data("serviceData");

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getQuotation({
      soldtoId,
      serviceRequestNumber,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  approve(document, saNumber = "") {
    let {
      isDocumentApproved,
      serviceAgreementNumber,
    } = this.state;

    isDocumentApproved[document] = true;

    if (saNumber) {
      serviceAgreementNumber = saNumber;
    }

    this.setState({
      isDocumentApproved,
      serviceAgreementNumber,
    });
  }

// ----------------------------------------

  submit() {
    const {
      soldtoId,
    } = this.Base.data("company");

    const {
      serviceRequestNumber,
    } = this.Base.data("serviceData");

    const {
      quotationNumber,
    } = this.Base.data("quotation");

    const {
      serviceAgreementNumber,
    } = this.state;

    this.props.approveDocuments(
      {
        soldtoId,
        serviceRequestNumber,
        quotationNumber,
        serviceAgreementNumber,
      },
      () => Navigation.replace("OrderApproved"),
      () => this.Base.openModal("LoadBlocker"),
      () => this.Base.closeModal("LoadBlocker")
    );
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTop() {
    return (
      <Spacer top={ 5 } space={ 5 }>
        <VerticalAnimator order={ 1 }>
          <ProceesInformationHeadFragment
            icon="invoice"
            iconFetured
            title="Approve Quotation"
            description="Please read and approve these two documents below, to continue your service order"
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderQuotationAccess() {
    const {
      isDocumentApproved,
    } = this.state;

    const {
      quotationNumber,
      file,
      amount,
    } = this.Base.data("quotation") ? this.Base.data("quotation") : {};

    return (
      <Spacer space={ 1 }>
        <VerticalAnimator order={ 2 }>
          <ServiceDocumentAccessCard
            name="Quotation"
            isDisabled={ !file || !quotationNumber }
            isApproved={ isDocumentApproved.quotation }
            onPress={ () => Navigation.push("QuotationApproval", {
              onApprove: () => this.approve("quotation"),
              quotationNumber,
              file,
              isApprovable: !isDocumentApproved.quotation && amount <= 50000000,
            }) }
            loading={ this.Base.isLoading("quotation") }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderServiceAgreementAccess() {
    const {
      isDocumentApproved,
    } = this.state;

    const {
      quotationNumber,
      initialQuotationNumber,
      file,
      amount,
    } = this.Base.data("quotation") ? this.Base.data("quotation") : {};

    if (amount > 50000000) {
      return null;
    }

    return (
      <VerticalAnimator order={ 3 }>
        <Spacer space={ 1 }>
          <ServiceDocumentAccessCard
            name="Service Agreement"
            isDisabled={ !file || !quotationNumber }
            isApproved={ isDocumentApproved.serviceAgreement }
            onPress={ () => Navigation.push("OrderConfirmation", {
              onApprove: (serviceAgreementNumber) => this.approve("serviceAgreement", serviceAgreementNumber),
              quotationNumber,
              initialQuotationNumber,
              file,
              isApprovable: !isDocumentApproved.serviceAgreement,
            }) }
            loading={ this.Base.isLoading("quotation") }
          />
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderTnCAccess() {
    const {
      isDocumentApproved,
    } = this.state;

    const {
      quotationNumber,
      file,
      amount,
    } = this.Base.data("quotation") ? this.Base.data("quotation") : {};

    if (amount > 50000000) {
      return null;
    }

    return (
      <VerticalAnimator order={ 4 }>
        <ServiceDocumentAccessCard
          name="Terms and Conditions"
          isDisabled={ !file || !quotationNumber }
          isApproved={ isDocumentApproved.tnc }
          onPress={ () => Navigation.push("TnCApproval", {
            onApprove: () => this.approve("tnc"),
            isApprovable: !isDocumentApproved.tnc,
          }) }
          loading={ this.Base.isLoading("quotation") }
        />
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderDocumentAccess() {
    return (
      <Spacer space={ 5 }>
        { this._renderQuotationAccess() }

        { this._renderServiceAgreementAccess() }

        { this._renderTnCAccess() }
      </Spacer>
    );
  }

// ----------------------------------------

_renderButton() {
  if (!this.Base.data("quotation")) {
    return null;
  }

  const {
    amount,
  } = this.Base.data("quotation");

  if (amount > 50000000) {
    return null;
  }

  const {
    isDocumentApproved,
  } = this.state;

  const isSubmitable = isDocumentApproved.quotation && isDocumentApproved.serviceAgreement && isDocumentApproved.tnc && !this.Base.isLoading("quotation");

  return (
    <VerticalAnimator order={ 1 }>
      <Padder>
        <ButtonFullPrimary
          disabled={ !isSubmitable }
          onPress={ () => this.submit() }
        >
          Submit & Continue
        </ButtonFullPrimary>
      </Padder>
    </VerticalAnimator>
  );
}


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  _renderMain() {
    return (
      <Padder>
        { this._renderTop() }

        { this._renderDocumentAccess() }

        { this._renderButton() }
      </Padder>
    );
  }

// ----------------------------------------

  render() {
    return (
      <BaseContainer
        leftHeaderMode="back"
        onLeftHeaderPress={ () => Navigation.pop() }
      >
        { this._renderMain() }

        <LoadBlockerModal
          isActive={ this.Base.isModalActive("LoadBlocker") }
        />
      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    approvedDocumentList: ServiceSelector.approvedDocuments(state, props),

    quotation: ServiceSelector.quotation(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getQuotation: (data, onComplete) => dispatch(ServiceAction.getQuotationList(data, onComplete)),
    approveDocuments: (data, onComplete, preFunction, postFunction) => dispatch(ServiceAction.approveDocuments(data, onComplete, preFunction, postFunction)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(DocumentApproval);

