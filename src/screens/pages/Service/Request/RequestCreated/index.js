/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  Animated,
  PanResponder,
  View,
} from "react-native";
import {connect} from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
	Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
	Config,
	Device,
	Navigation,
} from "@core-utils";
import Styles from "./style";
import {
	COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import BaseContainer from "@app-components-containers/BasePatterned";
import {
	Padder,
	Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  ServiceTicketNumberCard,
  ServiceAdvisorCallCard,
  ServiceRequestDetailCard,
} from "@app-components-cards";
import {
  ProceesInformationHeadFragment,
} from "@app-components-fragments";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  Contacts as ContactsModal,
} from "@app-modals/Company";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as ServiceSelector from "@app-selectors/service";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  ServiceAction,
} from "@app-actions";

const BASE_PAN = Config.base(13);
const START_PAN = -(Device.windows.height - (Device.isIphoneX ? Config.base(20) : Config.base(17)));
const END_PAN = Config.base(0);




/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  MAIN CLASS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/
class ServiceRequestCreated extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const baseData = this.Base.initBaseState(DATA, MODAL);
    baseData.data.requestDetail = props.navData.requestDetail ? props.navData.requestDetail : {};

    this.state = {
      ... baseData,

      detailPosition: new Animated.Value(START_PAN),
      detailVisibility: new Animated.Value(0),
      detailMode: "compact",
    };

    this._panResponsder = PanResponder.create({
      onStartShouldSetPanResponder: (event, panState) => true,
      onPanResponderMove: (event, panState) => this.onPanResponsderMove(event, panState),
      onPanResponderRelease: (event, panState) => this.onPanResponderRelease(event, panState),
    });
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("ticketNumber", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  onPanResponsderMove(event, panState) {
    let newValue = BASE_PAN - panState.moveY;

    if (newValue <= START_PAN) {
      newValue = START_PAN;
    } else if (newValue >= 0) {
      newValue = END_PAN;
    }

    Animated.timing(this.state.detailPosition, {
      toValue: newValue,
      duration: 0,
      delay: 0,
    }).start();

    Animated.timing(this.state.detailVisibility, {
      toValue: (newValue + (-START_PAN)) / (-START_PAN),
      duration: 0,
      delay: 0,
    }).start();

    if (newValue < -Config.base(24)) {
      this.setState({
        detailMode: "compact",
      });
    } else {
      this.setState({
        detailMode: "full",
      });
    }
  }

// ----------------------------------------

  onPanResponderRelease(event, panState) {
    let newValue = BASE_PAN - panState.moveY;

    if (newValue <= START_PAN) {
      newValue = START_PAN;
    } else if (newValue >= 0) {
      newValue = END_PAN;
    }

    if (newValue > (START_PAN * (1 / 2))) {
      Animated.timing(this.state.detailPosition, {
        toValue: 0,
        duration: 0,
        delay: 0,
      }).start();

      Animated.timing(this.state.detailVisibility, {
        toValue: 1,
        duration: 0,
        delay: 0,
      }).start();

      this.setState({
        detailMode: "full",
      });
    } else {
      Animated.timing(this.state.detailPosition, {
        toValue: START_PAN,
        duration: 0,
        delay: 0,
      }).start();

      Animated.timing(this.state.detailVisibility, {
        toValue: END_PAN,
        duration: 0,
        delay: 0,
      }).start();

      this.setState({
        detailMode: "compact",
      });
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

	_renderTop() {
		return (
			<Spacer top={ 5 } space={ 5 }>
        <VerticalAnimator order={ 1 }>
          <ProceesInformationHeadFragment
            icon="done"
            iconColor={ COLOR.green }
            title="Order Created"
            description={ "Your Service Request is now created.\nPlease check your email.\nThis change will be reflected in your app within 1 hour." }
          />
        </VerticalAnimator>
      </Spacer>
		);
	}

// ----------------------------------------

  _renderTicketNumber() {
    const ticketNumber = this.Base.data("ticketNumber");

    return (
      <Spacer space={ 2.5 }>
        <VerticalAnimator order={ 2 }>
          <ServiceTicketNumberCard
            ticketNumber={ ticketNumber }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderAdvisorCall() {
    return (
      <VerticalAnimator order={ 3 }>
        <ServiceAdvisorCallCard
          onPress={ () => this.Base.openModal("Contacts") }
        />
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderDetail() {
    const data = this.Base.data("requestDetail") ? this.Base.data("requestDetail") : {};
    const unit = data.unit;
    const branchOffice = data.branchOffice ? data.branchOffice : {};
    const salesRep = data.salesRep ? data.salesRep : {};

    return (
      <Animated.View
        style={{
          position: "absolute",
          bottom: this.state.detailPosition,
        }}
      >
        <Spacer
          animated
          backgroundColor={ COLOR.white }
          radius={ Config.base(1) }
          width={ Device.windows.width }
        >

          <Spacer
            height={ Config.base(2) }
            hAlign="center"
            vAlign="center"
          >
            <Spacer
              backgroundColor={ COLOR.grayT4 }
              width={ Config.base(7) }
              height={ Config.base(.5) }
              radius={ 50 }
            />
          </Spacer>

          <Spacer
            height={ Device.windows.height - Config.base(13) }
          >
            <ServiceRequestDetailCard
              { ... unit }
              { ... data }
              branchOffice={ branchOffice.name }
              salesRep={ salesRep.name }
              loading={ this.Base.isLoading("unit") }
              compact={ this.state.detailMode === "compact" }
              visibility={ this.state.detailVisibility }
              onPress={ () => Navigation.push("UnitDetail", {
                unitDetail: unit,
                isFromService: true,
              }) }
            />
          </Spacer>
        </Spacer>

        <View style={{
            position: "absolute",
            width: Device.windows.width,
            height: Config.base(7),
            backgroundColor: "transparent",
          }}
          { ...this._panResponsder.panHandlers }
        />
      </Animated.View>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

	render() {
		return (
			<BaseContainer
				leftHeaderMode="back"
				onLeftHeaderPress={ () => Navigation.pop() }
        static
			>
        <Padder>
          { this._renderTop() }

          { this._renderTicketNumber() }

          { this._renderAdvisorCall() }
        </Padder>

        { this._renderDetail() }

        <ContactsModal
          isActive={ this.Base.isModalActive("Contacts") }
          onClosePress={ () => this.Base.closeModal("Contacts") }
        />
			</BaseContainer>
		);
	}
}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    ticketNumber: ServiceSelector.activeTicket(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    createRequest: (data, onComplete) => dispatch(ServiceAction.createRequest(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(ServiceRequestCreated);

