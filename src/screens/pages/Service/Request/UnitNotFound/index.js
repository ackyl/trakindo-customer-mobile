/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  IMPORTS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullPrimary,
  ButtonFullSecondary,
} from "@app-components-core/Button";
import BaseContainer from "@app-components-containers/BasePatterned";
import {
  Padder,
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  ProceesInformationHeadFragment,
} from "@app-components-fragments";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  Contacts as ContactsModal,
} from "@app-modals/Company";




/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  MAIN CLASS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/
export default class NotFountUnit extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTop() {
    return (
      <Spacer top={ 5 } space={ 5 }>
        <VerticalAnimator order={ 1 }>
          <ProceesInformationHeadFragment
            icon="request-service"
            iconFetured
            title="Can't find your equipment?"
            description="If you cant find your equipment, you can register it by send us your equipment information or call us to do urgent service."
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButtons() {
    return (
      <Spacer space= { 2 }>
        <Spacer space= { 2 }>
          <VerticalAnimator order={ 2 }>
            <ButtonFullPrimary
              onPress={ () => this.Base.openModal("Contacts") }
            >
              Call for Urgent Service
            </ButtonFullPrimary>
          </VerticalAnimator>
        </Spacer>

        <VerticalAnimator order={ 3 }>
          <ButtonFullSecondary>
            Register Equipment
          </ButtonFullSecondary>
        </VerticalAnimator>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <BaseContainer
        leftHeaderMode="back"
        onLeftHeaderPress={ () => Navigation.pop() }
        static
      >
        <Padder pad={ 2.5 }>
          { this._renderTop() }

          { this._renderButtons() }
        </Padder>

        <ContactsModal
          isActive={ this.Base.isModalActive("Contacts") }
          onClosePress={ () => this.Base.closeModal("Contacts") }
        />
      </BaseContainer>
    );
  }

}
