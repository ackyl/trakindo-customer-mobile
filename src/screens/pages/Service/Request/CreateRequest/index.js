/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
  Validation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import BaseContainer from "@app-components-containers/BasePatterned";
import {
  Padder,
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  ServiceRequestScheduleFormCard,
  ServiceRequestUnitFormCard,
  ServiceRequestBranchFormCard,
  ServiceRequestPICFormCard,
  ServiceRequestNoteFormCard,
} from "@app-components-cards";
import {
  ButtonFullPrimary,
} from "@app-components-core/Button";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  LoadBlocker as LoadBlockerModal,
} from "@app-modals/Misc";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as CreditSelector from "@app-selectors/credit";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  ServiceAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class ServicePreventiveMaintenance extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const basedData = this.Base.initBaseState(DATA, MODAL);
    basedData.data.pageDetail = props.navData.pageDetail ? props.navData.pageDetail : {};

    this.state = {
      ... basedData,

      formData: {
        unit: props.navData.unit ? props.navData.unit : {},
        smu: null,
        date: null,
        location: null,
        branchOffice: {},
        salesRep: {},
        name: null,
        phoneNumber: null,
        note: null,
      },

      error: {
        unit: null,
        smu: null,
        date: null,
        location: null,
        branchOffice: null,
        salesRep: null,
        name: null,
        phoneNumber: null,
        note: null,
      },
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("payer", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  openUnitDetail(data) {
    Navigation.push("UnitDetail", {
      unitDetail: data,
      isFromService: true,
    });
  }

// ----------------------------------------

  setUnitData(data) {
    const {
      formData,
    } = this.state;

    formData.unit = data;

    this.setState({formData});
  }

// ----------------------------------------

  setData(key, data) {
    const {
      formData,
    } = this.state;

    formData[key] = data;

    this.setState({formData});
  }

// ----------------------------------------

  submit() {
    const {
      unit,
      smu,
      date,
      location,
      branchOffice,
      salesRep,
      name,
      phoneNumber,
      note,
    } = this.state.formData;

    const error = {
      unit: this.getUnitError(unit),
      smu: this.getSmuError(smu),
      date: this.getDateError(date),
      location: this.getLocationError(location),
      branchOffice: this.getBranchOfficeError(branchOffice),
      salesRep: this.getSalesRepError(salesRep),
      name: this.getNameError(name),
      phoneNumber: this.getPhoneNumberError(phoneNumber),
      note: this.getNoteError(note),
    };

    this.setState({
      error,
    });

    if (Validation.hasError(error)) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    const {
      payerId,
    } = this.Base.data("payer");

    const {
      serviceType,
    } = this.Base.data("pageDetail");

    const data = {
      soldtoId,
      payerId,
      serviceType,
      description: `${serviceType} Mobile APP ${soldtoId}`,
      unit,
      smu,
      date,
      location,
      branchOffice,
      salesRep,
      name,
      phoneNumber,
      note,
    };

    this.props.createRequest(
      data,
      () => Navigation.replace("RequestCreated", {
        requestDetail: data,
      }),
      () => this.Base.openModal("LoadBlocker"),
      () => this.Base.closeModal("LoadBlocker")
    );
  }

// ----------------------------------------

  getUnitError(unit) {
    if (!Validation.isNotEmpty(unit)) {
      return "Unit cannot be empty";
    }

    return null;
  }

// ----------------------------------------

  getSmuError(smu) {
    return null;
  }

// ----------------------------------------

  getDateError(date) {
    if (!Validation.isNotEmpty(date)) {
      return "Need-by-Date cannot be empty";
    }

    return null;
  }

// ----------------------------------------

  getLocationError(location) {
    return null;
  }

// ----------------------------------------

  getBranchOfficeError(branchOffice) {
    if (!Validation.isNotEmpty(branchOffice)) {
      return "Branch Office cannot be empty";
    }

    return null;
  }

// ----------------------------------------

  getSalesRepError(salesRep) {
    if (!Validation.isNotEmpty(salesRep)) {
      return "Sales Rep cannot be empty";
    }

    return null;
  }

// ----------------------------------------

  getNameError(name) {
    if (!Validation.isNotEmpty(name)) {
      return "PIC Name cannot be empty";
    }

    return null;
  }

// ----------------------------------------

  getPhoneNumberError(phoneNumber) {
    if (!Validation.isNotEmpty(phoneNumber)) {
      return "PIC Contact cannot be empty";
    }

    if (!Validation.isMax(phoneNumber + "", 12)) {
      return "PIC Contact should have maximum 12 digits";
    }

    return null;
  }

// ----------------------------------------

  getNoteError(note) {
    if (!Validation.isNotEmpty(note)) {
      return "Note cannot be empty";
    }

    return null;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderUnitFormCard() {
    return (
      <VerticalAnimator order={ 1 }>
        <Spacer space={ 2.5 }>
          <ServiceRequestUnitFormCard
            unit={ this.state.formData.unit }
            onDetailPress={ (data) => this.openUnitDetail(data) }
            onChange={ (data) => this.setUnitData(data) }
            unitError={ this.state.error.unit }
          />
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderScheduleFormCard() {
    return (
      <VerticalAnimator order={ 2 }>
        <Spacer space={ 2.5 }>
          <ServiceRequestScheduleFormCard
            smu={ this.state.formData.smu }
            date={ this.state.formData.date }
            onSMUChange={ (data) => this.setData("smu", data) }
            onDateSelect={ (data) => this.setData("date", data) }
            smuError={ this.state.error.smu }
            dateError={ this.state.error.date }
          />
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderBranchFormCard() {
    return (
      <VerticalAnimator order={ 3 }>
        <Spacer space={ 2.5 }>
          <ServiceRequestBranchFormCard
            location={ this.state.formData.location }
            branchOffice={ this.state.formData.branchOffice }
            salesRep={ this.state.formData.salesRep }
            onLocationChange={ (data) => this.setData("location", data) }
            onBranchOfficeSelect={ (data) => this.setData("branchOffice", data) }
            onSalesRepSelect={ (data) => this.setData("salesRep", data) }
            locationError={ this.state.error.location }
            branchOfficeError={ this.state.error.branchOffice }
            salesRepError={ this.state.error.salesRep }
          />
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderPICFormCard() {
    return (
      <VerticalAnimator order={ 4 }>
        <Spacer space={ 2.5 }>
          <ServiceRequestPICFormCard
            name={ this.state.formData.name }
            phoneNumber={ this.state.formData.phoneNumber }
            onNameChange={ (data) => this.setData("name", data) }
            onPhoneNumberChange={ (data) => this.setData("phoneNumber", data) }
            nameError={ this.state.error.name }
            phoneNumberError={ this.state.error.phoneNumber }
          />
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderNoteFormCard() {
    const {
      serviceType,
    } = this.Base.data("pageDetail");

    return (
      <VerticalAnimator order={ 4 }>
        <ServiceRequestNoteFormCard
          label={ serviceType === "PM" ? "PM Type: (250/500/1000)" : "Details of Problem" }
          note={ this.state.formData.note }
          onNoteChange={ (data) => this.setData("note", data) }
          noteError={ this.state.error.note }
        />
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderCards() {
    return (
      <Spacer space={ 2.5 }>
        { this._renderUnitFormCard() }

        { this._renderScheduleFormCard() }

        { this._renderBranchFormCard() }

        { this._renderPICFormCard() }

        { this._renderNoteFormCard() }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButton() {
    return (
      <VerticalAnimator order={ 2 }>
        <Spacer space={ 1 }>
          <ButtonFullPrimary
            onPress={ () => this.submit() }
          >
            Send Request
          </ButtonFullPrimary>
        </Spacer>
      </VerticalAnimator>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      title,
    } = this.Base.data("pageDetail");

    return (
      <BaseContainer
        title={ title }
        leftHeaderMode="back"
        onLeftHeaderPress={ () => Navigation.pop() }
      >
        <Padder>
          { this._renderCards() }

          { this._renderButton() }
        </Padder>

        <LoadBlockerModal
          isActive={ this.Base.isModalActive("LoadBlocker") }
        />
      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    payer: CreditSelector.payer(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    createRequest: (data, onComplete, preFunction, postFunction) => dispatch(ServiceAction.createRequest(data, onComplete, preFunction, postFunction)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(ServicePreventiveMaintenance);
