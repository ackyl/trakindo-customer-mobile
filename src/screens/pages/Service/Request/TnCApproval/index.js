/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  IMPORTS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";
import {
  ScrollView,
} from "react-native";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";
import {
  FileService,
} from "@core-services";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import BaseContainer from "@app-components-containers/BasePatterned";
import {
  Spacer,
  HorizontalAnimator,
} from "@core-components-enhancers";
import {
  PDFViewerFragment,
  PDFScrollInstructionFragment,
  TnCApprovalActionFragment,
} from "@app-components-fragments";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  Contacts as ContactsModal,
} from "@app-modals/Company";


// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as AuthSelector from "@app-selectors/auth";




/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  MAIN CLASS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/
class QuotationApproval extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),

      isApprovable: props.navData.isApprovable ? props.navData.isApprovable : false,

      isLoaded: false,
      isAtEnd: false,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  download() {
    FileService.download(
      "serviceTnCPdf",
      "",
      "Trakindo Service Terms and Conditions",
      "pdf",
      {
        Authorization: this.props.authorization.data,
      },
      this.props.dispatch
    );
  }

// ----------------------------------------

  submit(serviceAgreementNumber) {
    const {
      onApprove,
    } = this.props.navData;

    if (onApprove) {
      onApprove();
    }

    Navigation.pop();
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderInstruction() {
    if (!this.state.isLoaded || this.state.isAtEnd) {
      return null;
    }

    return (
      <HorizontalAnimator order={ 1 }>
        <PDFScrollInstructionFragment
          instruction={ "Scroll down to read the Terms and Conditions" }
        />
      </HorizontalAnimator>
    );
  }

// ----------------------------------------

  _renderMain() {
    let heightBuffer = Config.base(this.state.isApprovable ? 30 : 24);
    heightBuffer = heightBuffer - (Device.isIphoneX ? 0 : Config.base(3));

    return (
      <Spacer>
        <ScrollView>
          <Spacer height={ Device.windows.height - heightBuffer }>
            <PDFViewerFragment
              file={ FileService.constructUrl("serviceTnCPdf", "") }
              headers={{
                Authorization: this.props.authorization.data,
              }}
              shouldCache
              onLoadComplete={ (noOfPages) => this.setState({isLoaded: true}) }
              onPageChanged={ (current, total) => this.setState({isAtEnd: current >= total}) }
              onError={ () => this.setState({isApprovable: false}) }
              hPadding={ 0 }
              static
            />
          </Spacer>
        </ScrollView>

        { this._renderInstruction() }
      </Spacer>
    );
  }


// ----------------------------------------

_renderBottom() {
  return (
    <TnCApprovalActionFragment
      onSubmit={ () => this.submit() }
      isApprovable={ this.state.isApprovable && this.state.isLoaded }
    />
  );
}


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <BaseContainer
        title="Terms and Conditions"
        leftHeaderMode="back"
        rightHeaderMode="download"
        onLeftHeaderPress={ () => Navigation.pop() }
        onRightHeaderPress={ () => this.download() }
        onBackPress={ () => Navigation.pop() }
        solidHeader
        bottomContent={ this._renderBottom() }
        static
      >
        { this._renderMain() }
      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    authorization: AuthSelector.authorization(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => dispatch(action),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(QuotationApproval);

