/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
	Config,
	Device,
	Navigation,
} from "@core-utils";
import Styles from "./style";
import {
	COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
	ButtonFullSecondary,
} from "@app-components-core/Button";
import BaseContainer from "@app-components-containers/BasePatterned";
import {
	Padder,
	Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  ProceesInformationHeadFragment,
} from "@app-components-fragments";




/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  MAIN CLASS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/
export default class OrderApproved extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  onSeeOrdersPressed() {
    Navigation.pop();

    const {
      onSeeOrders,
    } = this.props.navData ? this.props.navData : {};

    if (onSeeOrders) {
      onSeeOrders();
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

	_renderTop() {
		return (
			<Spacer top={ 10 } space={ 5 } hAlign="center">
        <VerticalAnimator order={ 1 }>
          <ProceesInformationHeadFragment
            icon="done"
            iconColor={ COLOR.green }
            title="Quotation Approved"
            description={ "Please wait for a moment, now we are proceed to do scheduling for your service order.\nPlease check your email.\nThis change will be reflected in your app within 1 hour." }
          />
        </VerticalAnimator>
			</Spacer>
		);
	}

// ----------------------------------------

  _renderButton() {
    return (
      <VerticalAnimator order={ 2 }>
        <ButtonFullSecondary
          onPress={ () => this.onSeeOrdersPressed() }
        >
          See Service Orders
        </ButtonFullSecondary>
      </VerticalAnimator>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

	render() {
		return (
			<BaseContainer
				leftHeaderMode="back"
				onLeftHeaderPress={ () => Navigation.pop() }
        static
			>
        <Padder pad={ 4 }>
          { this._renderTop() }

          { this._renderButton() }
        </Padder>
			</BaseContainer>
		);
	}
}
