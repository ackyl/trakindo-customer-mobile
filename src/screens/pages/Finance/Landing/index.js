/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import BaseContainer from "@app-components-containers/BasePatterned";
import {
  Padder,
  Spacer,
  HorizontalAnimator,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  CreditBlockerList,
} from "@app-components-lists";
import {
  PaymentDueCard,
  CreditCard,
  SoADownloadCard,
  UnallocatedCreditAccessCard,
  InvoiceAccessCard,
} from "@app-components-cards";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  UnallocatedAmount as UnallocatedAmountModal,
} from "@app-modals/Invoice";
import {
  Contacts as ContactsModal,
} from "@app-modals/Company";
import {
  SoAFileSelection as SoAFileSelectionModal,
  ExpandedCredit as ExpandedCreditModal,
} from "@app-modals/Credit";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CreditSelector from "@app-selectors/credit";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  CreditAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Landing extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getCredit(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("payer", nextProps, nextState);
    this.Base.setData("credit", nextProps, nextState);

    this.Base.setData("isOverlimit", nextProps, nextState);
    this.Base.setData("hasOverdue", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getCredit() {
    const {
      payerId,
    } = this.Base.data("payer");
    
    this.props.getCredit({
      payerId,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderCredit() {
    const data = this.Base.data("credit");
    
    return (
      <Spacer space={ 2.5 }>
        <HorizontalAnimator order={ 1 }>
          <CreditCard
            { ... data }
            hasOverdue={ this.Base.data("hasOverdue") }
            loading={ this.Base.isLoading("credit") }
            onPress={ () => this.Base.openModal("ExpandedCredit") }
          />
        </HorizontalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderBlockers() {
    const isOverlimit = this.Base.data("isOverlimit");
    const hasOverdue = this.Base.data("hasOverdue");

    return (
      <Spacer space={ isOverlimit || hasOverdue ? 2.5 : 0 }>
        <CreditBlockerList
          isOverlimit={ isOverlimit }
          hasOverdue={ hasOverdue }
          onCallPress={ () => this.Base.openModal("Contacts") }
          showInvoices={ () => Navigation.push("InvoiceList") }
          loading={ this.Base.isLoading("credit") }
          order={ 2 }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderUnallocatedCredit() {
    const data = this.Base.data("credit");

    return (
      <Spacer space={ 2.5 }>
        <VerticalAnimator order={ 1 }>
          <UnallocatedCreditAccessCard
            { ... data }
            onPress={ () => this.Base.openModal("UnallocatedAmount") }
            loading={ this.Base.isLoading("credit") }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------


  _renderPaymentDue() {
    const data = this.Base.data("payer");

    return (
      <Spacer space={ 2.5 }>
        <VerticalAnimator order={ 2 }>
          <PaymentDueCard
            { ... data }
            loading={ this.Base.isLoading("payer") }
            onOverduePress={ () => Navigation.push("InvoiceList") }
            onCurrentPress={ () => Navigation.push("InvoiceList", {tabIndex: 1}) }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderStatementOfAccount() {
    return (
      <Spacer space={ 2.5 }>
        <VerticalAnimator order={ 3 }>
          <SoADownloadCard
            onPress={ () => this.Base.openModal("SoAFileSelection") }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderInvoice() {
    return (
      <Spacer space={ 2 }>
        <VerticalAnimator order={ 4 }>
          <InvoiceAccessCard
            onPress={ () => Navigation.push("InvoiceList") }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const credit = this.Base.data("credit");

    return (
      <BaseContainer
        title="Finance"
        leftHeaderMode="back"
        onLeftHeaderPress={ () => Navigation.pop() }
      >

        <Spacer top={ 1 }>
          <Padder>
            { this._renderCredit() }

            { this._renderBlockers() }

            { this._renderUnallocatedCredit() }

            { this._renderPaymentDue() }

            { this._renderStatementOfAccount() }

            { this._renderInvoice() }
          </Padder>
        </Spacer>

        <ExpandedCreditModal
          isActive={ this.Base.isModalActive("ExpandedCredit") }
          onClosePress={ () => this.Base.closeModal("ExpandedCredit") }
        />

        <SoAFileSelectionModal
          isActive={ this.Base.isModalActive("SoAFileSelection") }
          onClosePress={ () => this.Base.closeModal("SoAFileSelection") }
          navData={{
            credit: credit ? credit : {},
          }}
        />

        <UnallocatedAmountModal
          isActive={ this.Base.isModalActive("UnallocatedAmount") }
          onClosePress={ () => this.Base.closeModal("UnallocatedAmount") }
          navData={{
            credit: credit ? credit : {},
          }}
        />

        <ContactsModal
          isActive={ this.Base.isModalActive("Contacts") }
          onClosePress={ () => this.Base.closeModal("Contacts") }
        />

      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    payer: CreditSelector.payer(state, props),
    credit: CreditSelector.selected(state, props),

    isOverlimit: CreditSelector.isOverlimit(state, props),
    hasOverdue: CreditSelector.hasOverdue(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getCredit: (data, onComplete) => dispatch(CreditAction.getCredit(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Landing);

