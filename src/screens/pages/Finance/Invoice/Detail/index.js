/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullSecondary,
} from "@app-components-core/Button";
import BaseContainer from "@app-components-containers/Base";
import {
  Padder,
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  InvoiceDetailCard,
  InvoiceDownloadCard,
} from "@app-components-cards";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  VirtualAccount as VirtualAccountModal,
} from "@app-modals/Invoice";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Detail extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const basedData = this.Base.initBaseState(DATA, MODAL);
    basedData.data.invoiceDetail = props.navData.invoiceDetail;

    this.state = {
      ... basedData,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderDetail() {
    const data = this.Base.data("invoiceDetail");

    return (
      <Spacer space={ 2.5 }>
        <VerticalAnimator order={ 1 }>
          <InvoiceDetailCard
            { ... data }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDownload() {
    const data = this.Base.data("invoiceDetail");

    if (!data.file) {
      return null;
    }

    return (
      <VerticalAnimator order={ 2 }>
        <Spacer space={ 2.5 }>
          <InvoiceDownloadCard
            { ... data }
          />
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      status,
      file,
    } = this.Base.data("invoiceDetail");

    if (status === "CLOSED") {
      return null;
    }

    return (
      <VerticalAnimator order={ !file ? 2 : 3 }>
        <Spacer space={ 1 }>
          <ButtonFullSecondary onPress={ () => this.Base.openModal("VirtualAccount") }>
            Payment Information
          </ButtonFullSecondary>
        </Spacer>
      </VerticalAnimator>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <BaseContainer
        title="Invoice Detail"
        leftHeaderMode="back"
        onLeftHeaderPress={ () => Navigation.pop() }
      >

        <Spacer top={ 1 } overflowable>
          <Padder overflowable>
            { this._renderDetail() }

            { this._renderDownload() }

            { this._renderButton() }
          </Padder>
        </Spacer>

        <VirtualAccountModal
          isActive={ this.Base.isModalActive("VirtualAccount") }
          onClosePress={ () => this.Base.closeModal("VirtualAccount") }
          navData={{
            company: this.Base.data("company"),
          }}
        />

      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Detail);

