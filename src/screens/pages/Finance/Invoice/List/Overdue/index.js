/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
} from "@core-components-enhancers";
import {
  InvoiceList,
} from "@app-components-lists";
import {
  InvoiceSummaryFragment,
} from "@app-components-fragments";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as CreditSelector from "@app-selectors/credit";
import * as InvoiceSelector from "@app-selectors/invoice";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  InvoiceAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Overdue extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };

    this.tabBodyRef = null;
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getList(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (!nextProps.isActive) {
      return false;
    }

    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("payer", nextProps, nextState);

    this.Base.setData("list", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getList(page:number = 1) {
    const {
      soldtoId,
    } = this.Base.data("company");

    const {
      payerId,
    } = this.Base.data("payer");

    this.props.getList({
      soldtoId,
      payerId,
      page,
    });

    this.Base.setPage("list", page);
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderSummary() {
    const {
      currency,
      overdueAmount,
    } = this.Base.data("payer");

    let data = {
      currency,
      type: "overdue",
      amount: overdueAmount,
    };

    return (
      <InvoiceSummaryFragment
        { ... data }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      dataName,
      onInvoiceSelected,
    } = this.props;

    const data = this.Base.data("list");

    return (
      <Spacer flex={ 1 }>
        <Spacer flex={ 1 }>
          <InvoiceList
            data={ data }
            onItemPress={ onInvoiceSelected ?  (dataItem) => onInvoiceSelected(dataItem) : null }
            loading={ this.Base.isLoading("list") }
            page={ this.Base.currentPage("list") }
            emptyText={ `No ${dataName} Found` }
            loadNext={ (page) => this.getList(page) }
            order={ 2 }
            padded
          />
        </Spacer>

        { this._renderSummary() }
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    payer: CreditSelector.payer(state, props),

    list: InvoiceSelector.overdueList(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getList: (data, onComplete) => dispatch(InvoiceAction.getOverdueList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Overdue);

