/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullSecondary,
} from "@app-components-core/Button";
import {
  Padder,
  Spacer,
} from "@core-components-enhancers";
import {
  SalesRepCallList,
  HelpContactList,
} from "@app-components-lists";
import {
  SegmentTitleFragment,
} from "@app-components-fragments";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as MiscSelector from "@app-selectors/misc";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  CompanyAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class HelpCenter extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getSalesRepList(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("setting", nextProps, nextState);
    this.Base.setData("salesReps", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getSalesRepList() {
    if (this.Base.data("salesReps").length) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getSalesRepList({
      soldtoId,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderHelpContacts() {
    const data = this.Base.data("setting");

    return (
      <Spacer space={ 2.5 }>
        <HelpContactList
          { ... data }
          order={ 1 }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderSegmentHeader(title:string) {
    return (
      <SegmentTitleFragment order={ 2 }>
        { title }
      </SegmentTitleFragment>
    );
  }

// ----------------------------------------

  _renderSalesReps() {
    const data = this.Base.data("salesReps");

    return (
      <Spacer space={ 2.5 }>
        { this._renderSegmentHeader("Sales Representative") }

        <Spacer space={ 1 }>
          <SalesRepCallList
            data={ data }
            loading={ this.Base.isLoading("salesReps") }
            order={ 4 }
          />
        </Spacer>

      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        <Spacer top={ 1 }>
          <Padder>
            { this._renderHelpContacts() }

            { this._renderSalesReps() }
          </Padder>
        </Spacer>

      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    setting: MiscSelector.setting(state, props),

    salesReps: CompanySelector.salesReps(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getSalesRepList: (data, onComplete) => dispatch(CompanyAction.getSalesRepList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(HelpCenter);

