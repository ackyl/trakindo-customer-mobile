/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Padder,
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  CompanyDetailCard,
  UnitAccessCard,
  HelpCenterAccessCard,
  UnitRegistrationAccessCard,
} from "@app-components-cards";
import {
  SalesRepCallList,
} from "@app-components-lists";
import {
  SegmentTitleFragment,
} from "@app-components-fragments";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  CompanyAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Account extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getSalesRepList(),
      () => this.getContactList(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("salesReps", nextProps, nextState);
    this.Base.setData("contact", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getSalesRepList() {
    if (!this.isAllowed("COMPANY")) {
      return false;
    }

    if (this.Base.data("salesReps").length) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getSalesRepList({
      soldtoId,
    });
  }

// ----------------------------------------

  getContactList() {
    if (!this.isAllowed("COMPANY")) {
      return false;
    }

    if (this.Base.data("contact")) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getContactList({
      soldtoId,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  getModules() {
    return this.props.modules;
  }

// ----------------------------------------

  isAllowed(module:string) {
    const modules = this.getModules();

    return modules.indexOf(module) > -1;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderAccountContact() {
    const {
      name,
      phoneNumber,
      address,
    } = this.Base.data("company");

    const contactData = this.Base.data("contact");

    return (
      <Spacer space={ 2.5 }>
        <VerticalAnimator order={ 1 }>
          <CompanyDetailCard
            name={ name }
            { ... contactData }
            phoneNumber={ phoneNumber }
            address={ address }
            loading={ this.Base.isLoading("contact") }
            isCompanyAllowed={ this.isAllowed("COMPANY") }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderSegmentHeader(title:string) {
    return (
      <SegmentTitleFragment order={ 2 }>
        { title }
      </SegmentTitleFragment>
    );
  }

// ----------------------------------------

  _renderEquipment() {
    if (!this.isAllowed("COMPANY")) {
      return null;
    }

    const {
      noOfUnits,
    } = this.Base.data("company");

    return (
      <Spacer space={ 2.5 }>
        { this._renderSegmentHeader("Equipments") }

        <Spacer space={ 1 }>
          <VerticalAnimator order={ 3 }>
            <UnitAccessCard
              noOfUnits={ noOfUnits }
              onPress={ () => Navigation.push("UnitList") }
            />
          </VerticalAnimator>
        </Spacer>

        <VerticalAnimator order={ 4 }>
          <UnitRegistrationAccessCard
            // onPress={ () => alert("Open unit registration") }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderSalesReps() {
    if (!this.isAllowed("COMPANY")) {
      return null;
    }

    const data = this.Base.data("salesReps");

    return (
      <Spacer space={ 2.5 }>
        { this._renderSegmentHeader("Contacts") }

        <Spacer space={ 1 }>
          <SalesRepCallList
            data={ data }
            loading={ this.Base.isLoading("salesReps") }
            order={ 4 }
          />
        </Spacer>

        <VerticalAnimator order={ 5 }>
          <HelpCenterAccessCard
            onPress={ () => this.props.switchMenu("help") }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        <Spacer top={ 1 }>
          <Padder>
            { this._renderAccountContact() }

            { this._renderEquipment() }

            { this._renderSalesReps() }
          </Padder>
        </Spacer>
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    salesReps: CompanySelector.salesReps(state, props),
    contact: CompanySelector.selectedContact(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getSalesRepList: (data, onComplete) => dispatch(CompanyAction.getSalesRepList(data, onComplete)),
    getContactList: (data, onComplete) => dispatch(CompanyAction.getContactList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Account);

