/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
} from "@core-components-enhancers";
import {
  ServiceList,
} from "@app-components-lists";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as ServiceSelector from "@app-selectors/service";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  ServiceAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Service extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };

    this.tabBodyRef = null;
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getList(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (!nextProps.isActive) {
      return false;
    }

    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("prevSelected", nextProps, nextState);

    this.Base.setData("list", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getList(page:number = 1) {
    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getList({
      soldtoId,
      page,
    });

    this.Base.setPage("list", page);
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  onServiceSelected(data) {
    const prev = this.Base.data("prevSelected");

    if (
      data.status === "QUOTED" ||
      data.status === "SCHEDULED" ||
      // data.status === "IN_PROGRESS" ||
      (!prev || prev.serviceRequestNumber !== data.serviceRequestNumber)
    ) {
      this.props.setBase(data);
    }

    if (data.status === "QUOTED") {
      if (data.isQuotationApproved) {
        Navigation.push("OrderApproved");
      } else {
        Navigation.push("DocumentApproval", {
          serviceData: data,
        });
      }
    } else {
      Navigation.push("ServiceDetail", {
        serviceData: data,
      });
    }
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const data = this.Base.data("list");

    return (
      <Spacer flex={ 1 }>
        <ServiceList
          data={ data }
          onItemPress={ (dataItem) => this.onServiceSelected(dataItem) }
          loading={ this.Base.isLoading("list") }
          page={ this.Base.currentPage("list") }
          loadNext={ (page) => this.getList(page) }
          order={ 2 }
          padded
        />
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    prevSelected: ServiceSelector.selected(state, props),

    list: ServiceSelector.list(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getList: (data, onComplete) => dispatch(ServiceAction.getList(data, onComplete)),
    setBase: (data, onComplete) => dispatch(ServiceAction.setBaseFromList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Service);

