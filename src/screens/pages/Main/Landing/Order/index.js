/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
  TABS,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Padder,
  Spacer,
  Pager,
} from "@core-components-enhancers";
import {
  TabHeadFragment,
} from "@app-components-fragments";
import PartList from "./Part";
import ServiceList from "./Service";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class List extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const tabIndex = props.navData && props.navData.tabIndex ? props.navData.tabIndex : 0;

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),

      activeIndex: {
        orderTab: tabIndex,
      },

      isPageLoaded: [tabIndex === 0, tabIndex === 1],
      lastScrollTimeStamp: 0,
    };

    this.tabBodyRef = null;
  }

// ----------------------------------------

  componentDidMount() {
    const tabIndex = this.props.navData && this.props.navData.tabIndex ? this.props.navData.tabIndex : 0;

    setTimeout(
      () => this.tabBodyRef.scrollTo({x: (Device.windows.width * tabIndex), animated: false}),
      100
    );
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  switchTab(newIndex, byTap:bool = false) {
    const {
      activeIndex,
      isPageLoaded,
    } = this.state;

    if (activeIndex.orderTab !== newIndex && (this.state.lastScrollTimeStamp + 500) < (new Date()).getTime()) {
      activeIndex.orderTab = newIndex;
      isPageLoaded[newIndex] = true;

      this.setState({
        activeIndex,
        isPageLoaded,
        lastScrollTimeStamp: (new Date()).getTime(),
      });

      if (byTap) {
        this.tabBodyRef.scrollTo({x: (Device.windows.width * newIndex)});
      }
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderOrders(index) {
    if (
      index !== this.state.activeIndex.orderTab && !this.state.isPageLoaded[index]
    ) {
      return <Spacer width={ Device.windows.width }/>;
    }

    let main = null;

    switch (index) {
      case 0:
        main = (
          <PartList
            isActive={ index === this.state.activeIndex.orderTab }
          />
        );
        break;

      case 1:
        main = (
          <ServiceList
            isActive={ index === this.state.activeIndex.orderTab }
          />
        );
        break;
    }

    return (
      <Spacer width={ Device.windows.width }>
        { main }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderTabHead() {
    return (
      <Spacer top={ 1 } backgroundColor={ COLOR.gray }>
        <Padder>
          <TabHeadFragment
            tabs={ TABS }
            activeIndex={ this.state.activeIndex.orderTab }
            onTabSwitched={ (index, byTap) => this.switchTab(index, byTap) }
          />
        </Padder>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer flex={ 1 }>

        { this._renderTabHead() }

        <Pager
          cref={ ref => {this.tabBodyRef = ref;} }
          onPageSwitch={ (index) => this.switchTab(index) }
        >
          { this._renderOrders(0) }
          { this._renderOrders(1) }
        </Pager>
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {

  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(List);

