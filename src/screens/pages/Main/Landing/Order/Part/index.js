/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  PartList,
} from "@app-components-lists";
import {
  SearchBarFragment,
} from "@app-components-fragments";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as PartSelector from "@app-selectors/part";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  PartAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Part extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),

      search: "",
    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getList(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (!nextProps.isActive) {
      return false;
    }

    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("prevSelected", nextProps, nextState);

    this.Base.setData("list", nextProps, nextState);

    if (nextState.search.trim().length > 0) {
      this.Base.setData("searchedList", nextProps, nextState);
    }

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  onSearchTriggered(value:string = "") {
    let {
      search,
    } = this.state;

    if (search.trim().toLowerCase() === value.trim().toLowerCase()) {
      return null;
    }

    search = value;

    this.Base.resetList("searchedList", {search});

    const dataType = value.trim().length === 0 ? "list" : "searchedList";

    this.getList(1, search.trim().toLowerCase(), dataType);
  }

// ----------------------------------------

  getList(page:number = 1, search:string = "", dataType:string = "list") {
    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getList({
      soldtoId,
      page,
      search,
    });

    this.Base.setPage(dataType, page);
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  onPartSelected(data) {
    const prev = this.Base.data("prevSelected");

    if (!prev || prev.poNumber !== data.poNumber) {
      this.props.setBase(data);
    }

    Navigation.push("PartDetail", {
      orderDetail: data,
    });
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderSearchBar() {
    return (
      <Spacer backgroundColor={ COLOR.gray }>
        <Padder>
          <SearchBarFragment
            onSearchTriggered={ (value) => this.onSearchTriggered(value) }
            // value={ this.state.search }
          />
        </Padder>
      </Spacer>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const search = this.state.search;

    const dataType = search.trim().length === 0 ? "list" : "searchedList";

    const data = this.Base.data(dataType);

    return (
      <Spacer flex={ 1 }>
        { this._renderSearchBar() }

        <Spacer flex={ 1 }>
          <PartList
            data={ data }
            onItemPress={ (dataItem) => this.onPartSelected(dataItem) }
            loading={ this.Base.isLoading(dataType) }
            page={ this.Base.currentPage(dataType) }
            loadNext={ (page) => this.getList(page, search, dataType) }
            order={ 2 }
            key={ search }
            padded
          />
        </Spacer>
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    prevSelected: PartSelector.selected(state, props),

    list: PartSelector.list(state, props),
    searchedList: PartSelector.searchedList(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getList: (data, onComplete) => dispatch(PartAction.getList(data, onComplete)),
    setBase: (data, onComplete) => dispatch(PartAction.setBaseFromList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Part);

