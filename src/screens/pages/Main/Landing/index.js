/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";
import {
  Auth,
} from "aws-amplify";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ImageLocal,
} from "@app-components-core/Image";
import {
  Spacer,
} from "@core-components-enhancers";
import BaseContainer from "@app-components-containers/BasePatterned";
import NoAccess from "./NoAccess";
import Home from "./Home";
import Order from "./Order";
import Account from "./Account";
import ComingSoon from "./ComingSoon";
import HelpCenter from "./HelpCenter";
import Inbox from "./Inbox";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  LogoutConfirmation as LogoutConfirmationModal,
} from "@app-modals/Auth";
import {
  LoadBlocker as LoadBlockerModal,
} from "@app-modals/Misc";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as AuthSelector from "@app-selectors/auth";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  AuthAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Landing extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),

      activeMenu: "home",

      orderIndex: 0,
    };

    this._mainScrollRef = null;
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("auth", nextProps, nextState);
    this.Base.setData("modules", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  switchMenu(menu, orderIndex) {
    this.setState({
      activeMenu: menu,
      orderIndex: orderIndex !== null && orderIndex !== undefined ? orderIndex : 0,
    });
  }

// ----------------------------------------

  setTitle() {
    switch (this.state.activeMenu) {
      case "profile":
        return "Account";

      case "order":
        return "Orders";

      case "help":
        return "Help Center";

      case "inbox":
        return "Inbox";
    }
  }

// ----------------------------------------

  setRightButton() {
    switch (this.state.activeMenu) {
      case "profile":
        return {
          rightHeaderMode: "logout",
          onRightHeaderPress: () => this.Base.openModal("LogoutConfirmation"),
        };

      default:
        return {};
    }
  }

// ----------------------------------------

  logout() {
    const data = this.Base.data("auth");

    if (data) {
      this.Base.closeModal("LogoutConfirmation");

      setTimeout(() => {
        this.Base.openModal("LoadBlocker");
      }, 100);

      Auth.signOut()
        .then((response) => {
          this.props.logout(
            data,
            () => {
              setTimeout(() => {
                this.Base.closeModal("LoadBlocker");

                Navigation.reset("Login");
              }, 500);
            }
          );
        })
        .catch(err => {
          setTimeout(() => {
            this.Base.closeModal("LoadBlocker");
          }, 500);

          setTimeout(() => {
            this.Base.openModal("LogoutConfirmation");
          }, 600);

          console.warn("err", err);
        });
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderBackgroundImage() {
    switch (this.state.activeMenu) {
      case "home":
        return (
          <ImageLocal relWidth={ Device.windows.width + 2 } name="home.background"/>
        );

      case "order":
        return (
          <Spacer
            height={ Config.base(10) }
            backgroundColor={ COLOR.gray }
          />
        );

      default:
        return null;

    }
  }

// ----------------------------------------

  _renderMain() {
    const modules = this.Base.data("modules");

    switch (this.state.activeMenu) {
      case "home":
        return (
          <Home
            switchMenu={ (menu, tabIndex) => this.switchMenu(menu, tabIndex) }
            modules={ modules }
          />
        );

      case "order":
        if (modules.indexOf("SERVICE") < 0 || modules.indexOf("PART") < 0 ) {
          return (<NoAccess/>);
        }

        return (
          <Order
            navData={{
              tabIndex: this.state.orderIndex,
            }}
          />
        );

      case "inbox":
        if (modules.indexOf("NOTIFICATION") < 0) {
          return (<NoAccess/>);
        }

        return (
          <Inbox/>
        );

      case "help":
        if (modules.indexOf("HELP") < 0) {
          return (<NoAccess/>);
        }

        return (
          <HelpCenter/>
        );

      case "profile":
        return (
          <Account
            switchMenu={ (menu) => this.switchMenu(menu) }
            modules={ modules }
          />
        );

      default:
        return (
          <ComingSoon/>
        );
    }
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <BaseContainer
        // mainScrollRef={ (ref) => {this._mainScrollRef = ref;} }
        hasFooter
        title={ this.setTitle() }
        { ... this.setRightButton() }
        activeFooter={ this.state.activeMenu }
        backgroundImage={ this._renderBackgroundImage() }
        onSwitch={ (menu) => this.switchMenu(menu) }
        solidHeader={ this.state.activeMenu === "order" }
        static={ this.state.activeMenu === "order" }
      >
        { this._renderMain() }

        <LogoutConfirmationModal
          isActive={ this.Base.isModalActive("LogoutConfirmation") }
          onClosePress={ () => this.Base.closeModal("LogoutConfirmation") }
          navData={{
            onConfirm: () => this.logout(),
          }}
        />

        <LoadBlockerModal
          isActive={ this.Base.isModalActive("LoadBlocker") }
        />
      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    auth: AuthSelector.auth(state, props),
    modules: AuthSelector.accessibleModules(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    logout: (data, onComplete) => dispatch(AuthAction.logout(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
