/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import {
  ImageLocal,
} from "@app-components-core/Image";
import {
  Padder,
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  QuickAccessList,
  QuickReminderList,
  PartList,
  ServiceList,
} from "@app-components-lists";
import {
  SegmentTitleFragment,
} from "@app-components-fragments";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  RequestTypeSelection as RequestTypeSelectionModal,
} from "@app-modals/Service";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as CreditSelector from "@app-selectors/credit";
import * as PartSelector from "@app-selectors/part";
import * as ServiceSelector from "@app-selectors/service";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  CreditAction,
} from "@app-actions";
import {
  PartAction,
} from "@app-actions";
import {
  ServiceAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Home extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getPayerList(),
      () => this.getServiceBrief(),
      () => this.getPartList(),
      () => this.getServiceList(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);

    this.Base.setData("payer", nextProps, nextState);
    this.Base.setData("serviceBrief", nextProps, nextState);
    this.Base.setData("partList", nextProps, nextState);
    this.Base.setData("prevPartSelected", nextProps, nextState);
    this.Base.setData("serviceList", nextProps, nextState);
    this.Base.setData("prevServiceSelected", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getPayerList() {
    if (!this.isAllowed("FINANCE") && !this.isAllowed("SERVICE")) {
      return false;
    }

    if (this.Base.data("payer")) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getPayerList({
      soldtoId,
    });
  }

// ----------------------------------------

  getServiceBrief() {
    if (!this.isAllowed("SERVICE")) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getServiceBrief({
      soldtoId,
    });
  }

// ----------------------------------------

  getPartList() {
    if (!this.isAllowed("PART")) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getPartList({
      soldtoId,
      page: 1,
    });
  }

// ----------------------------------------

  getServiceList() {
    if (!this.isAllowed("SERVICE")) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getServiceList({
      soldtoId,
      page: 1,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  getModules() {
    return this.props.modules;
  }

// ----------------------------------------

  isAllowed(module:string) {
    const modules = this.getModules();

    return modules.indexOf(module) > -1;
  }

// ----------------------------------------

  goToQuickPage(screen) {
    if (!screen) {
      return null;
    }

    if (
      (screen === "InvoiceList" || screen === "FinanceLanding" || screen === "ServiceRequest") &&
      !this.Base.data("payer")
    ) {
      return null;
    }

    if (screen === "ServiceRequest") {
      this.Base.openModal("RequestTypeSelection");

      return null;
    }

    if (screen === "PartList") {
      this.props.switchMenu("order", 0);

      return null;
    }

    if (screen === "ServiceList") {
      this.props.switchMenu("order", 1);

      return null;
    }

    Navigation.push(screen);
  }

// ----------------------------------------

  onPartSelected(data) {
    const prev = this.Base.data("prevPartSelected");

    if (!prev || prev.poNumber !== data.poNumber) {
      this.props.setPartBase(data);
    }

    Navigation.push("PartDetail", {
      orderDetail: data,
    });
  }

// ----------------------------------------

  onServiceSelected(data) {
    const prev = this.Base.data("prevServiceSelected");

    if (!prev || prev.serviceRequestNumber !== data.serviceRequestNumber) {
      this.props.setServiceBase(data);
    }

    if (data.status === "QUOTED") {
      if (data.isQuotationApproved) {
        Navigation.push("OrderApproved", {
          onSeeOrders: () => this.props.switchMenu("order", 1),
        });
      } else {
        Navigation.push("DocumentApproval", {
          serviceData: data,
        });
      }
    } else {
      Navigation.push("ServiceDetail", {
        serviceData: data,
      });
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderHeader() {
    const {
      name,
    } = this.Base.data("company");

    return (
      <Spacer space={ 2 }>

        <VerticalAnimator order={ 1 }>
          <Spacer space={ 3 }>
            <ImageLocal name="main.logo"/>
          </Spacer>
        </VerticalAnimator>

        <VerticalAnimator order={ 2 }>
            <TextH2
              bold
              mode="light"
              numberOfLines={ 1 }
              flex={ 1 }
            >
              { name }
            </TextH2>
        </VerticalAnimator>

      </Spacer>
    );
  }

// ----------------------------------------

  _renderQuickAccess() {
    return (
      <Spacer space={ 2.5 }>
        <QuickAccessList
          order={ 7 }
          onItemPress={ (screen) => this.goToQuickPage(screen) }
          allowedModules={ this.getModules() }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderSegmentHeader(title:string) {
    return (
      <SegmentTitleFragment order={ 3 }>
        { title }
      </SegmentTitleFragment>
    );
  }

// ----------------------------------------

  _renderRemainder() {
    if (!this.isAllowed("FINANCE") && !this.isAllowed("SERVICE")) {
      return null;
    }

    let payment = {
      ... this.Base.data("payer"),
    };

    let service = {
      ... this.Base.data("serviceBrief"),
    };

    return (
      <Spacer space={ 2.5 }>

        { this._renderSegmentHeader("Reminder") }

        <QuickReminderList
          payment={ payment }
          service={ service }
          loadingPayment={ this.Base.isLoading("payer") }
          loadingServiceSummary={ this.Base.isLoading("serviceBrief") }
          onPaymentPress={ () => this.goToQuickPage("InvoiceList") }
          onServicePress={ () => this.goToQuickPage("ServiceList") }
          isFinanceAllowed={ this.isAllowed("FINANCE") }
          isServiceAllowed={ this.isAllowed("SERVICE") }
          order={ 7 }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderPartList() {
    if (!this.isAllowed("PART")) {
      return null;
    }

    const data = this.Base.data("partList").slice(0, 3);

    return (
      <VerticalAnimator order={ 4 }>
        <Spacer space={ 2.5 }>
          { this._renderSegmentHeader("Part Orders") }

          <PartList
            data={ data }
            onItemPress={ (dataItem) => this.onPartSelected(dataItem) }
            loading={ this.Base.isLoading("partList") }
            order={ 5 }
            noPaddingBottom
          />
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderServiceList() {
    if (!this.isAllowed("SERVICE")) {
      return null;
    }

    const data = this.Base.data("serviceList").slice(0, 3);
    return (
      <VerticalAnimator order={ 5 }>
        <Spacer space={ 2.5 }>
          { this._renderSegmentHeader("Service Orders") }

          <ServiceList
            data={ data }
            onItemPress={ (dataItem) => this.onServiceSelected(dataItem) }
            loading={ this.Base.isLoading("serviceList") }
            order={ 6 }
            noPaddingBottom
          />
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderBottom() {
    return (
      <Padder>
        <Spacer top={ 4 } space={ 4 }>
          <VerticalAnimator order={ 3 }>
            <Spacer space={ 1 }>
              <ImageLocal name="main.tagline"/>
            </Spacer>
          </VerticalAnimator>

          <VerticalAnimator order={ 4 }>
            <TextBS
              mode="light2"
            >
              Copyright @2017 PT TRAKINDO UTAMA. All Rights Reserved.
            </TextBS>
          </VerticalAnimator>
        </Spacer>

      </Padder>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <Spacer>
        <Spacer top={ 2 } borderBottomColor={ COLOR.gray }>
          <Padder>
            { this._renderHeader() }

            { this._renderQuickAccess() }

            { this._renderRemainder() }

            { this._renderPartList() }

            { this._renderServiceList() }
          </Padder>
        </Spacer>

        { this._renderBottom() }

        <RequestTypeSelectionModal
          isActive={ this.Base.isModalActive("RequestTypeSelection") }
          onClosePress={ () => this.Base.closeModal("RequestTypeSelection") }
        />
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),

    payer: CreditSelector.payer(state, props),
    serviceBrief: ServiceSelector.brief(state, props),
    prevPartSelected: PartSelector.selected(state, props),
    partList: PartSelector.list(state, props),
    serviceList: ServiceSelector.list(state, props),
    prevServiceSelected: ServiceSelector.selected(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getPayerList: (data, onComplete) => dispatch(CreditAction.getPayerList(data, onComplete)),
    getServiceBrief: (data, onComplete) => dispatch(ServiceAction.getBrief(data, onComplete)),
    getPartList: (data, onComplete) => dispatch(PartAction.getList(data, onComplete)),
    getServiceList: (data, onComplete) => dispatch(ServiceAction.getList(data, onComplete)),
    setPartBase: (data, onComplete) => dispatch(PartAction.setBaseFromList(data, onComplete)),
    setServiceBase: (data, onComplete) => dispatch(ServiceAction.setBaseFromList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Home);

