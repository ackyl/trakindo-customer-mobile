/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";
import {
  Auth,
} from "aws-amplify";
import OneSignal from "react-native-onesignal";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";
import ApiConfig from "@app-configs/Api";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBM,
} from "@app-components-core/Text";
import {
  ImageLocal,
} from "@app-components-core/Image";
import BaseContainer from "@app-components-containers/BasePatterned";
import {
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  ResourceVersionInfoFragment,
} from "@app-components-fragments";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as AuthSelector from "@app-selectors/auth";
import * as UnitSelector from "@app-selectors/unit";
import * as MiscSelector from "@app-selectors/misc";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  AuthAction,
  UnitAction,
  MiscAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class ResourceLoader extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };

    OneSignal.addEventListener("ids", (token) => this.setDeviceId(token.userId));
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getUnitFamilyList(),
      () => this.getBranchOfficeList(),
      () => this.getSettings(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("deviceId", nextProps, nextState);
    this.Base.setData("unitFamilyList", nextProps, nextState);
    this.Base.setData("branchOfficeList", nextProps, nextState);
    this.Base.setData("setting", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  proceed() {
    if (this.props.isLoggedIn.data) {
      ApiConfig.refreshToken(
        (data) => {
          const roleId = data.roleId;

          if (roleId > 1) {
            data.deviceId = this.Base.data("deviceId");
            data.noBaseSet = false;

            this.props.resetToken(
              data,
              () => {
                if (roleId > 4) {
                  Navigation.replace("CompanySelection");
                } else {
                  Navigation.replace("Home");
                }
              }
            );
          } else {
            Navigation.replace("Login");
          }
        },
        (err) => {
          console.warn("err", err);

          this.props.clearToken(
            {},
            () => Navigation.replace("Login")
          );
        }
      );

      return true;
    }

    Navigation.replace("Login");
  }

// ----------------------------------------

  setDeviceId(deviceId) {
    this.props.setDeviceId({
      deviceId,
    });
  }

// ----------------------------------------

  getUnitFamilyList() {
    this.props.getUnitFamilyList();
  }

// ----------------------------------------

  getBranchOfficeList() {
    this.props.getBranchOfficeList();
  }

// ----------------------------------------

  getSettings() {
    this.props.getSettings();
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  getTotalResource() {
    const resources = DATA;

    return Object.keys(resources).length;
  }

// ----------------------------------------

  getReadyResources() {
    let readyCounter = 0;

    for (let key in DATA) {
      if (!this.Base.isLoading(key)) {
        readyCounter++;
      }
    }

    if (readyCounter === this.getTotalResource()) {
      setTimeout(() => {
          this.proceed();
        },
        300
      );
    }

    return readyCounter;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderMainLogo() {
    return (
      <Spacer flex={ 1 } vAlign="center" hAlign="center">

        <Spacer style={{position: "absolute"}}>
          <ImageLocal name="resource.background"/>
        </Spacer>

        {/*<VerticalAnimator order={ 1 } noAnimation>
           <ImageLocal name="main.app_logo" relWidth={ 111 }/>
        </VerticalAnimator>*/}
      </Spacer>
    );
  }

// ----------------------------------------

  _renderTaglineLogo() {
    return (
      <VerticalAnimator order={ 2 }>
        <Spacer space={ 2 }>
          <ImageLocal name="main.tagline"/>
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderLoading() {
    return (
      <Spacer hAlign="center">
        <VerticalAnimator order={ 3 }>
          <Spacer space={ 1 } row>
            <TextBM
              mode="light"
            >
              Preparing your apps
            </TextBM>

            <Spacer horizontal space={ .4 }/>

            <TextBM
              mode="light"
            >
              { this.getReadyResources() }/{ this.getTotalResource() }
            </TextBM>
          </Spacer>
        </VerticalAnimator>

        <VerticalAnimator order={ 4 }>
          <Spacer
            width={ Config.base(22) }
            height={ Config.base(.5) }
            backgroundColor={ COLOR.whiteT4 }
            radius={ Config.base(1) }
          >
            <Spacer
              width={ this.getReadyResources() === 0 ? 1 : Config.base(22) * ((this.getReadyResources() / this.getTotalResource())) }
              height={ Config.base(.5) }
              backgroundColor={ COLOR.orange }
              radius={ Config.base(1) }
            />
          </Spacer>
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderVersion() {
    return (
      <Spacer top={ 5 }>
        <ResourceVersionInfoFragment

        />
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <BaseContainer
        static
      >
        <Spacer flex={ 1 } vAlign="center" hAlign="center">

          { this._renderMainLogo() }

          { this._renderLoading() }

        </Spacer>

        { this._renderVersion() }
      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    isLoggedIn: AuthSelector.isLoggedIn(state, props),
    refreshToken: AuthSelector.refreshToken(state, props),

    deviceId: AuthSelector.deviceId(state, props),
    unitFamilyList: UnitSelector.familyList(state, props),
    branchOfficeList: MiscSelector.branchList(state, props),
    setting: MiscSelector.setting(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getUnitFamilyList: (data, onComplete) => dispatch(UnitAction.getFamilyList(data, onComplete)),
    getBranchOfficeList: (data, onComplete) => dispatch(MiscAction.getBranchOfficeList(data, onComplete)),
    getSettings: (data, onComplete) => dispatch(MiscAction.getSettings(data, onComplete)),
    setDeviceId: (data, onComplete) => dispatch(AuthAction.setDeviceId(data, onComplete)),
    resetToken: (data, onComplete) => dispatch(AuthAction.loginCustomer(data, onComplete)),
    clearToken: (data, onComplete) => dispatch(AuthAction.logout(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(ResourceLoader);

