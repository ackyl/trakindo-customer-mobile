/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import BaseContainer from "@app-components-containers/BasePatterned";
import {
  Spacer,
  Padder,
} from "@core-components-enhancers";
import {
  CompanyList,
} from "@app-components-lists";
import {
  SearchBarFragment,
} from "@app-components-fragments";


// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as AuthSelector from "@app-selectors/auth";
import * as CompanySelector from "@app-selectors/company";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  CompanyAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class CompanySelection extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),

      search: "",
    };
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getList(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("salesRepId", nextProps, nextState);

    this.Base.setData("list", nextProps, nextState);

    if (nextState.search.trim().length > 0) {
      this.Base.setData("searchedList", nextProps, nextState);
    }

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getList(page:number = 1, search:string = "", dataType:string = "list") {
    const salesRepId = this.Base.data("salesRepId");

    this.props.getList({
      salesRepId,
      page,
      search,
    });

    this.Base.setPage(dataType, page);
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  onSearchTriggered(value:string = "") {
    let {
      search,
    } = this.state;

    if (search.trim().toLowerCase() === value.trim().toLowerCase()) {
      return null;
    }

    search = value;

    this.Base.resetList("searchedList", {search});

    const dataType = value.trim().length === 0 ? "list" : "searchedList";

    this.getList(1, search.trim().toLowerCase(), dataType);
  }

// ----------------------------------------

  onCompanySelected(data) {
    this.props.setCompanyBase(data);

    Navigation.push("Home");
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderSearchBar() {
    return (
      <Padder>
        <SearchBarFragment
          onSearchTriggered={ (value) => this.onSearchTriggered(value) }
          // value={ this.state.search }
        />
      </Padder>
    );
  }

// ----------------------------------------

  _renderCompanies() {
    const search = this.state.search;

    const dataType = search.trim().length === 0 ? "list" : "searchedList";

    const data = this.Base.data(dataType);

    return (
      <Spacer flex={ 1 }>
        <CompanyList
          data={ data }
          onItemPress={ (dataItem) => this.onCompanySelected(dataItem) }
          loading={ this.Base.isLoading(dataType) }
          page={ this.Base.currentPage(dataType) }
          loadNext={ (page) => this.getList(page, search, dataType) }
          order={ 2 }
          key={ search }
          padded
        />
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <BaseContainer
        static
        title="List of Customer"
      >
        { this._renderSearchBar() }

        { this._renderCompanies() }
      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    salesRepId: AuthSelector.salesRepId(state, props),

    list: CompanySelector.list(state, props),
    searchedList: CompanySelector.searchedList(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getList: (data, onComplete) => dispatch(CompanyAction.getList(data, onComplete)),
    setCompanyBase: (data, onComplete) => dispatch(CompanyAction.setBaseFromList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(CompanySelection);

