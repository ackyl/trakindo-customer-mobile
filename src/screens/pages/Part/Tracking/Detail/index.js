/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";
import {
  ScrollView,
} from "react-native";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
  TABS,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import BaseContainer from "@app-components-containers/BasePatterned";
import {
  TextBS,
} from "@app-components-core/Text";
import {
  Padder,
  Spacer,
  Pager,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  PartDetailCard,
  PartItemCounterCard,
  BranchLocationAccessCard,
} from "@app-components-cards";
import {
  SegmentTitleFragment,
  TabHeadFragment,
} from "@app-components-fragments";
import {
  DateFormat,
} from "@core-components-formatters";
import OutstandingList from "./Outstanding";
import ReadyList from "./Ready";
import TakenList from "./Taken";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  StatusTracking as StatusTrackingModal,
  BranchLocation as BranchLocationModal,
} from "@app-modals/Part";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as PartItemSelector from "@app-selectors/partItem";
import * as MiscSelector from "@app-selectors/misc";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  PartItemAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Detail extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const basedData = this.Base.initBaseState(DATA, MODAL);
    basedData.data.orderDetail = props.navData.orderDetail;

    const tabIndex = props.navData && props.navData.tabIndex ? props.navData.tabIndex : 0;

    this.state = {
      ... basedData,

      itemDetail: {},

      activeIndex: {
        itemTab: 0,
      },

      isPageLoaded: [tabIndex === 0, tabIndex === 1, tabIndex === 2],
      lastScrollTimeStamp: 0,

      scrollMode: "parent",
      useSolidHeader: false,

      childYOffset: [0, 0, 0],

      windowHeight: 0,
    };

    this.tabBodyRef = null;
  }

// ----------------------------------------

  componentDidMount() {
    this.getItemCounter();

    const tabIndex = this.props.navData && this.props.navData.tabIndex ? this.props.navData.tabIndex : 0;

    setTimeout(
      () => this.tabBodyRef.scrollTo({x: (Device.windows.width * tabIndex), animated: false}),
      100
    );
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("prevSelected", nextProps, nextState);
    this.Base.setData("branchOfficeList", nextProps, nextState);

    this.Base.setData("brief", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getItemCounter() {
    if (this.Base.data("brief")) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    const {
      poNumber,
    } = this.Base.data("orderDetail");

    this.props.getItemCounter({
      soldtoId,
      poNumber,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  setHeight(event) {
    if (this.state.windowHeight > 0) {
      return false;
    }

    const {
      nativeEvent,
    } = event;

    const windowHeight = nativeEvent.layout.height;

    this.setState({
      windowHeight,
    });
  }

// ----------------------------------------

  getBranch() {
    const {
      // branchCode,
      branchLocation,
    } = this.Base.data("orderDetail");

    return branchLocation ? {address: branchLocation} : {};

    // const branches = this.Base.data("branchOfficeList").filter((item) => item.code === branchCode);

    // return branches.length > 0 ? branches[0] : {};
  }

// ----------------------------------------

  switchTab(newIndex, byTap:bool = false) {
    const {
      activeIndex,
      isPageLoaded,
    } = this.state;

    if (activeIndex.itemTab !== newIndex && (this.state.lastScrollTimeStamp + 500) < (new Date()).getTime()) {
      activeIndex.itemTab = newIndex;
      isPageLoaded[newIndex] = true;

      this.setState({
        activeIndex,
        isPageLoaded,
        lastScrollTimeStamp: (new Date()).getTime(),
      });

      if (byTap) {
        this.tabBodyRef.scrollTo({x: (Device.windows.width * newIndex)});
      }
    }
  }

// ----------------------------------------

  onItemSelected(data) {
    const prev = this.Base.data("prevSelected");

    if (!prev || prev.caseNumber !== data.caseNumber) {
      this.props.setPartItemBase(data);
    }

    this.setState({
      itemDetail: {
        ... data,
        destination: this.Base.data("orderDetail").branchLocation,
      },
    }, () => {
      this.Base.openModal("StatusTracking");
    });
  }

// ----------------------------------------

  onParentScroll(event) {
    const yOffset = event.nativeEvent.contentOffset.y;

    // if (yOffset > (Config.base(55)) && this.state.scrollMode === "parent") {
    // if (yOffset > (Config.base(53))) {
    if (yOffset > (Config.base(50))) {
      this.setState({
        scrollMode: "child",
        useSolidHeader: true,
      });
    } else {
    // } else if (!this.state.scrollMode === "parent") {
      this.setState({
        scrollMode: "parent",
        useSolidHeader: false,
      });
    }

    // if (yOffset > (Config.base(55)) && !this.state.useSolidHeader) {
    //   this.setState({
    //     useSolidHeader: true,
    //   });
    // } else if (this.state.useSolidHeader) {
    //   this.setState({
    //     useSolidHeader: false,
    //   });
    // }



  }

// ----------------------------------------

  onChildScroll(event, index) {
    const yOffset = event.nativeEvent.contentOffset.y;

    const {
      childYOffset,
    } = this.state;

    childYOffset[index] = yOffset;

    // if (yOffset <= 0 && this.state.scrollMode === "child") {
    if (yOffset <= 0) {
      this.setState({
        scrollMode: "parent",
        childYOffset,
      });
    } else {
      this.setState({
        scrollMode: "child",
        childYOffset,
      });
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderUpdated() {
    const {
      updatedAt,
    } = this.Base.data("orderDetail");

    return (
      <Spacer space={ 1 }>
        <VerticalAnimator order={ 1 }>
          <Spacer backgroundColor={ COLOR.whiteT1 } radius={ Config.base(1) }>
            <Padder>
              <Spacer row top={ .5 } bottom={ .5 }>
                <TextBS
                  mode="light2"
                >
                  Update
                </TextBS>

                <Spacer horizontal space={ .5 }/>

                <TextBS
                  mode="light"
                >
                  <DateFormat value={ updatedAt }/>
                </TextBS>
              </Spacer>
            </Padder>
          </Spacer>
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderDetail() {
    const data = this.Base.data("orderDetail");

    return (
      <Spacer space={ 1 }>
        <VerticalAnimator order={ 2 }>
          <PartDetailCard
            { ... data }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderCounter() {
    const data = this.Base.data("brief");

    return (
      <Spacer space={ 1 }>
        <VerticalAnimator order={ 3 }>
          <PartItemCounterCard
            { ... data }
            loading={ this.Base.isLoading("brief")}
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

  // ----------------------------------------

  _renderBranchLocation() {
    return (
      <Spacer space={ 2.5 }>
        <VerticalAnimator order={ 4 }>
          <BranchLocationAccessCard
            onPress={ ()=>this.Base.openModal("BranchLocation") }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderTabHead() {
    return (
      <Spacer
        backgroundColor={ this.state.useSolidHeader ? COLOR.gray : null }
      >
        <Padder>
          <SegmentTitleFragment order={ 5 }>
            Part List
          </SegmentTitleFragment>

          <VerticalAnimator order={ 6 }>
            <TabHeadFragment
              tabs={ TABS }
              activeIndex={ this.state.activeIndex.itemTab }
              onTabSwitched={ (index, byTap) => this.switchTab(index, byTap) }
            />
          </VerticalAnimator>
        </Padder>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderMain() {
    return (
      <Spacer>
        <Padder>
          { this._renderUpdated() }

          { this._renderDetail() }

          { this._renderCounter() }

          { this._renderBranchLocation() }
        </Padder>

        { this._renderTabHead() }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderGroups(index) {
    if (
      index !== this.state.activeIndex.itemTab && !this.state.isPageLoaded[index]
    ) {
      return <Spacer width={ Device.windows.width }/>;
    }

    const {
      poNumber,
    } = this.Base.data("orderDetail");

    let main = null;

    switch (index) {
      case 0:
        main = (
          <OutstandingList
            scrollEnabled={ this.state.scrollMode === "child" || this.state.childYOffset[0] > 0 }
            onScroll={ (event) => this.onChildScroll(event, 0) }
            poNumber={ poNumber }
            dataName={ TABS[index].type }
            onItemSelected={ (data) => this.onItemSelected(data) }
            isActive={ index === this.state.activeIndex.itemTab }
          />
        );
        break;

      case 1:
        main = (
          <ReadyList
            scrollEnabled={ this.state.scrollMode === "child" || this.state.childYOffset[1] > 0 }
            onScroll={ (event) => this.onChildScroll(event, 1) }
            poNumber={ poNumber }
            dataName={ TABS[index].type }
            onItemSelected={ (data) => this.onItemSelected(data) }
            isActive={ index === this.state.activeIndex.itemTab }
          />
        );
        break;

      case 2:
        main = (
          <TakenList
            scrollEnabled={ this.state.scrollMode === "child" || this.state.childYOffset[2] > 0 }
            onScroll={ (event) => this.onChildScroll(event, 2) }
            poNumber={ poNumber }
            dataName={ TABS[index].type }
            onItemSelected={ (data) => this.onItemSelected(data) }
            isActive={ index === this.state.activeIndex.itemTab }
          />
        );
        break;
    }

    return (
      <Spacer width={ Device.windows.width }>
        { main }
      </Spacer>
    );
  }

// ----------------------------------------

  _renderList() {
    const height = this.state.windowHeight > 0 ? this.state.windowHeight : Device.windows.height;

    return (
      <Spacer height={ height - Config.base(6) }>
        <Pager
          cref={ ref => {this.tabBodyRef = ref;} }
          onPageSwitch={ (index) => this.switchTab(index) }
        >
          { this._renderGroups(0) }
          { this._renderGroups(1) }
          { this._renderGroups(2) }
        </Pager>

      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const branch = this.getBranch();

    return (
      <BaseContainer
        static
        title="Parts Order Tracking"
        leftHeaderMode="back"
        onLeftHeaderPress={ () => Navigation.pop() }
        solidHeader={ this.state.useSolidHeader }
      >
        <ScrollView
          // scrollEnabled={ this.state.scrollMode === "parent" }
          onScroll={ (event) => this.onParentScroll(event) }
          scrollEventThrottle={ 16 }
          bounces={ false }
          nestedScrollEnabled
          onLayout={ (event) => this.setHeight(event) }
        >
          { this._renderMain() }

          { this._renderList() }
        </ScrollView>

        <StatusTrackingModal
          isActive={ this.Base.isModalActive("StatusTracking") }
          onClosePress={ () => this.Base.closeModal("StatusTracking") }
          navData={{
            itemDetail: this.state.itemDetail,
          }}
        />

        <BranchLocationModal
          isActive={ this.Base.isModalActive("BranchLocation") }
          onClosePress={ () => this.Base.closeModal("BranchLocation") }
          navData={{
            branchLocation: branch.address,
          }}
        />

      </BaseContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    prevSelected: PartItemSelector.selected(state, props),
    branchOfficeList: MiscSelector.branchList(state, props),

    brief: PartItemSelector.brief(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getItemCounter: (data, onComplete) => dispatch(PartItemAction.getBrief(data, onComplete)),
    setPartItemBase: (data, onComplete) => dispatch(PartItemAction.setBaseFromList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Detail);