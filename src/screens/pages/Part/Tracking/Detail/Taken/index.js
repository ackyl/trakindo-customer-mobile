/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
} from "@core-components-enhancers";
import {
  PartItemGroupList,
} from "@app-components-lists";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as PartItemSelector from "@app-selectors/partItem";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  PartItemAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Taken extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };

    this.tabBodyRef = null;
  }

// ----------------------------------------

  componentDidMount() {
    this.Base.load([
      () => this.getList(),
    ]);
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (!nextProps.isActive) {
      return false;
    }

    this.Base.setData("company", nextProps, nextState);

    this.Base.setData("list", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getList(page:number = 1) {
    const {
      soldtoId,
    } = this.Base.data("company");

    const {
      poNumber,
    } = this.props;

    this.props.getList({
      soldtoId,
      poNumber,
      page,
    });

    this.Base.setPage("list", page);
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      dataName,
      onItemSelected,
      scrollEnabled,
      onScroll,
    } = this.props;

    const data = this.Base.data("list");

    return (
      <Spacer flex={ 1 }>
        <PartItemGroupList
          data={ data }
          onItemPress={ onItemSelected ? (dataItem) => onItemSelected(dataItem) : null }
          loading={ this.Base.isLoading("list") }
          emptyText={ `No ${dataName} Found` }
          page={ this.Base.currentPage("list") }
          loadNext={ (page) => this.getList(page) }
          order={ 6 }
          padded
          scrollEnabled={ scrollEnabled }
          onScroll={ onScroll }
        />
      </Spacer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),

    list: PartItemSelector.takenList(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getList: (data, onComplete) => dispatch(PartItemAction.getTakenItemGroupList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Taken);

