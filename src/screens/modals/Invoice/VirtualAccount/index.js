/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import ModalContainer from "@app-components-containers/Modal";
import {
  VAInfoList,
} from "@app-components-lists";
import {
  Spacer,
} from "@core-components-enhancers";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as MiscSelector from "@app-selectors/misc";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  CompanyAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class VirtualAccount extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("setting", nextProps, nextState);

    this.Base.setData("list", nextProps, nextState);

    return true;
  }

// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getVirtualAccountList() {
    if (this.Base.data("list").length) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getVirtualAccountList({
      soldtoId,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTitle() {
    return (
      <Spacer space={ 2 }>
        <Spacer>
          <TextH2
            mode="light"
          >
            Payment Information
          </TextH2>
          <TextBS
            mode="light2"
          >
            Pay your invoices by using our virtual account information, if payment has not been allocated, then the amount information will be accommodated in the unallocated amount menu.
          </TextBS>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderVirtualAccount() {
    const data = this.Base.data("list");
    const {
      virtualAccountName,
    } = this.Base.data("setting");

    return (
      <VAInfoList
        data={ data.map(item => {
          return {
            ... item,
            name: virtualAccountName,
          };
        }) }
        loading={ this.Base.isLoading("list") }
        order={ 1 }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
        onShow={ () => this.getVirtualAccountList() }
        scrollable
      >

        { this._renderTitle() }

        { this._renderVirtualAccount() }

      </ModalContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    setting: MiscSelector.setting(state, props),

    list: CompanySelector.virtualAccounts(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getVirtualAccountList: (data, onComplete) => dispatch(CompanyAction.getVirtualAccountList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(VirtualAccount);

