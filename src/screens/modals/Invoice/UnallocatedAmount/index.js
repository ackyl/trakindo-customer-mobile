/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBS,
} from "@app-components-core/Text";
import {
  UnallocatedCreditCard,
} from "@app-components-cards";
import {
  ButtonFullPrimary,
} from "@app-components-core/Button";
import ModalPartialContainer from "@app-components-containers/ModalPartial";
import {
  Spacer,
} from "@core-components-enhancers";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  Contacts as ContactsModal,
} from "@app-modals/Company";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class UnallocatedAmount extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderButton() {
    return (
      <Spacer>
        <Spacer space={ 2 }>
          <TextBS
            mode="light2"
          >
            You have unalocated amount, call us to discuss more about alocation process
          </TextBS>
        </Spacer>

        <Spacer space={ .5 } hAlign="flex-end">
          <TextBS
            mode="light2"
          >
            *Updated once a day
          </TextBS>
        </Spacer>

        <Spacer height={ Config.base(6) }>
          <ButtonFullPrimary onPress={ () => this.Base.openModal("Contacts") }>
            Call to Allocate
          </ButtonFullPrimary>
        </Spacer>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderTop() {
    const {
      currency,
      unallocated,
    } = this.props.navData.credit;

    return (
      <UnallocatedCreditCard
        currency={ currency }
        unallocated={ unallocated }
      />
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalPartialContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
      >

        { this._renderTop() }

        { this._renderButton() }

        <ContactsModal
          isActive={ this.Base.isModalActive("Contacts") }
          onClosePress={ () => this.Base.closeModal("Contacts") }
        />

      </ModalPartialContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => {
  return {

  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(UnallocatedAmount);

