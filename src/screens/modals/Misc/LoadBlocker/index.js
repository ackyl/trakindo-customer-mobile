/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  IMPORTS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import ModalContainer from "@app-components-containers/ModalBlocker";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  LoadSpinnerFragment,
} from "@app-components-fragments";




/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  MAIN CLASS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/
export default class LoadBlocker extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLoader() {
    const {
      isActive,
    } = this.props;

    if (!isActive) {
      return null;
    }

    return (
      <LoadSpinnerFragment
        isActive={ true }
      />
    );
  }



// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
      >
        <Spacer flex={ 1 } vAlign="center" hAlign="center">
          { this._renderLoader() }
        </Spacer>
      </ModalContainer>
    );
  }

}
