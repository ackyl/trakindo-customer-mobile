/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";
import {
  Linking,
} from "react-native";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBS,
} from "@app-components-core/Text";
import {
  ServiceRequestCard,
} from "@app-components-cards";
import {
  SegmentTitleFragment,
} from "@app-components-fragments";
import ModalPartialContainer from "@app-components-containers/ModalPartial";
import {
  Spacer,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class RequestTypeSelection extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    const basedData = this.Base.initBaseState(DATA, MODAL);
    basedData.data.unitDetail = props.navData && props.navData.unitDetail ? props.navData.unitDetail : {};

    this.state = {
      ... basedData,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  onPreventivePress() {
    const {
      onClosePress,
    } = this.props;

    if (onClosePress) {
      onClosePress();
    }

    Navigation.push("CreateRequest", {
      pageDetail: {
        title: "Preventive Maintenance",
        serviceType: "PM",
      },
      unit: this.Base.data("unitDetail"),
    });
  }

// ----------------------------------------

  onTroubleshootingPress() {
    const {
      onClosePress,
    } = this.props;

    if (onClosePress) {
      onClosePress();
    }

    Navigation.push("CreateRequest", {
      pageDetail: {
        title: "Troubleshooting Maintenance",
        serviceType: "TS",
      },
      unit: this.Base.data("unitDetail"),
    });
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTitle() {
    return (
      <SegmentTitleFragment
        noAnimation
      >
        Request Service
      </SegmentTitleFragment>
    );
  }

// ----------------------------------------

  _renderPreventive() {
    return (
      <Spacer space={ 1 }>
        <ServiceRequestCard
          type={ "preventive" }
          onPress={ () => this.onPreventivePress() }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderTroubleshooting() {
    return (
      <ServiceRequestCard
        type={ "troubleshooting" }
        onPress={ () => this.onTroubleshootingPress() }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalPartialContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
      >

        { this._renderTitle() }

        { this._renderPreventive() }

        { this._renderTroubleshooting() }

      </ModalPartialContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => {
  return {

  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(RequestTypeSelection);

