/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import ModalContainer from "@app-components-containers/Modal";
import {
  SalesRepSelectionItemList,
} from "@app-components-lists";
import {
  SegmentTitleFragment,
} from "@app-components-fragments";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  CompanyAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class SalesRepSelection extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);

    this.Base.setData("salesReps", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getSalesRepList() {
    if (this.Base.data("salesReps").length) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getSalesRepList({
      soldtoId,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  onOptionSelected(item, index) {
    const {
      onSelect,
    } = this.props.navData;

    if (onSelect) {
      onSelect(item, index);
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTitle() {
    return (
      <SegmentTitleFragment noAnimation>
        Sales Reps
      </SegmentTitleFragment>
    );
  }

// ----------------------------------------

  _renderSalesReps() {
    const data = this.Base.data("salesReps");

    const {
      activeIndex,
    } = this.props.navData;

    return (
      <SalesRepSelectionItemList
        data={ data }
        loading={ this.Base.isLoading("salesReps") }
        onItemPress={ (item, index) => this.onOptionSelected(item, index) }
        activeIndex={ activeIndex }
        order={ 1 }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
        onShow={ () => this.getSalesRepList() }
        scrollable
      >

        { this._renderTitle() }

        { this._renderSalesReps() }

      </ModalContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),

    salesReps: CompanySelector.salesReps(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getSalesRepList: (data, onComplete) => dispatch(CompanyAction.getSalesRepList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(SalesRepSelection);

