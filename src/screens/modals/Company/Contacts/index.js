/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
} from "@core-components-enhancers";
import ModalContainer from "@app-components-containers/Modal";
import {
  SalesRepCallList,
} from "@app-components-lists";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  CompanyAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Contacts extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);

    this.Base.setData("salesReps", nextProps, nextState);

    return true;
  }


// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getSalesRepList() {
    if (this.Base.data("salesReps").length) {
      return false;
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getSalesRepList({
      soldtoId,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderSalesReps() {
    const data = this.Base.data("salesReps");

    return (
      <Spacer space={ 1 }>
        <SalesRepCallList
          data={ data }
          loading={ this.Base.isLoading("salesReps") }
          order={ 1 }
        />
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
        onShow={ () => this.getSalesRepList() }
        scrollable
      >

        <Spacer top={ 2 }>
          { this._renderSalesReps() }
        </Spacer>

      </ModalContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),

    salesReps: CompanySelector.salesReps(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getSalesRepList: (data, onComplete) => dispatch(CompanyAction.getSalesRepList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);

