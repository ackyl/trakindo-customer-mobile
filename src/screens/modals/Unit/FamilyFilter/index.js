/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
} from "@app-components-core/Text";
import ModalContainer from "@app-components-containers/Modal";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  UnitFamilyFilterItemList,
} from "@app-components-lists";
import {
  SegmentTitleFragment,
} from "@app-components-fragments";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as UnitSelector from "@app-selectors/unit";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class FamilyFilter extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("list", nextProps, nextState);

    return true;
  }

// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  onFilterSelected(item, index) {
    const {
      onSelect,
    } = this.props.navData;

    if (onSelect) {
      onSelect(item, index);
    }
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTitle() {
    return (
      <SegmentTitleFragment noAnimation>
        Equipment Families
      </SegmentTitleFragment>
    );
  }

// ----------------------------------------

  _renderList() {
    const data = this.Base.data("list");

    const {
      activeIndex,
    } = this.props.navData;

    return (
      <UnitFamilyFilterItemList
        data={ data }
        onItemPress={ (item, index) => this.onFilterSelected(item, index) }
        activeIndex={ activeIndex }
        order={ 1 }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
        scrollable
      >

        { this._renderTitle() }

        { this._renderList() }

      </ModalContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    list: UnitSelector.familyList(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(FamilyFilter);

