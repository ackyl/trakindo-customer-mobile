/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import ModalContainer from "@app-components-containers/Modal";
import {
  UnitList,
} from "@app-components-lists";
import {
  Spacer,
} from "@core-components-enhancers";
import {
  FilterBarFragment,
  SegmentTitleFragment,
  AddUnitAccessFragment,
} from "@app-components-fragments";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as UnitSelector from "@app-selectors/unit";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  UnitAction,
} from "@app-actions";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class UnitSelection extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),

      activeIndex: {
        familyFilter: 0,
      },

      isLoaded: false,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("familyList", nextProps, nextState);

    this.Base.setData("list", nextProps, nextState);

    if (nextState.activeIndex.familyFilter > 0) {
      this.Base.setData("filteredList", nextProps, nextState);
    }

    return true;
  }

// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getUnitList(page:number = 1, family:string = "", dataType:string = "list") {
    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getUnitList({
      soldtoId,
      page,
      family,
    });

    this.setState({
      isLoaded: true,
    });

    this.Base.setPage(dataType, page);
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  getSelectedFamily() {
    const families = this.Base.data("familyList");
    const selectedFamily = families[this.state.activeIndex.familyFilter - 1];

    return selectedFamily ? selectedFamily : {
      name: "All Equipment",
      slug: "",
    };
  }

// ----------------------------------------

  onUnitSelected(data) {
    const {
      onSelect,
    } = this.props.navData;

    if (onSelect) {
      onSelect(data);
    }
  }

// ----------------------------------------

  onFilterSelected(item, index) {
    const {
      activeIndex,
    } = this.state;

    if (activeIndex.familyFilter === index) {
      return null;
    }

    activeIndex.familyFilter = index;

    this.Base.resetList("filteredList", {activeIndex});

    const selectedFamily = this.getSelectedFamily();

    const dataType = index === 0 ? "list" : "filteredList";

    this.getUnitList(1, selectedFamily.slug, dataType);

    this.Base.closeModal("FamilyFilter");
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTitle() {
    return (
      <SegmentTitleFragment>
        Choose Equipment
      </SegmentTitleFragment>
    );
  }

// ----------------------------------------

  _renderFilterHead() {
    return (
      <Spacer top={ 1 } >
        <FilterBarFragment
          onChange={ (item, index) => this.onFilterSelected(item, index) }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderEquipments() {
    const index = this.state.activeIndex.familyFilter;

    const dataType = index === 0 ? "list" : "filteredList";
    const selectedFamily = this.getSelectedFamily();
    const dataName = selectedFamily.name;

    const data = this.Base.data(dataType);

    return (
      <Spacer flex={ 1 }>
        <UnitList
          data={ data }
          onItemPress={ (itemData) => this.onUnitSelected(itemData) }
          loading={ this.Base.isLoading(dataType) }
          page={ this.Base.currentPage(dataType) }
          emptyText={ `No ${dataName} Found` }
          loadNext={ (page) => this.getUnitList(page, selectedFamily.slug, dataType) }
          order={ 2 }
          key={ index }
        />
      </Spacer>
    );
  }

// ----------------------------------------

  _renderBottom() {
    const {
      onAddNewPress,
    } = this.props.navData;

    return (
      <AddUnitAccessFragment
        onPress={ onAddNewPress }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
        onShow={ !this.state.isLoaded ? () => this.getUnitList() : null }
        bottomContent={ this._renderBottom() }
      >
        { this._renderTitle() }

        { this._renderFilterHead() }

        { this._renderEquipments() }
      </ModalContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    familyList: UnitSelector.familyList(state, props),

    list: UnitSelector.list(state, props),
    filteredList: UnitSelector.filteredList(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getUnitList: (data, onComplete) => dispatch(UnitAction.getList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(UnitSelection);

