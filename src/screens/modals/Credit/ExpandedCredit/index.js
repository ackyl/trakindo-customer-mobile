/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextBS,
} from "@app-components-core/Text";
import {
  PayerInformationCard,
  CreditExpandedCard,
} from "@app-components-cards";
import {
  ButtonFullPrimary,
} from "@app-components-core/Button";
import ModalContainer from "@app-components-containers/Modal";
import {
  Spacer,
  Padder,
  VerticalAnimator,
} from "@core-components-enhancers";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  Contacts as ContactsModal,
} from "@app-modals/Company";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as CreditSelector from "@app-selectors/credit";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class ExpandedCredit extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);
    this.Base.setData("payer", nextProps, nextState);
    this.Base.setData("credit", nextProps, nextState);

    return true;
  }

// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderAccountDetail() {
    const {
      payerId,
    } = this.Base.data("payer") ? this.Base.data("payer") : {};
    const {
      name,
    } = this.Base.data("company") ? this.Base.data("company") : {};

    return (
      <VerticalAnimator order={ 1 }>
        <Spacer space={ 2.5 }>
          <PayerInformationCard
            payerId={ payerId }
            name={ name }
          />
        </Spacer>
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderCredits() {
    const data = this.Base.data("credit");

    return (
      <VerticalAnimator order={ 2 }>
        <CreditExpandedCard
          { ... data }
        />
      </VerticalAnimator>
    );
  }

// ----------------------------------------

  _renderBottom() {
    return (
      <Spacer>
        <Padder>
          <Spacer space={ 1 }> 
            <TextBS
              mode="light2"
            >
              Call us if you need help about your credit status and agreement
            </TextBS>
          </Spacer>

          <ButtonFullPrimary
            onPress={ () => this.Base.openModal("Contacts") }
          >
            Call Us
          </ButtonFullPrimary>
        </Padder>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
      >

        <Spacer flex={ 1 } top={ 2 }>
          { this._renderAccountDetail() }

          { this._renderCredits() }
        </Spacer>

        { this._renderBottom() }

        <ContactsModal
          isActive={ this.Base.isModalActive("Contacts") }
          onClosePress={ () => this.Base.closeModal("Contacts") }
        />

      </ModalContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),
    payer: CreditSelector.payer(state, props),
    credit: CreditSelector.selected(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(ExpandedCredit);

