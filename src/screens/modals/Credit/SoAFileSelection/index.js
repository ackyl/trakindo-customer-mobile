/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";
import moment from "moment";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import ModalContainer from "@app-components-containers/Modal";
import {
  SoAFileSelectionItemList,
} from "@app-components-lists";
import {
  SegmentTitleFragment,
} from "@app-components-fragments";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CreditSelector from "@app-selectors/credit";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  CreditAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class SoAFileSelection extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),

      periodCounter: 0,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("payer", nextProps, nextState);

    this.Base.setData("list", nextProps, nextState);

    if (
      nextState.periodCounter === 0 &&
      nextState.data.list.length === 0 &&
      !nextState.loadingState.list &&
      nextProps.isActive
    ) {
      this.getSoaFileList(1);
    }

    return true;
  }

// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getSoaFileList(periodCounter:number = 0) {
    if (this.Base.data("list").length || periodCounter > 1) {
      return false;
    }

    this.Base.startLoading("list");

    const {
      payerId,
    } = this.Base.data("payer");

    let thisMonth = moment();
    let periodeDate = ((thisMonth.format("DD") * 1)) > 15 ? 2 : 1;

    if (periodCounter > 0) {
      periodeDate = periodeDate === 1 ? 2 : 1;
      if (periodeDate === 2) {
        thisMonth = thisMonth.subtract(1, "month");
      }
    }

    this.props.getSoaFileList({
      payerId,
      period: `${thisMonth.format("YYYY-MM")}-0${periodeDate}`,
    });

    this.setState({
      periodCounter,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  onSelected(item, index) {
    const {
      onSelect,
    } = this.props.navData;

    if (onSelect) {
      onSelect(item, index);
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTitle() {
    return (
      <SegmentTitleFragment noAnimation>
        Statement of Accounts
      </SegmentTitleFragment>
    );
  }

// ----------------------------------------

  _renderList() {
    const data = this.Base.data("list");
    const {
      payerId,
    } = this.Base.data("payer");

    const {
      activeIndex,
    } = this.props.navData;

    return (
      <SoAFileSelectionItemList
        data={ data.map((item) => {
          return {
            ... item,
            payerId,
          };
        }) }
        onItemPress={ (item, index) => this.onSelected(item, index) }
        loading={ this.Base.isLoading("list") }
        activeIndex={ activeIndex }
        order={ 2 }
      />
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
        onShow={ () => this.getSoaFileList() }
        scrollable
      >

        { this._renderTitle() }

        { this._renderList() }

      </ModalContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    payer: CreditSelector.payer(state, props),

    list: CreditSelector.soaFiles(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getSoaFileList: (data, onComplete) => dispatch(CreditAction.getSoaFileList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(SoAFileSelection);

