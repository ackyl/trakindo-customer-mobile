/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  TextH2,
  TextBS,
} from "@app-components-core/Text";
import ModalPartialContainer from "@app-components-containers/ModalPartial";
import {
  Spacer,
} from "@core-components-enhancers";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class BranchLocation extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderLocation() {
    const location = this.props.navData.branchLocation;

    return (
      <Spacer>
        <TextH2
          mode="light"
        >
          Branch location
        </TextH2>
        <TextBS
          mode="light2"
        >
          { location }
        </TextBS>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalPartialContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
      >

        { this._renderLocation() }

      </ModalPartialContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => {
  return {

  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(BranchLocation);

