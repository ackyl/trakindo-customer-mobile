/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
} from "@core-utils";
import {
  COLOR,
} from "./consts";



export default {
  va: {
    grouper: {
      backgroundColor: COLOR.grayT1,
      padding: Config.base(2),
      borderRadius: Config.base(1),
    },

    image: {
      container: {
        width: 70,
        height: 43,
        backgroundColor: COLOR.white,
        borderRadius: Config.base(1),
      },
    },
  },
};
