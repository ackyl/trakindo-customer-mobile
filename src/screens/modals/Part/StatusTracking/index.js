/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import ModalContainer from "@app-components-containers/Modal";
import {
  PartItemDetailCard,
} from "@app-components-cards";
import {
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  PartShippingTrackingCard,
} from "@app-components-cards";
import {
  SegmentTitleFragment,
} from "@app-components-fragments";
import {
  ButtonFullPrimary,
} from "@app-components-core/Button";

// ----------------------------------------
// MODAL IMPORTS
// ----------------------------------------
import {
  Contacts as ContactsModal,
} from "@app-modals/Company";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CompanySelector from "@app-selectors/company";
import * as PartShippingSelector from "@app-selectors/partShipping";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  PartShippingAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class StatusTracking extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),

      activeCaseNumber: 0,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    this.Base.setData("company", nextProps, nextState);

    this.Base.setData("list", nextProps, nextState);

    return true;
  }

// ----------------------------------------
// ----------------------------------------
// DATA METHODS
// ----------------------------------------

  getList() {
    const {
      caseNumber,
    } = this.props.navData.itemDetail;
    // } = this.Base.data("itemDetail");

    if (this.state.activeCaseNumber !== 0 && this.state.activeCaseNumber !== caseNumber) {
      this.Base.resetList("list");
    }

    const {
      soldtoId,
    } = this.Base.data("company");

    this.props.getList({
      soldtoId,
      caseNumber,
    });

    this.setState({
      activeCaseNumber: caseNumber,
    });
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTop(status:string = "ON_TIME", isOriginEmpty:bool = false) {
    const data = this.props.navData.itemDetail;

    return (
      <Spacer space={ 2.5 }>
        <VerticalAnimator order={ 1 }>
          <PartItemDetailCard
            { ... data }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderMain() {
    const data = this.Base.data("list");

    return (
      <Spacer space={ 2.5 }>
        <SegmentTitleFragment order={ 2 }>
          Parts Tracking
        </SegmentTitleFragment>

        <VerticalAnimator order={ 3 }>
          <PartShippingTrackingCard
            statuses={ data }
            loading={ this.Base.isLoading("list") }
            order={ 3 }
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButton() {
    return (
      <VerticalAnimator order={ 4 }>
        <ButtonFullPrimary onPress={ () => this.Base.openModal("Contacts") }>
          Need help?
        </ButtonFullPrimary>
      </VerticalAnimator>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
        onShow={ () => this.getList() }
        scrollable
      >

        <Spacer top={ 2.5 }>
          { this._renderTop() }

          { this._renderMain() }

          { this._renderButton() }
        </Spacer>

        <ContactsModal
          isActive={ this.Base.isModalActive("Contacts") }
          onClosePress={ () => this.Base.closeModal("Contacts") }
        />

      </ModalContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    company: CompanySelector.selected(state, props),

    list: PartShippingSelector.trackingList(state, props),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    getList: (data, onComplete) => dispatch(PartShippingAction.getShippingStatusList(data, onComplete)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(StatusTracking);

