/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullPrimary,
  ButtonFullSecondary,
} from "@app-components-core/Button";
import ModalContainer from "@app-components-containers/Modal";
import {
  Padder,
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  ProceesInformationHeadFragment,
} from "@app-components-fragments";




/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  MAIN CLASS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/
class LogoutConfirmation extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTop() {
    return (
      <Spacer top={ 6 } space={ 5 }>
        <VerticalAnimator order={ 1 }>
          <ProceesInformationHeadFragment
            icon="logout"
            title="Log Out"
            description="Are you sure want to log out now?"
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      onConfirm,
    } = this.props.navData;

    return (
      <VerticalAnimator order={ 2 }>
        <Spacer row width={ Device.windows.width - Config.paddingHorizontal(6) }>
          <Spacer flex={ 1 }>
            <ButtonFullPrimary
              onPress={ onConfirm }
            >
              OK
            </ButtonFullPrimary>
          </Spacer>

          <Spacer horizontal space={ 2 }/>

          <Spacer flex={ 1 }>
            <ButtonFullSecondary
              onPress={ this.props.onClosePress }
            >
              Cancel
            </ButtonFullSecondary>
          </Spacer>
        </Spacer>
      </VerticalAnimator>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
      >
        <Padder pad={ 4 }>
          { this._renderTop() }

          { this._renderButton() }
        </Padder>
      </ModalContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {

  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(LogoutConfirmation);

