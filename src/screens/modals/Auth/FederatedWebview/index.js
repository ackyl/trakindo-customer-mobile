/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";
import { WebView } from "react-native-webview";

// ----------------------------------------
// BASE IMPORTS
// ----------------------------------------
import {
  Screen,
} from "@core-abstracts";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
  DATA,
  MODAL,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import ModalContainer from "@app-components-containers/Modal";
import {
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";




/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  MAIN CLASS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/
class FederatedWebview extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.Base = new Screen(this);

    this.state = {
      ... this.Base.initBaseState(DATA, MODAL),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderWebview() {
    const {
      uri,
    } = this.props.navData;

    return (
      <Spacer top={ 3 }>
        <VerticalAnimator order={ 1 }>
          <Spacer
            height={ Device.windows.height * .65 }
            backgroundColor={ COLOR.whiteT4 }
            radius={ Config.base(1) }
            style={{ padding: Config.base(1) }}
          >

            <Spacer
              flex={ 1 }
              backgroundColor={ COLOR.white }
              radius={ Config.base(1) }
              style={{
                overflow: "hidden"
              }}
            >
              <WebView
                startInLoadingState={true}
                source={{ uri }}
                style={{ borderRadius: Config.base(1) }}
                onLoadProgress={({ nativeEvent }) => {
                  console.warn(nativeEvent.progress);
                }}
                mixedContentMode={ "always"}
              />
            </Spacer>
          </Spacer>
        </VerticalAnimator>
      </Spacer>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
      >
        { this._renderWebview() }
      </ModalContainer>
    );
  }

}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {

  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(FederatedWebview);

