/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  IMPORTS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullPrimary,
} from "@app-components-core/Button";
import ModalContainer from "@app-components-containers/Modal";
import {
  Padder,
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  ProceesInformationHeadFragment,
} from "@app-components-fragments";




/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  MAIN CLASS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/
export default class Unauthorized extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTop() {
    return (
      <Spacer top={ 6 } space={ 5 }>
        <VerticalAnimator order={ 1 }>
          <ProceesInformationHeadFragment
            icon="login"
            title="Session Expired"
            description="Please re-login ro renew your session"
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      onReloginPressed,
    } = this.props.navData;

    return (
      <VerticalAnimator order={ 2 }>
        <ButtonFullPrimary
          onPress={ onReloginPressed }
        >
          OK
        </ButtonFullPrimary>
      </VerticalAnimator>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        unclosable
      >
        <Padder pad={ 4 }>
          { this._renderTop() }

          { this._renderButton() }
        </Padder>
      </ModalContainer>
    );
  }

}
