/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  IMPORTS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Navigation,
} from "@core-utils";
import Styles from "./style";
import {
  COLOR,
} from "./consts";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  ButtonFullPrimary,
} from "@app-components-core/Button";
import ModalContainer from "@app-components-containers/Modal";
import {
  Padder,
  Spacer,
  VerticalAnimator,
} from "@core-components-enhancers";
import {
  ProceesInformationHeadFragment,
} from "@app-components-fragments";




/*
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*  MAIN CLASS
* ---------------------------------------------------------------------------------------
* ---------------------------------------------------------------------------------------
*/
export default class NetworkConnection extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderTop() {
    return (
      <Spacer top={ 6 } space={ 5 }>
        <VerticalAnimator order={ 1 }>
          <ProceesInformationHeadFragment
            icon="connection"
            title="Network Connection Error"
            description="The app is taking a long time to load. Please ensure that a strong internet connection"
          />
        </VerticalAnimator>
      </Spacer>
    );
  }

// ----------------------------------------

  _renderButton() {
    const {
      onRetryPressed,
    } = this.props.navData;

    return (
      <VerticalAnimator order={ 2 }>
        <ButtonFullPrimary
          onPress={ onRetryPressed }
        >
          Retry
        </ButtonFullPrimary>
      </VerticalAnimator>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ModalContainer
        visible={ this.props.isActive }
        onClosePress={ this.props.onClosePress }
      >
        <Padder pad={ 4 }>
          { this._renderTop() }

          { this._renderButton() }
        </Padder>
      </ModalContainer>
    );
  }

}
