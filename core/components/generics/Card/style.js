/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";

const THEME = Config.theme("Card.default");


export default {
  main: {
    borderWidth: 0,
    borderRadius: THEME.radius,
    backgroundColor: THEME.color,
    borderColor: THEME.borderColor,
    paddingHorizontal: THEME.hPadding,
    paddingVertical: THEME.vPadding,
    overflow: "hidden",
	},

  clickableContainer: {
    flex: -1,
  },

  shadow: {
    shadowColor: "#c7cad1",
    shadowOpacity: .5,
    shadowRadius: 6,
    shadowOffset: { width: 0, height: 2 },
    backgroundColor: "white",
    borderRadius: THEME.radius,
  },
};
