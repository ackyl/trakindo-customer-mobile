/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, { Component } from "react";
import ShadowView from "react-native-simple-shadow-view";
import {
  TouchableOpacity as RClickableCard,
  View as RCard,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import Styles from "./style";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Card extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  getComposedStyle() {
    const composedStyle = [Styles.main];
    const newStyle = {};

    if (this.props.flex !== undefined && this.props.flex !== null) {
      newStyle.flex = this.props.flex;
    }

    if (this.props.radius) {
      newStyle.borderRadius = this.props.radius;
    }

    if (this.props.color) {
      newStyle.backgroundColor = this.props.color;
    }

    if (this.props.bordered && this.props.borderColor) {
      newStyle.borderWidth = this.props.bordered;
      newStyle.borderColor = this.props.borderColor;
    }

    if (this.props.hPadding !== undefined && this.props.hPadding !== null) {
      newStyle.paddingHorizontal = this.props.hPadding;
    }

    if (this.props.vPadding !== undefined && this.props.vPadding !== null) {
      newStyle.paddingVertical = this.props.vPadding;
    }

    composedStyle.push(newStyle);
    composedStyle.push(this.props.style);

    return composedStyle;
  }

// ----------------------------------------

  getComposedShadowStyle() {
    const composedStyle = [Styles.shadow];
    const newStyle = {};

    if (this.props.flex !== undefined && this.props.flex !== null) {
      newStyle.flex = this.props.flex;
    }

    if (this.props.radius) {
      newStyle.borderRadius = this.props.radius;
    }

    if (this.props.shadowColor) {
      newStyle.shadowColor = this.props.shadowColor;
    }

    composedStyle.push(newStyle);
    // composedStyle.push(this.props.style);

    return composedStyle;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  _renderMain() {
    if (this.props.onPress) {
      return (
        <RClickableCard
          onPress={ this.props.onPress }
        >
          <RCard
            { ... this.props }
            style={ this.getComposedStyle() }
          >
            { this.props.children }
          </RCard>
        </RClickableCard>
      );
    }

    return (
      <RCard
        { ... this.props }
        style={ this.getComposedStyle() }
      >
        { this.props.children }
      </RCard>
    );
  }

// ----------------------------------------

  render() {
    const {
      shadowed,
      noShadow,
    } = this.props;

    if (!shadowed || noShadow) {
      return this._renderMain();
    }

    return (
      <ShadowView
        style={ this.getComposedShadowStyle() }
      >
        { this._renderMain() }
      </ShadowView>
    );
  }

// ----------------------------------------

}
