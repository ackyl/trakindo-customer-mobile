/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, { Component } from "react";
import {
  TextInput as RTextInput,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import Styles from "./style";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Input extends Component {

// ---------------------------------------------------
// ---------------------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ---------------------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }

// ---------------------------------------------------

  componentDidMount() {
    if (this.props.cref) {
      this.props.cref(this.ref);
    }
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  getComposedStyle() {
    const composedStyle = [Styles.main];
    const newStyle = {};

    if (this.props.line !== undefined && this.props.line !== null) {
      newStyle.height = this.props.line;
    }

    if (this.props.hPadding !== undefined && this.props.hPadding !== null) {
      newStyle.paddingHorizontal = this.props.hPadding;
    }

    if (this.props.vPadding !== undefined && this.props.vPadding !== null) {
      newStyle.paddingVertical = this.props.vPadding;
    }

    composedStyle.push(newStyle);
    composedStyle.push(this.props.style);

    return composedStyle;
  }


// ---------------------------------------------------
// ---------------------------------------------------
// MAIN RENDER
// ---------------------------------------------------

  render() {
    return (
      <RTextInput
        underlineColorAndroid={ "transparent" }
        { ... this.props }
        style={ this.getComposedStyle() }
        // placeholderTextColor={ this.props.placeholderColor ? this.props.placeholderColor : THEME.inactiveColor }
        secureTextEntry={ this.props.secured }
        //         autoCorrect={ this.props.autoCorrect ? this.props.autoCorrect : THEME.autoCorrect }
        //         autoCapitalize={ this.props.autoCapitalize ? this.props.autoCapitalize : THEME.autoCapitalize }
        //         keyboardType={ this.props.keyboard ? this.props.keyboard : THEME.keyboard }
        ref={ (r) => {this.ref = r;} }
      />
    );
  }
}
