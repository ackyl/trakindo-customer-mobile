/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";

const THEME = Config.theme("Input.default");
const TEXT_THEME = Config.theme("Text.InputValue");


export default {
	main: {
		flex: -1,
    paddingBottom: 0,
    marginBottom: 0,
    height: TEXT_THEME.line,
    paddingHorizontal: THEME.hPadding,
    paddingVertical: THEME.vPadding,
		color: THEME.textColor,
    fontSize: TEXT_THEME.size,
		// lineHeight: TEXT_THEME.line,
		letterSpacing: TEXT_THEME.spacing,
	},
};
