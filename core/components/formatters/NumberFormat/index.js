/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, { Component } from "react";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class NumberFormat extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props != nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  format(value:string = "0", delimitter:string = ".") {
    return value
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${delimitter}`);
  }

// ----------------------------------------

  currency(currency:string = "IDR", value:string = "0") {
    switch (currency) {
      case "USD":
        return `$ ${value}`;

      case "IDR":
      default:
        return `Rp. ${value}`;
    }
  }

// ----------------------------------------

  getDelimiter() {
    if (this.props.delimitter) {
      return this.props.delimitter;
    }

    switch (this.props.currency) {
      case "USD":
        return ",";

      case "IDR":
      default:
        return ".";
    }
  }

// ----------------------------------------

  addFormat(stringFormat, value:string) {
    return stringFormat(value);
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    let value = this.props.value ? this.props.value + "" : "0";

    if (this.props.currency && this.props.currency === "USD") {
      value = parseFloat(value).toFixed(2);
    }

    let formatted = this.format(value, this.getDelimiter());

    if (this.props.zeroLead && value < (10 * this.props.zeroLead)) {
      for (let x = 0; x < this.props.zeroLead; x++) {
        formatted = `0${formatted}`;
      }
    }

    if (this.props.currency) {
      formatted = this.currency(this.props.currency, formatted);
    }

    if (this.props.format) {
      formatted = this.addFormat(this.props.format, formatted);
    }

    return formatted;
  }


// ----------------------------------------

}
