/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import { connect } from "react-redux";

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import Locales from "@app-locales";

// ----------------------------------------
// SELECTORS IMPORTS
// ----------------------------------------
import * as CoreSelector from "@app-selectors/core";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class TranslateFormatter extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      children,
      selected,
      modifier,
    } = this.props;

    if (!children) {
      return null;
    }

    const tempText = children;

    if (typeof tempText !== "string") {
      return children;
    }

    const text = children.trim().replace(/ /g, "_").toLowerCase();

    let maps = text;
    
    if (!this.props.flat) {
      maps = text.split(".");
    } else {
      maps = text.split();
    }

    console.log(maps)

    let mapResult = Locales[selected.data];

    // console.log(mapResult)

    for (let i = 0; i < maps.length; i++) {
      if (mapResult[maps[i]] === undefined || mapResult[maps[i]] === null) {
        return children;
      }

      mapResult = mapResult[maps[i]];
    }

    console.log(mapResult)

    if (modifier) {
      mapResult = modifier(mapResult);
    }

    return mapResult;
  }


}


// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state, props) => {
  return {
    selected: CoreSelector.selectedLocale(state, props),
    // mapper: state.language.get("activeTranslation").toJS(),
  };
};

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(TranslateFormatter);
