/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  View,
  Animated,
  Easing,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class HorizontalAnimator extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      position: new Animated.Value(props.noAnimation ? 0 : Device.windows.width),
      opacity: new Animated.Value(props.noAnimation ? 1 : 0),
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state !== nextState) {
      return true;
    }

    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }

// ----------------------------------------

  componentDidMount() {
    if (!this.props.noAnimation) {
      Animated.parallel([
        Animated.timing(this.state.position, {
          toValue: 0,
          duration: 300,
          delay: !this.props.order ? 100 : (this.props.order * 60),
        }),

        Animated.timing(this.state.opacity, {
          toValue: 1,
          duration: 400,
          delay: !this.props.order ? 100 : (this.props.order * 60),
        }),
      ]).start();
    }
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const style = [];

    if (!this.props.noAnimation) {
      style.push({
          left: this.state.position,
          opacity: this.state.opacity,
        });
    }

    return (
      <Animated.View style={ style }>
        { this.props.children }
      </Animated.View>
    );
  }


// ----------------------------------------

}
