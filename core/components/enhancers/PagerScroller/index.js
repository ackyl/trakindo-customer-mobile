/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  ScrollView,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Pager,
  MainScroller,
} from "@core-components-enhancers";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class PagerScroller extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.pagerRef = null;
  }

// ----------------------------------------

  componentDidMount() {
    const {
      cref,
    } = this.props;

    if (cref) {
      cref(this.pagerRef);
    }
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onScroll(event) {
    const {
      onPageSwitch,
    } = this.props;

    if (!onPageSwitch) {
      return null;
    }

    const xOffset = event.nativeEvent.contentOffset.x;
    const newIndex = Math.round(xOffset / Device.windows.width);

    onPageSwitch(newIndex);
  }

// ----------------------------------------

  onPageSwitch(newIndex:number) {
    const {
      onPageSwitch,
    } = this.props;

    if (!onPageSwitch) {
      return null;
    }

    onPageSwitch(newIndex);
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderListInner(child, index:number) {
    return (
      <MainScroller
        width={ Device.windows.width }
        key={ index }
      >
        { child }
      </MainScroller>
    );
  }


// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      children,
    } = this.props;

    return (
      <Pager
        cref={ ref => {this.pagerRef = ref;} }
        onPageSwitch={ (newIndex) => this.onPageSwitch(newIndex) }
      >
        { (children ? children : []).map((child, index) => {
          return this._renderListInner(child, index);
        }) }
      </Pager>
    );
  }

}
