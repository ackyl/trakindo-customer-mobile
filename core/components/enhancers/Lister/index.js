/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  RefreshControl,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------
import {
  Spacer,
  MainScroller,
  ListStatic,
  ListScroller,
} from "@core-components-enhancers";
import {
  LoadSpinnerFragment,
} from "@app-components-fragments";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Lister extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      page: props.page ? props.page : 1,
      lastUpdated: 0,
      isRefreshing:false,
      showLoadMore: false,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      if (
        this.props.page < nextProps.page ||
        nextProps.page === 1 && nextState.isRefreshing
      ) {
        this.setState({
          page: nextProps.page,
          isRefreshing: false,
          lastUpdated: (new Date()).getTime(),
        });
      }

      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onReachingBottom() {
    if (this.state.isRefreshing) {
      return false;
    }

    const {
      loadNext,
      loading,
    } = this.props;

    if (
      this.state.lastUpdated < (new Date()).getTime() - 1000 &&
      !loading
    ) {
      const page = this.props.page + 1;

      this.setState({
        lastUpdated: (new Date()).getTime(),
        showLoadMore: true,
        page,
      });

      if (loadNext) {
        loadNext(page);
      }
    }
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderRefreshControl() {
    const {
      loadNext,
      loading,
      noRefresh,
    } = this.props;

    if (loading || !loadNext || noRefresh) {
      return null;
    }

    return (
      <RefreshControl
        onRefresh={ () => {

          this.setState({
            isRefreshing: true,
          });

          if (loadNext) {
            loadNext(1);
          }

        } }
        refreshing={ this.state.isRefreshing }
        colors={[Config.colors().orange]}
        tintColor={ Config.colors().orange }
      />
    );
  }

// ----------------------------------------

  _renderLoadMoreControl() {
    const {
      loadNext,
      loading,
    } = this.props;

    if (loading || !loadNext) {
      return null;
    }

    if (this.props.children.length % 15 > 0) {
      return null;
    }

    if (this.state.lastUpdated > ((new Date()).getTime() + (30 * 1000))) {
      return null;
    }

    return (
      <Spacer
        hAlign="center"
        vAlign="center"
        top={ 1 }
        bottom={ 1 }
      >
        <LoadSpinnerFragment
          isActive={ true }
          size={ 25 }
        />
      </Spacer>
    );
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      list,
      row,
      noPaddingBottom,
      padded,
      renderer,
      space,
      separator,
      scrollEnabled,
      onScroll,
    } = this.props;

    if (this.props.static) {
      return (
        <ListStatic
          renderer={ renderer }
          separator={ separator ? separator : (index) => <Spacer space={ space } key={ index }/> }
          noPaddingBottom={ noPaddingBottom }
          padded={ padded }
          onReachingBottom={ () => this.onReachingBottom() }
        >
          { this.props.children }
        </ListStatic>
      );
    }

    if (list) {
      return (
        <ListScroller
          renderer={ renderer }
          separator={ <Spacer space={ space }/> }
          noPaddingBottom={ noPaddingBottom }
          padded={ padded }
          onReachingBottom={ () => this.onReachingBottom() }
          scrollEnabled={ scrollEnabled }
          onScroll={ onScroll }
          refreshControl={ this._renderRefreshControl() }
          loadMoreControl={ this._renderLoadMoreControl() }
        >
          { this.props.children }
        </ListScroller>
      );
    }

    return (
      <MainScroller
        renderer={ renderer }
        separator={ (index) => <Spacer space={ space } key={ index }/> }
        horizontal={ row }
        noPaddingBottom={ noPaddingBottom }
        padded={ padded }
      >
        { this.props.children }
      </MainScroller>
    );
  }

}
