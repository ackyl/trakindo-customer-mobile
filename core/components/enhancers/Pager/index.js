/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  ScrollView,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Pager extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.pagerRef = null;
  }

// ----------------------------------------

  componentDidMount() {
    const {
      cref,
    } = this.props;

    if (cref) {
      cref(this.pagerRef);
    }
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

  onScroll(event) {
    const {
      onPageSwitch,
    } = this.props;

    if (!onPageSwitch) {
      return null;
    }

    const xOffset = event.nativeEvent.contentOffset.x;
    const newIndex = Math.round(xOffset / Device.windows.width);

    onPageSwitch(newIndex);
  }

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    return (
      <ScrollView
        ref={ ref => {this.pagerRef = ref;} }
        horizontal
        showsHorizontalScrollIndicator={ false }
        pagingEnabled
        onScroll={ (event) => this.onScroll(event) }
        scrollEventThrottle={ 16 }
        style={ Styles.container }
        removeClippedSubviews
      >
        { this.props.children }
      </ScrollView>
    );
  }

}
