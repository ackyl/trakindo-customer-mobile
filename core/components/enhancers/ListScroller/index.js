/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  FlatList,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class MainScroller extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      width,
      renderer,
      separator,
      horizontal,
      padded,
      noPaddingBottom,
      onReachingBottom,
      scrollEnabled,
      onScroll,
      refreshControl,
      loadMoreControl,
    } = this.props;

    return (
      <FlatList
        scrollEnabled={ scrollEnabled }
        onScroll={ (scrollEnabled === undefined || scrollEnabled !== false) &&  onScroll ? onScroll : null }
        scrollEventThrottle={ 16 }
        data={ this.props.children }
        renderItem={ ({item, index}) => renderer(item, index) }
        ItemSeparatorComponent={ separator ? () => separator : null }
        onEndReached={ onReachingBottom }
        onEndReachedThreshold={ .3 }
        keyExtractor={(item, index) => index.toString()}
        style={[
          // { backgroundColor: "red", flex: 1},
          { width: width ? width : null },
          {overflow: "hidden"},
        ]}
        contentContainerStyle={[
          Styles.contentContainer,
          horizontal ? {paddingVertical: 0, paddingBottom: 0} : {},
          noPaddingBottom ? {paddingBottom: 0} : {},
          padded ? {paddingHorizontal: Config.paddingHorizontal(),} : {},
        ]}
        bounces={ !!refreshControl }
        nestedScrollEnabled
        refreshControl={ refreshControl }
        ListFooterComponent={ loadMoreControl }
      />
    );
  }

}
