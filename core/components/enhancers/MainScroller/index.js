/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from "react";
import {
  ScrollView,
} from "react-native";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
  Validation,
  Navigation,
} from "@core-utils";
import Styles from "./style";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class MainScroller extends Component {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    super(props);

    this.state = {
      lastOffsetY: 0,
    };
  }

// ----------------------------------------

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props !== nextProps) {
      return true;
    }

    if (this.state !== nextState) {
      return true;
    }

    return false;
  }


// ----------------------------------------
// ----------------------------------------
// METHODS
// ----------------------------------------

// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

  _renderItems() {
    const {
      children,
      renderer,
      separator,
    } = this.props;

    const itemRenders = [];
    children.map((item, index) => {
      itemRenders.push(renderer(item, index));

      if (index < children.length - 1) {
        itemRenders.push(separator(index + "separator"));
      }
    });

    return itemRenders;
  }

// ----------------------------------------
// ----------------------------------------
// MAIN RENDER
// ----------------------------------------

  render() {
    const {
      width,
      horizontal,
      padded,
      noPaddingBottom,
    } = this.props;

    return (
      <ScrollView
        horizontal={ horizontal }
        showsHorizontalScrollIndicator={ false }
        style={[
          { width: width ? width : null },
          {overflow: "hidden"},
        ]}
        contentContainerStyle={[
          Styles.contentContainer,
          horizontal ? {paddingVertical: 0, paddingBottom: 0} : {},
          noPaddingBottom ? {paddingBottom: 0} : {},
          padded ? {paddingHorizontal: Config.paddingHorizontal(),} : {},
        ]}
      >
        { this._renderItems() }
      </ScrollView>
    );
  }

}
