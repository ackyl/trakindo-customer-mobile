/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import * as Sentry from "@sentry/react-native";


// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import ErrorReporting from "@app-configs/ErrorReporting";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  CONSTANTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
const setErrorReporting = !!ErrorReporting.dsn && ErrorReporting.enable;




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const up = () => {
  if (!setErrorReporting) {
    return null;
  }

  disableConsoleLoggings();

  Sentry.init({
    dsn: ErrorReporting.dsn,
  });
};

// ----------------------------------------

export const down = () => {
  if (!setErrorReporting) {
    return null;
  }
};

// ----------------------------------------

export const reportApiError = (error) => {
  if (!setErrorReporting) {
    return null;
  }

  let url = "";
  if (error.additionals) {
    for (let x in error.additionals) {
      Sentry.addBreadcrumb({
        category: "ERROR's " + x,
        message: error.additionals[x],
        level: Sentry.Severity.Info,
      });

      if (x === "url") {
        url = error.additionals[x];
      }
    }

    delete error.additionals;
  }

  if (url !== "") {
    let urlExp = url.split("?")[0];
    urlExp = url.split("/");

    url = urlExp[urlExp.length - 1];
  }

  const status = error.status;
  const errorConvert = JSON.stringify(error);

  Sentry.captureMessage("API Err | " + url + " | " + status + " | " + errorConvert, Sentry.Severity.warning);
};

// ----------------------------------------

export const disableConsoleLoggings = () => {
  if (ErrorReporting.consoleLogging) {
    return null;
  }

  console.log = () => {};
  console.group = () => {};
  console.warn = () => {};
  console.error = () => {};
  console.exception = () => {};
  console.info = () => {};
  console.debug = () => {};
  console.disableYellowBox = true;
  console.disableRedBox = true;
  console.reportErrorsAsExceptions = false;
};
