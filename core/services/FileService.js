/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import RNFetchBlob from "rn-fetch-blob";
import {
  Linking,
  PermissionsAndroid,
} from "react-native";


// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  Device,
} from "@core-utils";
import App from "@app-configs/App";
import ApiConfig from "@app-configs/Api";
import API from "@app-api-endpoints";
import APIDummy from "@app-api-dummies";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  ErrorAction,
} from "@app-actions";




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  CONSTANTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
const dataMode = App.dataMode;




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const download = (api, uri, name, ext, headers = {}, dispatch = null) => {
    const thisUrl = constructUrl(api, uri);

    const thisName = name;
    const thisExt = `.${ext}`;

    if (!Device.isAndroid) {

      Linking
        .openURL(thisUrl)
        .catch(err => console.warn("An error occurred", err)
      );

    } else {

      requestStoragePermission()
        .then((grantStatus) => {
          if (grantStatus === "granted" || grantStatus === "GRANTED") {
            const {
              config,
              fs,
            } = RNFetchBlob;

            const DownloadDir = Device.isAndroid ? fs.dirs.DownloadDir : fs.dirs.DocumentDir;

            const options = {
              fileCache: true,
              addAndroidDownloads : {
                useDownloadManager : true,
                notification : true,
                path:  DownloadDir + "/" + thisName + thisExt,
                description : thisName,
              },
            };

            config(options)
              .fetch("GET", thisUrl, headers)
              .then((downloadRes) => {
                // console.warn(downloadRes.info().status)
                // // alert("Success Downloaded");
                // console.warn("Success Downloaded");
              })
              .catch((errorMessage) => {
                errorMessage = (errorMessage + "").toLowerCase();

                if (dispatch) {
                  dispatch(ErrorAction.setErrorFile({errorMessage}));
                }

                // if (errorMessage.indexOf("failed to download")) {
                //   alert("FILE NOT FOUND");
                // } else {
                //   alert("DOWNLOAD FAILED");

                //   console.warn(errorMessage);
                // }
              });
          }
        });
    }
  };

// ----------------------------------------

  export const constructUrl = (api, uri) => {
    if ((dataMode === "DUMMY" || ApiConfig.forceMode[api] === "DUMMY") && ApiConfig.forceMode[api] !== "API") {

      return APIDummy[api]();

    } else {
      const {
        endpoint,
      } = API[api];

      return `${ApiConfig.baseUrl}${endpoint}` + uri;
    }
  };


// ----------------------------------------
// PRIVATE METHODS
// ----------------------------------------

  const requestStoragePermission = () => {
    if (!Device.isAndroid) {
      return new Promise((resolve) => {
        return resolve("GRANTED");
      });
    }

    try {
      return PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
        //   title: "Trakindo Mobile Storage Permission",
        //   message:
        //     "Trakindo Mobile needs access to your Storage to be able to store data to your device.",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Deny",
          buttonPositive: "Grant",
        },
      );

      // const grantStatus = PermissionsAndroid.RESULTS.GRANTED;

      // if (grantStatus === "granted" || grantStatus === "GRANTED") {
      //   console.log("You can use the Storage");
      // } else {
      //   console.log("Storage permission denied");
      // }
    } catch (err) {
      console.warn(err);
    }
  };
