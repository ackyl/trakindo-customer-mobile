/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import { fromJS, is } from "immutable";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const emptyList = () => ("EMPTY_LIST" + (new Date()).getTime());

// ----------------------------------------

export const emptyMap = () => ("EMPTY_MAP" + (new Date()).getTime());

// ----------------------------------------

export const isEmptyList = (item) => ((typeof item) === "string" &&  item.indexOf("EMPTY_LIST") > -1);

// ----------------------------------------

export const isEmptyMap = (item) => ((typeof item) === "string" &&  item.indexOf("EMPTY_MAP") > -1);

// ----------------------------------------

export const setStaticListState = (state, list, data) => {
  return state.set(list,
    (
      data.length > 0 ?
        fromJS(data)
      :
        emptyList())
    );
};

// ----------------------------------------

export const setPagedListState = (state, list, page, data) => {
  return state
    .set(list,
      page === 1 ?
        (data.length > 0 ? fromJS(data) : emptyList())
      :
        state
          .get(list)
          .push(... fromJS(data))
    );
};

// ----------------------------------------

export const pushListState = (state, list, data) => {
  if (!data) {
    return state;
  }

  if (state.get(list).indexOf(data) > -1) {
    return state;
  }

  if (isEmptyList(state.get(list))) {
    return state
      .set(list, fromJS([data]));
  }

  return state
    .set(list,
      state
        .get(list)
        .push(fromJS(data))
    );

};
