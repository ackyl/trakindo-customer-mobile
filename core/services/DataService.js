/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {
  MapperService,
  DummyService,
  ApiService,
  ErrorReportingService,
} from "@core-services";
import App from "@app-configs/App";
import Api from "@app-configs/Api";

// ----------------------------------------
// ACTION CREATOR IMPORTS
// ----------------------------------------
import {
  ErrorAction,
} from "@app-actions";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  CONSTANTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
const dataMode = App.dataMode;


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function request(api, param, onSuccess, preFunction, postFunction) {
  param = MapperService.mapRequestData(api, param);

  return (dispatch, getState) => {
    if (preFunction) {
      preFunction(dispatch);
    }

    let response = null;
    if ((dataMode === "DUMMY" || Api.forceMode[api] === "DUMMY") && Api.forceMode[api] !== "API") {
      response = DummyService.request(api, param);
    } else {
      response = ApiService.request(api, param, getState);
    }

    response
      .then(responseData => onSuccess(responseData, dispatch))
      .finally(() => {
        if (postFunction) {
          postFunction(dispatch);
        }
      })
      .catch(error => {

        ErrorReportingService.reportApiError(error);

        const currentPriority = getState().error.get("priority");

        let type = "";
        let priority = 1000;
        if (currentPriority >= 1 && error.status === 401) {
          type = "UnauthorizedError";
          priority = 1;
        } else if (currentPriority >= 2 && error.status === 999) {
          type = "NetworkError";
          priority = 2;
        } else if (currentPriority >= 3 && error.status === 500) {
          type = "ServerError";
          priority = 3;
        } else {
          // type = "GeneralError";
          type = "ServerError";
          priority = 4;
        }

        dispatch(ErrorAction.set({
          type,
          priority,
          code: error.code,
          message: error.message,
        }));

        console.warn("XERR", error);
      });
  };
}

// ----------------------------------------

export function direct(api, param, onSuccess) {
  return (dispatch, getState) => {
    onSuccess(null, dispatch);
  };
}

