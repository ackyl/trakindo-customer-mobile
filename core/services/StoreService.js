/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import {
  createStore,
  applyMiddleware,
  combineReducers,
  compose,
} from "redux";
import thunk from "redux-thunk";
import {
  persistStore,
  persistReducer,
} from "redux-persist";
import immutableTransform from "redux-persist-transform-immutable";
import AsyncStorage from "@react-native-community/async-storage";

// ----------------------------------------
// CONFIG IMPORTS
// ----------------------------------------
import reducers from "@app-reducers";
import StoreConfig from "@app-configs/Store";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const init = () => {
  const middleware = applyMiddleware(thunk);
  const rootReducers = combineReducers(reducers);
  const persistConfig = {
    key: "root",
    storage: AsyncStorage,
    transforms: [immutableTransform()],
    whitelist: StoreConfig.persistedReducers,
    blacklist: StoreConfig.volatileReducers,
  };

  const persistedReducer = persistReducer(persistConfig, rootReducers);
  const store = createStore(persistedReducer, compose(middleware, window.devToolsExtension ? window.devToolsExtension() : func => func));

  if (!StoreConfig.shouldPersist) {
    persistStore(store)
      .purge();
  } else {
    persistStore(store);
  }

  return store;
};
