/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import axios from "axios";

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import ApiConfig from "@app-configs/Api";
import API from "@app-api-endpoints";



/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export function request(api, param, getState) {
  const shouldLogApi = ApiConfig.devLog && ((ApiConfig.devLogApis.length === 0 || ApiConfig.devLogApis.indexOf(api) > -1) || ApiConfig.breakDevLogToken);

  const {
    endpoint,
    method,
    data,
  } = API[api];

  const url = `${ApiConfig.baseUrl}${endpoint}`;
  const params = {
    ... param,
    ... data,
  };

  let token = ApiConfig.getToken(getState);

  if (shouldLogApi && ApiConfig.breakDevLogToken) {
    console.error(token);
  }

  const headers = {
    // "Content-Type": "application/json",
    // "Accept": "application/json",
    "Authorization": `Bearer ${token}`,
  };

  const axiosConfig = {
    method,
    url,
    headers,
    timeout: ApiConfig.timeout * 1000,
  };

  if (method.toUpperCase() === "GET") {
    axiosConfig.params = params;
  } else {
    axiosConfig.data = params;
  }

  if (shouldLogApi) {
    if (ApiConfig.breakDevLogParams) {
      console.error(params);
    }

    console.warn("[request start]\n\n-url: ", url, "\n\n-method: ", method, "\n\n-params: ", params, "\n\n-token: ", token);
  }

  return new Promise((resolve, reject) => {
    axios(axiosConfig)
    .then(response => {

      if (shouldLogApi) {
        if (ApiConfig.breakDevLogResult) {
          console.error(response.data);
        }

        console.warn("[response received]\n\n-url: ", url, "\n\n-method: ", method, "\n\n-params: ", params, "\n\n-response: ", response);
      }

      if (ApiConfig.isSuccess(response)) {
          const body = response.data;

          if (shouldLogApi) {
            console.warn("[response is success]\n\n-url: ", url, "\n\n-method: ", method, "\n\n-params: ", params, "\n\n-body: ", body);
          }

          resolve(ApiConfig.extractData(body));
        } else if (ApiConfig.isError(response)) {
          const body = response.data;
          const {
            code,
            message,
          } = ApiConfig.extractError(body);

          if (shouldLogApi) {
            console.warn("[response is error]\n\n-url: ", url, "\n\n-method: ", method, "\n\n-params: ", params, "\n\n-body: ", body);
          }

          reject({
            status: response.status,
            code,
            message,
            additionals: {
              url: url,
              method: method,
              params: JSON.stringify(params),
              body: JSON.stringify(body),
            },
          });
        } else {
          const body = response.data;

          if (shouldLogApi) {
            console.warn("[response is invalid]\n\n-url: ", url, "\n\n-method: ", method, "\n\n-params: ", params, "\n\n-http status: ", response.status, "\n\n-body: ", body);
          }

          reject({
            status: 500,
            code: "x888",
            message: "Invalid response",
            additionals: {
              url: url,
              method: method,
              params: JSON.stringify(params),
              httpStatus: (response.status + ""),
              body: JSON.stringify(body),
            },
          });
        }
      })
      .catch(err => {

        if (shouldLogApi) {
          console.warn("[error catched]\n\n-url: ", url, "\n\n-method: ", method, "\n\n-params: ", params, "\n\n-error: ", err);
        }

        reject({
          status: err.response && err.response.status ? err.response.status : 999,
          code: "x999",
          message: err.message,
          additionals: {
            url: url,
            method: method,
            params: JSON.stringify(params),
          },
        });
      });
  });
}

