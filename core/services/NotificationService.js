/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import OneSignal from "react-native-onesignal";


// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import Notification from "@app-configs/Notification";


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  CONSTANTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
const setNotification = !!Notification.token;




/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const up = () => {
  if (!setNotification) {
    return null;
  }

  OneSignal.init(Notification.token);

  OneSignal.addEventListener("received", Notification.onReceived);
  OneSignal.addEventListener("opened", Notification.onOpened);
  // OneSignal.addEventListener("ids", Notification.onIds);
};

// ----------------------------------------

export const down = () => {
  if (!setNotification) {
    return null;
  }

  OneSignal.removeEventListener("received", Notification.onReceived);
  OneSignal.removeEventListener("opened", Notification.onOpened);
  OneSignal.removeEventListener("ids", Notification.onIds);
};
