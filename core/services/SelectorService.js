/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import { fromJS, is } from "immutable";
import { createSelectorCreator, defaultMemoize, createSelector } from "reselect";
import * as ReducerService from "@core-services/ReducerService";
// const createCustomSelector = createSelectorCreator(defaultMemoize, is);
const createCustomSelector = createSelector;


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export const select = (states:array = [], selector:func) => createCustomSelector(
  [... states],
  (... items) => {
    items = items.map((item) => {
      if (ReducerService.isEmptyList(item)) {
        return fromJS([]);
      }

      if (ReducerService.isEmptyMap(item)) {
        return fromJS({});
      }

      return item;
    });
    return {
      data: selector(... items),
      updatedAt: (new Date().getTime()),
    };
  }
);
