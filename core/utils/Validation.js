/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  METHODS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  const hasError = (errors) => {
    for (let x in errors) {
      if (!!errors[x]) {
        return true;
      }
    }

    return false;
  };

// ----------------------------------------

  const isNotEmpty = (value) => {
    if (value === undefined) {
      return false;
    }

    if (value === null) {
      return false;
    }

    if (typeof value === "string" && value.trim().length < 1) {
      return false;
    }

    if (typeof value === "number" && value < 1) {
      return false;
    }

    if (Array.isArray(value) && value.trim().length < 1) {
      return false;
    }

    if (typeof value === "object" && Object.entries(value).length < 1) {
      return false;
    }

    return true;
  };

// ----------------------------------------

  const isEmail = (value) => {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      return re.test(String(value).toLowerCase());
  };

// ----------------------------------------

  const isMin = (value, min = 0) => {
    if (value === undefined) {
      return false;
    }

    if (value === null) {
      return false;
    }

    if (Number.isInteger(value)) {
      return value >= min;
    }

    if (typeof value === "string") {
      return value.length >= min;
    }
  };

// ----------------------------------------

  const isMax = (value, max = null) => {
    if (value === undefined) {
      return false;
    }

    if (value === null) {
      return false;
    }

    if (Number.isInteger(value)) {
      return value <= max;
    }

    if (typeof value === "string") {
      return value.length <= max;
    }
  };


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  EXPORT
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

export default {
  hasError,
  isNotEmpty,
	isEmail,
  isMin,
  isMax,
};
