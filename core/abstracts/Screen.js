/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {
  Config,
  Device,
} from "@core-utils";

// ----------------------------------------
// COMPONENT IMPORTS
// ----------------------------------------


/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class Screen {

// ----------------------------------------
// ----------------------------------------
// CONSTRUCTOR AND LIFE CYCLES
// ----------------------------------------

  constructor(props) {
    this.component = props;
  }


// ----------------------------------------
// ----------------------------------------
// MAIN METHODS
// ----------------------------------------

  initBaseState(DATA, MODAL) {
    const props = this.component.props;

    const data = {};
    const loadingState = {};
    const updatedAt = {};
    const page = {};
    for (let x in DATA) {
      if (
        props[x] &&
        props[x].data !== null &&
        props[x].data !== undefined &&
        (typeof props[x].data !== "object" || Object.entries(props[x].data).length !== 0) &&
        (!Array.isArray(props[x].data) || props[x].data.length)
      ) {
        data[x] = props[x].data;
        // data[x] = !Array.isArray(props[x].data) ? props[x].data : props[x].data.splice(0, 5);
        updatedAt[x] = props[x].updatedAt;
        loadingState[x] = false;
      } else {
        data[x] = DATA[x];
        updatedAt[x] = (new Date()).getTime();
        loadingState[x] = true;
      }

      if (Array.isArray(DATA[x])) {
        page[x] = 1;
      }
    }

    const modalState = {};
    if (MODAL) {
      MODAL.map(item => {
        modalState[item] = false;
      });
    }

    return {
      data,
      loadingState,
      updatedAt,
      modalState,
      page,
    };
  }

// ----------------------------------------

  setData(key:string, nextProps, nextState) {
    if (nextProps[key].updatedAt <= this.component.state.updatedAt[key]) {
      return false;
    }

    const {
      data,
      loadingState,
      updatedAt,
      page,
    } = this.component.state;

    page[page]++;
    data[key] = nextProps[key].data;
    loadingState[key] = false;
    updatedAt[key] = nextProps[key].updatedAt;

    this.component.setState({
      data,
      loadingState,
      updatedAt,
      page,
    });
  }

// ----------------------------------------

  setPage(key:string, newPage:number = 1) {
    const {
      page,
      loadingState,
    } = this.component.state;

    if (newPage === 1) {
      loadingState[key] = true;
    }

    if (page[key] !== newPage) {
      page[key] = newPage;
    }

    this.component.setState({
      page,
      loadingState,
    });
  }

// ----------------------------------------

  startLoading(key:string, force:bool = false) {
    const {
      data,
      loadingState,
    } = this.component.state;

    if (!data[key] && (!loadingState[key] || force)) {
      loadingState[key] = true;

      this.component.setState({
        loadingState,
      });
    }
  }

// ----------------------------------------

  stopLoading(key:string) {
    const {
      loadingState,
    } = this.component.state;

    if (loadingState[key]) {
      loadingState[key] = false;

      this.component.setState({loadingState});
    }
  }

// ----------------------------------------

  isLoading(key:string) {
    const {
      loadingState,
    } = this.component.state;

    return loadingState[key] === true;
  }

// ----------------------------------------

  currentPage(key:string) {
    const {
      page,
    } = this.component.state;

    return page[key] ? page[key] : 1;
  }

// ----------------------------------------

  data(key:string) {
    const {
      data,
    } = this.component.state;

    if (data[key] === null || data[key] === undefined) {
      return null;
    }

    return data[key];
  }

// ----------------------------------------

  load(funcs:array = []) {
    for (let x in funcs) {
      setTimeout(
        () => funcs[x](),
        (x * 100) + 300,
      );
    }
  }

// ----------------------------------------

  resetList(key:string, additionalState = {}) {
    const {
      data,
      loadingState,
      updatedAt,
      page,
    } = this.component.state;

    data[key] = [];
    loadingState[key] = true;
    updatedAt[key] = (new Date()).getTime() + 100;
    page[key] = 1;

    this.component.setState({
      data,
      loadingState,
      updatedAt,
      page,
      ... additionalState,
    });
  }

// ----------------------------------------

  openModal(key:string) {
    const {
      modalState,
    } = this.component.state;

    if (!modalState[key]) {
      modalState[key] = true;

      this.component.setState({modalState});
    }
  }

// ----------------------------------------

  closeModal(key:string) {
    const {
      modalState,
    } = this.component.state;

    if (modalState[key]) {
      modalState[key] = false;

      this.component.setState({modalState});
    }
  }

// ----------------------------------------

  isModalActive(key:string) {
    return this.component.state.modalState[key] === true;
  }


// ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

}
